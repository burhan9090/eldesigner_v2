<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


$mainRouts = function () {
    Route::get( '/', 'Auth\LoginController@showLoginForm' )->name('root');
    Route::get( 'pricing', 'HomeController@pricing' )->name('home.pricing');
    Route::get( 'how-it-works', 'HomeController@howItWorks' )->name('home.how_it_works');
    Route::get( '/home', 'HomeController@index' )->name('home');
    Route::get( '/login-form', 'HomeController@login' )->name('home.login');
    Route::get( 'switchLang', 'LangController@switchLang' );
    Route::get( 'timeline', 'TimelineController@index' )->name( 'timeline' );
    Route::get( 'discover-friends', 'TimelineController@discoverFriends' )->name( 'timeline.discover_friends' );
    Route::get( 'profile', 'ProfileController@me' )->name( 'profile.show' );
    Route::get( 'profile/{user_name}', 'ProfileController@index' );
    Route::get( 'show-chat', 'ProfileController@showAllMessages' );
    Route::get( 'get-friends', 'ProfileController@getAllFriends' );
    Route::get( 'get-follow-request', 'FollowController@getFollowRequests' );
    Route::get( 'accept-follower', 'FollowController@acceptFollower' );
    Route::get( 'friend-request-follow', 'FollowController@friendRequestFollow' );
    Route::get( 'public-friend-request-follow', 'FollowController@publicFriendRequestFollow' );
    Route::get( 'delete-request-follow', 'FollowController@cancelFriendRequestFollow' );
    Route::get( 'delete-friend', 'FollowController@deleteFriend' );
    Route::get( 'messages', 'MessageController@getMessages' );
    Route::get( 'side-bar-messages', 'MessageController@showSideMenuMessage' );
    Route::get( 'update-as-seen', 'MessageController@updateAsSeen' );
    Route::post( 'set-message', 'MessageController@setMessages' );
    Route::get( 'get-notification', 'NotificationController@getNotification' );
    Route::get( 'notification', 'ProfileController@showAllNotification' )->name( 'profile.notification' );
    Route::get('mark-all-read', 'NotificationController@MarkAllAsRead');
    Route::get( 'search-bar', 'TimelineController@searchBar' )->name( 'timeline.search' );
    Route::get( 'followers-following', 'ProfileController@followingFollowersPage' )->name( 'profile.followers-following' );
    Route::get( 'following', 'ProfileController@followingPage' );
    Route::get( 'followers', 'ProfileController@followersPage' );
    Route::get( 'get-friend-suggestions', 'TimelineController@friendSuggestions' );
    Route::get( 'profile/{user_name}/about', 'ProfileController@notFriendAbout' )->name( 'profile.user.about' );
    Route::get( 'profile/about/{user_name}', 'ProfileController@FriendAbout' )->name( 'profile.user.about' );
    Route::get( 'profile/{user_name}/projects', 'ProfileController@notFriendProjects' )->name( 'profile.user.projects' );
    Route::get( 'profile/projects/{user_name}', 'ProfileController@FriendProjects' )->name( 'profile.user.projects' );
    Route::get( 'profile/{user_name}/articles', 'ProfileController@notFriendArticles' )->name( 'profile.user.articles' );
    Route::get( 'profile/articles/{user_name}', 'ProfileController@FriendArticles' )->name( 'profile.user.articles' );
    Route::get( 'home/discover-friends', 'HomeController@discoverDesignersGuest' )->name( 'home.discover-friends' );
    Route::get( 'home/discover-designs', 'HomeController@discoverDesign' )->name( 'home.discover-designs' );
    Route::get( 'home/blogs', 'HomeController@HomeBlogs' )->name( 'home.home_blog' );
    Route::get( 'home/ajax-discover-designs/{offset}', 'HomeController@ajaxDiscoverDesign' )->name( 'home.ajax-discover-designs' );

    //public profiles route
    Route::get( 'home/profile/{username}', 'PublicProfileController@index' );
    Route::get( 'home/profile/{username}/about', 'PublicProfileController@about' );
    Route::get( 'home/profile/{username}/project', 'PublicProfileController@project' );
    Route::get( 'home/profile/{username}/articles', 'PublicProfileController@articles' );
    Route::get( 'friend-requests', 'ProfileController@showAllFriendRequests' )->name( 'profile.friend_requests' );
    Auth::routes();

    Route::get( 'chat/{filename}', 'DownloadController@chatFile');
    Route::get( 'download/{filename}', 'DownloadController@downloadFile');

    /*** POST CONTROLLER START */
    Route::post('addPost', 'PostController@addPost')->name('addPost');
    Route::post('addMediaPost', 'PostController@addMediaPost')->name('addMediaPost');
    Route::get('viewPost/{post_id}', 'PostController@show')->name('viewPost');
    Route::get('open-notification', 'PostController@openNotification');
    Route::get('editPost', 'PostController@editPost')->name('editPost');
    /*** POST CONTROLLER END  */

    /*** COMMENT  CONTROLLER START */
    Route::post('addComment', 'CommentController@addComment')->name('addComment');
    Route::post('addReply', 'CommentController@addReply')->name('addReply');
    /*** COMMENT CONTROLLER END   */

    /*** LOVE CONTROLLER START */
    Route::post('addLove', 'LoveController@addLove')->name('addLove');
    Route::post('removeLove', 'LoveController@removeLove')->name('removeLove');
    /*** LOVE CONTROLLER END  */

    /*** Design Quiz Start*/
    Route::get('tool/quiz', 'ToolController@quiz')->name('tool.quiz');
    Route::get('getOrders/{user_id}', 'ToolController@getOrders')->name('orders');
    Route::get('quiz/{user_id}', 'QuizController@takeQuiz')->name('tool.quiz.take');
    Route::get('getSubServices', 'QuizController@getSubServices')->name('sub.services');
    Route::post('saveProcess', 'QuizController@saveProcess')->name('process.save');
    Route::post('tool/quiz/save', 'QuizController@saveQuiz')->name('tool.quiz.save');
    /*** Design Quiz End*/

    // *** pricing ***
    Route::get('pricing/bundles', 'PricingController@bundles')->name('pricing.bundles');
    Route::get('pricing/bundles/{bundle_name}', 'PricingController@bundlespayment')->name('pricing.bundles.plans');
    Route::get('pricing/response/{bundle_name}', 'PricingController@TransactionPlansResponse')->name('pricing.bundles.response');
    // *** pricing ***

    // *** Community Articles *** //
    Route::get('community/articles' , 'ArticlesController@CommunityArticles')->name('community.articles.list');
    // *** Community Articles *** //

    // *** Community Projects *** //
    Route::get('community/projects' , 'ProjectsController@discoverDesign')->name('community.projects.list');
    // *** Community Projects *** //

    // *** Paybal Actions *** //
    Route::post('paypal/callback','PaybalController@PaypalCallback');
    // *** Paybal Actions *** //

    include "website_dashboard.php";

    include "user_settings.php";

    include "payment.php";

    Route::group( [
        "prefix" => '/{user_name}',
        "where"  => [ 'user_name' => '[\w\d\.\-]+' ],
        'as'     => 'inner.'
    ], function () {
        include "website.php";
    } );
};

//Route::domain( 'www.'.env( 'APP_URL' ) )->group($mainRouts);
//Route::domain( env( 'APP_URL' ) )->group($mainRouts);

Route::domain( env( 'APP_URL' ) )->group($mainRouts);

Route::group( [

    'middleware' => 'check_domain.outer',

    'as'         => 'outer.'

], function () {

    include "website.php";

} );
//Route::domain( env( 'APP_URL' ) )->group($mainRouts);

//
//Route::group( [
//
//    'middleware' => 'check_domain.outer',
//
//    'as'         => 'outer.'
//
//], function () {
//
//    include "website.php";
//
//} );


//
//Route::group( [
//    'middleware' => 'check_domain.outer',
//    'as'         => 'outer.'
//], function () {
//    include "website.php";
//} );
//	Route::group( [
//		"prefix" => '/{user_name}',
//		"where"  => [ 'user_name' => '[\w\d\.\-]+' ],
//		'as'     => 'inner.'
//	], function () {
//		include "website.php";
//	} );

//};

//Route::domain( env( 'APP_URL' ) )->group($mainRouts);
//Route::domain( 'www.'.env( 'APP_URL' ) )->group($mainRouts);
//
//Route::group( [
//	'middleware' => 'check_domain.outer',
//	'as'         => 'outer.'
//], function () {
//	include "website.php";
//} );
