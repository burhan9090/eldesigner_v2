<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 1/19/19
 * Time: 4:23 AM
 */


Route::prefix( 'user' )->middleware( 'auth' )->group( function () {

	Route::get( 'about/{id}', 'WebsiteDashboard\PersonalInformationController@about' )->name( 'user.about' );
	Route::get( 'change_password', 'WebsiteDashboard\PersonalInformationController@changePassword' )->name( 'user.change_password' );
	Route::post( 'change_password/save', 'WebsiteDashboard\PersonalInformationController@changePasswordSave' )->name( 'user.change_password.save' );


});