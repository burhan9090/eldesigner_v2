<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 12/28/18
 * Time: 00:50 AM
 */

Route::prefix( 'website_dashboard' )->middleware( 'auth' )->group( function () {
	Route::prefix( 'article' )->group( function () {
		Route::get( 'library', 'WebsiteDashboard\ContentController@library' )->name( 'article.index' );

		Route::get( 'community', 'WebsiteDashboard\ContentController@community' )->name( 'article.view' );

		Route::get( 'add', 'WebsiteDashboard\ContentController@addArticle' )->name( 'add_article' );
		Route::post( 'save', 'WebsiteDashboard\ContentController@add' )->name( 'add_article.save' );

		Route::get( 'edit/{id}', 'WebsiteDashboard\ContentController@edit' )->name( 'add_article.edit' );
		Route::post( 'update', 'WebsiteDashboard\ContentController@update' )->name( 'add_article.update' );

		Route::post( 'publish', 'WebsiteDashboard\ContentController@publish' )->name( 'article.publish' );
		Route::post( 'delete', 'WebsiteDashboard\ContentController@delete' )->name( 'article.delete' );
		Route::post( 'hide', 'WebsiteDashboard\ContentController@hidden' )->name( 'article.hide' );


	} );


	Route::prefix( 'projects' )->group( function () {
		Route::get( 'index', 'WebsiteDashboard\ProjectsController@index' )->name( 'projects.index' );
		Route::get( 'library', 'WebsiteDashboard\ProjectsController@library' )->name( 'projects.library' );
		Route::post( 'save', 'WebsiteDashboard\ProjectsController@save' )->name( 'projects.save' );
		Route::post( 'update', 'WebsiteDashboard\ProjectsController@update' )->name( 'projects.update' );
		Route::post( 'publish', 'WebsiteDashboard\ProjectsController@publish' )->name( 'projects.publish' );
		Route::post( 'hide', 'WebsiteDashboard\ProjectsController@hidden' )->name( 'projects.hide' );
		Route::post( 'repost', 'WebsiteDashboard\ProjectsController@repost' )->name( 'projects.repost' );
		Route::get( 'delete', 'WebsiteDashboard\ProjectsController@delete' )->name( 'projects.delete' );
		Route::get( 'ajax', 'WebsiteDashboard\ProjectsController@getProjectAjax' )->name( 'projects.get_project_ajax' );
		Route::get( 'check-project-permission', 'WebsiteDashboard\ProjectsController@checkProjects' )->name( 'projects.checkProjects' );
	} );

	Route::get( 'contact_us', 'WebsiteDashboard\ContactUsController@index' )->name( 'contact_us' );
	Route::post( 'contact_us', 'WebsiteDashboard\ContactUsController@save' )->name( 'contact_us.save' );

	Route::get( 'service', 'WebsiteDashboard\ServicesController@index' )->name( 'services.index' );
	Route::post( 'service', 'WebsiteDashboard\ServicesController@save' )->name( 'services.save' );

	Route::get( 'personal_information', 'WebsiteDashboard\PersonalInformationController@index' )->name( 'personal_information.index' );
	Route::post( 'personal_information', 'WebsiteDashboard\PersonalInformationController@save' )->name( 'personal_information.save' );

	Route::get( 'user_history/index', 'WebsiteDashboard\UserHistoryController@index' )->name( 'user_history.index' );
	Route::post( 'user_history/save', 'WebsiteDashboard\UserHistoryController@save' )->name( 'user_history.save' );

	Route::get( 'themes', 'WebsiteDashboard\ThemeController@index' )->name( 'themes.index' );
	Route::post( 'themes/save', 'WebsiteDashboard\ThemeController@save' )->name( 'themes.save' );

	Route::get( 'style', 'WebsiteDashboard\StyleController@index' )->name( 'style.index' );
	Route::post( 'style', 'WebsiteDashboard\StyleController@save' )->name( 'style.save' );

	Route::get( 'messages', 'WebsiteDashboard\DashboardController@messages' )->name( 'messages.index' );
	Route::get( 'message/{id}', 'WebsiteDashboard\DashboardController@message' )->name( 'messages.show' );

	Route::get( 'statistics', 'WebsiteDashboard\DashboardController@statistics' )->name( 'statistics.index' );

	Route::get( 'user_services', 'WebsiteDashboard\ServicesController@userServices' )->name( 'user_services' );
	Route::post( 'user_services', 'WebsiteDashboard\ServicesController@userServicesSave' )->name( 'user_services.save' );

	Route::get( 'skills', 'WebsiteDashboard\ServicesController@userSkills' )->name( 'skills' );
	Route::post( 'skills', 'WebsiteDashboard\ServicesController@userSkillsSave' )->name( 'skills.save' );

	Route::get( 'members', 'WebsiteDashboard\ServicesController@teamMember' )->name( 'members' );
	Route::post( 'members', 'WebsiteDashboard\ServicesController@teamMemberSave' )->name( 'members.save' );

	Route::get( 'setting/welcome', 'WebsiteDashboard\WelcomeController@index' )->name( 'welcome.index' );
	Route::post( 'setting/welcome', 'WebsiteDashboard\WelcomeController@save' )->name( 'welcome.save' );

	Route::group( [ 'as' => 'setting.', 'prefix' => 'setting' ], function () {

		Route::get( 'main', 'WebsiteDashboard\SettingsController@mainIndex' )->name( 'main' );
		Route::post( 'main', 'WebsiteDashboard\SettingsController@mainSave' )->name( 'main.save' );

		Route::get( 'seo', 'WebsiteDashboard\SettingsController@seoIndex' )->name( 'seo' );
		Route::post( 'seo', 'WebsiteDashboard\SettingsController@seoSave' )->name( 'seo.save' );

	} );

	Route::group( [ 'as' => 'tools.', 'prefix' => 'tools' ], function () {
		Route::get( 'watermark', 'WebsiteDashboard\ToolsController@index' )->name( 'water_mark' );
		Route::post( 'watermark', 'WebsiteDashboard\ToolsController@save' )->name( 'water_mark.save' );

		Route::get( 'domain', 'WebsiteDashboard\DomainController@index' )->name( 'domain.index' );
		Route::get( 'select_domain/{domainName}', 'WebsiteDashboard\DomainController@select_domain' )->name( 'domain.select' );

	} );

	Route::get( 'index', 'WebsiteDashboard\DashboardController@index' )->name( 'dashboard.index' );


} );


Route::prefix( 'website_dashboard' )->group( function () {
	Route::get( 'article/{id}', 'WebsiteDashboard\ContentController@show' )->name( 'article.show' );
	Route::get( 'projects/{id}', 'WebsiteDashboard\ProjectsController@show' )->name( 'projects.show' );
});http://el.com:8000/website_dashboard/projects/11


Route::get( 'city', 'WebsiteDashboard\PersonalInformationController@getCities' )->name( 'ajax.city' );