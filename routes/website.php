<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 12/28/18
 * Time: 00:50 AM
 */

Route::get( '/', 'WebsiteController@index' )->name( 'website.index' );

Route::get( '/blog', 'WebsiteController@blog_list' )->name( 'website.blog.list' );
Route::get( '/blog/{id}', 'WebsiteController@blog_show' )->name( 'website.blog.show' );

Route::get( '/project', 'WebsiteController@project_list' )->name( 'website.project.list' );
Route::get( '/project/{id}', 'WebsiteController@project_show' )->name( 'website.project.show' );

Route::post( '/contact_us', 'WebsiteController@contact_us' )->name( 'website.contact_us' );