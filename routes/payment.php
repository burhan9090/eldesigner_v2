<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 3/13/19
 * Time: 2:11 AM
 */


/**

for testing payment you can use the follow information

CardNumber       =  5271045423029111
CardHolderName   =  Card Holder Name
ExpiryDateYear   =  22
ExpiryDateMonth  =  01
SecurityCode     =  684


 */


//Route::get('/test', function(\App\Classes\Payment $payment){
//	return $payment->create(7,'You are purchasing Start Plan which is');
////	return $payment->create(100,'keefak ya man', 'resp1');
//});

//Route::get('/resp1', function(\App\Classes\Payment $payment){
//	$payment->isSuccess();
//	$payment->isFail();
//
//	$payment->getResponseMessage();
//	$payment->getModel();
//
//
//	dd($payment);
//})->name('resp1');



Route::as( 'payment.' )->middleware( 'auth' )->group( function () {
	Route::get('/payment', 'PricingController@payment')->name('form');
});