$(function () {
    ordersPagination();
    var pathname = window.location.pathname;
    var mainHeader = document.getElementById("header--standard");
        window.onscroll = function () {
            if (window.scrollY >= 40) {
                mainHeader.classList.add("fixed-header");
            }
            else {
                mainHeader.classList.remove("fixed-header");
            }
        };
    if (pathname.indexOf('/show-chat') === 0) {
        $('.right_side_bar').hide();
    }
    $('.no-profile-pic').css('background', getRandomColor());
    $('.change_lang').on('click', function () {
        var lang = $('.dropdown-menu li.selected a img').attr('data-lang');
        if (lang == 'en') {
            lang = 'ar';
        } else {
            lang = 'en';
        }
        $.ajax({
            url: "/switchLang",
            method: "get",
            data: {
                "lang": lang
            },
            dataType: "json",
            beforeSend: function () {
            },
            success: function (response) {
                if (response) {
                    if (response.success) {
                        if (response.data.is_user) {
                            location.reload(true);
                        } else {
                            //@TODO set lang in local storge and set localization with lang
                            alert(response.msg)
                        }
                    } else {
                        alert(response.msg)
                    }

                } else {
                    alert("Something went wrong, try again")
                }
            }
        });
    })
    $("form#add_status_post").validate({
        rules: {
            status: {
                required: true,
            },
        },
        messages: {
            status: "Oppps , you looks like wanna share status that is empty",
        },
        submitHandler: function (form) {
            $('#post_loader').fadeIn(10);
            var status = $('textarea[name="status"]').val();
            $.ajax({
                url: "/addPost",
                method: "post",
                data: {
                    "status": status,
                    "_token": CSRF_TOKEN
                },
                dataType: "json",
                beforeSend: function () {
                },
                success: function (response) {
                    if (response) {
                        if (response.success) {
                            var post = response.data.post;
                            $('.list_of_posts').prepend(post);
                            $('#post_loader').fadeOut(500);
                            $('.no-profile-pic').css('background', getRandomColor());
                        } else {
                            alert(response.msg)
                        }
                    } else {
                        alert("Something went wrong, try again")
                    }
                },
                error: function (jqXHR, textStatus) {
                    alert("Something went wrong, try again")
                },
                complete: function () {
                    form.reset()
                }
            });
        }
    });
    $("form#add_media_post").validate({
        submitHandler: function (form) {
            $('#post_loader').fadeIn(10);
            var formData = new FormData();
            var status = $(form).find('textarea[name="status"]').val()
            var images = $(form).find('input[name="images"]')[0].files
            var video = $(form).find('input[name="video"]')[0].files
            if (video.length == 0 && images.length == 0 && status.length == 0) {
                alert('you cant share empty post')
                return false;
            }
            $(images).each(function (i, file) {
                var file = images[i];
                if (file.size > 5242880) {
                    var errorMessage = 'Image num # ' + i + ' must be less than 5MB, we will ignore it';
                    alert(errorMessage);
                } else {
                    formData.append('images[]', images[i]);
                }
            });
            if (video.length > 0) {
                if (video[0].size > 10485760) {
                    var errorMessage = 'Video must be less than 10MB, we will ignore it';
                    alert(errorMessage);
                } else {
                    formData.append('video', video[0]);
                }
            }
            formData.append('status', status);
            formData.append('_token', CSRF_TOKEN);
            $.ajax({
                url: "/addMediaPost",
                method: "POST",
                data: formData,
                dataType: "JSON",
                contentType: false,
                cache: false,
                processData: false,
                beforeSend: function () {
                },
                success: function (response) {
                    if (response) {
                        if (response.success) {
                            var post = response.data.post;
                            $('.list_of_posts').prepend(post);
                            $('#post_loader').fadeOut(500);
                            $('.no-profile-pic').css('background', getRandomColor());
                            $('.image-preview-container').empty();
                            $('.image-preview-container').fadeOut(100);
                            $('.video-preview-container').empty();
                            $('.video-preview-container').fadeOut(100);
                        } else {
                            alert(response.msg)
                        }
                    } else {
                        alert("Something went wrong, try again")
                    }
                },
                error: function (jqXHR, textStatus) {
                    alert("Something went wrong, try again")
                },
                complete: function () {
                    form.reset()
                }
            });
        }
    });
    $(document).on('submit', "form#addComment", function (form) {
        form.preventDefault();
        $(this).validate({
            submitHandler: function (form) {
                var formData = new FormData();
                var comment_text = $(form).find('textarea[name="comment_text"]').val()
                var post_id = $(form).find('input[name="post_id"]').val()
                var images = $(form).find('input[name="comment-image"]')[0].files
                if (images.length == 0 && comment_text.length == 0) {
                    alert('you cant share an empty comment')
                    return false;
                }
                $(images).each(function (i, file) {
                    var file = images[i];
                    if (file.size > 5242880) {
                        var errorMessage = 'Image num # ' + i + ' must be less than 5MB, we will ignore it';
                        alert(errorMessage);
                    } else {
                        formData.append('images[]', images[i]);
                    }
                });
                formData.append('post_id', post_id);
                formData.append('comment_text', comment_text);
                formData.append('_token', CSRF_TOKEN);
                $.ajax({
                    url: "/addComment",
                    method: "POST",
                    data: formData,
                    dataType: "JSON",
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        $(document).find('#post_loader-' + post_id).fadeIn(10);
                    },
                    success: function (response) {
                        if (response) {
                            if (response.success) {
                                var comment = response.data.comment;
                                console.log('response back')
                                console.log(comment)
                                $(document).find('#comments-list-' + post_id).append(comment);
                                $(document).find('#post_loader-' + post_id).fadeOut(500);
                                $(document).find('.comment-image-preview-container').empty();
                                $(document).find('.comment-image-preview-container').fadeOut(100);
                            } else {
                                alert(response.msg)
                            }
                        } else {
                            alert("Something went wrong, try again")
                        }
                    },
                    error: function (error,jqXHR, textStatus) {
                        alert("Something went wrong, try again")
                    },
                    complete: function () {
                        form.reset()
                    }
                });
            }
        })
    });

    window.readURL = function(input, element, index) {

        if (input.files && input.files[index]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                element.attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[index]);
        }
    }

    $(document).on('change', "input[name = 'comment-image']", function () {
        $('.comment-image-preview-container').empty();
        $(this).each(function (index, field) {
            for (var i = 0; i < field.files.length; i++) {
                var html = '<img class = "image-preview-' + i + ' " src="" style="height: 100px;width: 100px;">'
                $('.comment-image-preview-container').append(html);
                readURL(this, $('.image-preview-' + i + ''), i);
            }
            $('.comment-image-preview-container').fadeIn(350);
        });
    });
    $(document).on('change', "input[name = 'reply-image']", function () {
        var perview_class = $(this).attr('data-preview-class');
        $('.' + perview_class).empty();

        $(this).each(function (index, field) {
            for (var i = 0; i < field.files.length; i++) {
                var html = '<img class = "image-preview-' + i + ' " src="" style="height: 100px;width: 100px;">'
                $('.' + perview_class).append(html);
                readURL(this, $('.image-preview-' + i + ''), i);
            }
            $('.' + perview_class).fadeIn(350);
        });
    });
    $("input[name = 'images']").change(function () {
        $('.image-preview-container').empty();
        $(this).each(function (index, field) {
            for (var i = 0; i < field.files.length; i++) {
                var html = '<img class = "image-preview-' + i + ' " src="" style="height: 100px;width: 100px;">'
                $('.image-preview-container').append(html);
                readURL(this, $('.image-preview-' + i + ''), i);
            }
            $('.image-preview-container').fadeIn(350);
        });
    });
    $(document).on("change", "input[name = 'video']", function (evt) {
        var source = $('#previewVid');
        source.attr('src', URL.createObjectURL(this.files[0]));
        source.parent()[0].load();
        $('.video-preview-container').fadeIn(350);
    });
    $(document).on('click', '.reply', function (e) {
        e.preventDefault();
        var form_id = $(this).attr('data-form-reply');
        if ($('.' + form_id).is(':visible')) {
            $('.' + form_id).slideUp(200);
        } else {
            $('.' + form_id).slideDown(200);
        }
    })
    $(document).on('submit', 'form#submit-reply', function (form) {
        form.preventDefault();
        var replies_list_id = $(this).attr('data-replies-list');
        $(this).validate({
            submitHandler: function (form) {
                var formData = new FormData();
                var reply_text = $(form).find('textarea[name="reply_text"]').val()
                var comment_id = $(form).find('input[name="comment_id"]').val()
                var images = $(form).find('input[name="reply-image"]')[0].files
                if (images.length == 0 && reply_text.length == 0) {
                    alert('you cant share an empty comment')
                    return false;
                }
                $(images).each(function (i, file) {
                    var file = images[i];
                    if (file.size > 5242880) {
                        var errorMessage = 'Image num # ' + i + ' must be less than 5MB, we will ignore it';
                        alert(errorMessage);
                    } else {
                        formData.append('images[]', images[i]);
                    }
                });
                formData.append('comment_id', comment_id);
                formData.append('reply_text', reply_text);
                formData.append('_token', CSRF_TOKEN);
                $.ajax({
                    url: "/addReply",
                    method: "POST",
                    data: formData,
                    dataType: "JSON",
                    contentType: false,
                    cache: false,
                    processData: false,
                    beforeSend: function () {
                        $('#post_loader-reply-' + comment_id).fadeIn(10);
                    },
                    success: function (response) {
                        if (response) {
                            if (response.success) {
                                var reply = response.data.reply;
                                $(document).find("ul#" + replies_list_id).append(reply);
                                $('#post_loader-reply-' + comment_id).fadeOut(500);
                                $('.no-profile-pic').css('background', getRandomColor());
                                $('.reply-image-preview-container-' + comment_id).empty();
                                $('.reply-image-preview-container-' + comment_id).fadeOut(100);
                                $('.addReply-Comment-' + comment_id).fadeOut(100);

                            } else {
                                alert(response.msg)
                            }
                        } else {
                            alert("Something went wrong, try again")
                        }
                    },
                    error: function (jqXHR, textStatus) {
                        alert("Something went wrong, try again")
                    },
                    complete: function () {
                        form.reset()
                    }
                });
            }
        })
    })
    $(document).on('click', '.love-post', function (e) {
        e.preventDefault();
        var post_id = $(this).attr('data-post_id');
        var num_of_loves_selector = $('.num_of_loves-' + post_id);
        var love_selector = $('[data-post_id = "' + post_id + '"]');
        if ($(love_selector).hasClass('loved')) {
            $.ajax({
                url: "/removeLove",
                method: "POST",
                data: {
                    'resource_type' : 'post',
                    'resource_id' : post_id,
                    '_token' : CSRF_TOKEN
                },
                dataType: "JSON",
                beforeSend: function () {
                },
                success: function (response) {
                    if (response) {
                        if (response.success) {
                            love_selector.css('fill', '')
                            $(love_selector).removeClass('loved')
                            var decrease_loves = parseInt(num_of_loves_selector.html()) - 1;
                            num_of_loves_selector.html(decrease_loves)
                        } else {
                            alert(response.msg)
                        }
                    } else {
                        alert("Something went wrong, try again")
                    }
                },
                error: function (jqXHR, textStatus) {
                    alert("Something went wrong, try again")
                },
                complete: function () {
                }
            });
        } else {
            $.ajax({
                url: "/addLove",
                method: "POST",
                data: {
                    'resource_type' : 'post',
                    'resource_id' : post_id,
                    '_token' : CSRF_TOKEN
                },
                dataType: "JSON",
                beforeSend: function () {
                },
                success: function (response) {
                    if (response) {
                        if (response.success) {
                            var increase_loves = parseInt(num_of_loves_selector.html()) + 1;
                            num_of_loves_selector.html(increase_loves)
                            $(love_selector).addClass('loved')
                            love_selector.css('fill', 'red')
                        } else {
                            alert(response.msg)
                        }
                    } else {
                        alert("Something went wrong, try again")
                    }
                },
                error: function (jqXHR, textStatus) {
                    alert("Something went wrong, try again")
                },
                complete: function () {
                }
            });
        }
    })
    $(document).on('click', '.love-comment', function (e) {
        e.preventDefault();
        var comment_id= $(this).attr('data-comment_id');
        var num_of_loves_selector = $('.num_of_loves_comment-' + comment_id);
        var love_selector = $('[data-comment_id = "' + comment_id + '"]');
        if ($(love_selector).hasClass('loved')) {
            $.ajax({
                url: "/removeLove",
                method: "POST",
                data: {
                    'resource_type' : 'comment',
                    'resource_id' : comment_id,
                    '_token' : CSRF_TOKEN
                },
                dataType: "JSON",
                beforeSend: function () {
                },
                success: function (response) {
                    if (response) {
                        if (response.success) {
                            love_selector.css('fill', '')
                            $(love_selector).removeClass('loved')
                            var decrease_loves = parseInt(num_of_loves_selector.html()) - 1;
                            num_of_loves_selector.html(decrease_loves)
                        } else {
                            alert(response.msg)
                        }
                    } else {
                        alert("Something went wrong, try again")
                    }
                },
                error: function (jqXHR, textStatus) {
                    alert("Something went wrong, try again")
                },
                complete: function () {
                }
            });
        } else {
            $.ajax({
                url: "/addLove",
                method: "POST",
                data: {
                    'resource_type' : 'comment',
                    'resource_id' : comment_id,
                    '_token' : CSRF_TOKEN
                },
                dataType: "JSON",
                beforeSend: function () {
                },
                success: function (response) {
                    if (response) {
                        if (response.success) {
                            var increase_loves = parseInt(num_of_loves_selector.html()) + 1;
                            num_of_loves_selector.html(increase_loves)
                            $(love_selector).addClass('loved')
                            love_selector.css('fill', 'red')
                        } else {
                            alert(response.msg)
                        }
                    } else {
                        alert("Something went wrong, try again")
                    }
                },
                error: function (jqXHR, textStatus) {
                    alert("Something went wrong, try again")
                },
                complete: function () {
                }
            });
        }
    })
    $(document).on('click', '.love-reply', function (e) {
        e.preventDefault();
        var reply_id = $(this).attr('data-reply_id');
        var num_of_loves_selector = $('.num_of_loves_reply-' + reply_id);
        var love_selector = $('[data-reply_id = "' + reply_id + '"]');
        if ($(love_selector).hasClass('loved')) {
            $.ajax({
                url: "/removeLove",
                method: "POST",
                data: {
                    'resource_type' : 'reply',
                    'resource_id' : reply_id,
                    '_token' : CSRF_TOKEN
                },
                dataType: "JSON",
                beforeSend: function () {
                },
                success: function (response) {
                    if (response) {
                        if (response.success) {
                            love_selector.css('fill', '')
                            $(love_selector).removeClass('loved')
                            var decrease_loves = parseInt(num_of_loves_selector.html()) - 1;
                            num_of_loves_selector.html(decrease_loves)
                        } else {
                            alert(response.msg)
                        }
                    } else {
                        alert("Something went wrong, try again")
                    }
                },
                error: function (jqXHR, textStatus) {
                    alert("Something went wrong, try again")
                },
                complete: function () {
                }
            });
        } else {
            $.ajax({
                url: "/addLove",
                method: "POST",
                data: {
                    'resource_type' : 'reply',
                    'resource_id' : reply_id,
                    '_token' : CSRF_TOKEN
                },
                dataType: "JSON",
                beforeSend: function () {
                },
                success: function (response) {
                    if (response) {
                        if (response.success) {
                            var increase_loves = parseInt(num_of_loves_selector.html()) + 1;
                            num_of_loves_selector.html(increase_loves)
                            $(love_selector).addClass('loved')
                            love_selector.css('fill', 'red')
                        } else {
                            alert(response.msg)
                        }
                    } else {
                        alert("Something went wrong, try again")
                    }
                },
                error: function (jqXHR, textStatus) {
                    alert("Something went wrong, try again")
                },
                complete: function () {
                }
            });
        }
    })
    var reachedBottom = false;
    var morePostsPage = 1;
    var last_page = $('#last_page').val()
    $(window).scroll(function(){
        if(!reachedBottom && morePostsPage <= last_page){
            if($(window).innerHeight()+$(document).scrollTop() > $(document).height() - 100){
                morePostsPage++;
                reachedBottom = true;
                if(morePostsPage <= 5){
                    $('#load_more_posts').css('display' , 'block');
                    $.get('/timeline', {page:morePostsPage}).done(function(d){
                        $('#newsfeed-items-grid').append(d);
                    }).always(function () {
                        reachedBottom = false;
                        $('.no-profile-pic').css('background', getRandomColor());
                        $('#load_more_posts').fadeOut(50);
                    });
                }else{
                    $('#btn-load-more').fadeIn(50);
                }
            }
        }
    });
    $(document).on('click' , '#btn-load-more' , function () {
        if(morePostsPage <= last_page){
            $('#load_more_posts').css('display' , 'block');
            $.get('/timeline', {page:morePostsPage}).done(function(d){
                $('#newsfeed-items-grid').append(d);
            }).always(function () {
                reachedBottom = false;
                $('.no-profile-pic').css('background', getRandomColor());
                $('#load_more_posts').fadeOut(50);
                $('#btn-load-more').fadeOut(50);
            });
        }else{
            $('#btn-load-more').fadeOut(50);
        }
    });
    $(document).on('click','.public_friend_request',(function(e) {
        e.preventDefault();
        $(this).empty();
        $(this).html('<img src="/svg-icons/spinner.svg" style="padding-bottom: 7px !important;" alt="arrow" class="back-icon">');
        var request_id = $('#request_id').val();
        if (request_id == undefined){
            request_id = $(this).attr("data-request_id")
        }
        var user_id = $('#user_id').val();
        var csrf_toke = document.head.querySelector('meta[name="csrf-token"]').content;
        $.ajax({
            type: "GET",
            url: '/public-friend-request-follow?request_id='+request_id+'&user_id='+user_id+'&_csrf='+csrf_toke,
            success: function (res) {
                if(res.status != undefined && res.status == true){
                    $.notify('Successfully Followed',{
                        animate: {
                            enter: 'animated fadeInRight',
                            exit: 'animated fadeOutRight'
                        },
                        element: 'body',
                        type: 'success'
                    });
                    setTimeout(function(){
                        location.reload();
                    }, 3000);

                }else{
                    $('.friend_request').empty();
                    $('.friend_request').html('<svg class="olymp-star-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg>');
                    $.notify('Something Went Wrong , Please Try Again Later',{
                        animate: {
                            enter: 'animated fadeInRight',
                            exit: 'animated fadeOutRight'
                        },
                        element: 'body',
                        type: 'danger'
                    });
                }
            },
            error: function (xhr, status, error) {
                $('.friend_request').empty();
                $('.friend_request').html('<svg class="olymp-star-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg>');
                $.notify('Something Went Wrong , Please Try Again Later',{
                    animate: {
                        enter: 'animated fadeInRight',
                        exit: 'animated fadeOutRight'
                    },
                    element: 'body',
                    type: 'danger'
                });
            }
        });

    }));
    $(document).on('click','.friend_request',(function(e) {
        e.preventDefault();
        $(this).empty();
        $(this).html('<img src="/svg-icons/spinner.svg" style="padding-bottom: 7px !important;" alt="arrow" class="back-icon">');
        var request_id = $('#request_id').val();
        var csrf_toke = document.head.querySelector('meta[name="csrf-token"]').content;

        if (request_id == undefined){
            request_id = $(this).attr("data-request_id")
        }

        $.ajax({
            type: "GET",
            url: '/friend-request-follow?request_id='+request_id+'&_csrf='+csrf_toke,
            success: function (res) {
                if(res.status != undefined && res.status == true){
                    $('.friend_request').empty();
                    $('.friend_request').html('<svg class="olymp-star-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-clock-icon"></use></svg>');
                    $('.friend_request').removeClass('bg-primary').addClass('bg-grey-light').addClass('friend_request_cancel').removeClass('friend_request');
                    $.notify('Follow Request Sent Successfully',{
                        animate: {
                            enter: 'animated fadeInRight',
                            exit: 'animated fadeOutRight'
                        },
                        element: 'body',
                        type: 'success'
                    });
                    setTimeout(function(){
                        location.reload();
                    }, 2000);
                }else{
                    $('.friend_request').empty();
                    $('.friend_request').html('<svg class="olymp-star-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg>');
                    $.notify('Something Went Wrong , Please Try Again Later',{
                        animate: {
                            enter: 'animated fadeInRight',
                            exit: 'animated fadeOutRight'
                        },
                        element: 'body',
                        type: 'danger'
                    });
                }
            },
            error: function (xhr, status, error) {

            }
        });
    }));
    $(document).on('click','.friend_delete',(function(e) {
        e.preventDefault();
        $(this).empty();
        $(this).html('<img src="/svg-icons/spinner.svg" style="padding-bottom: 7px !important;" alt="arrow" class="back-icon">');
        var request_id = $('#request_id').val();
        var csrf_toke = document.head.querySelector('meta[name="csrf-token"]').content;
        $.ajax({
            type: "GET",
            url: '/delete-friend?request_id='+request_id+'&_csrf='+csrf_toke,
            success: function (res) {
                if(res.status != undefined && res.status == true){
                    location.reload(true);
                }else{
                    $('.friend_delete').empty();
                    $('.friend_request_cancel').html('<svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 512 512"><defs><style>.cls-1{fill:url(#linear-gradient);}</style><linearGradient id="linear-gradient" x1="-126.2" y1="588.24" x2="347.84" y2="588.24" gradientTransform="translate(834.99 136.57) rotate(90)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#00f2fe"/><stop offset="0.02" stop-color="#03effe"/><stop offset="0.29" stop-color="#24d2fe"/><stop offset="0.55" stop-color="#3cbdfe"/><stop offset="0.8" stop-color="#4ab0fe"/><stop offset="1" stop-color="#4facfe"/></linearGradient></defs><title>unfollow</title><path class="cls-1" d="M224.23,446.6H63.42a18.47,18.47,0,0,1-18.54-18.43,18.23,18.23,0,0,1,.89-5.72C64,365.65,119.75,326,181.26,326a138.31,138.31,0,0,1,79.53,25,18.49,18.49,0,0,0,21.14-30.33,175.15,175.15,0,0,0-36.86-19.72,94.11,94.11,0,0,0,30.61-62.08A139.12,139.12,0,0,1,452.09,224.4a18.49,18.49,0,1,0,21.28-30.24l-.12-.08a174.94,174.94,0,0,0-36.88-19.62A94.27,94.27,0,1,0,303.21,168q3,3.27,6.23,6.25c-2.58,1-5.14,2-7.72,3.16a175.78,175.78,0,0,0-32.55,18.69A94.26,94.26,0,1,0,118,300.54C67.83,319.29,27.18,359.67,10.62,411.13a55.46,55.46,0,0,0,52.8,72.44H224.23a18.49,18.49,0,0,0,0-37ZM374.06,46.11A57.47,57.47,0,1,1,316.6,103.6v0A57.53,57.53,0,0,1,374.06,46.11ZM182.19,173a57.46,57.46,0,1,1-57.46,57.46h0A57.52,57.52,0,0,1,182.19,173ZM418.45,371.9V431H316.11c-19.49,0-35.29-13.22-35.29-29.54s15.8-29.54,35.29-29.54H418.45m0,0h31.8c19.49,0,35.29,13.23,35.29,29.54S469.74,431,450.25,431h-31.8"/></svg>');

                    $.notify('Something Went Wrong , Please Try Again Later',{
                        animate: {
                            enter: 'animated fadeInRight',
                            exit: 'animated fadeOutRight'
                        },
                        element: 'body',
                        type: 'danger'
                    });
                }
            },
            error: function (xhr, status, error) {

            }
        });
    }));
    $(document).on('click','.friend_request_cancel',(function(e) {
        e.preventDefault();
        $(this).empty();
        $(this).html('<img src="/svg-icons/spinner.svg" style="padding-bottom: 7px !important;" alt="arrow" class="back-icon">');
        var request_id = $('#request_id').val();
        var csrf_toke = document.head.querySelector('meta[name="csrf-token"]').content;
        $.ajax({
            type: "GET",
            url: '/delete-request-follow?request_id='+request_id+'&_csrf='+csrf_toke,
            success: function (res) {
                if(res.status != undefined && res.status == true){
                    $('.friend_request_cancel').empty();
                    $('.friend_request_cancel').html('<svg class="olymp-star-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg>');
                    $('.friend_request_cancel').removeClass('bg-grey-light').addClass('bg-primary').addClass('friend_request').removeClass('friend_request_cancel');
                    $.notify('Follow Request Canceled Successfully',{
                        animate: {
                            enter: 'animated fadeInRight',
                            exit: 'animated fadeOutRight'
                        },
                        element: 'body',
                        type: 'success'
                    });
                }else{
                    $('.friend_request_cancel').empty();
                    $('.friend_request').html('<svg class="olymp-star-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg>');
                    $.notify('Something Went Wrong , Please Try Again Later',{
                        animate: {
                            enter: 'animated fadeInRight',
                            exit: 'animated fadeOutRight'
                        },
                        element: 'body',
                        type: 'danger'
                    });
                }
            },
            error: function (xhr, status, error) {

            }
        });
    }));
    $(document).on('click','.friend_request_accept',(function(e) {
        e.preventDefault();
        $(this).empty();
        $(this).html('<img src="/svg-icons/spinner.svg" style="padding-bottom: 7px !important;" alt="arrow" class="back-icon">');
        var request_id = $('#request_id').val();
        var user_id = $('#user_id').val();
        var csrf_toke = document.head.querySelector('meta[name="csrf-token"]').content;
        $.ajax({
            type: "GET",
            url: '/accept-follower?user_id='+user_id+'&follower_id='+request_id+'&_csrf='+csrf_toke,
            success: function (res) {
                if(res.status != undefined && res.status == true){
                    document.location.href = '';
                }
            },
            error: function (xhr, status, error) {

            }
        });
    }));
    if(pathname.indexOf('/show-chat') === 0) {
        $('ul.pagination').hide();
        $(function () {
            $('.infinite-scroll').jscroll({
                autoTrigger: true,
                loadingHtml: '<div style="text-align: center;"><img class="center-block" src="/img/post_loader.gif" style="text-align: center;height: 30px;width: 30px" alt="Loading..." /></div>',
                padding: 0,
                nextSelector: '.pagination li.active + li a',
                contentSelector: 'div.infinite-scroll',
                callback: function () {
                    $('ul.pagination').remove();
                }
            });

        });
    }

    function paginationTabs(){
        $('.one ul.pagination').hide();
        $('.two ul.pagination').hide();
        console.log($('#home').hasClass('active'))
        console.log($('#profile').hasClass('active'))
        if(!$('#home').hasClass('active')) {


            $(function () {

                $('.infinite-scroll').jscroll({
                    autoTrigger: true,
                    loadingHtml: '<div style="text-align: center;"><img class="center-block" src="/img/post_loader.gif" style="text-align: center;height: 30px;width: 30px" alt="Loading..." /></div>',
                    padding: 12,
                    nextSelector: '.one .pagination li.active + li a',
                    contentSelector: 'div.infinite-scroll',
                    callback: function () {
                        $('.one ul.pagination').remove();
                    }
                });

            });
        }else {


            $(function () {
                $('.infinite-scroll_two').jscroll({
                    autoTrigger: true,
                    loadingHtml: '<div style="text-align: center;"><img class="center-block" src="/img/post_loader.gif" style="text-align: center;height: 30px;width: 30px" alt="Loading..." /></div>',
                    padding: 12,
                    nextSelector: '.two .pagination li.active + li a',
                    contentSelector: 'div.infinite-scroll_two',
                    callback: function () {
                        $('.two ul.pagination').remove();
                    }
                });

            });
        }
    }
    if(pathname.indexOf('/followers-following') === 0) {
        $(document).on('click','.nav-link',(function(e) {
            paginationTabs()
        }));
        paginationTabs()
    }
    if(pathname.indexOf('/notification') === 0){
        $('ul.pagination').hide();
        $(function () {
            $('.infinite-scroll').jscroll({
                autoTrigger: true,
                loadingHtml: '<div style="text-align: center;"><img class="center-block" src="/img/post_loader.gif" style="text-align: center;height: 30px;width: 30px" alt="Loading..." /></div>',
                padding: 0,
                nextSelector: '.pagination li.active + li a',
                contentSelector: 'div.infinite-scroll',
                callback: function () {
                    $('ul.pagination').remove();
                }
            });

        });
    }
    if(pathname.indexOf('/friend-requests') === 0){
        $('ul.pagination').hide();
        $(function () {
            $('.infinite-scroll').jscroll({
                autoTrigger: true,
                loadingHtml: '<div style="text-align: center;"><img class="center-block" src="/img/post_loader.gif" style="text-align: center;height: 30px;width: 30px" alt="Loading..." /></div>',
                padding: 0,
                nextSelector: '.pagination li.active + li a',
                contentSelector: 'div.infinite-scroll',
                callback: function () {
                    $('ul.pagination').remove();
                }
            });

        });
    }
    $(document).on('click','.friend_request_cancel',(function(e) {
        e.preventDefault();
        $(this).empty();
        $(this).html('<img src="/svg-icons/spinner.svg" style="padding-bottom: 7px !important;" alt="arrow" class="back-icon">');
        var request_id = $('#request_id').val();
        var csrf_toke = document.head.querySelector('meta[name="csrf-token"]').content;
        $.ajax({
            type: "GET",
            url: '/delete-request-follow?request_id='+request_id+'&_csrf='+csrf_toke,
            success: function (res) {
                if(res.status != undefined && res.status == true){
                    $('.friend_request_cancel').empty();
                    $('.friend_request_cancel').html('<svg class="olymp-star-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg>');
                    $('.friend_request_cancel').removeClass('bg-grey-light').addClass('bg-primary').addClass('friend_request').removeClass('friend_request_cancel');
                    $.notify('Follow Request Canceled Successfully',{
                        animate: {
                            enter: 'animated fadeInRight',
                            exit: 'animated fadeOutRight'
                        },
                        element: 'body',
                        type: 'success'
                    });
                }else{
                    $('.friend_request_cancel').empty();
                    $('.friend_request').html('<svg class="olymp-star-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg>');
                    $.notify('Something Went Wrong , Please Try Again Later',{
                        animate: {
                            enter: 'animated fadeInRight',
                            exit: 'animated fadeOutRight'
                        },
                        element: 'body',
                        type: 'danger'
                    });
                }
            },
            error: function (xhr, status, error) {

            }
        });
    }));

    $(document).on('click','.friend_request_discover',(function(e) {
        e.preventDefault();
        var self =  $(this);
        $(this).empty();
        $(this).html('<img src="/svg-icons/spinner.svg" style="padding-bottom: 7px !important;" alt="arrow" class="back-icon">');
        var request_id = self.attr("data-id")
        console.log(request_id);
        var csrf_toke = document.head.querySelector('meta[name="csrf-token"]').content;

        $.ajax({
            type: "GET",
            url: '/friend-request-follow?request_id='+request_id+'&_csrf='+csrf_toke,
            success: function (res) {
                if(res.status != undefined && res.status == true){
                    self.empty();
                    self.html('<svg class="olymp-star-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-clock-icon"></use></svg>');
                    self.removeClass('bg-primary').addClass('bg-grey-light').removeClass('friend_request');
                    $.notify('Follow Request Sent Successfully',{
                        animate: {
                            enter: 'animated fadeInRight',
                            exit: 'animated fadeOutRight'
                        },
                        element: 'body',
                        type: 'success'
                    });
                }else{

                    self.empty();
                    self.html('<svg class="olymp-star-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-happy-face-icon"></use></svg>');
                    $.notify('Something Went Wrong , Please Try Again Later',{
                        animate: {
                            enter: 'animated fadeInRight',
                            exit: 'animated fadeOutRight'
                        },
                        element: 'body',
                        type: 'danger'
                    });
                }
            },
            error: function (xhr, status, error) {

            }
        });
    }));
    // $(document).on('click' , '.edit-post' , function (e) {
    //     alert('editModal')
    //     e.preventDefault()
    //     var post_id = $(this).attr('data-post_id');
    //     $('.edit_post_modal_container').empty();
    //     $.ajax({
    //         type: "GET",
    //         url: '/editPost',
    //         data: {
    //             "post_id": post_id
    //         },
    //         success: function (response) {
    //             $('.edit_post_modal_container').append(response.data.modal);
    //             $(document).find('.fire_edit_post_modal').click();
    //         }
    //     });
    // })
    $(document).on('click' , '.open-post' , function (e) {
        e.preventDefault()
        var post_id = $(this).attr('data-post_id');
        $('.open_post_modal_container').empty();
        $.ajax({
            type: "GET",
            url: '/viewPost/' + post_id,
            success: function (response) {
                $('.open_post_modal_container').append(response.data.modal);
                $(document).find('.fire_open_post_modal').click();
            }
        });
    })
    $(document).on('click' , '.open_notification' , function (e) {
        var notification_id = $(this).attr('data-id');
        var user_id = $('#user_id').val();
        var type =  $(this).attr('data-type');
        var user_name = $(this).attr('data-user_name');
        $.ajax({
            type: "GET",
            url: '/open-notification?user_id='+user_id+'&notification_id='+notification_id+'&_csrf='.CSRF_TOKEN,
            success: function (response) {
                if(response.status != undefined && response.status == true){
                    if(response.data != undefined ){
                        if(type != 'follow'){
                            var resource_id = response.data;
                            location.href = '/viewPost/'+resource_id;
                        }else{
                            location.href = '/profile/'+user_name;
                        }

                    }
                }

            }
        });
    });
    var swiper = new Swiper('.swiper-container', {
        navigation: {
            nextEl: '.btn-next-without',
            prevEl: '.btn-prev-without ',
        },
    });
    $('.select_filtiration').on('change', function() {
        if(this.value != undefined && this.value != 0){
            $("#submit_search_order_by").submit();
        }
    });

    $('#select-repo').selectize({
        valueField: 'name',
        labelField: "name",
        searchField: ["name","title"],
        createOnBlur: true,
        openOnFocus:false,
        maxOptions:5,
        closeAfterSelect:true,
        render: {
            option: function(e, i) {
                    console.log(e,i)
                    return '<div class=" ">' + (e.logo ? '<div class="author-thumb header_logo"><img src="/' + i(e.logo) + '" alt="avatar"></div>' : "") + '<div class="notification-event">' + (e.name ? '<span class="h6 notification-friend"></a>' + i(e.name) + "</span>" : "") + (e.message ? '<span class="chat-message-item">' + i(e.message) + "</span>" : "") + "</div>" + (e.icon ? '<span class="notification-icon"><svg class="' + i(e.icon) + '"><use xlink:href="icons/icons.svg#' + i(e.icon) + '"></use></svg></span>' : "") + "</div>"

            },
            item: function(e, i) {
                location.href = '/profile/'+e.user_name
                return '<div><span class="label">' + i(e.name) + "</span></div>"
            }

        },
        load: function(query, callback) {
            if (!query.length) return callback();
            $.ajax({
                url: '/search-bar/?search=' + encodeURIComponent(query),
                type: 'GET',
                error: function() {
                    callback();
                },
                success: function(res) {
                    callback(res);
                }
            });
        }
    });
    function ordersPagination() {

        var limit = 9;
        var ordersCount        = $('#count').val();
        var ordersTotalPages   = Math.ceil( ordersCount / limit);
        var ordersVisiblePages = 4;
        if(ordersCount != undefined && Number(ordersCount) > Number(limit)){
            $('#project-pagination').twbsPagination({
                totalPages: ordersTotalPages,
                visiblePages: ordersVisiblePages,
                initiateStartPageClick: false,
                onPageClick: function (event, page) {
                    doPagination(page,limit)
                }
            });
        }
    }
    function doPagination(page,limit) {
        var offset = (page * limit) - limit;
        var filterby = $('#filterby').find(":selected").val();
        var sort = $('#sort').find(":selected").val();
        var type = $('#type').val();
        waitPage();
        $.ajax({
            url: '/home/ajax-discover-designs/'+offset+"/?filterby="+filterby+"&sort="+sort+"&type="+type,
            type: 'GET',
            error: function() {
                $('.des_con').empty()
            },
            success: function(res) {
                $('.des_con').empty()
                $('.des_con').html(res)
                $('html, body').stop().animate({
                    scrollTop: 0
                }, 1000);
            }
        });
    }
    function waitPage() {
        $('.des_con').empty()
        $('.des_con').html('<div style="text-align: center;    margin: auto;    margin-top: 66px;"><img class="center-block" src="/img/post_loader.gif" style="text-align: center;height: 64px;width: 64px" alt="Loading..." /></div>')
    }
    $(document).on('click' , '.add_new_project' , function (e) {
        e.preventDefault();
        var project_count = $("#project_count").val();
        console.log(project_count);
        $.ajax({
            url: '/website_dashboard/projects/check-project-permission',
            type: 'GET',
            error: function() {

            },
            success: function(res) {
                if (res['data'] != undefined){
                    if (res['data'] == 'unlimited'){
                        $('#create-photo-album').modal('show');
                        return;
                    }
                    if(project_count < res['data']){
                        $('#create-photo-album').modal('show');
                        return;
                    }else{
                        var message = 'You only can have up to '+res['data']+' Projects , Please upgrade your account <a href="/pricing/bundles">Here</a> to create more projects';
                        $("#message_alert").fadeOut()
                        $("#message_alert").html('<div class="alert alert-danger" role="alert"><button type="button" class="close" data-dismiss="alert">x</button>'+message+'</div>')
                        $("#message_alert").fadeIn()
                    }
                }

            }
        });

        // $('#create-photo-album').modal('show')
        console.log("Ah ok")
    });
    $(".article_disabled").click(function() {
        $('html, body').stop().animate({
            scrollTop: 680
        }, 1000);
    });
    $(".article_generation").click(function() {
        $('html, body').animate({
            scrollTop: 370
        }, 1000);
    });

});
function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

$('.menu-open-fixed').on("click",function(){
    $('.fixed-sidebar').toggleClass("fixed-sidebar-responsive open");
});