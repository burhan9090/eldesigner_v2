$(function() {
    paypal.Buttons({
        createOrder: function(data, actions) {
            // Set up the transaction
            return actions.order.create({
                purchase_units: [{
                    amount: {
                        value: amount_usd,
                    }
                }]
            });
        },
        onApprove: function(data, actions) {
            console.log('ON APPROVE DATA : ',data);
            console.log('ON APPROVE ACTION : ',actions);
                return actions.order.capture().then(function(details) {
                    console.log('ON APPROVE orderDetails : ',details);
                    var order_payload = {"data" : details ,'secure_hashed' : secure_hashed,'bundle_name' : bundle_name , _csrf: CSRF_TOKEN };
                    order_payload = JSON.stringify(order_payload);
                    console.log(order_payload);
                    $.ajax({
                        url: "/paypal/callback",
                        method: "POST",
                        contentType: "application/json",
                        dataType: 'JSON',
                        data: order_payload,
                        success: function (response) {
                            console.log(response)
                            if(response['status'] != undefined && response['status'] == true){
                                window.location.href = '/pricing/response/'+bundle_name
                            }else {
                                $.notify('Something Went Wrong , Please Try Again Later',{
                                    animate: {
                                        enter: 'animated fadeInRight',
                                        exit: 'animated fadeOutRight'
                                    },
                                    element: 'body',
                                    type: 'danger'
                                });
                            }

                        },
                        error: function (jqXHR, textStatus) {
                            $.notify('Something Went Wrong , Please Try Again Later',{
                                animate: {
                                    enter: 'animated fadeInRight',
                                    exit: 'animated fadeOutRight'
                                },
                                element: 'body',
                                type: 'danger'
                            });
                        },
                    });
                })

        }
    }).render('#paypal-button-container');
});