<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 1/19/19
 * Time: 5:01 AM
 */
return [
    'errors' => [
        'not_authorized' => 'You don`t have authorization to perform this action',
    ]
];