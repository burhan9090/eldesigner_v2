<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 2019-01-05
 * Time: 21:37
 */
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class FollowRequest extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'follow_request';
    protected $fillable = [
        'user_id','requested_user_id','approved'
    ];
    public function request_user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
