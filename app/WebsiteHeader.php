<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteHeader extends Model {

	const TYPE_PUBLISHING_ARTICLES = 1;
	const TYPE_ARTICLES_LIBRARY = 2;
	const TYPE_CONTACT_US_TAB = 3;
	const TYPE_PROJECTS = 4;
	const TYPE_SERVICES_TAB = 5;
	const TYPE_WELCOME_SCREEN = 6;
	const TYPE_USER_HISTORY = 7;
	const TYPE_COMMUNITY_PROJECTS = 8;
	const TYPE_EDIT_ARTICLE = 9;
	const TYPE_SELECT_THEME = 10;
	const TYPE_STYLE = 11;
	const TYPE_STATISTICS = 12;
	const TYPE_MESSAGES = 13;
	const TYPE_USER_SERVICES = 14;
	const TYPE_USER_SKILLS = 15;
	const TYPE_TEAM_MEMBERS = 16;
	const TYPE_WATER_MARK = 17;
	const TYPE_SETTINGS = 18;
	const TYPE_SEO = 19;
	const TYPE_GET_DOMAIN = 20;


	public static function getRow( $type ) {
		return Static::findOrNew( $type );
	}

}
