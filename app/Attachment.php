<?php
namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Attachment extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'attachment';
    protected $fillable = [
        'message_id', 'user_id', 'file_path','file_ext','file_original_name'
    ];
    public function message(){
        return $this->belongsTo(Message::class,'message_id');
    }
}