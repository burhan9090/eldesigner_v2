<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * Get the replies on the comments.
     */
    public function replies()
    {
        return $this->hasMany(Comment_reply::class , 'comment_id');
    }
}
