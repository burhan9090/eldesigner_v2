<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 2019-01-01
 * Time: 20:42
 */
namespace App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

class Follows extends Model
{
    protected $fillable = [
        'follower_user_id','followed_user_id'
    ];
    public function user(){
        return $this->belongsTo(User::class,'followed_user_id');
    }
    public function userfollower(){
        return $this->belongsTo(User::class,'follower_user_id');
    }
    public static function getMutualFriend($requestUserId){
        $aData = DB::select(DB::raw("SELECT u.name
                        FROM follows f1 INNER JOIN follows f2
                          ON f1.followed_user_id = f2.followed_user_id
                          left join users u on f1.followed_user_id = u.id
                        WHERE f1.follower_user_id = ?
                        AND f2.follower_user_id = ?  limit 1 "), [Auth::user()->id, $requestUserId]);

        if(isset($aData[0]->name)){
            return $aData[0]->name;
        }
        return null;

    }
}