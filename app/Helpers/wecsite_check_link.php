<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 1/13/19
 * Time: 2:15 PM
 */

if ( ! function_exists( 'website_check_facebook_link' ) ) {
	function website_check_facebook_link( $link ) {
		return $link;
	}
}

if ( ! function_exists( 'website_check_twitter_link' ) ) {
	function website_check_twitter_link( $link ) {
		return $link;
	}
}

if ( ! function_exists( 'website_check_instagram_link' ) ) {
	function website_check_instagram_link( $link ) {
		return $link;
	}
}

if ( ! function_exists( 'website_check_pinterest_link' ) ) {
	function website_check_pinterest_link( $link ) {
		return $link;
	}
}