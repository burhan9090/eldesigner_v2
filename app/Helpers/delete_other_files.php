<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 1/13/19
 * Time: 2:15 PM
 */

if(!function_exists('delete_other_files')){
	function delete_other_files($path, $filename) {
		if(file_exists(public_path($path))) {
			foreach ( scandir( public_path( $path ) ) as $f ) {
				if ( in_array( $f, [ '.', '..' ] ) ) {
					continue;
				}

				if ( file_exists( public_path( $f = str_replace( '//', '/', $path . '/' . $f ) ) ) ) {
					if ( public_path( $f ) != public_path( $filename ) ) {
						@unlink( $f );
					}
				}
			}
		}
	}
}