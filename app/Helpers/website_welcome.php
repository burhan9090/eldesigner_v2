<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 1/13/19
 * Time: 2:15 PM
 */

if ( ! function_exists( 'website_welcome' ) ) {
	function website_welcome( $user_id) {

		$welcome = \App\WebsiteWelcome
			::where( 'user_id', $user_id )
			->first();

		return $welcome?: new \App\WebsiteWelcome();
	}
}