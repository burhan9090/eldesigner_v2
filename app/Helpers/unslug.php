<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 1/13/19
 * Time: 2:15 PM
 */

if(!function_exists('unslug')){
	function unslug($str) {

		$str = preg_replace('/[\-\_\s]+/', ' ', $str);
		return trim($str);
	}
}