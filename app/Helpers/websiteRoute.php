<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 1/13/19
 * Time: 2:15 PM
 */

if(!function_exists('websiteRoute')){
	function websiteRoute($name, $parameters = [], $absolute = true) {
		$user_name=optional(request()->route())->parameter('user_name', '');

		$domain = request('outer', false)? 'outer': 'inner';

		if(request('outer', false)&&isset($parameters['user_name'])){
			unset($parameters['user_name']);
		}
		else {
			$parameters['user_name']= $user_name;
		}

		return route("$domain.$name", $parameters, $absolute);
	}
}