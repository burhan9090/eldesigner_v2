<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 1/13/19
 * Time: 2:15 PM
 */

if (! function_exists('optional')) {
	/**
	 * Provide access to optional objects.
	 *
	 * @param  mixed  $value
	 * @param  callable|null  $callback
	 * @return mixed
	 */
	function optional($value = null, callable $callback = null)
	{
		if (is_null($callback)) {
			return new \Illuminate\Support\Optional($value);
		} elseif (! is_null($value)) {
			return $callback($value);
		}
	}
}