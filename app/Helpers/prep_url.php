<?php

if ( ! function_exists('prep_url'))
{
	/**
	 * Prep URL
	 *
	 * Simply adds the http:// part if no scheme is included
	 *
	 * @param   string  the URL
	 * @return  string
	 */
	function prep_url($str = '')
	{
		if ($str === 'http://' OR $str === '')
		{
			return '';
		}
		$url = parse_url($str);
		if ( ! $url OR ! isset($url['scheme']))
		{
			return 'http://'.$str;
		}
		return $str;
	}
}