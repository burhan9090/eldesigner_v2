<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 1/13/19
 * Time: 2:15 PM
 */

if ( ! function_exists( 'website_team' ) ) {
	function website_team( $user_id ) {

		$teamMembers = \App\TeamMember::where( 'user_id', $user_id )->first();

		if ( $teamMembers ) {

			if ( $teamMembers->member ) {

				try {
					$membersText = json_decode( $teamMembers->member, 1 );

					$data = [];

					foreach ( $membersText as $key => $row ) {
						if ( trim( $row['title'] ) == '' ) {
							continue;
						}

						$data[] = [
							'title'       => $row['title'],
							'description' => $row['description'],
							'image'       => isset( $row['image'] ) ? asset( $row['image'] ) : ''
						];
					}

					return $data;

				} catch ( Exception $e ) {
				}
			}
		}

		return null;
	}
}