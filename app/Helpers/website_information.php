<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 1/13/19
 * Time: 2:15 PM
 */

if ( ! function_exists( 'website_information' ) ) {
	function website_information( $user_id) {

		$userHistory = \App\UserHistory
			::where( 'user_id', $user_id )
			->orderBy('type')
			->get();

		return $userHistory;
	}
}