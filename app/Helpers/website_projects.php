<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 1/13/19
 * Time: 2:15 PM
 */

if ( ! function_exists( 'website_projects' ) ) {
	function website_projects( $user_id, $limit = 10, $random=false ) {

		$projects = \App\WebsiteProject
			::with( [ 'tags', 'image', 'category', 'images.tags', 'user' ] )
			->whereHas( 'timeline', function ( $q ) use ( $user_id ) {
				$q->where( 'user_id', $user_id );
			} );

		if ( $filter = request( 'filter', [] )) {
			if ( isset( $filter['search_project'] ) ) {

				$projects->where( function ( $query ) use ( $filter ) {
					$query->where( 'title', 'like', "%{$filter['search_project']}%" );
					$query->orWhereHas( 'tags', function ( $q ) use ( $filter ) {
						$filter['search_project'] = preg_replace( '/[\s]{2,}/', ' ', $filter['search_project'] );
						$q->whereIn( 'text', explode( ' ', $filter['search_project'] ) );
					} );
				} );

			}
		}

		if ( $random ) {
			$projects->orderByRaw('rand()');
		}

		if ( $limit ) {
			return $projects->paginate( $limit );
		}

		return $projects->get();
	}
}