<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 1/13/19
 * Time: 2:15 PM
 */

if ( ! function_exists( 'website_contact_us' ) ) {
	function website_contact_us( $user_id ) {

		$contactUs = \App\WebsiteContactUs::where( 'user_id', $user_id )->first();

		return $contactUs?: new \App\WebsiteContactUs();
	}
}