<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 1/13/19
 * Time: 2:15 PM
 */

if(!function_exists('slugArray')){
	function slugArray($arr = []) {
		$result =[];

		foreach ($arr as &$row){
			$result[] = str_slug($row);
		}

		return $result;
	}
}