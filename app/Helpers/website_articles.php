<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 1/13/19
 * Time: 2:15 PM
 */

if ( ! function_exists( 'website_articles' ) ) {
	function website_articles( $user_id, $limit = 10, $random = false ) {

		$articles = \App\WebsiteArticles
			::with( [ 'user', 'category.parent' ] )
			->whereHas( 'timeline', function ( $q ) use ( $user_id ) {
				$q->where( 'user_id', $user_id );
			} );

		if ( $filter = request( 'filter', [] ) ) {

			if ( isset( $filter['category'] ) ) {
				$articles->whereIn( 'category_id', $filter['category'] );
			} elseif ( isset( $filter['category_parent'] ) ) {
				$articles->whereIn( 'category_id', function ( $q ) use ( $filter ) {
					$q->select( 'id' );
					$q->from( 'website_articles_categories' );
					$q->where( 'category_id', $filter['category_parent'] );
				} );
			}

			if ( isset( $filter['search_article'] ) ) {

				$articles->where( function($query) use($filter){
					$query->where( 'title', 'like', "%{$filter['search_article']}%" );
					$query->orWhereHas( 'tags', function ( $q ) use ( $filter ) {
						$filter['search_article'] = preg_replace( '/[\s]{2,}/', ' ', $filter['search_article'] );
						$q->whereIn( 'text', explode( ' ', $filter['search_article'] ) );
					} );
				});

			}
		}

		if ( $random ) {
			$articles->orderByRaw( 'rand()' );
		}
		else {
			$articles->orderByDesc( 'id' );
		}

		return $articles->paginate( $limit );
	}
}