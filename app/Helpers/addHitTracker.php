<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 1/13/19
 * Time: 2:15 PM
 */

if(!function_exists('addHitTracker')){
	function addHitTracker($user_id) {
		return \App\WebsiteTracking::addHit($user_id);
	}
}