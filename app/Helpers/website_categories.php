<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 1/13/19
 * Time: 2:15 PM
 */

if ( ! function_exists( 'website_categories' ) ) {
	function website_categories( $user_id ) {

		return  \App\WebsiteArticlesCategory
			::with('children')
			->whereHas('children.articles', function($q) use($user_id){
					$q->where('user_id', $user_id);
			})
			->get();
	}
}