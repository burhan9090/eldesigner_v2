<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 1/13/19
 * Time: 2:15 PM
 */

if ( ! function_exists( 'website_service' ) ) {
	function website_service( $user_id ) {

		$service  = \App\WebsiteService::where('user_id', $user_id)->first();

		if($service) {

			if($service->show_service) {

				//{"title":["ser,"description":["desc 11","desc 22","desc 33","desc 44"]}

				try {
					$serviceText = json_decode($service->service_text, 1);
					$icons = json_decode($service->icon, 1);
					if(!$icons){
						$icons = [];
					}

					$data= [];

					foreach ($serviceText['title'] as  $key=>$title){
						if(trim($title)==''){
							continue;
						}

						$data[] = [
							'title' => $title,
							'description' => $serviceText['description'][$key],
							'icons' => isset($icons[$key])? asset($icons[$key]): ''
						];
					}

					return $data;

				}
				catch (Exception $e){}
			}
		}

		return null;
	}
}