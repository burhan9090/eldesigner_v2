<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 1/13/19
 * Time: 2:15 PM
 */

if ( ! function_exists( 'user_skills' ) ) {
	function user_skills( $user_id ) {

		$userSkills  = \App\UserSkill::where('user_id', $user_id)->first();

		if($userSkills) {

			if($userSkills->skills) {

				//{"title":["asdfa",null,"asdffds","wer"],"weight":[10,0,60,10]}

				try {
					$skillText = json_decode($userSkills->skills, 1);

					$data= [];

					foreach ($skillText['title'] as  $key=>$title){
						if(trim($title)=='' || intval($skillText['weight'])==0){
							continue;
						}

						$data[] = [
							'title' => $title,
							'weight' => $skillText['weight'][$key]
						];
					}

					return $data;

				}
				catch (Exception $e){}
			}
		}

		return null;
	}
}