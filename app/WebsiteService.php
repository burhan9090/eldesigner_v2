<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WebsiteService
 * @package App
 *
 * @mixin \Eloquent
 */
class WebsiteService extends Model
{
    protected $fillable = ["user_id", "show_service", "service_text", "icon"];
}
