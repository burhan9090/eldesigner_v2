<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHistory extends Model
{

	const TYPE_EDUCATION = 1;
	const TYPE_EMPLOYMENT = 2;

	protected $table = "user_history";
	protected $fillable = ['user_id', 'type', 'title', 'period', 'description'];

}
