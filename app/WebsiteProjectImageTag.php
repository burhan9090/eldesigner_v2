<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteProjectImageTag extends Model
{
	public $timestamps=false;
	protected $fillable = ["image_id", "tag_id"];

}
