<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteProjectTimeLine extends Model
{
	protected $table="website_project_timeline";
    protected $fillable = ["user_id", "project_id"];


    public function project(){
    	return $this->belongsTo(WebsiteProject::class, 'project_id');
    }
}
