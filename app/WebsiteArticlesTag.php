<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WebsiteArticlesTag
 * @package App
 *
 * @mixin \Eloquent
 */
class WebsiteArticlesTag extends Model
{

	public $timestamps=false;

	protected $fillable = ["article_id", "tag_id"];
}
