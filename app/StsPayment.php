<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StsPayment extends Model {


	protected $fillable = [
		'user_id',
		'transaction_id',
		'amount',
		'payment_description',
		'route_success',
		'route_failed',

		'secure_hash',
		'card_holder_name',
		'card_expiry_date',
		'card_number',
		'approval_code',
		'currency_ ',
		'status_code',
		'status_description',
		'gateway_status_code',
		'gateway_status_description',
		'rrn',
		'gateway_name',
		'full_request'
	];

	public function getFullRequestAttribute( $value ) {
		return json_decode($value, 1)?: [];
	}

	public function setFullRequestAttribute( $value ) {
		return json_encode($value)?: '[]';
	}
}
