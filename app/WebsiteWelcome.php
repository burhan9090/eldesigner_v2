<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteWelcome extends Model {
	protected $table="website_welcome";
	protected $fillable = [
		"user_id", "background", "logo", "main", "description", "title",
		"keyword", "meta_description", "fb_page_id", "gplus", "user_picture",
		"created_at", "updated_at"
	];
}
