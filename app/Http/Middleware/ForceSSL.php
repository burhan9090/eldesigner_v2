<?php

namespace App\Http\Middleware;

use Closure;

class ForceSSL
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
	    $domain = explode( ':', $request->server( 'HTTP_HOST' ), 2 )[0];

	    if (!$request->secure() && \App::environment() === 'production' && env('DOMAIN')==$domain) {
		     return redirect()->secure($request->getRequestUri());
	    }

        return $next($request);
    }
}
