<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class CheckDomainOuter
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

	    $domain = explode( ':', $request->server( 'HTTP_HOST' ), 2 )[0];

	    $user = User::where( 'domain', $domain )->first();

	    if ( ! $user ) {
		    abort( '403', 'page not found' );
	    }

	    $request->merge(['outer' => true, ]);
	    $request->route()->setParameter('user_name', $user->user_name);
	    $request->route()->setParameter('user_id', $user->id);

        return $next($request);
    }
}
