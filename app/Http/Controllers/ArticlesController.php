<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 4/1/19
 * Time: 9:50 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WebsiteArticles;

class ArticlesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function CommunityArticles(Request $request){
        $searchblog = $request->input('searchblog');
        if(!empty($searchblog)){
            $WebsiteArticles = WebsiteArticles::with('user')->Where('title', 'like', "%{$searchblog}%")->paginate(4);
        }else{
            $WebsiteArticles = WebsiteArticles::with('user')->paginate(4);
        }
        $WebsiteArticlesTop = WebsiteArticles::with('user')->limit(3)->get();

        return view('profile.community_articles')->with(['articles' => $WebsiteArticles,'top' => $WebsiteArticlesTop]);

    }
}