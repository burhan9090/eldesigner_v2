<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 4/1/19
 * Time: 10:27 PM
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProjectsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function discoverDesign(Request $request){
        $aOfTypes = ['arch' => 1,'intd' => 2,'gd'=>3,'fd' => 4,'pg' =>5,'ill' => 6,'mo' => 7];
        $filterby = $request->input('filterby');
        $type = $request->input('type');
        $sort = $request->input('sort');
        $conditions = ' RAND() ';
        if(!empty($filterby) && !empty($sort)){
            $conditions = $this->filterBy($filterby,$sort);
        }
        $whereCondtion = ' where website_projects.is_demo= false ';
        if(!empty($type) && array_key_exists(trim($type),$aOfTypes) ){
            $catId = $aOfTypes[trim($type)];
            $whereCondtion .= " AND website_projects.category_id= '".$catId."' ";
        }

        $qury = "select U.profile_setting,U.user_name,U.name, image, project_count,project_id ,title,website_projects.id as pid from website_project_images join website_projects on website_project_images.project_id=website_projects.id LEFT JOIN users U on website_projects.user_id = U.id $whereCondtion  order by  $conditions limit 9 ";
        Log::info('discoverDesign Query '.$qury);
        $aProjects = DB::select(DB::raw($qury));
        $iProjectCount = DB::select(DB::raw("select image, project_id ,title from website_project_images join website_projects on website_project_images.project_id=website_projects.id $whereCondtion "));
        $iProjectCount = count($iProjectCount);

        $aProjects = array_map(function ($value) {
            return (array)$value;
        }, $aProjects);
        return view('profile.community_projects')->with(['projects'=>$aProjects,'project_count' => $iProjectCount,'type' => $type]);
    }
    public function filterBy($filterby,$sort){
        $statment = ' RAND() ';
        switch ($filterby){
            case 'MP' :
                $statment = ' project_count ';
                break;
            case  'HR' :
                $statment = ' project_count ';
                break;
            default :
                $statment = ' RAND() ';
                break;
        }
        if(!empty($statment) && !empty($sort)){
            if($sort == 'ASC'){
                $statment .= ' ASC ';
            }else{
                $statment .= ' DESC ';
            }
        }
        return $statment;
    }
    public function ajaxDiscoverDesign($offset,Request $request){
        $aOfTypes = ['arch' => 1,'intd' => 2,'gd'=>3,'fd' => 4,'pg' =>5,'ill' => 6,'mo' => 7];
        $type = $request->input('type');
        $filterby = $request->input('filterby');
        $sort = $request->input('sort');
        $conditions = ' RAND() ';
        if(!empty($filterby) && !empty($sort)){
            $conditions = $this->filterBy($filterby,$sort);
        }
        $whereCondtion = ' where website_projects.is_demo= false ';
        if(!empty($type) && array_key_exists(trim($type),$aOfTypes) ){
            $catId = $aOfTypes[trim($type)];
            $whereCondtion .= " AND website_projects.category_id= '".$catId."' ";
        }
        $qury = "select U.profile_setting, U.user_name,U.name,project_count, image, project_id ,title,website_projects.id as pid from website_project_images join website_projects on website_project_images.project_id=website_projects.id LEFT JOIN users U on website_projects.user_id = U.id $whereCondtion   order by  $conditions limit 9 offset ? ";
        Log::info('Ajax discoverDesign Query '.$qury.' and th offset ');
        $aProjects = DB::select(DB::raw($qury),[$offset]);

        $aProjects = array_map(function ($value) {
            return (array)$value;
        }, $aProjects);

        return view('home._discover_designs')->with(['projects'=>$aProjects]);
    }
}