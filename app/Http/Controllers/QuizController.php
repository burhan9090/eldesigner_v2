<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderInsight;
use App\Quiz;
use App\Quiz_service;
use App\Step;
use App\Sub_service;
use App\User;
use App\UserPermission;
use Illuminate\Http\Request;
use Image;


class QuizController extends Controller
{
    public function saveQuiz(Request $request){
        $hasPermission = UserPermission::checkPermission('design_request');

        $this->middleware('auth');
        $dataOut = array('success' => false, 'msg' => '', 'data' => []);
        if(!$hasPermission){
            $dataOut['msg'] = 'You Dont Have Permission To Create New Design Request Please Upgrade your plan';
            return response()->json($dataOut);
        }
        if (!$request->ajax()) {
            $dataOut['msg'] = 'Invalid Request';
            return response()->json($dataOut);
        }
        $user = User::findOrFail(\Auth::user()->id);
        if($user->has_quiz){
            $user->has_quiz = 0;
            $user->save();
        }
        Quiz::where('user_id' , $user->id)->delete();
        try{
            $payload = $request->all();
            $quiz = new Quiz();
            $quiz->user_id = $user->id;
            $quiz->quiz_level = $payload['process_level'];
            $quiz->quiz_type = $payload['process_type'];
            $quiz->required_info_name = in_array('name' , $payload['requiredFields'] )?1:0;
            $quiz->required_info_email = in_array('email' , $payload['requiredFields'])?1:0;
            $quiz->required_info_phone = in_array('phone' , $payload['requiredFields'] )?1:0;
            $quiz->required_info_address = in_array('address' , $payload['requiredFields'])?1:0;
            $quiz->required_info_attachment = in_array('attachment' , $payload['requiredFields'])?1:0;
            $quiz->required_info_budget = in_array( 'budget' , $payload['requiredFields'])?1:0;
            $quiz->required_info_overview = in_array( 'overview' , $payload['requiredFields'])?1:0;
            $quiz->required_info_location = in_array('location' , $payload['requiredFields'])?1:0;
            $quiz->active = 0;

            $quiz->save();

            $services = $payload['services'];
            foreach ($services as $service){
                $quiz_service = new Quiz_service();
                $quiz_service->quiz_id = $quiz->id;
                $quiz_service->service = $service['service'];
                $quiz_service->save();
                $sub_services_batch = array();
                $sub_services = $str_arr = explode (",", $service['sub_service']);
                foreach ($sub_services as $sub_service){
                    $sub_services_batch[] = array(
                        'quiz_service_id' => $quiz_service->id,
                        'sub_service_name' => $sub_service,
                    );
                }
                Sub_service::insert($sub_services_batch);
            }
            $steps = $request['steps'];
            foreach ($steps as $step){
                $quiz_step = new Step();
                $quiz_step->quiz_id = $quiz->id;
                $quiz_step->question = $step['question'];

                $quiz_step->title_1 = $step['1']['title'];
                $quiz_step->image_1 = $this->saveImage($step['1']['image']);

                $quiz_step->title_2 = $step['2']['title'];
                $quiz_step->image_2 = $this->saveImage($step['2']['image']);

                $quiz_step->title_3 = $step['3']['title'];
                $quiz_step->image_3 = $this->saveImage($step['3']['image']);

                $quiz_step->save();
            }
        }catch (\Exception $e){
            $dataOut['msg'] = $e;
            return response()->json($dataOut);
        }
        $quiz->active = 1;
        $quiz->save();
        $user->has_quiz = 1;
        $user->save();
        $dataOut['success'] = true;
        $dataOut['msg'] = 'Quiz Saved Successfully';
        return response()->json($dataOut);
    }
    private function saveImage($image){
        $imageName = time() . '_' . $image->getClientOriginalName();
        $imagePath = 'quizzes/';
        $img = Image::make($image);
        if ($img->save($imagePath . $imageName)) {
             return $imageName;
        }else{
            return 'no_image.jpg';
        }
    }
    public function takeQuiz($user_id){
        $quizInfo = Quiz::where('user_id' , $user_id)->first();
        $services = Quiz_service::where('quiz_id' , $quizInfo->id)->get();
        $quizLeveles = Step::where('quiz_id' , $quizInfo->id)->get();
        $steps = 2 + $quizInfo->quiz_level;
        return view('quiz.take')
            ->with('levels', $quizLeveles)
            ->with('steps', $steps)
            ->with('quizInfo', $quizInfo)
            ->with('services', $services);
    }
    public function getSubServices(Request $request){
        $dataOut = array('success' => false, 'msg' => '', 'data' => []);
        if (!$request->ajax()) {
            $dataOut['msg'] = 'Invalid Request';
            return response()->json($dataOut);
        }
        $service_id = $request->input('service_id');
        $sub_services = Sub_service::where('quiz_service_id' , $service_id)->get();
        $dataOut['success'] = true;
        $dataOut['data'] = $sub_services;
        return response()->json($dataOut);
    }
    public function saveProcess(Request $request){
        $dataOut = array('success' => false, 'msg' => '', 'data' => []);
        if (!$request->ajax()) {
            $dataOut['msg'] = 'Invalid Request';
            return response()->json($dataOut);
        }
        $payload = json_decode($request->input('payload'));
        $order = new Order();
        foreach ($payload as $row){
            if($row->name == 'user_id'){
                $order->user_id = $row->value;
                $order->client_id = $row->value;
            }
            if($row->name == 'required_info_name'){
                $order->client_name = $row->value;
            }
            if($row->name == 'required_info_email'){
                $order->client_email = $row->value;
            }
            if($row->name == 'required_info_phone'){
                $order->client_phone = $row->value;
            }
            if($row->name == 'required_info_address'){
                $order->client_address = $row->value;
            }
            if($row->name == 'required_info_budget'){
                $order->project_budget = $row->value;
            }
            if($row->name == 'required_info_location'){
                $order->project_location = $row->value;
            }
            if($row->name == 'service'){
                $order->requested_service = $row->value;
            }
            if($row->name == 'sub_service'){
                $order->requested_sub_service = $row->value;
            }
            $order->save();
            if($row->name == 'answers'){
                $insightsBatch = array();
                foreach ($row->value as $insight){
                    if($insight->question && $insight->answer_title && $insight->answer_title){
                        $insightsBatch[] = array(
                            'question' => $insight->question,
                            'answer_title' => $insight->answer_title,
                            'answer_image' => $insight->answer_title,
                            'order_id' => $order->id
                        );
                    }
                }
                OrderInsight::insert($insightsBatch);
            }
        }
        $dataOut['success'] = true;
        $dataOut['msg'] = 'Your request has been received';
        return response()->json($dataOut);
    }
}
