<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 2019-03-23
 * Time: 17:02
 */

namespace App\Http\Controllers;

use App\Classes\Payment;
use App\Paybal;
use App\Plans;
use App\UserPermission;
use Carbon\Carbon;

class PricingController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function bundles()
    {
        $iUserId = \Auth::user()->id;
        $userPermission = UserPermission::Where('user_id',$iUserId)->where('is_deleted',false)->with('plan')->get()->first();
        $sExpirationDateTime = 0;
        if($userPermission){
            $iExpirationTimeStamp = $userPermission->expiration_timestamp;
            $sExpirationDateTime = date('d M Y H:m:s',$iExpirationTimeStamp);

        }
        return view('pricing.bundles')
            ->with('user_permission', $userPermission)
            ->with('expiration_datetime',$sExpirationDateTime);
    }

    public function payment(Paybal $payment) {
        $payment->getData();
        return view('payment.form')
            ->with('payment', $payment);
    }
    public function bundlesPayment($bundle_name)
    {
        if (!empty($bundle_name)) {
            $bundle_name = strtoupper($bundle_name);
            $oPlans = Plans::where('plans_name', $bundle_name)->get()->first();
            if ($oPlans) {
                $iUserId = \Auth::user()->id;
                $userPermission = UserPermission::Where('user_id',$iUserId)->where('plans_id',$oPlans->plans_id)->where('is_deleted',false)->with('plan')->get()->first();
                if($userPermission){
                    \Session::flash('error', 'You already subscribed to the same plan , Please use another plan');
                    return redirect()->route('pricing.bundles');
                }
                $oPayment = new Paybal();
                $planDescription = 'You are purchasing  ' . $oPlans->description . '  which is total amount equal  ' . $oPlans->plans_price . '.00 USD  ';
                $successCallBackRoute = 'pricing.bundles.response';
                $aAdditionalParams = ['bundle_name' => $bundle_name];
                $sHashedKey = $iUserId.'|'.$oPlans->plans_price.'|'.$oPlans->plans_name;
                $cipher = 'aes128';
                $iv = "1234567891011212";
                $sKey = 'BURHAN_BURHAN';
                $sSecureHashed = openssl_encrypt($sHashedKey,$cipher,$sKey,$options=0, $iv);
                return $oPayment->processPaypal($oPlans->plans_name,$oPlans->plans_price,$sSecureHashed, $planDescription);
            }

        }

    }

    public function TransactionPlansResponse($bundle_name, Paybal $payment)
    {
        $iUserId = \Auth::user()->id;
        if ($payment->isSuccess()) {
            $payment->flushData();
            $oPlans = Plans::where('plans_name', strtoupper(trim($bundle_name)))->get()->first();
            if ($oPlans) {
                $date_object = Carbon::now()->addMonths(1)->setTime(23, 50, 59);
                $date_timestamp = $date_object->timestamp;
                UserPermission::create([
                    'user_id' => $iUserId,
                    'plans_id' => $oPlans->plans_id,
                    'expiration_timestamp' => $date_timestamp,
                    'is_deleted' => 0,
                ]);
                $date = $date_object->toDateTimeString();
                return view('pricing.response', ['status' => true,'plan_name' => $bundle_name, 'expiration_date' => $date]);
            }
        }
        $payment->flushData();
        return view('pricing.response', ['status' => false,'plan_name' => $bundle_name, 'expiration_date' => '']);
    }
}