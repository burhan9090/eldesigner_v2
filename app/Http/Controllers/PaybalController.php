<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Log;

use App\Paybal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaybalController  extends Controller {
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function PaypalCallback(Request $request){
            $aResponse =['status' => false , 'message' => 'Something went wrong , please try again later'];
            if ($request->method('post')){
                $aPayPalData = $request->input('data');
                Log::info('PaypalCallback_user_id : '.strval(Auth::user()->id)." response : ".json_encode($aPayPalData));
                $sSecure_hashed = $request->input('secure_hashed');

                $sRequestedBundleName = $request->input('bundle_name');

                $sEncryptedKey = $this->encryptKey($sSecure_hashed);

                $aEncryptedKey = explode('|',$sEncryptedKey);
                $sUserId = $iPrice = $sBundle = '';

                if (isset($aEncryptedKey[0])){
                    $sUserId = $aEncryptedKey[0];
                }
                if (isset($aEncryptedKey[1])){
                    $iPrice = $aEncryptedKey[1];

                }
                if (isset($aEncryptedKey[2])){
                    $sBundle = $aEncryptedKey[2];

                }

                if($sUserId == Auth::user()->id){

                    if($aPayPalData){
                        $oPayPal = new Paybal();
                        $aShapedPaypalData = $oPayPal->shapePaypalResponse($aPayPalData);
                        if(isset($aShapedPaypalData['paybal_status']) && $aShapedPaypalData['paybal_status'] == 'COMPLETED'){

                            if (strtolower($sRequestedBundleName) == strtolower($sBundle)){

                                if(isset($aShapedPaypalData['paybal_payments_amount']) && $aShapedPaypalData['paybal_payments_amount'] == $iPrice){

                                    $aShapedPaypalData['bundle'] = strtolower($sBundle);
                                    $aShapedPaypalData['user_id'] = Auth::user()->id;
                                    $oPayPal->savePayPal($aShapedPaypalData);
                                    $aResponse['status'] = true;
                                    $aResponse['message'] = 'Transaction Completed';

                                }
                            }
                        }
                    }
                }
            }
        return \Response::json($aResponse);
    }
    public function encryptKey($sSecure_hashed){
        $cipher = 'aes128';
        $iv = "1234567891011212";
        $sKey = 'BURHAN_BURHAN';
        $original_plaintext = openssl_decrypt($sSecure_hashed, $cipher, $sKey, $options=0, $iv);
        return $original_plaintext;
    }
}