<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Comment_reply;
use App\Events\NotificationEvent;
use App\Jobs\ImageProcess;
use App\Notification;
use App\Post;
use Illuminate\Http\Request;
use Auth;
use Image;
use DB;

class CommentController extends Controller
{
    public function addComment(Request $request){
        $dataOut = array('success' => false, 'msg' => '', 'data' => []);
        if (!$request->ajax()) {
            $dataOut['msg'] = 'Invalid Request';
            return response()->json($dataOut);
        }
        $payload = $request->all();
        $comment = new Comment();
        $images = $request->file('images');
        $aImagesName = [];
        for ($i = 0; $i < count($images); $i++) {
            $imageName = time() . '_' . $images[$i]->getClientOriginalName();
            $imagePath = 'user_media_comments/';
            $img = Image::make($_FILES['images']['tmp_name'][$i]);
            if ($img->save($imagePath . $imageName)) {
                $comment->image_path = $imageName;
                $aImagesName[] = $imageName;
            }
        }
        $comment->resource_id = $payload['post_id'];
        $comment->user_id = Auth::user()->id;
        $comment->comment_text = $payload['comment_text']?:'';
        $comment->resource_type = 'post';
        $comment->save();

        if(!empty($aImagesName)){
            ImageProcess::dispatch(Auth::user(),$comment,$aImagesName[0],'OPTIMIZE','COMMENT');
        }

        $comment_block = view('comments.comment')
            ->with('user', Auth::user())
            ->with('comment', $comment)
            ->render();
        $dataOut['success'] = true;
        $dataOut['msg'] = "Comment published";
        $dataOut['data']['comment'] = $comment_block;

        broadcast(new NotificationEvent(Auth::user(),$comment,'comment'))->toOthers();
        return response()->json($dataOut);
    }
    public function addReply(Request $request){
        $dataOut = array('success' => false, 'msg' => '', 'data' => []);
        if (!$request->ajax()) {
            $dataOut['msg'] = 'Invalid Request';
            return response()->json($dataOut);
        }
        $payload = $request->all();
        $reply = new Comment_reply();
        $images = $request->file('images');
        $aImagesNames = [];
        for ($i = 0; $i < count($images); $i++) {
            $imageName = time() . '_' . $images[$i]->getClientOriginalName();
            $imagePath = 'user_media_replies/';
            $img = Image::make($_FILES['images']['tmp_name'][$i]);
            if ($img->save($imagePath . $imageName)) {
                $reply->image_path = $imageName;
                $aImagesNames[] = $imageName;
            }
        }
        $reply->comment_id = $payload['comment_id'];
        $reply->user_id = Auth::user()->id;
        $reply->reply_text = $payload['reply_text']?:'';
        $reply->save();
        if(!empty($aImagesNames)){
            ImageProcess::dispatch(Auth::user(),$reply,$aImagesNames[0],'OPTIMIZE','REPLIES');
        }
        broadcast(new NotificationEvent(Auth::user(),$reply,'replies'))->toOthers();
        $reply_block = view('comments.reply')
            ->with('user', Auth::user())
            ->with('reply', $reply)
            ->render();
        $dataOut['success'] = true;
        $dataOut['msg'] = "Comment published";
        $dataOut['data']['reply'] = $reply_block;
        return response()->json($dataOut);
    }
}
