<?php

namespace App\Http\Controllers;

use App\User;
use App\WebsiteArticles;
use App\WebsiteProject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('eldesigners.home');
    }
    public function pricing(){
        return view('eldesigners.pricing');
    }
    public function howItWorks(){
        return view('eldesigners.how_it_works');
    }
    public function login(){
        return view('eldesigners.login');
    }
    public function HomeBlogs(Request $request){
        $searchblog = $request->input('searchblog');
        if(!empty($searchblog)){
            $WebsiteArticles = WebsiteArticles::with('user')->Where('title', 'like', "%{$searchblog}%")->paginate(4);
        }else{
            $WebsiteArticles = WebsiteArticles::with('user')->paginate(4);
        }
        $WebsiteArticlesTop = WebsiteArticles::with('user')->limit(3)->get();

        return view('home.home_blog')->with(['articles' => $WebsiteArticles,'top' => $WebsiteArticlesTop]);

    }

    public function discoverDesign(Request $request){
        $aOfTypes = ['arch' => 1,'intd' => 2,'gd'=>3,'fd' => 4,'pg' =>5,'ill' => 6,'mo' => 7];
        $filterby = $request->input('filterby');
        $type = $request->input('type');
        $sort = $request->input('sort');
        $conditions = ' RAND() ';
        if(!empty($filterby) && !empty($sort)){
            $conditions = $this->filterBy($filterby,$sort);
        }
        $whereCondtion = ' where website_projects.is_demo= false ';
        if(!empty($type) && array_key_exists(trim($type),$aOfTypes) ){
            $catId = $aOfTypes[trim($type)];
            $whereCondtion .= " AND website_projects.category_id= '".$catId."' ";
        }

        $qury = "select U.profile_setting,U.user_name,U.name, image, project_count,project_id ,title,website_projects.id as pid from website_project_images join website_projects on website_project_images.project_id=website_projects.id LEFT JOIN users U on website_projects.user_id = U.id $whereCondtion  order by  $conditions limit 9 ";
        Log::info('discoverDesign Query '.$qury);
        $aProjects = DB::select(DB::raw($qury));
        $iProjectCount = DB::select(DB::raw("select image, project_id ,title from website_project_images join website_projects on website_project_images.project_id=website_projects.id $whereCondtion "));
        $iProjectCount = count($iProjectCount);

        $aProjects = array_map(function ($value) {
            return (array)$value;
        }, $aProjects);
        return view('home.discover_designs')->with(['projects'=>$aProjects,'project_count' => $iProjectCount,'type' => $type]);
    }
    public function filterBy($filterby,$sort){
        $statment = ' RAND() ';
        switch ($filterby){
            case 'MP' :
                $statment = ' project_count ';
                break;
            case  'HR' :
                $statment = ' project_count ';
                break;
            default :
                $statment = ' RAND() ';
                break;
        }
        if(!empty($statment) && !empty($sort)){
            if($sort == 'ASC'){
                $statment .= ' ASC ';
            }else{
                $statment .= ' DESC ';
            }
        }
        return $statment;
    }
    public function ajaxDiscoverDesign($offset,Request $request){
        $aOfTypes = ['arch' => 1,'intd' => 2,'gd'=>3,'fd' => 4,'pg' =>5,'ill' => 6,'mo' => 7];
        $type = $request->input('type');
        $filterby = $request->input('filterby');
        $sort = $request->input('sort');
        $conditions = ' RAND() ';
        if(!empty($filterby) && !empty($sort)){
            $conditions = $this->filterBy($filterby,$sort);
        }
        $whereCondtion = ' where website_projects.is_demo= false ';
        if(!empty($type) && array_key_exists(trim($type),$aOfTypes) ){
            $catId = $aOfTypes[trim($type)];
            $whereCondtion .= " AND website_projects.category_id= '".$catId."' ";
        }
        $qury = "select U.profile_setting, U.user_name,U.name,project_count, image, project_id ,title,website_projects.id as pid from website_project_images join website_projects on website_project_images.project_id=website_projects.id LEFT JOIN users U on website_projects.user_id = U.id $whereCondtion   order by  $conditions limit 9 offset ? ";
        Log::info('Ajax discoverDesign Query '.$qury.' and th offset ');
        $aProjects = DB::select(DB::raw($qury),[$offset]);

        $aProjects = array_map(function ($value) {
            return (array)$value;
        }, $aProjects);

        return view('home._discover_designs')->with(['projects'=>$aProjects]);
    }
    public function discoverDesignersGuest(Request $request){
        $search_value = $request->input('search');
        $order_by_method = $request->input('orderbymethod');
        $orderByCases = ['RA','MP','NoP','CO','FL'];
        if(!empty($search_value)){
            $aDiscoverFriends = User::Where('name', 'like', "%{$search_value}%")->orWhere('user_name', 'like', "%{$search_value}%")->withCount('posts','follows','projects')->with('country','city');
        }else{
            $aDiscoverFriends = User::withCount('posts','follows','projects')->with('country','city');
        }

        if(!empty($order_by_method) && $order_by_method !== 0 && in_array($order_by_method,$orderByCases)){

            if($order_by_method == 'CO'){
                $aDiscoverFriends->where('profile_type','company');
            }else if($order_by_method == 'FL'){
                $aDiscoverFriends->where('profile_type','freelancer');
            }else {
                $method = self::getOrderBy($order_by_method);
                if (!empty($method) && is_array($method)) {

                    $order_by = $method['order_by'];
                    $direction = $method['direction'];
                    $aDiscoverFriends = $aDiscoverFriends->orderBy($order_by, $direction);


                }
            }
        }
        $aDiscoverFriends = $aDiscoverFriends->paginate(12);

        $order_by_options = [
            [
                'value' => 0,
                'text' => 'Select Order By Method'
            ],
            [
                'value' => 'RA',
                'text' => 'Recently Updated'
            ],
            [
                'value' => 'MP',
                'text' => 'Most popular'
            ],
            [
                'value' => 'NoP',
                'text' => 'Number of projects'
            ],
            [
                'value' => 'CO',
                'text' => 'Companies only'
            ],
            [
                'value' => 'FL',
                'text' => 'Freelancers'
            ],
        ];

        return view('home.discover_friend')->with(['discover'=>$aDiscoverFriends,'order_by_options'=>$order_by_options]);
    }
    public static function getOrderBy($method){

        switch ($method){
            case 'MP' :
                $method = [
                    'order_by' => 'profile_count',
                    'direction' => 'DESC'
                ];
                break;
            case 'NoP' :
                $method = [
                    'order_by' => 'projects_count',
                    'direction' => 'DESC'
                ];
                break;
            case 'RA' :
                $method = [
                    'order_by' => 'id',
                    'direction' => 'DESC'
                ];
                break;

        }
        return $method;

    }
}
