<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 2018-12-23
 * Time: 23:57
 */

namespace App\Http\Controllers;

use App\Attachment;
use App\Events\MessagePosted;
use App\User;
use Illuminate\Http\Request;
use App\Message;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Carbon\Carbon;
class MessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getMessages(Request $request)
    {
        $iUserId = $request->input('user_id');
        $iFriendId = $request->input('friend_id');
        $messages = ['status' => false, 'data' => []];
        $aFriendInfo = [];
        if ($iUserId == Auth::user()->id) {
            $aData = Message::with('user', 'user_to','attachments')->where(function ($query) use ($iFriendId) {
                $query->where('user_id', '=', Auth::user()->id)->where('user_id_to', '=', $iFriendId);
            })->orWhere(function ($query) use ($iFriendId) {
                $query->where('user_id', '=', $iFriendId)->where('user_id_to', '=', Auth::user()->id);
            })->orderBy('created_at', 'desc')->limit(10)->get()->toArray();
            //if the data is empty we will git only friend data
            if(empty($aData)){
              $friendUserData = User::where('id','=',$iFriendId)->get()->toArray();
              $aFriendInfo[0]['user_to'] = $friendUserData[0];
              $aFriendInfo[0]['user_id'] = Auth::user()->id;
            }
            $messages = ['status' => true, 'data' => $aData, 'friend_info' =>$aFriendInfo];
        }
        return \Response::json($messages);
    }


    public function showSideMenuMessage(Request $request)
    {
        $iUserId = $request->input('user_id');
        $messages = ['status' => false, 'data' => []];
        if ($iUserId == Auth::user()->id) {
            $aData = DB::select(DB::raw("SELECT t1.* , u.name , u.logo, u2.name as name_2 , u2.logo logo_2 FROM messages t1
                        JOIN (
                          SELECT MAX(created_at) AS 'time', user_id_to, user_id
                          FROM messages
                          WHERE user_id=? or  user_id_to=?  GROUP BY conversation_id
                        ) t2 ON (t1.created_at = t2.time AND ((t1.user_id_to = t2.user_id_to  AND t1.user_id = t2.user_id) or (t1.user_id_to = t2.user_id  AND t1.user_id = t2.user_id_to)))
                        LEFT JOIN users u on t1.user_id = u.id LEFT JOIN  users u2 ON t1.user_id_to = u2.id order by t1.created_at desc "), [$iUserId, $iUserId]);
            $messages = ['status' => true, 'data' => $aData];
        }

        return \Response::json($messages);
    }

    public function setMessages(Request $request)
    {
        Log::info('Request Is Here');


        $messages = ['status' => false, 'data' => []];
        if ($request->isMethod('post')) {
            $iUserId = $request->input('user_id');
            $iFriendId = $request->input('friend_id');
            $sChatMessage = $request->input('message');

            if ($iUserId == Auth::user()->id) {
                $conversation_id = trim($iUserId) . '_' . trim($iFriendId);
                $bHaveAttatchement = false;
                if ($request->hasFile('file')) {
                    Log::info('Request For File : '.json_encode($request->file));
                    $bHaveAttatchement = true;
                }
                if (intval($iUserId) > intval($iFriendId)) {
                    $conversation_id = trim($iFriendId) . '_' . trim($iUserId);
                }

                if (!empty($sChatMessage) || $bHaveAttatchement) {
                    $message_id = Message::create([
                        'user_id' => Auth::user()->id,
                        'user_id_to' => $iFriendId,
                        'message' => $sChatMessage,
                        'conversation_id' => $conversation_id,
                        'have_attachments' => $bHaveAttatchement
                    ]);
                    if ($bHaveAttatchement) {
                        foreach ($request->file as $file) {
                            $filename = $file->storeAs('chat',Str::random(40).'_'.Carbon::now()->timestamp.'_.'.$file->getClientOriginalExtension());
                            if (!empty($filename)) {
                                Attachment::create([
                                    'user_id' => Auth::user()->id,
                                    'message_id' => $message_id->id,
                                    'file_path' => basename($filename),
                                    'file_ext' => $file->getClientOriginalExtension(),
                                    'file_original_name' => $file->getClientOriginalName()
                                ]);
                            }
                        }
                    }
                    $aData = Message::with('user', 'user_to','attachments')->where(function ($query) use ($iFriendId) {
                        $query->where('user_id', '=', Auth::user()->id)->where('user_id_to', '=', $iFriendId);
                    })->orWhere(function ($query) use ($iFriendId) {
                        $query->where('user_id', '=', $iFriendId)->where('user_id_to', '=', Auth::user()->id);
                    })->orderBy('created_at', 'desc')->limit(10)->get()->toArray();
                    $oUser = User::find($iUserId);

                    event(new MessagePosted($message_id->id, $oUser));
                    $messages = ['status' => true, 'data' => $aData];
                }
            }
        }
        return \Response::json($messages);
    }

    //update the message to as seen
    public function updateAsSeen(Request $request)
    {
        $iUserId = $request->input('user_id');
        $iConversationId = $request->input('conversation_id');

        if ($iUserId == Auth::user()->id) {
            Message::where('conversation_id', $iConversationId)->update(['is_seen' => true]);
        }
    }

}
