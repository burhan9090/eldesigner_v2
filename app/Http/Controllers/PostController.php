<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Comment_reply;
use App\Jobs\ImageProcess;
use App\Media;
use App\Notification;
use App\Post;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\Response;
use Image;
use Config;
use DB;


class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function addPost(Request $request)
    {
        $dataOut = array('success' => false, 'msg' => '', 'data' => []);
        if (!$request->ajax()) {
            $dataOut['msg'] = 'Invalid Request';
            return response()->json($dataOut);
        }
        $status = $request->input('status');

        $post = new Post();

        $post->user_id = Auth::user()->id;
        $post->post_text = $status;

        $post->save();

        $post_block = view('posts.media_post')
            ->with('user', Auth::user())
            ->with('post', $post)
            ->with('images', [])
            ->with('video', [])
            ->render();
        $dataOut['success'] = true;
        $dataOut['msg'] = "post_published";
        $dataOut['data']['post'] = $post_block;
        return response()->json($dataOut);
    }

    public function addMediaPost(Request $request)
    {
        $dataOut = array('success' => false, 'msg' => '', 'data' => []);
        if (!$request->ajax()) {
            $dataOut['msg'] = 'Invalid Request';
            return response()->json($dataOut);
        }
        $status = $request->input('status');
        $images = $request->file('images');
        $video = $request->file('video');

        $post = new Post();

        $post->user_id = Auth::user()->id;
        $post->post_text = $status;
        $post->save();
        $video_name = '';
        if ($video) {
            $video_name = time() . '_' . $video->getClientOriginalName();
            $path = public_path() . '/user_media/';
            if ($video->move($path, $video_name)) {
                $dataMedia = array(
                    'resource_id' => $post->id,
                    'resource_type' => 'post',
                    'file_name' => $video_name,
                    'media_type' => 'video',
                );
                DB::table('user_media')->insert($dataMedia);
            }
        }
        $imagesBatch = array();
        $aImageNames = [];
        for ($i = 0; $i < count($images); $i++) {
            $imageName = time() . '_' . $images[$i]->getClientOriginalName();
            $imagePath = 'user_media/';
            $img = Image::make($_FILES['images']['tmp_name'][$i]);
            if ($img->save($imagePath . $imageName)) {
                $imagesBatch[] = array(
                    'resource_id' => $post->id,
                    'resource_type' => 'post',
                    'file_name' => $imageName,
                    'media_type' => 'image',
                );
                $aImageNames[] = $imageName;
            }
        }
        if (count($imagesBatch)) {
            $bDbResponse = DB::table('user_media')->insert($imagesBatch);
            if($bDbResponse && !empty($aImageNames) ){
                // insert images into queue to optimize
                foreach ($aImageNames as $nameOfImage){
                    ImageProcess::dispatch(Auth::user(),$post->id,$nameOfImage,'OPTIMIZE','USER_MEDIA');
                }
            }
        }
        $post_block = view('posts.media_post')
            ->with('user', Auth::user())
            ->with('post', $post)
            ->with('images', $imagesBatch)
            ->with('video', $video_name)
            ->render();
        $dataOut['success'] = true;
        $dataOut['msg'] = "post_published";
        $dataOut['data']['post'] = $post_block;
        return response()->json($dataOut);
    }
    public function editPost(Request $request){
        $dataOut = array('success' => false, 'msg' => '', 'data' => []);
        if (!$request->ajax()) {
            $dataOut['msg'] = 'Invalid Request';
            return response()->json($dataOut);
        }
        $post_id = $request->input('post_id');
        $post = Post::findOrFail($post_id);
        if($post->user_id != Auth::user()->id){
            $dataOut['msg'] = Config::get('constants.errors.not_authorized');
            return response()->json($dataOut);
        }
        $post_media = Media::where(array('resource_id' => $post_id , 'resource_type' => 'post'))->get();
        $edit_post_modal = view('posts.edit_post')
            ->with('user', Auth::user())
            ->with('post', $post)
            ->with('medias', $post_media)
            ->render();
        $dataOut['success'] = true;
        $dataOut['data']['modal'] = $edit_post_modal;
        return response()->json($dataOut);
    }

    public function show($id)
    {
        if(request()->ajax()){
            $dataOut = array('success' => false, 'msg' => '', 'data' => []);
            $posts = Post::where('id', $id)->get();
            $post_modal =  view('posts.open_post_modal')
                ->with('lastPage', 99999)
                ->with('posts', $posts)
                ->render();
            $dataOut['success'] = true;
            $dataOut['data']['modal'] = $post_modal;
            return response()->json($dataOut);
        }else{
            $posts = Post::where('id', $id)->get();
            return view('posts.view_post')
                ->with('lastPage', 99999)
                ->with('posts', $posts);
        }

    }
    public function openNotification(Request $request){
        $messages = ['status' => false, 'data' => ''];
        $iUserId = $request->input('user_id');
        $iNotificationId = $request->input('notification_id');
        if($iUserId == Auth::user()->id){
            $oNotification = Notification::where('id',$iNotificationId)->where('user_id',Auth::user()->id)->get()->first();
            if($oNotification){
                if($oNotification->is_seen == false){
                    $oNotification->update(['is_seen' => true]);
                }
                $messages = ['status' => true,'data' => $oNotification->resource_id ];
            }
        }
        return \Response::json($messages);
    }
    public function updatePost(){

    }
}
