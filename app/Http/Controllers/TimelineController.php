<?php

namespace App\Http\Controllers;

use App\Follows;
use App\Jobs\ImageProcess;
use App\Post;
use App\Tag;
use App\User;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class TimelineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $posts = Post::orderBy('created_at', 'desc')->paginate(15);
//        $posts = Post::orderBy('created_at', 'desc')->whereIn('user_id', function ($query) {
//            $query->select('followed_user_id')->from('follows')->where([['follower_user_id', '=', Auth::user()->id]]);
//        })->orWhere('user_id',Auth::user()->id)->paginate(15);
        return view(request()->ajax()? 'timeline.post': 'timeline.index')->with('posts', $posts);
    }
    public function friendSuggestions(Request $request){
        $iUserId = $request->input('user_id');
        $messages = ['status' => false, 'data' => []];
        if ($iUserId == Auth::user()->id) {
            $friendSuggestions = User::where([['id', '!=', Auth::user()->id]])->whereNotIn('id', function ($query) {
                $query->select('followed_user_id')->from('follows')->where([['follower_user_id', '=', Auth::user()->id]]);
            })->limit(3)->inRandomOrder()->get()->toArray();

            if(!empty($friendSuggestions) && count($friendSuggestions) > 0){
                foreach($friendSuggestions as &$user){
                    $mutual_count = $this->getMutualFriend($user['id']);
                    $user['mutual_count'] =  $mutual_count;
                }
                $messages['status'] = true;
                $messages['data'] = $friendSuggestions;
            }
        }
        return \Response::json($messages);

    }
    public function getMutualFriend($requestUserId){
        $aData = DB::select(DB::raw("SELECT u.name
                        FROM follows f1 INNER JOIN follows f2
                          ON f1.followed_user_id = f2.followed_user_id
                          left join users u on f1.followed_user_id = u.id
                        WHERE f1.follower_user_id = ?
                        AND f2.follower_user_id = ?  "), [Auth::user()->id, $requestUserId]);

        if(isset($aData[0]->name)){
            return count($aData);
        }
        return null;

    }
    public function discoverFriends(Request $request)
    {

        $search_value = $request->input('search');
        $order_by_method = $request->input('orderbymethod');
        $orderByCases = ['RA','MP','NoP','CO','FL'];
        if(!empty($search_value)){
            $aDiscoverFriends = User::where([['id','!=',Auth::user()->id]])->whereNotIn('id',function ($query){
                $query->select('followed_user_id')->from('follows')->where([['follower_user_id','=',Auth::user()->id]]);
            })->whereNotIn('id',function ($sQuery){
                $sQuery->select('requested_user_id')->from('follow_request')->where([['user_id','=',Auth::user()->id]]);
            })->Where('name', 'like', "%{$search_value}%")->orWhere('user_name', 'like', "%{$search_value}%")->withCount('posts','follows','projects')->with('country','city');
        }else{
            $aDiscoverFriends = User::where([['id','!=',Auth::user()->id]])->whereNotIn('id',function ($query){
                $query->select('followed_user_id')->from('follows')->where([['follower_user_id','=',Auth::user()->id]]);
            })->whereNotIn('id',function ($sQuery){
                $sQuery->select('requested_user_id')->from('follow_request')->where([['user_id','=',Auth::user()->id]]);
            })->withCount('posts','follows','projects')->with('country','city');
        }

        if(!empty($order_by_method) && $order_by_method !== 0 && in_array($order_by_method,$orderByCases)){

            if($order_by_method == 'CO'){
                $aDiscoverFriends->where('profile_type','company');
            }else if($order_by_method == 'FL'){
                $aDiscoverFriends->where('profile_type','freelancer');
            }else {
                $method = self::getOrderBy($order_by_method);
                if (!empty($method) && is_array($method)) {

                    $order_by = $method['order_by'];
                    $direction = $method['direction'];
                    $aDiscoverFriends = $aDiscoverFriends->orderBy($order_by, $direction);


                }
            }
        }
        $aDiscoverFriends = $aDiscoverFriends->paginate(12);

        $order_by_options = [
            [
                'value' => 0,
                'text' => 'Select Order By Method'
            ],
            [
                'value' => 'RA',
                'text' => 'Recently Updated'
            ],
            [
                'value' => 'MP',
                'text' => 'Most popular'
            ],
            [
                'value' => 'NoP',
                'text' => 'Number of projects'
            ],
            [
                'value' => 'CO',
                'text' => 'Companies only'
            ],
            [
                'value' => 'FL',
                'text' => 'Freelancers'
            ],
        ];

        return view('timeline.discover_friend')->with(['discover'=>$aDiscoverFriends,'order_by_options'=>$order_by_options]);
    }
    public static function getOrderBy($method){

        switch ($method){
            case 'MP' :
                $method = [
                  'order_by' => 'profile_count',
                  'direction' => 'DESC'
                ];
                break;
            case 'NoP' :
                $method = [
                    'order_by' => 'projects_count',
                    'direction' => 'DESC'
                ];
                break;
            case 'RA' :
                $method = [
                    'order_by' => 'id',
                    'direction' => 'DESC'
                ];
                break;

        }
        return $method;
    }
    public function searchBar(Request $request){
        $search_value = $request->input('search');

        $aDiscoverFriends = [];
        if(!empty($search_value)){
            $aDiscoverFriends = User::where([['id','!=',Auth::user()->id]])->whereNotIn('id',function ($query){
                $query->select('followed_user_id')->from('follows')->where([['follower_user_id','=',Auth::user()->id]]);
            })->Where('name', 'like', "%{$search_value}%")->orWhere('user_name', 'like', "%{$search_value}%")->withCount('posts','follows','projects')->limit(4)->get()->toArray();
            $aData = DB::select(DB::raw("SELECT distinct wp.* FROM tags t left join website_project_tags wpt on wpt.tag_id = t.id left join website_projects wp on wp.id = wpt.project_id where t.text like ? "), ['%'.$search_value.'%']);
            $aData = array_map(function ($value) {
                return (array)$value;
            }, $aData);

            $aDiscoverFriends = array_merge($aDiscoverFriends,$aData);
        }
        return \Response::json($aDiscoverFriends);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
       //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

}
