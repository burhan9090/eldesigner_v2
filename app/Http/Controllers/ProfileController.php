<?php

namespace App\Http\Controllers;

use App\FollowRequest;
use App\Follows;
use App\Message;
use App\Notification;
use App\User;
use App\Post;
use App\WebsiteArticles;
use App\WebsiteHeader;
use App\WebsiteProject;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\DB;
class ProfileController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function me(){
        $recentArticle = DB::select(DB::raw("select distinct website_articles.* from website_articles join website_article_time_line on website_article_time_line.article_id = website_articles.id order by rand();"));
        $aProjects = DB::select(DB::raw("select image, project_id from website_project_images join website_projects on website_project_images.project_id=website_projects.id where website_projects.user_id=? order by rand() limit 9"),[Auth::user()->id]);

        $followingCompany = Follows::where('follower_user_id','=',Auth::user()->id)->with('user')->whereHas('user',function ($query){
            $query->where('profile_type','=','company');
        })->get();
        $aProjects = array_map(function ($value) {
            return (array)$value;
        }, $aProjects);
	    $posts = Post::where('user_id',Auth::user()->id)->orderBy('created_at', 'desc')->paginate(15);
	    $followers = Follows::with('userfollower')->where('followed_user_id','=',Auth::user()->id)->limit(14)->offset(0)->get();
	    $followers_count = Follows::where('followed_user_id','=',Auth::user()->id)->count();
	    return view('profile.index',['posts' => $posts,'user_info'=>Auth::user(),'followers' => $followers,'followers_count' =>$followers_count,'recent_designs' => $aProjects,'recent_art' =>$recentArticle,'following_company' => $followingCompany ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index($user_name)
    {

        if(Auth::user()->user_name == trim($user_name)){
            return $this->me();
        }else {

            $RequestedUser = User::where([['user_name','=',trim($user_name)]])->with('country','city')->get()->first();
            $this->addViewCountProfile($RequestedUser);
            if(!empty($RequestedUser)){

                $aData = $this->getIfFriends($RequestedUser->id);
                // checking if the friend is following from one way only
                $friendOneWay = Follows::where('follower_user_id',Auth::user()->id)->where('followed_user_id',$RequestedUser->id)->get()->first();
                if($friendOneWay){
                    $aData = $friendOneWay;
                }
                //if not empty it means they are friends to each other
                if(!empty($aData)){
                    $recentArticle = DB::select(DB::raw("select distinct website_articles.* from website_articles join website_article_time_line on website_article_time_line.article_id = website_articles.id order by rand();"));
                    $aProjects = DB::select(DB::raw("select image, project_id from website_project_images join website_projects on website_project_images.project_id=website_projects.id where website_projects.user_id=? order by rand() limit 9"),[$RequestedUser->id]);

                    $followingCompany = Follows::where('follower_user_id','=',$RequestedUser->id)->with('user')->whereHas('user',function ($query){
                        $query->where('profile_type','=','company');
                    })->get();
                    $aProjects = array_map(function ($value) {
                        return (array)$value;
                    }, $aProjects);
                    $followers = Follows::with('userfollower')->where('followed_user_id','=',$RequestedUser->id)->limit(14)->offset(0)->get();
                    $followers_count = Follows::where('followed_user_id','=',$RequestedUser->id)->count();
                    $posts = Post::orderBy('created_at', 'desc')->where([['user_id','=',$RequestedUser->id]])->paginate(15);
                    return view('profile.index',['request_friend_waiting'=> 0,'posts' => $posts,'user_info'=>$RequestedUser,'oUserInfo'=>$RequestedUser,'followers' => $followers,'followers_count' =>$followers_count,'recent_designs' => $aProjects,'recent_art' =>$recentArticle,'following_company' => $followingCompany]);
                }else{
                    $RequestFollowRequest = FollowRequest::where('user_id' , Auth::user()->id)->where('requested_user_id' , $RequestedUser->id)->get()->first();

                   if(empty($RequestFollowRequest))
                   {
                       $RequestFollowRequest = FollowRequest::where('user_id' , $RequestedUser->id)->where('requested_user_id' , Auth::user()->id)->get()->first();
                       if(!empty($RequestFollowRequest)){
                           $RequestFollowRequest = 2;
                       }
                   }
                    if($RequestFollowRequest !== 2){
                        $RequestFollowRequest = !empty($RequestFollowRequest) ? 1 : 0;
                    }
                    $posts = Post::orderBy('created_at', 'desc')->where([['user_id','=',$RequestedUser->id]])->paginate(15);
                    $aProjects = DB::select(DB::raw("select image, project_id from website_project_images join website_projects on website_project_images.project_id=website_projects.id where website_projects.user_id=? order by rand() limit 9"),[$RequestedUser->id]);
                    $aProjects = array_map(function ($value) {
                        return (array)$value;
                    }, $aProjects);
                    return view('profile.not_friend_profile',['oUserInfo' => $RequestedUser,'request_friend_waiting' => $RequestFollowRequest,'posts' => $posts,'recent_designs' => $aProjects]);
                }
            }else{
                return view('profile.not_found');
            }
        }
    }
    public function getIfFriends($RequestedUser){
        $aData = DB::select(DB::raw("SELECT f1.follower_user_id,f2.follower_user_id
                        FROM follows f1 LEFT JOIN follows f2
                          ON f1.followed_user_id = f2.follower_user_id and f1.follower_user_id = f2.followed_user_id
                        WHERE f1.follower_user_id = ?
                        AND f2.follower_user_id = ?  limit 2 "), [Auth::user()->id, $RequestedUser]);

        return $aData;

    }
    public function notFriendAbout($user_name){
        $RequestedUser = User::with('history')->where('user_name','=',$user_name)->get()->first();
        $RequestFollowRequest = FollowRequest::where('user_id' , Auth::user()->id)->where('requested_user_id' , $RequestedUser->id)->get()->first();

        if(empty($RequestFollowRequest))
        {
            $RequestFollowRequest = FollowRequest::where('user_id' , $RequestedUser->id)->where('requested_user_id' , Auth::user()->id)->get()->first();
            if(!empty($RequestFollowRequest)){
                $RequestFollowRequest = 2;
            }
        }
        if($RequestFollowRequest !== 2){
            $RequestFollowRequest = !empty($RequestFollowRequest) ? 1 : 0;
        }



            return view( 'profile.about' )
                ->with( ['user'=> $RequestedUser ,'oUserInfo' =>$RequestedUser,'request_friend_waiting' => $RequestFollowRequest] );

    }
    public function FriendAbout($user_name){
        $RequestedUser = User::with('history')->where('user_name','=',$user_name)->get()->first();
        $RequestFollowRequest = FollowRequest::where('user_id' , Auth::user()->id)->where('requested_user_id' , $RequestedUser->id)->get()->first();

        if(empty($RequestFollowRequest))
        {
            $RequestFollowRequest = FollowRequest::where('user_id' , $RequestedUser->id)->where('requested_user_id' , Auth::user()->id)->get()->first();
            if(!empty($RequestFollowRequest)){
                $RequestFollowRequest = 2;
            }
        }
        if($RequestFollowRequest !== 2){
            $RequestFollowRequest = !empty($RequestFollowRequest) ? 1 : 0;
        }



        return view( 'profile.friend_about' )
            ->with( ['user'=> $RequestedUser ,'user_info' =>$RequestedUser,'request_friend_waiting' => $RequestFollowRequest] );

    }
    public function notFriendProjects($user_name){

        $RequestedUser = User::with('history')->where('user_name','=',$user_name)->get()->first();
        $RequestFollowRequest = FollowRequest::where('user_id' , Auth::user()->id)->where('requested_user_id' , $RequestedUser->id)->get()->first();
        if(empty($RequestFollowRequest))
        {
            $RequestFollowRequest = FollowRequest::where('user_id' , $RequestedUser->id)->where('requested_user_id' , Auth::user()->id)->get()->first();
            if(!empty($RequestFollowRequest)){
                $RequestFollowRequest = 2;
            }
        }
        if($RequestFollowRequest !== 2){
            $RequestFollowRequest = !empty($RequestFollowRequest) ? 1 : 0;
        }

        return view( 'profile.projects' )
            ->with( 'title', 'Manage Your Projects' )
            ->with( 'oUserInfo', $RequestedUser )
            ->with( 'request_friend_waiting', $RequestFollowRequest )
            ->with( 'header', WebsiteHeader::getRow( WebsiteHeader::TYPE_PROJECTS ) )
            ->with( 'projects', WebsiteProject
                ::where( 'user_id', $RequestedUser->id )
                ->where( 'is_demo', 0 )
                ->with( [ 'image', 'timeline' ] )
                ->orderByDesc( 'id' )
                ->paginate( 8 ) );
    }
    public function FriendProjects($user_name){

        $RequestedUser = User::with('history')->where('user_name','=',$user_name)->get()->first();
        $RequestFollowRequest = FollowRequest::where('user_id' , Auth::user()->id)->where('requested_user_id' , $RequestedUser->id)->get()->first();
        if(empty($RequestFollowRequest))
        {
            $RequestFollowRequest = FollowRequest::where('user_id' , $RequestedUser->id)->where('requested_user_id' , Auth::user()->id)->get()->first();
            if(!empty($RequestFollowRequest)){
                $RequestFollowRequest = 2;
            }
        }
        if($RequestFollowRequest !== 2){
            $RequestFollowRequest = !empty($RequestFollowRequest) ? 1 : 0;
        }

        return view( 'profile.friend_projects' )
            ->with( 'title', 'Manage Your Projects' )
            ->with( 'user_info', $RequestedUser )
            ->with( 'request_friend_waiting', $RequestFollowRequest )
            ->with( 'header', WebsiteHeader::getRow( WebsiteHeader::TYPE_PROJECTS ) )
            ->with( 'projects', WebsiteProject
                ::where( 'user_id', $RequestedUser->id )
                ->with( [ 'image', 'timeline' ] )
                ->orderByDesc( 'id' )
                ->paginate( 8 ) );
    }

    public function notFriendArticles($user_name){
        $RequestedUser = User::with('history')->where('user_name','=',$user_name)->get()->first();
        $mine = false;
        $WebsiteArticles = WebsiteArticles
            ::where( function($query) use($mine,$RequestedUser){
                $query->where('user_id', (true? '=': '!='), $RequestedUser->id);

            })
            ->with('user')
            ->paginate(6);

        $RequestFollowRequest = FollowRequest::where('user_id' , Auth::user()->id)->where('requested_user_id' , $RequestedUser->id)->get()->first();
        if(empty($RequestFollowRequest))
        {
            $RequestFollowRequest = FollowRequest::where('user_id' , $RequestedUser->id)->where('requested_user_id' , Auth::user()->id)->get()->first();
            if(!empty($RequestFollowRequest)){
                $RequestFollowRequest = 2;
            }
        }
        if($RequestFollowRequest !== 2){
            $RequestFollowRequest = !empty($RequestFollowRequest) ? 1 : 0;
        }

        return view( 'profile.article' )
            ->with( 'title', 'Articles Library' )
            ->with( 'oUserInfo', $RequestedUser )
            ->with( 'request_friend_waiting', $RequestFollowRequest )
            ->with( 'header', WebsiteHeader::getRow(WebsiteHeader::TYPE_ARTICLES_LIBRARY) )
            ->with( 'mine', $mine )
            ->with( 'articles',$WebsiteArticles );
    }
    public function FriendArticles($user_name){
        $RequestedUser = User::with('history')->where('user_name','=',$user_name)->get()->first();
        $mine = false;
        $WebsiteArticles = WebsiteArticles
            ::where( function($query) use($mine,$RequestedUser){
                $query->where('user_id', (true? '=': '!='), $RequestedUser->id);

            })
            ->with('user')
            ->paginate(6);

        $RequestFollowRequest = FollowRequest::where('user_id' , Auth::user()->id)->where('requested_user_id' , $RequestedUser->id)->get()->first();
        if(empty($RequestFollowRequest))
        {
            $RequestFollowRequest = FollowRequest::where('user_id' , $RequestedUser->id)->where('requested_user_id' , Auth::user()->id)->get()->first();
            if(!empty($RequestFollowRequest)){
                $RequestFollowRequest = 2;
            }
        }
        if($RequestFollowRequest !== 2){
            $RequestFollowRequest = !empty($RequestFollowRequest) ? 1 : 0;
        }

        return view( 'profile.friend_article' )
            ->with( 'title', 'Articles Library' )
            ->with( 'user_info', $RequestedUser )
            ->with( 'request_friend_waiting', $RequestFollowRequest )
            ->with( 'header', WebsiteHeader::getRow(WebsiteHeader::TYPE_ARTICLES_LIBRARY) )
            ->with( 'mine', $mine )
            ->with( 'articles',$WebsiteArticles );
    }

    public function showAllMessages()
    {
        return view('profile.chat',['user_info' => Auth::user()]);
    }
    public function getAllFriends(Request $request){
        $aData = Follows::with('user')->where('follower_user_id','=',Auth::user()->id)->get()->toArray();
        return \Response::json($aData);
    }
    public function showAllNotification(){
        $aData = Notification::with('user')->where(['user_id'=>Auth::user()->id])->orderBy('created_at','desc')->paginate(20);
        return view('profile.notification',['notification' =>$aData ]);
    }
    public function showAllFriendRequests(){
        return view('profile.friend_requests');
    }
    public function addViewCountProfile($oUser){
        $profileCout = intval($oUser->profile_count) + 1;
        $oUser->profile_count = $profileCout;
        $oUser->save();
    }
    public function addViewCountProject($project){
        $profileCout = intval($project->project_count) + 1;
        $project->profile_count = $profileCout;
        $project->save();
    }

    public function followingFollowersPage(){
        $following = Follows::with('user')->where('follower_user_id','=',Auth::user()->id)->paginate(4);
        $following->withPath('following/');
        $followers = Follows::with('userfollower')->where('followed_user_id','=',Auth::user()->id)->paginate(4);
        $followers->withPath('followers/');
        return view('profile.follow-following',['following' =>$following ,'followers' => $followers]);
    }
    public function followingPage(){
        $following = Follows::with('user')->where('follower_user_id','=',Auth::user()->id)->paginate(4);
        $following->withPath('following/');
        $followers = Follows::with('userfollower')->where('followed_user_id','=',Auth::user()->id)->paginate(4);
        $followers->withPath('followers/');
        return view('profile.follow-following',['following' =>$following ,'followers' => $followers]);
    }
    public function followersPage(){
        $following = Follows::with('user')->where('follower_user_id','=',Auth::user()->id)->paginate(4);
        $following->withPath('following/');
        $followers = Follows::with('userfollower')->where('followed_user_id','=',Auth::user()->id)->paginate(4);
        $followers->withPath('followers/');
        return view('profile.follow-following',['following' =>$following ,'followers' => $followers]);
    }
}
