<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 3/7/19
 * Time: 4:47 PM
 */

namespace App\Http\Controllers;


use App\Classes\Payment;

class PaymentController extends Controller {

	public function payment(Payment $payment) {
		$payment->data();

		return view('payment.form')
			->with('payment', $payment);
	}

	public function paymentResponse(Payment $payment){
		$payment->process();

		if($payment->isSuccess()){

            $aAdditionalParams = $payment->getDataItem('additional_params');
            if(!empty($aAdditionalParams)){
                $aAdditionalParams = json_decode($aAdditionalParams,true);
                return redirect()->route($payment->getDataItem('success'),$aAdditionalParams);
            }
			return redirect()->route($payment->getDataItem('success'));
		}

		return redirect()->route($payment->getDataItem('failed'));
	}

	public function result(Payment $payment){
		return view('payment.responseResult')->with('payment', $payment);
	}

}