<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use App\UserPermission;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/timeline';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'user_name' => 'unique:users,user_name|required|string|max:255|regex:/(^([a-zA-Z]+)(\d+)?$)/u',
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
            'profile_type' => 'required|in:freelancer,company',
        ]);
        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $sCode ='';
        if (isset($data['code'])){
            $sCode = $data['code'];
        }
        $user = User::create([
            'user_name' => str_replace([' ' , '/' , '.' , '$' , '?' , '#' , '%' , '&' , '*' , '!'], '_' , $data['user_name']),
            'name' => $data['name'],
            'profile_type' => $data['profile_type'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'code' => $sCode,
        ]);
        $user_hash = hash_hmac(
            'sha256', // hash function
            $user->id, // user's id
            'uuA40syUWvUZybz1bVWmmsa9bO9hp8CzpX91BMnf' // secret key (keep safe!) intercom
        );
        $user->user_hash = $user_hash;
        // set defualt country and city [Jordan/Amman]
        $user->country_id = '219';
        $user->city_id = '7726';

        $user->save();
        $sGivePermissionTime = 0;
//         give permission for these promo code 3 months , 6 months , 1 year
        if(isset($data['code'])){
            switch ($data['code']){
                case 'EV-201902' :
                    $sGivePermissionTime = 3;
                    break;
                case 'DF-201901':
                case 'DF-2019-6':
                    $sGivePermissionTime = 6;
                    break;
                case 'EV-201901':
                case 'DF-2019-12':
                    $sGivePermissionTime = 12;
                    break;
                case 'UPWORK':
                    $sGivePermissionTime = 3;
                    break;
                case 'upwork':
                    $sGivePermissionTime = 3;
                    break;
            }
            if ($sGivePermissionTime > 0){
                $date_object = \Carbon\Carbon::now()->addMonths($sGivePermissionTime)->setTime(23, 50, 59);
                $date_timestamp = $date_object->timestamp;
                UserPermission::create([
                    'user_id' => $user->id,
                    'plans_id' => 5, // custom plan only to use it here
                    'expiration_timestamp' => $date_timestamp,
                    'is_deleted' => 0,
                ]);
            }
        }
        

        return $user;
    }
}
