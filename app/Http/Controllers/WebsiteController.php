<?php

namespace App\Http\Controllers;

use App\ContactUs;
use App\User;
use App\WebsiteArticles;
use App\WebsiteProject;
use Carbon\Carbon;
use Illuminate\Http\Request;

class WebsiteController extends Controller {

	public function index() {

		$user_name = optional( request()->route() )->parameter( 'user_name', '' );

		$user = User::where( 'user_name', $user_name ?: '' )
		            ->with( [ 'websiteWelcome', 'websiteContactUs', 'services', 'skills' ] )
		            ->firstOrFail();

		$theme = $user->theme_name;

		return view( 'website.' . $theme . '.index' )
			->with( 'theme', $theme )
			->with( 'welcome', website_welcome( $user->id ) )
			->with( 'information', website_information( $user->id ) )
			->with( 'services', website_service( $user->id ) )
			->with( 'categories', website_categories( $user->id ) )
			->with( 'articles', website_articles( $user->id, 10, true ) )
			->with( 'projects', website_projects( $user->id, 10, true ) )
			->with( 'contact_us', website_contact_us( $user->id ) )
			->with( 'skills', user_skills( $user->id ) )
			->with( 'team', website_team( $user->id ) )
			->with( 'nav', 'home' )
			->with( 'user', $user );
	}

	public function blog_list() {

		$user_name = optional( request()->route() )->parameter( 'user_name', '' );

		$user = User::where( 'user_name', $user_name ?: '' )
		            ->with( [ 'websiteWelcome', 'websiteContactUs' ] )
		            ->firstOrFail();


		$theme = $user->theme_name;

		return view( 'website.' . $theme . '.articles' )
			->with( 'theme', $theme )
			->with( 'nav', 'article' )
			->with( 'welcome', website_welcome( $user->id ) )
			->with( 'information', website_information( $user->id ) )
			->with( 'services', website_service( $user->id ) )
			->with( 'categories', website_categories( $user->id ) )
			->with( 'articles', website_articles( $user->id, 10 ) )
			->with( 'projects', website_projects( $user->id, 10, true ) )
			->with( 'contact_us', website_contact_us( $user->id ) )
			->with( 'user', $user );
	}

	public function blog_show( Request $request ) {
		$user_name = optional( request()->route() )->parameter( 'user_name', '' );

		$user = User::where( 'user_name', $user_name ?: '' )
		            ->with( [ 'websiteWelcome', 'websiteContactUs' ] )
		            ->firstOrFail();

		$theme = $user->theme_name;

		$article = WebsiteArticles
			::with( [ 'user', 'category' ] )
			->where( 'id', $request->id )
			->whereHas( 'timeline', function ( $q ) use ( $user ) {
				$q->where( 'user_id', $user->id );
			} )->firstOrFail();

		$welcome = website_welcome( $user->id );

		if ( $article->image ) {
			$header['logoHeader'] = $article->image;
		}
		if ( $article->title ) {
			$header['titleHeader'] = $article->title;
		}
		if ( $article->tags ) {
			$tagsArray = $article->tags->pluck('text')->toArray();
			$header['keywordHeader'] = implode( ', ', $tagsArray ) . ', ' . $welcome->keyword;
			$header['keywordHeader'] = trim( $header['keywordHeader'] );
			$header['keywordHeader'] = trim( $header['keywordHeader'], ',' );
			$header['keywordHeader'] = trim( $header['keywordHeader'] );
		}

		$header['descriptionHeader'] = str_limit( strip_tags( $article->body ) );

		return view( 'website.' . $theme . '.article_show' )
			->with( $header )
			->with( 'theme', $theme )
			->with( 'nav', 'article' )
			->with( 'welcome', $welcome )
			->with( 'information', website_information( $user->id ) )
			->with( 'services', website_service( $user->id ) )
			->with( 'categories', website_categories( $user->id ) )
			->with( 'articles', website_articles( $user->id, 10 ) )
			->with( 'projects', website_projects( $user->id, 10, true ) )
			->with( 'contact_us', website_contact_us( $user->id ) )
			->with( 'user', $user )
			->with( 'article', $article );
	}

	public function project_list() {

		$user_name = optional( request()->route() )->parameter( 'user_name', '' );

		$user = User::where( 'user_name', $user_name ?: '' )
		            ->with( [ 'websiteWelcome', 'websiteContactUs' ] )
		            ->firstOrFail();


		$theme = $user->theme_name;

		return view( 'website.' . $theme . '.projects' )
			->with( 'theme', $theme )
			->with( 'nav', 'project' )
			->with( 'welcome', website_welcome( $user->id ) )
			->with( 'information', website_information( $user->id ) )
			->with( 'services', website_service( $user->id ) )
			->with( 'categories', website_categories( $user->id ) )
			->with( 'articles', website_articles( $user->id, 10 ) )
			->with( 'projects', website_projects( $user->id, 10 ) )
			->with( 'contact_us', website_contact_us( $user->id ) )
			->with( 'user', $user );
	}

	public function project_show( Request $request ) {
		$user_name = optional( request()->route() )->parameter( 'user_name', '' );

		$user = User::where( 'user_name', $user_name ?: '' )
		            ->firstOrFail();


		$project = WebsiteProject
			::with( [ 'tags', 'images.tags', 'image' ] )
			->whereHas( 'timeline', function ( $q ) use ( $user ) {
				$q->where( 'user_id', $user->id );
			} )
			->where( 'id', $request->id )
			->firstOrFail();

		$welcome = website_welcome( $user->id );

		if ( $project->image ) {
			$header['logoHeader'] = $project->image->image;
		}
		if ( $project->title ) {
			$header['titleHeader'] = $project->title;
		}
		if ( $project->tags ) {
			$tagsArray = $project->tags->pluck('text')->toArray();
			$header['keywordHeader'] = implode( ', ', $tagsArray ) . ', ' . $welcome->keyword;
			$header['keywordHeader'] = trim( $header['keywordHeader'] );
			$header['keywordHeader'] = trim( $header['keywordHeader'], ',' );
			$header['keywordHeader'] = trim( $header['keywordHeader'] );
		}

		$header['descriptionHeader'] = str_limit( strip_tags( $project->description ) );

		$theme = $user->theme_name;


		return view( 'website.' . $theme . '.project_show' )
			->with( $header )
			->with( 'theme', $theme )
			->with( 'nav', 'project' )
			->with( 'welcome', $welcome )
			->with( 'titleHeader', $project->title )
			->with( 'information', website_information( $user->id ) )
			->with( 'services', website_service( $user->id ) )
			->with( 'categories', website_categories( $user->id ) )
			->with( 'articles', website_articles( $user->id, 10 ) )
			->with( 'projects', website_projects( $user->id, 10, true ) )
			->with( 'contact_us', website_contact_us( $user->id ) )
			->with( 'user', $user )
			->with( 'project', $project );
	}

	public function contact_us( Request $request ) {
		$user_name = optional( request()->route() )->parameter( 'user_name', '' );

		$user = User::where( 'user_name', $user_name ?: '' )
		            ->firstOrFail();
		$contactUs = ContactUs::insert( [
			'user_id' => $user->id,
			'title'   => $request->get( 'title' ),
			'message' => $request->get( 'message' ),
			'is_read' => false,
			'created_at' => Carbon::now()
		] );

		if ( $contactUs ) {
			$message = 'Message has sent!';
		} else {
			$message = 'Something Goes Wrong!';
		}


		if ( $request->ajax() ) {
			return [ 'status' => $contactUs, 'message' => $message ];
		}


		$request->session()->put( $contactUs ? 'save' : 'error', $message );

		return redirect()->back();

	}
}
