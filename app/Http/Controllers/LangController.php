<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\App;

class LangController extends Controller
{
    public function switchLang(Request $request)
    {
        $dataOut = array('success' => false, 'msg' => '', 'data' => []);
        if (!$request->ajax()) {
            $dataOut['msg'] = 'Invalid Request';
            outJson($dataOut);
        }
        $lang = $request->input('lang');
        if( Auth::user()){
            $dataOut['data']['is_user'] = true;
            App::setLocale($lang);
            //@TODO update default user lang
        }else{
            $dataOut['data']['is_user'] = false;
        }
        $dataOut['success'] = true;
        $dataOut['msg'] = 'Language set successfully';
        $dataOut['data']['lang'] = $lang;
        outJson($dataOut);
    }
}
