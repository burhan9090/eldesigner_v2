<?php

namespace App\Http\Controllers;

use App\Order;
use App\Service;
use App\UserPermission;
use Illuminate\Http\Request;

class ToolController extends Controller
{   public function __construct()
    {
        $this->middleware('auth');
    }

    public function quiz()
    {

        $hasPermission = UserPermission::checkPermission('design_request');
        $services = Service::all();
        return view('quiz.create')
            ->with('has_permission', $hasPermission)
            ->with('services', $services);

    }

    public function getOrders($user_id)
    {
        $orders = Order::where('user_id', $user_id)->get();
        return view('orders.list')
            ->with('orders', $orders);
    }
}
