<?php

namespace App\Http\Controllers;

use App\Events\NotificationEvent;
use Illuminate\Http\Request;
use App\Love;
use Auth;



class LoveController extends Controller
{
    public function addLove(Request $request){
        $dataOut = array('success' => false, 'msg' => '', 'data' => []);
        if (!$request->ajax()) {
            $dataOut['msg'] = 'Invalid Request';
            return response()->json($dataOut);
        }
        $payload = $request->all();
        $love = new Love();

        $love->resource_id = $payload['resource_id'];
        $love->resource_type = $payload['resource_type'];
        $love->user_id = Auth::user()->id;

        $love->save();
        broadcast(new NotificationEvent(Auth::user(),$love,'love'))->toOthers();
        $dataOut['success'] = true;
        $dataOut['msg'] = "Loved";
        return response()->json($dataOut);
    }
    public function removeLove(Request $request){
        $dataOut = array('success' => false, 'msg' => '', 'data' => []);
        if (!$request->ajax()) {
            $dataOut['msg'] = 'Invalid Request';
            return response()->json($dataOut);
        }
        $payload = $request->all();

        Love::where('resource_id' , $payload['resource_id'])
                    ->where('resource_type' , $payload['resource_type'])
                    ->where('user_id' , Auth::user()->id)
                    ->delete();

        $dataOut['success'] = true;
        $dataOut['msg'] = "Removed Love";
        return response()->json($dataOut);
    }
}
