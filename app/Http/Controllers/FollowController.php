<?php

namespace App\Http\Controllers;

use App\Events\FollowRequestEvent;
use App\Events\NotificationEvent;
use App\FollowRequest;
use App\Follows;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class FollowController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    //function to get the how trying to follow you
    public function getFollowRequests(Request $request){
        $iUserId = $request->input('user_id');
        $messages = ['status' => false, 'data' => []];
        if ($iUserId == Auth::user()->id) {
            $aData = FollowRequest::with('request_user')->where([['requested_user_id' ,'=',Auth::user()->id],['approved','=',false]])->get()->toArray();
            if(!empty($aData)){
                foreach ($aData as $key => $requestFollow){
                    $sMutulaFriend = Follows::getMutualFriend($requestFollow['user_id']);
                    if(!empty($sMutulaFriend) && $sMutulaFriend != null){
                        $aData[$key]['mutual_friend'] = $sMutulaFriend;
                    }
                }
            }
            $messages = ['status' => true, 'data' =>$aData];
        }
        return \Response::json($messages);
    }
    public function getMutualFriend($requestUserId){
            $aData = DB::select(DB::raw("SELECT u.name
                        FROM follows f1 INNER JOIN follows f2
                          ON f1.followed_user_id = f2.followed_user_id
                          left join users u on f1.followed_user_id = u.id
                        WHERE f1.follower_user_id = ?
                        AND f2.follower_user_id = ?  limit 1 "), [Auth::user()->id, $requestUserId]);

            if(isset($aData[0]->name)){
                return $aData[0]->name;
            }
            return null;

    }
    public function acceptFollower(Request $request){
        $iUserId = $request->input('user_id');
        $iFollowerId = $request->input('follower_id');
        $messages = ['status' => false];
        $confirmFollower = FollowRequest::where([['user_id','=',$iFollowerId],['requested_user_id','=',Auth::user()->id],['approved','=',false]])->get()->toArray();
        if ($iUserId == Auth::user()->id && !empty($confirmFollower)) {

            $insertFollower = $this->checkIfNotFollower($iFollowerId);
            if(!empty($insertFollower)){
                $batchInserFollowers = Follows::insert($insertFollower);
                if(!empty($batchInserFollowers)){
                    $result = FollowRequest::where([['user_id','=',$iFollowerId],['requested_user_id','=',Auth::user()->id]])->get()->first();

                    if($result->delete()){
                        broadcast(new NotificationEvent(Auth::user(),$iFollowerId,'follow'))->toOthers();
                        $messages = ['status' => true];
                    }

                }
            }


        }
        return \Response::json($messages);
    }
    public function checkIfNotFollower($iFollowerId){
        $insertFollower = [];
        $first = Follows::select('id')->where('follower_user_id',Auth::user()->id)->where('followed_user_id',$iFollowerId)->get()->first();
        if(empty($first)){
            $insertFollower[] = [
                'follower_user_id' => Auth::user()->id,
                'followed_user_id' => $iFollowerId
            ];
        }
        $second = Follows::select('id')->where('follower_user_id',$iFollowerId)->where('followed_user_id',Auth::user()->id)->get()->first();
        if(empty($second)){
            $insertFollower[] = [
                'follower_user_id' => $iFollowerId,
                'followed_user_id' => Auth::user()->id
            ];
        }
        return $insertFollower;

    }
    public function friendRequestFollow(Request $request){
        $aResponse = ['status' => false];
        $iFollowerId = $request->input('request_id');
        $followRequestCheck = FollowRequest::where([['user_id','=',Auth::user()->id],['requested_user_id','=',$iFollowerId]])->get()->first();
        if(empty($followRequestCheck)){
            $oFollow = FollowRequest::create([
                'user_id' => Auth::user()->id,
                'requested_user_id' => $iFollowerId,
            ]);
            if (!empty($oFollow)){
                event(new FollowRequestEvent($iFollowerId));
                $aResponse = ['status' => true];
            }
        }
        return \Response::json($aResponse);
    }
    public function publicFriendRequestFollow(Request $request){
        $aResponse = ['status' => false];
        $iFollowerId = $request->input('request_id');
        $iUserId = $request->input('user_id');

        if ($iUserId == Auth::user()->id){
            $friendOneWay = Follows::where('follower_user_id',Auth::user()->id)->where('followed_user_id',$iFollowerId)->get()->first();
            if(!$friendOneWay) {
                $insertFollower = [
                    [
                        'follower_user_id' => Auth::user()->id,
                        'followed_user_id' => $iFollowerId
                    ],
                ];
                $batchInserFollowers = Follows::insert($insertFollower);
                if (!empty($batchInserFollowers)) {
                    broadcast(new NotificationEvent(Auth::user(), $iFollowerId, 'follow'))->toOthers();
                    $aResponse = ['status' => true];
                }
            }
        }
        return \Response::json($aResponse);
    }
    public function cancelFriendRequestFollow(Request $request){
        $aResponse = ['status' => false];
        $iFollowerId = $request->input('request_id');
        $followRequestCheck = FollowRequest::where([['user_id','=',Auth::user()->id],['requested_user_id','=',$iFollowerId]])->get()->first();
        if(!empty($followRequestCheck)){
            $followRequestCheck->delete();
            $aResponse = ['status' => true];
        }
        return \Response::json($aResponse);
    }
    public function deleteFriend(Request $request){
        $aResponse = ['status' => false];
        $iFollowerId = $request->input('request_id');
        $userId= Auth::user()->id;
        $isFriends = Follows::where('follower_user_id',$userId)->where('followed_user_id',$iFollowerId)->get()->first();
        if ($isFriends){
            $aResponse['status'] = true;
            $isFriends->delete();
            $isBothFriends = Follows::where('follower_user_id',$iFollowerId)->where('followed_user_id',$userId)->get()->first();
            if ($isBothFriends){
                $isBothFriends->delete();
            }
        }
        return \Response::json($aResponse);

    }

}
