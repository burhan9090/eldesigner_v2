<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 2019-02-08
 * Time: 16:45
 */
namespace App\Http\Controllers;

use App\User;
use App\WebsiteArticles;
use App\WebsiteHeader;
use App\WebsiteProject;

class PublicProfileController extends Controller
{
    public function index($username){


        $RequestedUser = User::Where([['user_name','=',trim($username)]])->with('country','city')->get()->first();

        if(!empty($RequestedUser) && $RequestedUser->profile_setting == 'public'){
            return view('home.profile.public_profile',['oUserInfo' => $RequestedUser]);
        }else{

            return redirect('/login-form');
        }

    }
    public function about($username){
        $RequestedUser = User::with('history')->where('user_name','=',$username)->get()->first();
        if(!empty($RequestedUser) && $RequestedUser->profile_setting == 'public'){
            return view( 'home.profile.public_about' )
                ->with( ['user'=> $RequestedUser ,'oUserInfo' =>$RequestedUser] );
        }else{
            return redirect('/login-form');
        }

    }
    public function project($username){

        $RequestedUser = User::with('history')->where('user_name','=',$username)->get()->first();
        if(!empty($RequestedUser) && $RequestedUser->profile_setting == 'public') {
            return view('home.profile.public_project')
                ->with('title', 'Manage Your Projects')
                ->with('oUserInfo', $RequestedUser)
                ->with('header', WebsiteHeader::getRow(WebsiteHeader::TYPE_PROJECTS))
                ->with('projects', WebsiteProject
                    ::where('user_id', $RequestedUser->id)
	                ->where( 'is_demo', 0 )
	                ->with(['image', 'timeline'])
                    ->orderByDesc('id')
                    ->paginate(8));
        }else{
            return redirect('/login-form');
        }
    }
    public function articles($username){
        $RequestedUser = User::with('history')->where('user_name','=',$username)->get()->first();
        if(!empty($RequestedUser) && $RequestedUser->profile_setting == 'public') {
            $mine = false;
            $WebsiteArticles = WebsiteArticles
                ::where(function ($query) use ($mine, $RequestedUser) {
                    $query->where('user_id', (true ? '=' : '!='), $RequestedUser->id);

                })
                ->with('user')
                ->paginate(6);
            return view('home.profile.public_articles')
                ->with('title', 'Articles Library')
                ->with('user_info', $RequestedUser)
                ->with('oUserInfo', $RequestedUser)
                ->with('header', WebsiteHeader::getRow(WebsiteHeader::TYPE_ARTICLES_LIBRARY))
                ->with('mine', $mine)
                ->with('articles', $WebsiteArticles);
        }else{
            return redirect('/login-form');
        }
    }
}