<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 2019-01-13
 * Time: 23:35
 */
namespace App\Http\Controllers;
use App\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function getNotification(Request $request){
        $iUserId = $request->input('user_id');
        $messages = ['status' => false, 'data' => []];
        if ($iUserId == Auth::user()->id) {
            $aData = Notification::with('user')->where(['user_id'=>Auth::user()->id])->orderBy('created_at','desc')->limit(10)->get()->toArray();
            $messages = ['status' => false, 'data' => $aData];
        }
        return \Response::json($messages);
    }
    public function markAsRead(Request $request){
        $iUserId = $request->input('user_id');
        $iNotificationId = $request->input('user_id');
        if ($iUserId == Auth::user()->id) {
            Notification::where('id',$iNotificationId)->where('user_id',Auth::user()->id)->update(['is_seen'=>true]);
        }
    }
    public function MarkAllAsRead(Request $request){
        $iUserId = $request->input('user_id');
        $messages = ['status' => false];
        if ($iUserId == Auth::user()->id) {
            Notification::where('user_id',Auth::user()->id)->update(['is_seen'=>true]);
            $messages = ['status' => true];
        }
        return \Response::json($messages);
    }

}