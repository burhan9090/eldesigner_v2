<?php

namespace App\Http\Controllers\WebsiteDashboard;

use App\Service;
use App\TeamMember;
use App\UserSkill;
use App\WebsiteHeader;
use App\WebsiteService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ServicesController extends Controller {

	public function index() {

		return view( 'profile.website.services' )
			->with( 'title', 'Services Tab' )
			->with( 'header', WebsiteHeader::getRow( WebsiteHeader::TYPE_SERVICES_TAB ) )
			->with( 'service', WebsiteService::firstOrNew( [ 'user_id' => auth()->id() ] ) );
	}

	public function save( Request $request ) {

		$valid = \Validator::make( $request->all(), [
			'service_text' => 'required|array'
		] );

		if ( $valid->fails() ) {
			return [ 'status' => false, 'message' => $valid->errors() ];
		}


		$row = WebsiteService::firstOrNew( [ 'user_id' => auth()->id() ] );


		$service = $row->icon ?: "[]";

		try {
			$service = json_decode( $service, 1 );
			if ( ! $service ) {
				$service = [];
			}
		} catch ( \Exception $e ) {
			$service = [];
		}

		$serviceDemoIcons = [
			"img/service1.svg",
			"img/service2.svg",
			"img/service3.svg",
			"img/service4.svg"
		];

		if ( $images = request()->file( 'icon' ) ) {
			foreach ( $images as $key => $image ) {
				/** @var $image \Illuminate\Http\UploadedFile */

				try {
					if ( $service[ $key ] ) {
						if(!in_array($service[ $key ], $serviceDemoIcons)) {
							@unlink( $service[ $key ] );
						}
					}
				} catch ( \Exception $e ) {
				}

				$service[ $key ] = $image->move( 'uploads/user_service/' . auth()->id(), $image->getBasename() . '.' . $image->getClientOriginalExtension() ) . '';
			}
		}


		$row->show_service = boolval( $request->get( 'show_service', 0 ) );
		$row->service_text = json_encode( $request->get( 'service_text' ) );
		$row->icon         = json_encode( $service );

		return [ 'status' => $row->save(), 'message' => [] ];
	}

	public function userServices() {

		return view( 'profile.website.user_services' )
			->with( 'title', 'Services Type' )
			->with( 'header', WebsiteHeader::getRow( WebsiteHeader::TYPE_USER_SERVICES ) )
			->with( 'services', Service::all() )
			->with( 'userServices', auth()->user()->services->pluck( 'id' )->toArray() );
	}

	public function userServicesSave( Request $request ) {

		$valid = \Validator::make( $request->all(), [
			'user_service' => 'required|array'
		] );

		if ( $valid->fails() ) {
			return [ 'status' => false, 'message' => $valid->errors() ];
		}

		auth()->user()->services()->sync( $request->get( 'user_service', [] ) );


		return [ 'status' => true, 'message' => [] ];
	}

	public function userSkills() {

		return view( 'profile.website.user_skills' )
			->with( 'title', 'Freelancer Services Tab' )
			->with( 'header', WebsiteHeader::getRow( WebsiteHeader::TYPE_USER_SKILLS ) )
			->with( 'skills', auth()->user()->skills );
	}

	public function userSkillsSave( Request $request ) {

		$valid = \Validator::make( $request->all(), [
			'skill' => 'required|array'
		] );

		if ( $valid->fails() ) {
			return [ 'status' => false, 'message' => $valid->errors() ];
		}

		$skills = $request->get( 'skill', [ 'title' => [], 'weight' => [] ] );

		foreach ( $skills['weight'] as $key => $skill ) {
			$skills['weight'][ $key ] = intval( $skill );
		}

		UserSkill::updateOrCreate( [ 'user_id' => auth()->id() ], [ 'skills' => json_encode( $skills ) ] );


		return [ 'status' => true, 'message' => [] ];
	}

	public function teamMember() {

		return view( 'profile.website.team_member' )
			->with( 'title', 'Services Tab' )
			->with( 'header', WebsiteHeader::getRow( WebsiteHeader::TYPE_TEAM_MEMBERS ) )
			->with( 'members', TeamMember::firstOrNew( [ 'user_id' => auth()->id() ] ) );
	}

	public function teamMemberSave( Request $request ) {

		$valid = \Validator::make( $request->all(), [
			'member' => 'required|array',
			'image' => 'required|array',
			'image.*'  => 'file|image'
		] );

		if ( $valid->fails() ) {
			return [ 'status' => false, 'message' => $valid->errors() ];
		}

		$row = TeamMember::firstOrNew( [ 'user_id' => auth()->id() ] );

		$members = $row->member ?: "[]";

		try {
			$members = json_decode( $members, 1 );
			if ( ! $members ) {
				$members = [];
			}
		} catch ( \Exception $e ) {
			$members = [];
		}

		$demoImgs = [
			"img/sm-team1.png",
			"img/sm-team2.png",
			"img/sm-team3.png",
			"img/sm-team4.png"
		];

		if ( $images = $request->file( 'image' ) ) {
			foreach ( $images as $key => $image ) {
				/** @var $image \Illuminate\Http\UploadedFile */

				try {
					if ( $members[ $key ] && !in_array($members[ $key ], $demoImgs) ) {
						@unlink( $members[ $key ] );
					}
				} catch ( \Exception $e ) {
				}

				$members[ $key ]['image'] = $image->move( 'uploads/team_members/' . auth()->id(), $image->getBasename() . '.' . $image->getClientOriginalExtension() ) . '';
			}
		}


		foreach ($request->get( 'member', []) as $key => $member){
			$members[ $key ]['title'] = $member['title'] ?? '';
			$members[ $key ]['description'] = $member['description'] ?? '';
		}

		$row->member = json_encode( $members );

		return [ 'status' => $row->save(), 'message' => [] ];
	}
}
