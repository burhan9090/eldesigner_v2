<?php

namespace App\Http\Controllers\WebsiteDashboard;

use App\WebsiteHeader;
use App\WebsiteTheme;
use App\Http\Controllers\Controller;

class StyleController extends Controller {

	public function index() {

		$theme = WebsiteTheme::where('folder', auth()->user()->theme_name)->first();
		$style = [];
		if($theme){
			$style = $theme->getJSON();

		}

		return view( 'profile.website.style' )
			->with( 'title', 'Theme Style' )
			->with( 'header', WebsiteHeader::getRow( WebsiteHeader::TYPE_STYLE) )
			->with( 'user', json_decode(auth()->user()->theme_setting, 1)?: [])
			->with( 'style', $style );
	}

	public function save() {

		$data = request()->all();

		$user = auth()->user();
		$user->theme_setting = json_encode($data);
		$user->save();

		request()->session()->put( 'save', "Theme Style has saved" );

		return redirect()->route('style.index');
	}
}
