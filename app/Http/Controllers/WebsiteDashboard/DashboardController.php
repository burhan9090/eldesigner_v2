<?php

namespace App\Http\Controllers\WebsiteDashboard;

use App\ContactUs;
use App\UserPermission;
use App\WebsiteHeader;
use App\Http\Controllers\Controller;

class DashboardController extends Controller {

	public function index() {
//	       removed by request of abdullah
//        $hasPermission = UserPermission::checkPermission('website_generator');
        $hasPermission = true;
		return view( 'profile.website.dashboard' )
			->with( 'title', 'Welcoming Screen' )
             ->with('has_permission', $hasPermission);
	}

	public function statistics(){

		$monthsHit = \DB::select("select count(*) as hits, DATE_FORMAT(created_at, '%Y %b') as date_hit from website_tracking where user_id=? group by date_hit order by date_hit desc limit 30", [auth()->id()]);
		$countriesHit = \DB::select("select count(website_tracking.id) as hits, countries.text from website_tracking left join countries on countries.code = website_tracking.country_code where user_id=? group by countries.code order by hits desc limit 7", [auth()->id()]);
		$usersHit = \DB::select("select count(*) as hits, IF(client_id=0, 'out', 'in') as hit_type, DATE_FORMAT(created_at, '%Y %M') as date_hit from website_tracking where user_id=? group by hit_type, date_hit order by date_hit desc limit 30", [auth()->id()]);
		$deviceTypeHit = \DB::select("select count(*) as hits, `type` from website_tracking where user_id=? group by website_tracking.`type` desc limit 4", [auth()->id()]);


		return view( 'profile.website.statistics' )
			->with( 'title', 'Statistics' )
			->with( 'monthsHit', $monthsHit )
			->with( 'countriesHit', $countriesHit )
			->with( 'usersHit', $usersHit )
			->with( 'deviceTypeHit', $deviceTypeHit )
			->with( 'header', WebsiteHeader::getRow( WebsiteHeader::TYPE_STATISTICS ) );
	}

	public function messages(){

		$messages = ContactUs::where('user_id', auth()->id())->orderByDesc('id')->paginate(20);

		return view('profile.website.messages')
			->with( 'title', 'Messages Via Website' )
			->with( 'messages', $messages )
			->with( 'header', WebsiteHeader::getRow( WebsiteHeader::TYPE_STATISTICS ) );
	}

	public function message($id){

		$message = ContactUs::where('user_id', auth()->id())->findOrFail($id);

		$message->update(['is_read'=>1]);

		return view('profile.website.message')
			->with( 'title', 'Messages Via Website' )
			->with( 'message', $message )
			->with( 'header', WebsiteHeader::getRow( WebsiteHeader::TYPE_STATISTICS ) );
	}

}
