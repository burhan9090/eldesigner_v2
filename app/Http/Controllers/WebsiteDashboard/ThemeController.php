<?php

namespace App\Http\Controllers\WebsiteDashboard;

use App\WebsiteHeader;
use App\WebsiteTheme;
use App\Http\Controllers\Controller;

class ThemeController extends Controller {

	public function index() {

		return view( 'profile.website.theme' )
			->with( 'title', 'Welcoming Screen' )
			->with( 'header', WebsiteHeader::getRow( WebsiteHeader::TYPE_SELECT_THEME) )
			->with( 'current_theme', WebsiteTheme::where('folder', auth()->user()->theme_name)->first() )
			->with( 'themes', WebsiteTheme::where('type', auth()->user()->profile_type)->paginate(9) );
	}

	public function save() {

		$theme = WebsiteTheme::find(request('id'));

		if ( !$theme ) {
			request()->session()->put( 'error', "Something goes wrong!" );

			return redirect()->back();
		}

		$user = auth()->user();
		$user->theme_name = $theme->folder;
		$user->save();

		request()->session()->put( 'save', "Theme has updated" );

		return redirect()->route('themes.index');
	}
}
