<?php

namespace App\Http\Controllers\WebsiteDashboard;

use App\Post;
use App\Service;
use App\Jobs\ImageProcess;
use app\modules\user\models\Auth;
use App\Tag;
use App\UserMedia;
use App\UserPermission;
use App\WebsiteArticles;
use App\WebsiteHeader;
use App\WebsiteProject;
use App\WebsiteProjectImage;
use App\WebsiteProjectImageTag;
use App\WebsiteProjectTag;
use App\WebsiteProjectTimeLine;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class ProjectsController extends Controller {

	public function index() {
        $iProjectCount = WebsiteProject::where( 'user_id', auth()->id() )->count();

		return view( 'profile.website.project.project_list' )
			->with( 'title', 'Manage Your Projects' )
			->with( 'header', WebsiteHeader::getRow( WebsiteHeader::TYPE_PROJECTS ) )
			->with( 'categories', Service::all() )
			->with( 'projects_count', $iProjectCount )
			->with( 'projects', WebsiteProject
				::where( 'user_id', auth()->id() )
				->with( [ 'image', 'timeline', 'category' ] )
				->orderByDesc( 'id' )
				->paginate( 8 ) );
	}

	public function library() {
		return view( 'profile.website.project.project_library' )
			->with( 'title', 'Community Projects' )
			->with( 'header', WebsiteHeader::getRow( WebsiteHeader::TYPE_COMMUNITY_PROJECTS ) )
			->with( 'projects', WebsiteProject
				::where( 'user_id', '!=', auth()->id() )
				->where( 'is_demo', 0 )
				->where( 'repost', true )
				->with( [ 'image', 'timeline', 'category' ] )
				->orderByDesc( 'id' )
				->paginate( 9 ) );
	}

	public function show( $id ) {
		$project = WebsiteProject::with( ['images', 'user' ])->findOrFail( $id ) ;
//		dd($project->user);
		return view( 'profile.website.project.project_show' )
			->with( 'user_info', $project->user )
			->with( 'project', $project);
	}

	public function save( Request $request ) {
        $iProjectCount = WebsiteProject::where( 'user_id', auth()->id() )->count();
        $projectPermission = UserPermission::checkProjectsAndArticles();
        $status = false;
        if($projectPermission == 'unlimited' || $iProjectCount < $projectPermission){
            $vali = \Validator::make( $request->all(), [
                'image'          => 'sometimes|file|image',
                "title"          => 'required',
                "category_id"    => 'required|integer|exists:services,id',
                "description"    => 'required',
                "picture-file"   => 'required|array',
                "picture-file.*" => 'file|image',
                "picture"        => 'required|array',
//			"picture.*.description" => 'required'

            ] );

            if ( $vali->fails() ) {
                return [ "status" => false, 'message' => $vali->errors() ];
            }

            $add = WebsiteProject::create( [
                "user_id"     => auth()->id(),
                "title"       => $request->get( 'title' ),
                "description" => $request->get( 'description' ),
                "category_id" => $request->get( 'category_id' ),
            ] );


            if ( $add ) {

                $ids = [];

                $tags = $request->get( 'tags' );
                $tags = explode( ',', $tags );

                foreach ( $tags as $tag ) {
                    if ( trim( $tag ) != '' ) {
                        $row = Tag::firstOrNew( [ 'text' => trim( $tag ) ] );
                        if ( ! $row->id ) {
                            $row->save();
                        }

                        $ids[] = [
                            "project_id" => $add->id,
                            "tag_id"     => $row->id
                        ];
                    }
                }

                if ( count( $ids ) ) {
                    WebsiteProjectTag::insert( $ids );
                }


                WebsiteProjectTimeLine::create([
	                'user_id' => auth()->id(),
					'project_id' => $add->id,
                ]);

                $pictures = $request->get( 'picture', [] );

                /** @var $image \Illuminate\Http\UploadedFile */
                foreach ( request()->file( 'picture-file' ) as $key => $picture ) {
                    $file = $picture->move( 'uploads/project/' . auth()->id() . '/' . $add->id, $picture->getBasename() . '.' . $picture->getClientOriginalExtension() ) . '';

                    $image = WebsiteProjectImage::create( [
                        "project_id"  => $add->id,
                        "image"       => $file,
                        "description" => isset( $pictures[ $key ]['description'] ) ? $pictures[ $key ]['description'] : '',
                    ] );
                    //optimize image and add water mark [QUEUE]
                    ImageProcess::dispatch(\Illuminate\Support\Facades\Auth::user(),$image,$file,'OPTIMIZE|WATERMARK');
                    $ids = [];

                    $tags = $pictures[ $key ]['tags'];
                    $tags = explode( ',', $tags );

                    foreach ( $tags as $tag ) {
                        if ( trim( $tag ) != '' ) {
                            $row = Tag::firstOrNew( [ 'text' => trim( $tag ) ] );
                            if ( ! $row->id ) {
                                $row->save();
                            }

                            $ids[] = [
                                "image_id" => $add->id,
                                "tag_id"   => $row->id
                            ];
                        }
                    }

                    if ( count( $ids ) ) {
                        WebsiteProjectImageTag::insert( $ids );
                    }
                }
                Post::addPostByType('project',$add->id,$add->title);
            }
            $status =  boolval( $add->id );

        }
        return [ "status" => $status ];
	}

	public function update( Request $request ) {

		$vali = \Validator::make( $request->all(), [
			'image'          => 'sometimes|file|image',
			"title"          => 'required',
			"description"    => 'required',
			"category_id"    => 'required|integer|exists:services,id',
			"picture-file"   => 'array',
			"picture-file.*" => 'file|image',
			"picture"        => 'required|array',
//			"picture.*.description" => 'required'

		] );

		if ( $vali->fails() ) {
			$request->session()->put( 'error', "Something goes wrong!, " . implode( "\n", $vali->messages()->all() ) );

			return redirect()->back();
		}

		$project = WebsiteProject::where( "user_id", auth()->id() )->findOrFail( $request->get( 'id', 0 ) );

		$project->title       = $request->get( 'title' );
		$project->description = $request->get( 'description' );
		$project->category_id = $request->get( 'category_id' );

		$project->save();

		$ids = [];

		$tags = $request->get( 'tags' );
		$tags = explode( ',', $tags );

		foreach ( $tags as $tag ) {
			if ( trim( $tag ) != '' ) {
				$row = Tag::firstOrNew( [ 'text' => trim( $tag ) ] );
				if ( ! $row->id ) {
					$row->save();
				}

				$ids[] = [
					"project_id" => $project->id,
					"tag_id"     => $row->id
				];
			}
		}

		$project->tags()->sync( $ids );

		$pictures = $request->get( 'picture', [] );


		foreach ( $project->images()->get() as $image ) {
			if ( isset( $pictures[ $image->id ] ) ) {
				$image->description = isset( $pictures[ $image->id ]['description'] ) ? $pictures[ $image->id ]['description'] : '';

				$ids = [];

				$tags = $pictures[ $image->id ]['tags'];
				$tags = explode( ',', $tags );

				foreach ( $tags as $tag ) {
					if ( trim( $tag ) != '' ) {
						$row = Tag::firstOrNew( [ 'text' => trim( $tag ) ] );
						if ( ! $row->id ) {
							$row->save();
						}

						$ids[] = [
							"image_id" => $image->id,
							"tag_id"   => $row->id
						];
					}
				}

				$image->tags()->sync( $ids );
			} else {
				$image->delete();
			}
		}


		/** @var $image \Illuminate\Http\UploadedFile */
		foreach ( request()->file( 'picture-file', [] ) as $key => $picture ) {
			$file = $picture->move( 'uploads/project/' . auth()->id() . '/' . $project->id, $picture->getBasename() . '.' . $picture->getClientOriginalExtension() ) . '';

			$image = WebsiteProjectImage::create( [
				"project_id"  => $project->id,
				"image"       => $file,
				"description" => isset( $pictures[ $key ]['description'] ) ? $pictures[ $key ]['description'] : '',
			] );

			$ids = [];

			$tags = $pictures[ $key ]['tags'];
			$tags = explode( ',', $tags );

			foreach ( $tags as $tag ) {
				if ( trim( $tag ) != '' ) {
					$row = Tag::firstOrNew( [ 'text' => trim( $tag ) ] );
					if ( ! $row->id ) {
						$row->save();
					}

					$ids[] = [
						"image_id" => $image->id,
						"tag_id"   => $row->id
					];
				}
			}

			if ( count( $ids ) ) {
				WebsiteProjectImageTag::insert( $ids );
			}
		}

		return redirect()->back();
	}

	public function delete( Request $request ) {
		$id = $request->get( 'id', 0 );

		if ( $id and WebsiteProject::where( 'id', $id )->where( 'user_id', auth()->id() )->delete() ) {
			WebsiteProjectTag::where( 'project_id', $id )->delete();

			$ids = WebsiteProjectImage::where( 'project_id', $id )->get( [ 'id', 'image' ] );
			WebsiteProjectImageTag::whereIn( 'id', $ids->pluck( 'id' ) )->delete();

			$demoLinks = [
				'img/sm-project1.png',
				'img/sm-project2.png',
				'img/sm-project3.png',
				'img/sm-project4.png',
				'img/sm-project5.png',
				'img/sm-project6.png'
			];


			foreach ( $ids as $row ) {

				if(in_array($row['image'], $demoLinks)){
					continue;
				}

				try {
					unlink( public_path( $row['image'] ) );
				} catch ( \Exception $e ) {

				}
			}

			WebsiteProjectImage::where( 'project_id', $id )->delete();
			WebsiteProjectTimeLine::where( 'project_id', $id )->delete();
            Post::deletePostByType('project', $id);

            return [ 'status' => true, 'message' => "Project has deleted." ];
		}

		return [ 'status' => false, 'message' => "Project dose not deleted." ];
	}

	public function publish( Request $request ) {
		if ( $request->get( 'project_id', false ) ) {
			$art = WebsiteProjectTimeLine::firstOrNew( [
				'user_id'    => auth()->id(),
				'project_id' => $request->get( 'project_id' )
			] );

			$art->save();
			$oProject = WebsiteProject::Where('id',$request->get( 'project_id' ))->get()->first();
			if($oProject){
			    Post::addPostByType('project',$oProject->id,$oProject->title);
            }

			return [ "status" => true ];
		}
		return [ "status" => false ];
	}

	public function hidden( Request $request ) {
		if ( $request->get( 'project_id', false ) ) {
			WebsiteProjectTimeLine::where( [
				'user_id'    => auth()->id(),
				'project_id' => $request->get( 'project_id' )
			] )->delete();
            Post::deletePostByType('project', $request->get( 'project_id'));
			return [ "status" => true ];
		}

		return [ "status" => false ];
	}

	public function repost( Request $request ) {
		if ( $request->get( 'project_id', false ) ) {
			$project         = WebsiteProject::findOrFail( $request->get( 'project_id' ) );
			$project->repost = ! $project->repost;

			return [ "status" => $project->save() ];
		}

		return [ "status" => false ];
	}

	public function getProjectAjax( Request $request ) {

		$result = [
			'data' => WebsiteProject
				::where( 'user_id', auth()->id() )
				->with( [ 'images.tags', 'tags', 'category' ] )
				->orderByDesc( 'id' )
				->find( $request->get( 'id', 0 ) )
		];

		if ( $result['data'] ) {

			$result['data'] = $result['data']->toArray();

			foreach ( $result['data']['images'] as &$row ) {
				$row['image'] = asset( $row['image'] );
			}
		}

		return $result;
	}
	public function checkProjects(){

	    $iCheckPro = UserPermission::checkProjectsAndArticles();
        $aResponse['data'] = $iCheckPro;
        return \Response::json($aResponse);
    }
}
