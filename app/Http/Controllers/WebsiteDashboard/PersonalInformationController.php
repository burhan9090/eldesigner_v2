<?php

namespace App\Http\Controllers\WebsiteDashboard;

use App\City;
use App\Jobs\ImageProcess;
use app\modules\user\models\Auth;
use App\User;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PersonalInformationController extends Controller {
	public function index() {
		return view( 'profile.website.personal_information' )
			->with( 'user', auth()->user() );
	}

	public function save( Request $request ) {

		$user = auth()->user();
        $updateLogo = false;
        $updateCover = false;

		if ( $request->has( 'name' ) ) {
			$user->name = $request->get( 'name' );
		}

		if ( $request->has( 'email' ) ) {
			$user->email = $request->get( 'email' );
		}

		if ( $logo = $request->file( 'logo' ) ) {
			/** @var $image \Illuminate\Http\UploadedFile */
			$file = $logo->move( 'uploads/user', "logo_".$user->id . '.' . $logo->getClientOriginalExtension() ) . '';
			$user->logo = $file;
			$updateLogo = true;
		}

		if ( $cover = $request->file( 'cover' ) ) {
			/** @var $image \Illuminate\Http\UploadedFile */
			$file = $cover->move( 'uploads/user', "cover_".$user->id . '.' . $cover->getClientOriginalExtension() ) . '';
			$user->cover = $file;
			$updateCover = true;
		}

		if ( $request->has( 'about' ) ) {
			$user->about = $request->get( 'about' );
		}

		if ( $request->has( 'gender' ) ) {
			if ( in_array( $request->get( 'gender' ), [ 'male', 'female' ] ) ) {
				$user->gender = $request->get( 'gender' );
			}
		}

		if ( $request->has( 'phone' ) ) {
			$user->phone = $request->get( 'phone' );
		}

		if ( $request->has( 'last_name' ) ) {
			$user->last_name = $request->get( 'last_name' );
		}

		if ( $request->has( 'website' ) ) {
			$user->website = $request->get( 'website' );
		}

		if ( $request->has( 'bod' ) ) {
			$user->bod = $request->get( 'bod' );
		}

		if ( $request->has( 'country_id' ) ) {
			$user->country_id = intval($request->get( 'country_id' ));
		}

		if ( $request->has( 'city_id' ) ) {
			$user->city_id = intval($request->get( 'city_id' ));
		}

		if ( $request->has( 'description' ) ) {
			$user->description = $request->get( 'description' );
		}

		if ( $request->has( 'occupation' ) ) {
			$user->occupation = $request->get( 'occupation' );
		}

		if ( $request->has( 'fb_link' ) ) {
			$user->fb_link = $request->get( 'fb_link' );
		}

		if ( $request->has( 'ig_link' ) ) {
			$user->ig_link = $request->get( 'ig_link' );
		}

		if ( $request->has( 'tw_link' ) ) {
			$user->tw_link = $request->get( 'tw_link' );
		}

		if ( $request->has( 'pin_link' ) ) {
			$user->pin_link = $request->get( 'pin_link' );
		}

		if ( $request->has( 'lang_code' ) ) {
			$user->lang_code = $request->get( 'lang_code' );
		}
        $bStatus = $user->save();
		if($bStatus){
		    if($updateLogo){
		        ImageProcess::dispatch(auth()->user(),null,$user->logo,'OPTIMIZE','USER_LOGO');
            }
            if($updateCover){
                ImageProcess::dispatch(auth()->user(),null,$user->cover,'OPTIMIZE','USER_COVER');
            }
        }
		return ['status' => $bStatus];
	}

	public function getCities( Request $request ) {

		return City::where( 'country_id', $request->get( 'country_id', 0 ) )->get();
	}

	public function about($id){
		return view( 'profile.website.about' )
			->with( 'user', User::with('history')->findOrFail($id) );
	}

	public function changePassword(){
		return view('profile.settings.change_password');
	}

	public function changePasswordSave(Request $request){
		$request->session()->put('error', "Something goes wrong!, Password has not changed");

		$user = auth()->user();

		if(password_verify('123456', $user->getAuthPassword())){
			if($request->get('new_password') == $request->get('confirm_password')) {
				$user->password = bcrypt($request->get('new_password'));
				$user->save();

				$request->session()->remove('error');
				$request->session()->put('save', "Password has changed");
			}
		}

		return redirect()->route("dashboard.index");
	}
}
