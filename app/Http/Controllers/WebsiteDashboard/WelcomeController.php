<?php

namespace App\Http\Controllers\WebsiteDashboard;

use App\Jobs\ImageProcess;
use App\WebsiteHeader;
use App\WebsiteWelcome;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class WelcomeController extends Controller {

	public function index() {

		return view( 'profile.website.settings.welcome_screen' )
			->with( 'title', 'Welcoming Screen' )
			->with( 'header', WebsiteHeader::getRow( WebsiteHeader::TYPE_WELCOME_SCREEN ) )
			->with( 'user', auth()->user() )
			->with( 'websiteWelcome', WebsiteWelcome::firstOrNew( [ 'user_id' => auth()->id() ] ) );
	}

	public function save( Request $request ) {

		$valid = \Validator::make( $request->all(), [
			'background'   => 'sometimes|image',
			'logo'         => 'sometimes|image',
			'user_picture' => 'sometimes|image',
			'main'         => 'required',
			'description'  => 'required'
		] );

		if ( $valid->fails() ) {
			return [ 'status' => false, 'message' => $valid->errors() ];
		}

		$background = "";
		/** @var $image \Illuminate\Http\UploadedFile */
		if ( $image = request()->file( 'background' ) ) {
			$background = $image->move( 'uploads/background/' . auth()->id(), $image->getBasename() . '.' . $image->getClientOriginalExtension() ) . '';
		}

		$logo = "";
		/** @var $image \Illuminate\Http\UploadedFile */
		if ( $image = request()->file( 'logo' ) ) {
			$logo = $image->move( 'uploads/logo/' . auth()->id(), $image->getBasename() . '.' . $image->getClientOriginalExtension() ) . '';
		}

		$userPicture = "";
		/** @var $image \Illuminate\Http\UploadedFile */
		if ( $image = request()->file( 'user_picture' ) ) {
			$userPicture = $image->move( 'uploads/user_picture/' . auth()->id(), $image->getBasename() . '.' . $image->getClientOriginalExtension() ) . '';
		}


		$row = WebsiteWelcome::firstOrNew( [ 'user_id' => auth()->id() ] );

		if ( $background ) {
			$row->background = $background;
		}
		if ( $logo ) {
			$row->logo = $logo;
		}
		if ( $userPicture ) {
			$row->user_picture = $userPicture;
		}
		$row->main             = $request->get( 'main' );
		$row->description      = $request->get( 'description' );

		$result = $row->save();

		if ( $result ) {
			if ( $background ) {
				delete_other_files( 'uploads/background/' . auth()->id(), $background );
				ImageProcess::dispatch(\Auth::user(),null,$background,'OPTIMIZE','WELCOME_BG');
			}
			if ( $logo ) {
				delete_other_files( 'uploads/logo/' . auth()->id(), $logo );
                ImageProcess::dispatch(\Auth::user(),null,$logo,'OPTIMIZE','WELCOME_LOGO');
			}
            if ( $userPicture ) {
                ImageProcess::dispatch(\Auth::user(),null,$userPicture,'OPTIMIZE','WELCOME_USER_PROFILE');
            }
		}

		return [ 'status' => $result, 'message' => [] ];
	}
}
