<?php

namespace App\Http\Controllers\WebsiteDashboard;

use App\WebsiteContactUs;
use App\WebsiteHeader;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ContactUsController extends Controller {

	public function index() {

		return view( 'profile.website.contact_us' )
			->with( 'title', 'Articles Library' )
			->with( 'header', WebsiteHeader::getRow( WebsiteHeader::TYPE_CONTACT_US_TAB ) )
			->with( 'contactUs', WebsiteContactUs::firstOrNew( [ 'user_id' => auth()->id() ] ) );
	}

	public function save(Request $request) {

		$valid = \Validator::make($request->all(), [
			'title' => 'required',
			'message' => 'required'
		]);

		if($valid->fails()){
			return ['status'=>false, 'message'=>$valid->errors()];
		}

		$row = WebsiteContactUs::firstOrNew([ 'user_id' => auth()->id() ] );

		$row->show_tab = boolval($request->get('show_tab',0));
		$row->email_notification = boolval($request->get('email_notification', 0));
		$row->title = $request->get('title');
		$row->message = $request->get('message');

		return ['status' => $row->save(), 'message'=> []];
	}
}
