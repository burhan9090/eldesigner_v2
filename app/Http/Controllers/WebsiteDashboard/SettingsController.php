<?php

namespace App\Http\Controllers\WebsiteDashboard;

use App\WebsiteHeader;
use App\WebsiteWelcome;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SettingsController extends Controller {

	public function mainIndex() {

		return view( 'profile.website.settings.main' )
			->with( 'title', 'Settings' )
			->with( 'header', WebsiteHeader::getRow( WebsiteHeader::TYPE_SETTINGS ) )
			->with( 'user', auth()->user() )
			->with( 'websiteWelcome', WebsiteWelcome::firstOrNew( [ 'user_id' => auth()->id() ] ) );
	}

	public function mainSave( Request $request ) {

		$user = auth()->user();

		$user->email_notification = $request->get( 'email_notification', 0 );

		$profile_setting       = in_array( $request->get( 'profile_setting', 'public' ), [ 'private', 'public' ] ) ?
				$request->get( 'profile_setting' ) : 'public';
		$user->profile_setting = $profile_setting;

		$follow_setting       = in_array( $request->get( 'follow_setting', 'public' ), [
			'private',
			'public',
			'none'
		] ) ? $request->get( 'follow_setting' ) : 'public';

		$user->follow_setting = $follow_setting;

		return [ 'status' => $user->save(), 'message' => [] ];
	}

	public function seoIndex() {

		return view( 'profile.website.settings.seo' )
			->with( 'title', 'SEO' )
			->with( 'header', WebsiteHeader::getRow( WebsiteHeader::TYPE_SEO ) )
			->with( 'user', auth()->user() )
			->with( 'websiteWelcome', WebsiteWelcome::firstOrNew( [ 'user_id' => auth()->id() ] ) );
	}

	public function seoSave( Request $request ) {

		$row = WebsiteWelcome::firstOrNew( [ 'user_id' => auth()->id() ] );

		$row->title            = $request->get( 'title' );
		$row->keyword          = $request->get( 'keyword' );
		$row->meta_description = $request->get( 'meta_description' );
		$row->fb_page_id       = $request->get( 'fb_page_id' );
		$row->gplus            = $request->get( 'gplus' );
		$row->tracking_script  = $request->get( 'tracking_script' );


		return [ 'status' => $row->save(), 'message' => [] ];
	}
}
