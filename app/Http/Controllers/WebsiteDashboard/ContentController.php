<?php

namespace App\Http\Controllers\WebsiteDashboard;

use App\Http\Controllers\Controller;
use App\Jobs\ImageProcess;
use App\Post;
use App\Tag;
use App\UserPermission;
use App\WebsiteArticles;
use App\WebsiteArticlesCategory;
use App\WebsiteArticlesTag;
use App\WebsiteArticleTimeLine;
use App\WebsiteHeader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ContentController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
//		$this->middleware( 'auth' );
	}

	/**
	 * Show the application dashboard.
	 *
	 * @param boolean $mine
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index($mine) {

		return view( 'profile.website.article_list' )
			->with( 'title', 'Articles Library' )
			->with( 'header', WebsiteHeader::getRow(WebsiteHeader::TYPE_ARTICLES_LIBRARY) )
			->with( 'mine', $mine )
			->with( 'articles', WebsiteArticles
				::where( function($query) use($mine){
					$query->where('user_id', ($mine? '=': '!='), auth()->id());
					if(!$mine){
						$query->where('repost', true);
					}
				})
				->with('user')
				->paginate(6) );
	}

	public function library(){
		return $this->index(true);
	}

	public function Community(){
		return $this->index(false);
	}

	public function show($id) {

		$limit = 4;

		$article = WebsiteArticles::with(['tags', 'user', 'category'])->findOrFail($id);

		$tagsID = $article->tags->pluck('id')->toArray();


		if($tagsID) {
			$related = \DB::select(
"select count(website_articles_tags.tag_id) as p, website_articles.*, 
website_articles_categories.text as category_text, users.name as user_first_name from website_articles
join website_articles_tags on website_articles.id = website_articles_tags.article_id
join website_articles_categories on website_articles_categories.id = website_articles.category_id
join users on users.id = website_articles.user_id
where tag_id in (" . implode( ',', $tagsID ) . ") and website_articles.id!={$id}
group by website_articles_tags.article_id
order by p desc limit $limit" );
		}
		else {
			$related = WebsiteArticles
				::with(['user', 'category'])
				->where("category_id", $article->category_id)
				->where("id", '!=', $id)
				->orderByDesc('created_at')
				->limit($limit)
				->get()
				->toArray();
		}

		return view( 'profile.website.article_show' )
			->with( 'article',  $article)
			->with( 'related',  $related);

	}

	public function edit($id){
		$article = WebsiteArticles::with(['tags', 'user', 'category.parent'])->where('user_id', auth()->id())->findOrFail($id);

		return view( 'profile.website.article_edit' )
			->with( 'title', 'Edit Article' )
			->with( 'header', WebsiteHeader::getRow(WebsiteHeader::TYPE_PUBLISHING_ARTICLES) )
			->with( 'categories', WebsiteArticlesCategory::where( 'category_id', 0 )->with( 'children' )->get() )
			->with( 'article',  $article);

	}


	public function update(Request $request){

		$vali = \Validator::make( $request->all(), [
			'id'          => 'required',
			'image'       => 'sometimes|file|image',
			"title"       => 'required',
			"category_id" => 'required|exists:website_articles_categories,id',
			"body"        => 'required'
		] );


		if ( $vali->fails() ) {
			$request->session()->put( 'error', "Something goes wrong!, " . implode( "\n", $vali->messages()->all() ) );

			return redirect()->back();
		}

		$article = WebsiteArticles::where('user_id', auth()->id())->findOrFail($request->get('id'));

		/** @var $image \Illuminate\Http\UploadedFile */
		if ( $image = request()->file( 'image' ) ) {
			$file = $image->move( 'uploads/article', $image->getBasename() . '.' . $image->getClientOriginalExtension() ) . '';
			$article->image = $file;
		}

		$article->title       = $request->get( 'title' );
		$article->category_id = $request->get( 'category_id' );
		$article->body        = $request->get( 'body' );
		$article->repost      = $request->get( 'repost', '' ) != '';

		if ($article->save()) {
			$ids = [];

			$tags = $request->get( 'tags' );
			$tags = preg_replace('/[\s]{2,}/',' ', $tags );
			$tags = str_replace([' , ', ', ', ' ,'],',', $tags );
			$tags = explode( ',', $tags );
			$tags = array_unique($tags);

			foreach ( $tags as $tag ) {
				if ( trim( $tag ) != '' ) {
					$row = Tag::firstOrNew( [ 'text' => $tag ] );
					if ( ! $row->id ) {
						$row->save();
					}

					$ids[] = [
						"article_id" => $article->id,
						"tag_id"     => $row->id
					];
				}
			}

			if(count($ids)){
				$article->tags()->sync($ids);
			}
		}

		$request->session()->put( 'save', "Article has saved " );

		return redirect()->route('article.index');
	}


	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function addArticle() {
        $iArticlesCount = WebsiteArticles::Where('user_id',Auth::user()->id)->count();
        $ArticlePermission = UserPermission::checkProjectsAndArticles(true);

		return view( 'profile.website.article_add' )
			->with( 'title', 'Publishing Articles' )
			->with( 'articles_count', $iArticlesCount )
			->with( 'articles_permission', $ArticlePermission)
			->with( 'header', WebsiteHeader::getRow(WebsiteHeader::TYPE_PUBLISHING_ARTICLES) )
			->with( 'categories', WebsiteArticlesCategory::where( 'category_id', 0 )->with( 'children' )->get() );
	}

	public function add( Request $request ) {
        $iArticlesCount = WebsiteArticles::Where('user_id',Auth::user()->id)->count();
        $ArticlePermission = UserPermission::checkProjectsAndArticles(true);
        $status = false;
        if($ArticlePermission == 'unlimited' || $iArticlesCount < $ArticlePermission) {


            $vali = \Validator::make($request->all(), [
                'image' => 'sometimes|file|image',
                "title" => 'required',
                "category_id" => 'required|exists:website_articles_categories,id',
                "body" => 'required'
            ]);

            if ($vali->fails()) {
                return ["status" => false, 'message' => $vali->errors()];

            }


            $file = "";
            /** @var $image \Illuminate\Http\UploadedFile */
            if ($image = request()->file('image')) {
                $file = $image->move('uploads/article', $image->getBasename() . '.' . $image->getClientOriginalExtension()) . '';
            }


            $add = WebsiteArticles::create([
                "user_id" => auth()->id(),
                "image" => $file,
                "title" => $request->get('title'),
                "category_id" => $request->get('category_id'),
                "body" => $request->get('body'),
                "repost" => $request->get('repost', '') != ''
            ]);
            if (!empty($file) && $file != '') {
                ImageProcess::dispatch(Auth::user(), $add, $file, 'OPTIMIZE');
            }

            if ($add) {

                $ids = [];

                $tags = $request->get('tags');
                $tags = preg_replace('/[\s]{2,}/', ' ', $tags);
                $tags = str_replace([' , ', ', ', ' ,'], ',', $tags);
                $tags = explode(',', $tags);
                $tags = array_unique($tags);


                foreach ($tags as $tag) {
                    if (trim($tag) != '') {
                        $row = Tag::firstOrNew(['text' => trim($tag)]);
                        if (!$row->id) {
                            $row->save();
                        }

                        $ids[] = [
                            "article_id" => $add->id,
                            "tag_id" => $row->id
                        ];
                    }
                }

                if (count($ids)) {
                    WebsiteArticlesTag::insert($ids);
                }

            }


            $status = boolval($add->id);
        }
        return ["status" => $status];
	}

	public function delete( Request $request ) {
		WebsiteArticles::where('id', $request->get('article_id', 0))->delete();
        Post::deletePostByType('article', $request->get( 'article_id'));
		return ['status'=> true];
	}

	public function publish( Request $request ) {
		if($request->get('article_id', false)){
			$art = WebsiteArticleTimeLine::firstOrNew([
				'user_id'=>auth()->id(),
				'article_id' => $request->get('article_id')
			]);
			$art->save();
			$oArticle = WebsiteArticles::Where('id',$request->get('article_id'))->get()->first();
			if($oArticle){
                Post::addPostByType('article',$oArticle->id,$oArticle->title);
            }

			return ["status" => true];
		}

		return ["status" => false];
	}

	public function hidden( Request $request ) {
		if($request->get('article_id', false)){
			WebsiteArticleTimeLine::where([
				'user_id'=>auth()->id(),
				'article_id' => $request->get('article_id')
			])->delete();

            Post::deletePostByType('article', $request->get( 'article_id'));

            return ["status" => true];
		}

		return ["status" => false];
	}
}
