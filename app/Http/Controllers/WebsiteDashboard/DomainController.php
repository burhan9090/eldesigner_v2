<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 2/24/19
 * Time: 5:17 AM
 */

namespace App\Http\Controllers\WebsiteDashboard;

use App\Classes\DomainInterface;
use App\Http\Controllers\Controller;
use App\UserPermission;
use App\WebsiteHeader;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DomainController extends Controller {

	public function index( DomainInterface $domain, Request $request ) {
        $hasPermission = UserPermission::checkPermission('unique_domain');

		return view( 'profile.website.tools.domains.find_domain' )
			->with( 'title', 'Get a Domain' )
            ->with('has_permission', $hasPermission)
            ->with( 'hasDomain', ( boolval( auth()->user()->domain ) && boolval( auth()->user()->domain_created_at ) ) )
			->with( 'header', WebsiteHeader::getRow( WebsiteHeader::TYPE_GET_DOMAIN ) )
			->with( 'domains', $domain->checkDomain( $request->get( 'domain', '' ) ) );
	}

	public function select_domain( $domainName, DomainInterface $domain )
    {

        if (!(boolval(auth()->user()->website) && boolval(auth()->user()->domain_created_at))) {
            if ($domainName) {
                if ($rDomain = $domain->bookDomain($domainName)) {
//					$rDomain['status']==''
//					if($rDomain['error']==''){

                    $user = auth()->user();
                    $user->domain = $domainName;
                    $user->domain_created_at = date('Y-m-d');
                    $user->domain_renew_at = Carbon::now()->addYear()->format('Y-m-d');
                    $user->save();
//					}
                }
            }
        }

        return redirect()->back();

        //domain_created_at
        //domain_renewed_at
        //domain_renew_at
    }
//    public function index(DomainInterface $domain, Request $request) {
//        $hasPermission = UserPermission::checkPermission('unique_domain');
//        return view('profile.website.tools.domains.find_domain')
//            ->with('title', 'Get a Domain')
//            ->with('has_permission', $hasPermission)
//            ->with('header', WebsiteHeader::getRow(WebsiteHeader::TYPE_GET_DOMAIN))
//            ->with('domains', $domain->checkDomain($request->get('domain', '')));
//
//    }
}