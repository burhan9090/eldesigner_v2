<?php

namespace App\Http\Controllers\WebsiteDashboard;

use App\UserPermission;
use App\WebsiteHeader;
use App\WebsiteWelcome;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ToolsController extends Controller {

	public function index() {
            $hasPermission = UserPermission::checkPermission('image_protection');
            return view('profile.website.tools.water_mark')
                ->with('title', 'Image Protectiwatermarkon')
                ->with('has_permission', $hasPermission)
                ->with('header', WebsiteHeader::getRow(WebsiteHeader::TYPE_WATER_MARK))
                ->with('user', auth()->user())
                ->with('websiteWelcome', WebsiteWelcome::firstOrNew(['user_id' => auth()->id()]));
	}

	public function save( Request $request ) {

		$watermark = "";
        $hasPermission = UserPermission::checkPermission('image_protection');
		/** @var $image \Illuminate\Http\UploadedFile */
		if ( $image = request()->file( 'watermark_file' ) && $hasPermission) {
                $watermark = $image->move( 'uploads/watermark/' . auth()->id(), $image->getBasename() . '.' . $image->getClientOriginalExtension() ) . '';
		}

		$user = auth()->user();

		if ( $watermark && $hasPermission) {
			$user->watermark_file = $watermark;
		}


		$user->watermark_file_active = $request->get( 'watermark_file_active', 0 );
		$user->watermark_text_active = $request->get( 'watermark_text_active', 0 );
		$user->watermark_text        = $request->get( 'watermark_text', '' );

		$result = $user->save();

		if ( $result ) {
			if ( $watermark && $hasPermission) {
				delete_other_files( 'uploads/watermark/' . auth()->id(), $watermark );
			}
		}

		return [ 'status' => $result, 'message' => [] ];
	}
}
