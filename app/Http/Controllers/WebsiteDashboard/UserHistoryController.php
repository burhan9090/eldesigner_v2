<?php

namespace App\Http\Controllers\WebsiteDashboard;

use App\UserHistory;
use App\WebsiteHeader;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserHistoryController extends Controller {

	public function index() {

		return view( 'profile.website.user_history' )
			->with( 'title', 'Manage Your Projects' )
			->with( 'header', WebsiteHeader::getRow( WebsiteHeader::TYPE_PROJECTS ) )
			->with( 'histories', UserHistory::where( 'user_id', auth()->id() )->get() );
	}


	public function save( Request $request ) {

		$ids = $request->get( 'id' );

		foreach ( $request->get( 'title' ) as $key => $val ) {
			if ( isset( $ids[ $key ] ) ) {
				$userHistory     = UserHistory::firstOrNew( [ 'id' => $ids[ $key ], 'user_id' => auth()->id()] );
				if($userHistory->id != $ids[ $key ]){
					$userHistory->id = null;
				}
			} else {
				$userHistory          = new UserHistory();
				$userHistory->user_id = auth()->id();
			}

			$userHistory->type        = $request->get( 'type' );
			$userHistory->title       = $request->get( 'title' )[ $key ];
			$userHistory->period      = $request->get( 'period' )[ $key ];
			$userHistory->description = $request->get( 'description' )[ $key ];

			$userHistory->save();

			$ids[ $key ] = $userHistory->id;
		}

		if($request->has('delete')) {
			UserHistory::whereIn('id', $request->get('delete'))->delete();
		}

		return [ "status" => true, "data" => $ids ];
	}

}
