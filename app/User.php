<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		'name', 'email', 'password', 'user_name', 'logo', 'cover', 'about', 'fb_link', 'ig_link', 'tw_link', 'pin_link', 'lang_code', 'gender', 'profile_type', 'user_type', 'phone', 'last_name', 'website', 'bod', 'country_id', 'city_id', 'description', 'occupation','profile_count' , 'code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function websiteWelcome(){
    	return $this->hasOne(WebsiteWelcome::class);
    }

    public function websiteContactUs(){
    	return $this->hasOne(WebsiteContactUs::class);
    }

	public function country(){
		return $this->belongsTo(Country::class, 'country_id');
	}
	public function city(){
		return $this->belongsTo(City::class, 'city_id');
	}
    public function messages(){
        return $this->hasMany(Message::class);
    }
    public function follows(){
        return $this->hasMany(Follows::class,'followed_user_id');
    }
    public function follow_requests(){
        return $this->hasMany(FollowRequest::class);
    }
    public function posts(){
        return $this->hasMany(Post::class);
    }
    public function projects(){
        return $this->hasMany(WebsiteProject::class);
    }
    public function notifications(){
        return $this->hasMany(Notification::class);
    }
    public function history(){
    	return $this->hasMany(UserHistory::class)->orderBy('type');
    }
    public function services(){
    	return $this->belongsToMany(Service::class, 'user_service', 'user_id', 'service_id');
    }
    public function skills(){
        return $this->hasOne(UserSkill::class, 'user_id', 'id');
    }
    public function permissions(){
        return $this->hasMany(UserPermission::class, 'user_id', 'id')->where('is_deleted' , false);
    }

    public function plans()
    {
        return $this->belongsToMany(Plans::class , 'user_permission' , 'plans_id' , 'user_id' , 'id' , 'plans_id');
    }
    public function articles(){
        return $this->hasMany(WebsiteArticles::class);
    }
}
