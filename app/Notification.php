<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'notification';
    protected $fillable = [
        'user_id','user_id_from','notification_text','notification_type','is_seen','resource_id'
    ];

    public function user(){
        return $this->belongsTo(User::class,'user_id_from');
    }

}