<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 2019-03-04
 * Time: 00:20
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
class PlansOptionsSelected extends Model{
    protected $table = 'plans_options_selected';
    protected $fillable = ['plans_id', 'plans_options_id'];

    public function options(){
        return $this->hasMany(PlansOptions::class,'plans_options_id');
    }
}