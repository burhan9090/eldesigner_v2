<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteTracking extends Model {
	protected $table = "website_tracking";

	protected $fillable = [
		'user_id',
		'client_id',
		'uri',
		'client_ip',
		'host',
		'user_agent',
		'referer',
		'country_code',
		'browser_name',
		'browser_version',
		'engine',
		'os',
		'device',
		'type'
	];


	public static function addHit( $user_id , $userAgent=null, $ip=null) {

		$agent = new \WhichBrowser\Parser( $userAgent?: request()->server( 'HTTP_USER_AGENT' ) );

		$client_id = 0;
		if ( auth()->check() && auth()->id() != $user_id ) {
			$client_id = auth()->id();
		}

		// todo: api = https://www.geojs.io/docs/v1/endpoints/country/
		// todo: api = https://get.geojs.io/v1/ip/country/92.241.40.129.json

		try {
			$geoip = json_decode( file_get_contents( 'https://get.geojs.io/v1/ip/country/' . ( $ip ?: request()->server( 'REMOTE_ADDR', '' ) ) . '.json' ), 1 );
			if($geoip){
				$iso_code = isset($geoip['country'])? $geoip['country']: '';
			}
		}
		catch (\Exception $e){
			$iso_code = '';
		}

		$iso_code = trim($iso_code);

		return static::create( [
			'user_id'    => $user_id,
			'client_id'  => $client_id,
			'uri'        => request()->server( 'REQUEST_URI', '/' ),
			'client_ip'  => $ip?: request()->server( 'REMOTE_ADDR', '' ),
			'host'       => request()->server( 'HTTP_HOST', '' ),
			'user_agent' => $userAgent?: request()->server( 'HTTP_USER_AGENT', '' ),
			'referer'    => request()->server( 'HTTP_REFERER', '' ),
			'country_code'    => $iso_code,
			'browser_name'    => $agent->browser->toString(),
			'browser_version' => $agent->browser->getVersion(),
			'engine'          => $agent->engine->toString(),
			'os'              => $agent->os->toString(),
			'device'          => $agent->device->toString(),
			'type'            => $agent->getType(),
		] );
	}
}
