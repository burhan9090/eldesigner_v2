<?php

namespace App\Providers;

use App\ContactUs;
use App\User;
use Carbon\Carbon;
use function foo\func;
use Illuminate\Support\ServiceProvider;

class UserHandlerServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot() {
		User::creating( function ( $user ) {
			$user->theme_name = 'daniels';
			$user->city_id    = 1;
			$user->country_id = 1;
		} );

		User::created( function ( $user ) {
			ContactUs::insert( [
				'user_id'    => $user->id,
				'title'      => 'Welcome ' . config( 'app.name' ),
				'message'    => 'Welcome message',
				'is_read'    => false,
				'created_at' => Carbon::now(),
			] );
		} );
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register() {
		//
	}
}
