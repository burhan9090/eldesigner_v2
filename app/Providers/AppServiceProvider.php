<?php

namespace App\Providers;

use App\Classes\DomainInterface;
use App\Classes\Godaddy;
use App\Classes\Menagator;
use App\TeamMember;
use App\User;
use App\UserHistory;
use App\UserSkill;
use App\WebsiteContactUs;
use App\WebsiteProject;
use App\WebsiteProjectImage;
use App\WebsiteProjectTag;
use App\WebsiteProjectTimeLine;
use App\WebsiteService;
use App\WebsiteWelcome;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema; //Import Schema
use Intervention\Image\ImageManager;

class AppServiceProvider extends ServiceProvider {
	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot() {
		Schema::defaultStringLength( 191 ); //Solved by increasing StringLength

		User::creating( function ( $row ) {
			$row->description = "Here's where you should add a brief description about who you are what you do. This can give visitors an opportunity to know who you are :)";

			if($row->profile_type=='company'){
				$row->theme_name='herox';
			}
		} );

		User::created( function ( $user ) {
			WebsiteWelcome::create([
				'user_id' => $user->id,
				'background' => 'img/background.png',
				'logo' => '',
				'main' => 'Business Name',
				'description' => 'Welcome to our website',
				'title' => '',
				'keyword' => '',
				'meta_description' => '',
				'fb_page_id' => '',
				'gplus' => '',
				'tracking_script' => '',
				'user_picture' => 'img/user.png',
			]);

			UserSkill::create( [
				'user_id' => $user->id,
				'skills'  => '{"title":["Skill A","Skill B","Skill C", "Skill D"],"weight":[50,100,80,85]}'
			] );

			WebsiteService::create( [
				'user_id'      => $user->id,
				'show_service' => 1,
				'service_text' => '{"title":["Design Services","3D Modeling","Technical Drawings","Branding Services"],' .
				                  '"description":["Here’s the place to describe the service you provide. [example]",' .
				                  '"Here’s the place to describe the service you provide. [example]",' .
				                  '"Here’s the place to describe the service you provide. [example]",' .
				                  '"Here’s the place to describe the service you provide. [example]"]}',
				'icon'         => '["img\/service1.svg","img\/service2.svg","img\/service3.svg","img\/service4.svg"]'
			] );

			UserHistory::insert( [
				[
					'user_id'     => $user->id,
					'type'        => UserHistory::TYPE_EDUCATION,
					'title'       => 'Name of Institute',
					'period'      => '2000-2005',
					'description' => 'Name of the granted degree',
				],
				[
					'user_id'     => $user->id,
					'type'        => UserHistory::TYPE_EDUCATION,
					'title'       => 'Name of Institute',
					'period'      => '2005-2008',
					'description' => 'Name of the granted degree',
				],
				[
					'user_id'     => $user->id,
					'type'        => UserHistory::TYPE_EDUCATION,
					'title'       => 'Name of Employer',
					'period'      => '2008-2012',
					'description' => 'Main job title.',
				],
				[
					'user_id'     => $user->id,
					'type'        => UserHistory::TYPE_EDUCATION,
					'title'       => 'Name of Employer',
					'period'      => '2012-2019',
					'description' => 'Main job title.',
				]
			] );

			$p = [1=>'A', 2=>'B', 3=>'C', 4=>'D', 5=>'E', 6=>'F'];
			foreach ($p as $key=>$letter) {
				$project = WebsiteProject::create( [
					'user_id'       => $user->id,
					'title'         => 'Project ' . $letter,
					'description'   => "Project $letter description goes here...",
					'repost'        => 0,
					'project_count' => 0,
					'category_id'   => 1,
					'is_demo'       => 1,
				] );

				WebsiteProjectImage::create( [
					'image'                     => "img/sm-project{$key}.png",
					'project_id'                => $project->id,
					'description'               => '',
					'optimized_and_watermarked' => 0,
				] );

				WebsiteProjectTag::create( [
					'project_id' => $project->id,
					'tag_id'     => $key,
				] );

				WebsiteProjectTimeLine::create([
					'user_id' => $user->id,
					'project_id' => $project->id
				]);


				$members=[];
				foreach (range(1,4) as $key){
					$members[] = [
						'image' => "img/sm-team{$key}.png",
						'title' => "Designer {$key} Name",
						'description' => "Designer Position.\nMore information about the designer goes here.",
					];
				}

				TeamMember::create([
					'user_id' => $user->id,
					'member' => json_encode($members)
				]);

				WebsiteContactUs::create([
					'user_id' => $user->id,
					'show_tab' => 1,
					'email_notification' => 0,
					'title' => 'Enter Email',
					'message' => 'Enter Message',
				]);


			}


		} );
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register() {
		App::singleton( 'imagick', function () {
			return new ImageManager( array( 'driver' => 'imagick' ) );
		} );

		App::bind( DomainInterface::class, Menagator::class );


	}
}
