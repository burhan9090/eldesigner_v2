<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 2/24/19
 * Time: 6:10 AM
 */

namespace App\Classes;


class Godaddy implements DomainInterface {

	private $host = 'https://api.ote-godaddy.com';
	private $key = '3mM44UZCV4ZDPM_6UR2oo3m44YZQ3kLupYUvT';
	private $secret = '6URtUsHAnpwCWWzjWihr87';

	public function __construct() {

		if ( app()->environment() == 'production' ) {

			$this->host = 'https://api.godaddy.com';
		}


	}

	public function checkDomain( $domain ) {


		$domain = explode( '.', $domain );

		if ( count( $domain ) != 2 ) {
			return false;
		}

		$tlds   = [ 'com', 'net', 'org' ];
		$tlds[] = urlencode( $domain[1] );

		$tlds = array_unique($tlds);

		$curl = curl_init();

		curl_setopt_array( $curl, array(
			CURLOPT_URL            => $this->host . "/v1/domains/suggest?query=" . urlencode( $domain[0] ) . "&sources=extension,keywordspin&tlds=".implode(',', $tlds)."&waitMs=50",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING       => "",
			CURLOPT_MAXREDIRS      => 10,
			CURLOPT_TIMEOUT        => 30,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST  => "GET",
			CURLOPT_HTTPHEADER     => array(
//				"cache-control: no-cache",
				"Authorization: sso-key {$this->key}:{$this->secret}",
			),
		) );

		$response = curl_exec( $curl );
		$err      = curl_error( $curl );

		curl_close( $curl );

		if ( $err ) {
			return false;
		}

		try {
			$result = json_decode( $response, 1 );

			return array_column($result, 'domain');
		} catch ( \Exception $e ) {
			return false;
		}
	}

	public function bookDomain( $domain ) {

		$curl = curl_init();

		$url = [
			"https://test.httpapi.com/api/domains/register.xml?auth-userid=764288",
			"api-key=c9Fcq7hOSfEZ1DOdOqaCHM94h9SPfFqe",
			"domain-name={$domain}",
			"years=1",
			"ns=ns1.domain.com",
//			"ns=ns.eldesigners.com", //ns=ns1.domain.com",
//			"ns=".env('IP_ADDRESS'), //""ns=ns2.domain.com",
			"customer-id=19836574",
			"reg-contact-id=81929288",
			"admin-contact-id=81929288",
			"tech-contact-id=81929288",
			"billing-contact-id=81929288",
			"invoice-option=KeepInvoice"
		];


		curl_setopt_array( $curl, array(
			CURLOPT_URL            => implode( '&', $url ),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING       => "",
			CURLOPT_MAXREDIRS      => 10,
			CURLOPT_TIMEOUT        => 30,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST  => "POST",
			CURLOPT_HTTPHEADER     => [
				"cache-control: no-cache"
			],
		) );

		$response = curl_exec( $curl );
		$err      = curl_error( $curl );

		curl_close( $curl );

		if ( $err ) {
			return false;
		}

		try {

			$xml    = simplexml_load_string( $response );
			$objArr = [];
			if ( property_exists( $xml, "hashtable" ) and property_exists( $xml->hashtable, "string" ) ) {
				for ( $i = 0; $i < count( $xml->hashtable->string ); $i = $i + 2 ) {
					$objArr[ (string) $xml->hashtable->string[ $i ] ] = (string) $xml->hashtable->string[ $i + 1 ];
				}

				return $objArr;
			}

			return false;
		} catch ( \Exception $e ) {
			return false;
		}
	}
}