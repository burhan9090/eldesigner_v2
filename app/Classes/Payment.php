<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 3/7/19
 * Time: 4:15 PM
 */

namespace App\Classes;

use App\StsPayment;

class Payment {
	private $key = 'payment-data';
	private $successRoute = 'payment.result';
	private $stsPayment = 'https://srstaging.stspayone.com/SmartRoutePaymentWeb/SRPayMsgHandler';
	private $ItemID = "337";
	private $MerchantID = "2000000035";
	private $AuthenticationToken = 'M2I5MWRkYWI5MTNhMDYzNzc2ZWM2ZDQ4';
	private $model = false;
	public function __construct() {
		session_start();
	}


	public function create( $amount, $descriptionPayment, $successCallBackRoute = null, $failedCallBackRoute = null,$aAdditionalParams = null ) {
		$this->flushData();

		if ( ! $amount ) {
			return false;
		}

		$this->setData( [
			'order_id'    => auth()->id() . "t" . time(),
			'amount'      => $amount,
			'description' => $descriptionPayment,
			'success'     => $successCallBackRoute ?: $this->successRoute,
			'failed'      => $failedCallBackRoute ?: ( $successCallBackRoute ?: $this->successRoute ),
            'additional_params' => is_array($aAdditionalParams) ? json_encode($aAdditionalParams) : '',
		] );

		return redirect()->route( 'payment.form' );
	}

	public function process() {
		$data = $this->getData();
		if ( ! $data ) {
			return false;
		}

		$this->model = StsPayment::create( [
			'user_id'             => auth()->id(),
			'transaction_id'      => $this->getDataItem( 'order_id' ),
			'amount'              => $this->getDataItem( 'amount' ),
			'payment_description' => $this->getDataItem( 'description' ),

			'route_success' => $this->getDataItem( 'success' ),
			'route_failed'  => $this->getDataItem( 'failed' ),


			'secure_hash'                => request()->get( 'Response_SecureHash' ),
			'card_holder_name'           => request()->get( 'Response_CardHolderName' ),
			'card_expiry_date'           => request()->get( 'Response_CardExpiryDate' ),
			'card_number'                => request()->get( 'Response_CardNumber' ),
			'approval_code'              => request()->get( 'Response_ApprovalCode' ),
			'currency_'                  => request()->get( 'Response_CurrencyISOCode' ),
			'status_code'                => request()->get( 'Response_StatusCode' ),
			'status_description'         => request()->get( 'Response_StatusDescription' ),
			'gateway_status_code'        => request()->get( 'Response_GatewayStatusCode' ),
			'gateway_status_description' => request()->get( 'Response_GatewayStatusDescription' ),
			'rrn'                        => request()->get( 'Response_RRN' ),
			'gateway_name'               => request()->get( 'Response_GatewayName' ),

			'full_request' => request()->all()
		] );


		$this->setDataItem( 'model_id', $this->model->id );
	}

	public function data() {
		$data = $this->getData();
		if ( ! $data ) {
			return false;
		}

		$payload = [
			"ItemID"             => $this->ItemID,
			"MessageID"          => "1",
			"Channel"            => "0",
			"Version"            => "1.0",
			"TransactionID"      => $this->getDataItem('order_id'),
			"MerchantID"         => $this->MerchantID,
			"Amount"             => intval( $this->getDataItem( 'amount' ) * 100 ),
			"Language"           => "en",
			"CurrencyISOCode"    => "400",
			"PaymentMethod"      => "1",
			"ResponseBackURL"    => route( 'payment.response' ),
			"PaymentDescription" => trim( substr( preg_replace( '/[\n\s]+/', '+', $this->getDataItem( 'description' ) ), 0, '100' ), '+' )
		];

		ksort( $payload );

		$payload["SecureHash"] = hash( "sha256", $this->AuthenticationToken . implode( '', $payload ) );

		$payload["PaymentDescription"] = str_replace( '+', ' ', $payload["PaymentDescription"] );

		return $payload;
	}

	public function getPaymentLink() {
		return $this->stsPayment;
	}

	public function isSuccess() {
		if ( $this->getModel() ) {
			return $this->getModel()->status_code == 0;
		}

		return false;
	}

	public function isFail() {
		return ! $this->isSuccess();
	}

	public function getResponseMessage() {
		if ( $this->getModel() ) {
			return $this->getModel()->status_description;
		}

		return '';
	}

	public function getRoute() {
		if ( $this->isSuccess() ) {
			return $this->getModel()->route_success;
		}

		return $this->getModel()->route_failed;
	}

	private function setData( $data ) {
		$_SESSION[$this->key] = $data;
	}

	private function getData() {
		return $_SESSION[$this->key];
	}

	public function setDataItem( $label, $value ) {
		$data = $_SESSION[$this->key];

		$data[ $label ] = $value;

		$this->setData( $data );
	}

	public function getDataItem( $label, $default = '' ) {
		$data = $_SESSION[$this->key];

		return isset( $data[ $label ] ) ? $data[ $label ] : $default;
	}

	public function getModel() {
		if ( ! $this->model ) {
			$this->model = StsPayment::find( $this->getDataItem( 'model_id' ) );
		}

		return $this->model;
	}

	public function flushData() {
		$_SESSION[$this->key]=[];
	}
}