<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 2/24/19
 * Time: 6:11 AM
 */

namespace App\Classes;


interface DomainInterface {

	public function checkDomain( $domain );

	public function bookDomain($domain);

}