<?php
/**
 * Created by PhpStorm.
 * User: Abdelqader Osama
 * Date: 2/24/19
 * Time: 6:10 AM
 */

namespace App\Classes;


class Menagator implements DomainInterface {

	public function checkDomain( $domain ) {


		$domain = explode( '.', $domain );

		if ( count( $domain ) != 2 ) {
			return false;
		}

		$curl = curl_init();

		curl_setopt_array( $curl, array(
			CURLOPT_URL            => "https://test.httpapi.com/api/domains/v5/suggest-names.json?auth-userid=764288&api-key=lg8zMX9OcoKChIUVhgRY50WFIwBs5k2V&keyword=" . urlencode( $domain[0] ) . "&tlds=" . urlencode( $domain[1] ) . "&add-related=true&no-of-results=10&hyphen-allowed=true",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING       => "",
			CURLOPT_MAXREDIRS      => 10,
			CURLOPT_TIMEOUT        => 30,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST  => "GET",
			CURLOPT_HTTPHEADER     => array(
				"cache-control: no-cache"
			),
		) );

		$response = curl_exec( $curl );
		$err      = curl_error( $curl );

		curl_close( $curl );

		if ( $err ) {
			return false;
		}

		try {

			$result = [];
			foreach (json_decode( $response, 1 ) as $domain=>$value) {
				if($value['status']== 'available') {
					$result[]= $domain;
				}
			}

			return $result;

		} catch ( \Exception $e ) {
			return false;
		}
	}

	public function bookDomain( $domain ) {

		$curl = curl_init();

		$url = [
			"https://test.httpapi.com/api/domains/register.xml?auth-userid=764288",
			"api-key=lg8zMX9OcoKChIUVhgRY50WFIwBs5k2V",
			"domain-name={$domain}",
			"years=1",
			"ns=ns1.domain.com",
//			"ns=ns.eldesigners.com", //ns=ns1.domain.com",
//			"ns=".env('IP_ADDRESS'), //""ns=ns2.domain.com",
			"customer-id=19836574",
			"reg-contact-id=82706389",
			"admin-contact-id=82706389",
			"tech-contact-id=82706389",
			"billing-contact-id=82706389",
			"invoice-option=KeepInvoice"
		];


		curl_setopt_array( $curl, array(
			CURLOPT_URL            => implode('&', $url),
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING       => "",
			CURLOPT_MAXREDIRS      => 10,
			CURLOPT_TIMEOUT        => 30,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST  => "POST",
			CURLOPT_HTTPHEADER     => [
				"cache-control: no-cache"
			],
		) );

		$response = curl_exec( $curl );
		$err      = curl_error( $curl );

		curl_close( $curl );

		if ( $err ) {
			return false;
		}

		try {

			$xml=simplexml_load_string($response);
			$objArr = [];
			if(property_exists($xml, "hashtable") and property_exists($xml->hashtable, "string")){
				for($i=0; $i<count($xml->hashtable->string); $i=$i+2){
					$objArr[(string) $xml->hashtable->string[$i]] = (string)$xml->hashtable->string[$i+1];
				}

				return $objArr;
			}

			return false;
		} catch ( \Exception $e ) {
			return false;
		}
	}
}