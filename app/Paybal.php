<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
class Paybal extends Model
{
    private $model = false;
    private $key = 'paypal-data';
    protected $table = 'paybal';
    protected $fillable = [
        'user_id',
        'bundle',
        'paybal_order_id',
        'paybal_status',
        'paybal_payer_id',
        'paybal_payer_email',
        'paybal_payer_name',
        'paybal_payer_surname',
        'paybal_payer_phone_number',
        'paybal_payments_id',
        'paybal_payments_amount',
        'paybal_payments_currency_code',
        'paybal_payments_create_time',
        'paybal_payments_update_time'
    ];
    public function __construct(array $attributes = []) {
        parent::__construct($attributes);
        if(!isset($_SESSION))
        {
            session_start();
        }

    }
    public function processPaypal( $bundleName,$amount,$sSecureHashed, $descriptionPayment ) {
        $this->flushData();

        if ( ! $amount ) {
            return false;
        }

        $this->setData( [
            'amount'      => $amount,
            'description' => $descriptionPayment,
            'bundle_name' => $bundleName,
            'secure_hashed' => $sSecureHashed,
        ] );

        return redirect()->route( 'payment.form' );
    }
    private function setData( $data ) {
        $_SESSION[$this->key] = $data;
    }
    public function shapePaypalResponse($aData){
        $aShapedData = [];
        if (isset($aData['id'])){
            $aShapedData['paybal_order_id'] = $aData['id'];
        }
        if (isset($aData['status'])){
            $aShapedData['paybal_status'] = $aData['status'];
        }
        if(isset($aData['payer'])){
            if (isset($aData['payer']['payer_id'])){
                $aShapedData['paybal_payer_id'] = $aData['payer']['payer_id'];
            }
            if (isset($aData['payer']['email_address'])){
                $aShapedData['paybal_payer_email'] = $aData['payer']['email_address'];
            }
            if (isset($aData['payer']['name']) && isset($aData['payer']['name']['given_name']) ){
                $aShapedData['paybal_payer_name'] = $aData['payer']['name']['given_name'];
            }
            if (isset($aData['payer']['name']) && isset($aData['payer']['name']['surname']) ){
                $aShapedData['paybal_payer_surname'] = $aData['payer']['name']['surname'];
            }
            if (isset($aData['payer']['phone']) && isset($aData['payer']['phone']['phone_number']) ){
                if(isset($aData['payer']['phone']['phone_number']['national_number'])){
                    $aShapedData['paybal_payer_phone_number'] = $aData['payer']['phone']['phone_number']['national_number'];
                }

            }
        }
        if(isset($aData['purchase_units']) && isset($aData['purchase_units'][0])){
            $aPurchase_units = $aData['purchase_units'][0];
            if (isset($aPurchase_units['payments']) && isset($aPurchase_units['payments']['captures']) && isset($aPurchase_units['payments']['captures'][0])){
                $aPaymentsData = $aPurchase_units['payments']['captures'][0];
                if(isset($aPaymentsData['id'])){
                    $aShapedData['paybal_payments_id'] = $aPaymentsData['id'];
                }
                if(isset($aPaymentsData['amount'])){
                    if(isset($aPaymentsData['amount']['value'])){
                        $aShapedData['paybal_payments_amount'] = $aPaymentsData['amount']['value'];
                    }
                    if(isset($aPaymentsData['amount']['currency_code'])){
                        $aShapedData['paybal_payments_currency_code'] = $aPaymentsData['amount']['currency_code'];
                    }

                }
                if (isset($aPaymentsData['create_time'])){
                    $aShapedData['paybal_payments_create_time'] = $aPaymentsData['create_time'];
                }
                if (isset($aPaymentsData['create_time'])){
                    $aShapedData['paybal_payments_update_time'] = $aPaymentsData['update_time'];
                }

            }
        }

        return $aShapedData;

    }
    public  function savePayPal($aData){
        $this->model = Paybal::create($aData);
        $this->setDataItem( 'id', $this->model->id );
    }
    public function isSuccess() {
        if ( $this->getModel() ) {
            return $this->getModel()->paybal_status == 'COMPLETED';
        }

        return false;
    }
    public function getDataItem( $label, $default = '' ) {
        $data = $_SESSION[$this->key];

        return isset( $data[ $label ] ) ? $data[ $label ] : $default;
    }
    public function getModel() {
        if ( ! $this->model ) {
            $this->model = Paybal::find( $this->getDataItem( 'id' ) );
        }

        return $this->model;
    }

    public function setDataItem( $label, $value ) {
        $data = $_SESSION[$this->key];

        $data[ $label ] = $value;

        $this->setData( $data );
    }
    public function flushData() {
        $_SESSION[$this->key]=[];
    }
    public function getData() {
        return $_SESSION[$this->key];
    }


}
