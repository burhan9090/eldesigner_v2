<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserSkill
 * @package App
 *
 * @mixin  \Eloquent
 */
class UserSkill extends Model
{
	public $timestamps = false;
    protected $fillable = ['user_id', 'skills'];
}
