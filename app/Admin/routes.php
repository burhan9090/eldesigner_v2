<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {

    $router->get('/', 'HomeController@index');
    $router->resource('users/index', UsersController::class);
    $router->resource('users/articles', ArticlesController::class);
    $router->resource('users/projects', ProjectsController::class);
    $router->resource('users/permissions', PermissonsController::class);
    $router->resource('users/project/images', ProjectImagesController::class);
});
