<?php

namespace App\Admin\Controllers;

use App\Plans;
use \App\UserPermission;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class PermissonsController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new UserPermission);

        $grid->id('Id');
        $grid->user_id('User id');
        $grid->plans_id('Plans id');
        $grid->expiration_timestamp('Expiration timestamp');
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');
        $grid->is_deleted('Is deleted');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(UserPermission::findOrFail($id));

        $show->id('Id');
        $show->user_id('User id');
        $show->plans_id('Plans id');
        $show->expiration_timestamp('Expiration timestamp');
        $show->created_at('Created at');
        $show->updated_at('Updated at');
        $show->is_deleted('Is deleted');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new UserPermission);

        if(request('user_id' , false)){
            $form->hidden('user_id')->default(request('user_id'));
        }else{
            $form->number('user_id', 'User id');
        }
        $form->select('plans_id', 'Plans id')->options(
            Plans::all()->pluck('plans_name' , 'plans_id')
        )->rules('required');
        $form->date('expiration_timestamp', 'Expiration timestamp')->rules('required');
        $form->hidden('is_deleted')->default(0);
        $form->saving(function ($data){
            $data->expiration_timestamp = strtotime($data->expiration_timestamp);
        });
        return $form;
    }

    public function destroy($id)
    {
        if ($this->form()->model()->where('id' , $id)->update(array('is_deleted' => 1))) {

            $data = [
                'status'  => true,
                'message' => trans('admin.delete_succeeded'),
            ];
        } else {
            $data = [
                'status'  => false,
                'message' => trans('admin.delete_failed'),
            ];
        }

        return response()->json($data);
    }
}
