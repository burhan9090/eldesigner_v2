<?php

namespace App\Admin\Controllers;

use \App\WebsiteArticles;
use App\Http\Controllers\Controller;
use App\WebsiteArticlesCategory;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ArticlesController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new WebsiteArticles);

        $grid->id('Id');
        $grid->title('Title');
        $grid->repost('Repost');
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');
        $grid->optimized('Optimized');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(WebsiteArticles::findOrFail($id));

        $show->id('Id');
        $show->user_id('User id');
        $show->image('Image');
        $show->title('Title');
        $show->category_id('Category id');
        $show->body('Body');
        $show->repost('Repost');
        $show->created_at('Created at');
        $show->updated_at('Updated at');
        $show->optimized('Optimized');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new WebsiteArticles);
        if(request('user_id' , false)){
            $form->hidden('user_id')->default(request('user_id'));
        }else{
            $form->number('user_id');
        }
        $form->image('image', 'Image')->move('uploads/article/'.request('project_id'))->uniqueName()->required();
        $form->text('title', 'Title');
        $categories = array();
        foreach (WebsiteArticlesCategory::where('category_id' , 0)->with('children')->get() as $cat  ){
            $categories[] = [
                'label' => $cat->text,
                'options' => $cat->children->pluck('text' , 'id')
            ];
        }
        $form->select('category_id', 'Category')->groups($categories)->required();
        $form->editor('body', 'Body')->required();
        $form->hidden('repost')->default(0);
        $form->hidden('optimized')->default(0);

        return $form;
    }
}
