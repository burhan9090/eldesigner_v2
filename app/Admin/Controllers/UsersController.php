<?php

namespace App\Admin\Controllers;

use \App\User;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class UsersController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new User);

        $grid->id('Id');
//        $grid->column('logo' , 'Logo')->display(function ($image){
//            return '<img src="'.asset($image).'">';
//        });
//        $grid->cover('Cover');
        $grid->name('Name');
        $grid->email('Email');
        $grid->user_name('User name');
        $grid->created_at('Created at');
        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(User::findOrFail($id));

        $show->id('Id');
        $show->name('Name');
        $show->email('Email');
        $show->password('Password');
        $show->remember_token('Remember token');
        $show->created_at('Created at');
        $show->updated_at('Updated at');
        $show->user_name('User name');
        $show->logo('Logo');
        $show->cover('Cover');
        $show->about('About');
        $show->fb_link('Fb link');
        $show->ig_link('Ig link');
        $show->tw_link('Tw link');
        $show->pin_link('Pin link');
        $show->lang_code('Lang code');
        $show->profile_type('Profile type');
        $show->user_type('User type');
        $show->phone('Phone');
        $show->last_name('Last name');
        $show->website('Website');
        $show->bod('Bod');
        $show->country_id('Country id');
        $show->city_id('City id');
        $show->description('Description');
        $show->occupation('Occupation');
        $show->domain('Domain');
        $show->follow_setting('Follow setting');
        $show->profile_count('Profile count');
        $show->profile_setting('Profile setting');
        $show->email_notification('Email notification');
        $show->watermark_text_active('Watermark text active');
        $show->watermark_text('Watermark text');
        $show->watermark_file_active('Watermark file active');
        $show->watermark_file('Watermark file');
        $show->gender('Gender');
        $show->theme_name('Theme name');
        $show->theme_setting('Theme setting');
        $show->optimized_logo('Optimized logo');
        $show->optimized_cover('Optimized cover');
        $show->has_quiz('Has quiz');
        $show->user_hash('User hash');
        $show->code('Code');
        $show->domain_created_at('Domain created at');
        $show->domain_renewed_at('Domain renewed at');
        $show->domain_renew_at('Domain renew at');

        $show->permissions('plan type' , function (\Encore\Admin\Grid  $plan){

            $plan->setResource('/admin/users/permissions');
            $plan->disableTools(true);
            $plan->actions(function (\Encore\Admin\Grid\Displayers\Actions $action){
                $action->disableEdit();
                $action->disableView();
            });
            $plan->plan()->plans_name('Plan Name');
            $plan->expiration_timestamp('Expire At')->display(function ($val){
                return date('Y-m-d' , $val);
            });
        });


        $show->articles('User Articles' , function (\Encore\Admin\Grid  $article){

            $article->setResource('/admin/users/articles');
//            $plan->disableTools(true);
//            $plan->actions(function (\Encore\Admin\Grid\Displayers\Actions $action){
//                $action->disableEdit();
//                $action->disableView();
//            });
            $article->image('Article Cover')->display(function ($cover){
                return '<img src="'.asset($cover).'" style = "max-height:200;max-width:300">';
            });
            $article->title('Title');
            $article->created_at('Created At');
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new User);

        $form->text('name', 'Name');
        $form->email('email', 'Email');
        $form->text('user_name', 'User name');
        $form->text('logo', 'Logo');
        $form->image('cover', 'Cover');
        $form->text('about', 'About');
        $form->text('fb_link', 'Fb link');
        $form->text('ig_link', 'Ig link');
        $form->text('tw_link', 'Tw link');
        $form->text('pin_link', 'Pin link');
        $form->text('lang_code', 'Lang code')->default('en');
        $form->text('profile_type', 'Profile type');
        $form->text('user_type', 'User type');
        $form->mobile('phone', 'Phone');
        $form->text('last_name', 'Last name');
        $form->text('website', 'Website');
        $form->date('bod', 'Bod')->default(date('Y-m-d'));
        $form->number('country_id', 'Country id');
        $form->number('city_id', 'City id');
        $form->textarea('description', 'Description');
        $form->text('occupation', 'Occupation');
        $form->text('domain', 'Domain');
        $form->text('follow_setting', 'Follow setting')->default('public');
        $form->number('profile_count', 'Profile count');
        $form->text('profile_setting', 'Profile setting')->default('public');
        $form->switch('email_notification', 'Email notification')->default(1);
        $form->switch('watermark_text_active', 'Watermark text active');
        $form->text('watermark_text', 'Watermark text');
        $form->switch('watermark_file_active', 'Watermark file active');
        $form->text('watermark_file', 'Watermark file');
        $form->text('gender', 'Gender')->default('unknown');
        $form->text('theme_name', 'Theme name');
        $form->text('theme_setting', 'Theme setting');
        $form->switch('optimized_logo', 'Optimized logo');
        $form->switch('optimized_cover', 'Optimized cover');
        $form->switch('has_quiz', 'Has quiz');
        $form->text('user_hash', 'User hash');
        $form->text('code', 'Code')->default('de-rgg-01');
        $form->date('domain_created_at', 'Domain created at')->default(date('Y-m-d'));
        $form->date('domain_renewed_at', 'Domain renewed at')->default(date('Y-m-d'));
        $form->date('domain_renew_at', 'Domain renew at')->default(date('Y-m-d'));


        return $form;
    }
}
