<?php

namespace App\Admin\Controllers;

use \App\WebsiteProject;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ProjectsController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new WebsiteProject);

        $grid->id('Id');
        $grid->user()->name('user name');
        $grid->title('project title');
        $grid->category()->service_name('category');
        $grid->created_at('Created at');
        $grid->updated_at('Updated at');
        $grid->repost('Repost');
        $grid->project_count('Project count');
        $grid->is_demo('Is demo');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(WebsiteProject::findOrFail($id));

        $show->id('Id');
        $show->user_id('User id');
        $show->title('Title');
        $show->description('Description');
        $show->created_at('Created at');
        $show->updated_at('Updated at');
        $show->repost('Repost');
        $show->project_count('Project count');
        $show->category_id('Category id');
        $show->is_demo('Is demo');
        $show->images('project images' , function ($image){
            $image->setResource('/admin/users/project/images');
            $image->actions(function (\Encore\Admin\Grid\Displayers\Actions $action){
                $action->disableView();
            });
            $image->image('image')->display(function ($img){
                return '<img src="'.asset($img).'" style = "max-height:200px;max-width:300px;">';
            });
            $image->description('description');
        });

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new WebsiteProject);

        $form->number('user_id', 'User id');
        $form->text('title', 'Title');
        $form->editor('description', 'Description');
        $form->switch('repost', 'Repost')->default(1);
        $form->number('project_count', 'Project count');
        $form->number('category_id', 'Category id');
        $form->switch('is_demo', 'Is demo');

        return $form;
    }
}
