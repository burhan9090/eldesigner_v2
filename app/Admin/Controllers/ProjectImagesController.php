<?php

namespace App\Admin\Controllers;

use App\WebsiteProject;
use \App\WebsiteProjectImage;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\HasResourceActions;
use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Layout\Content;
use Encore\Admin\Show;

class ProjectImagesController extends Controller
{
    use HasResourceActions;

    /**
     * Index interface.
     *
     * @param Content $content
     * @return Content
     */
    public function index(Content $content)
    {
        return $content
            ->header('Index')
            ->description('description')
            ->body($this->grid());
    }

    /**
     * Show interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function show($id, Content $content)
    {
        return $content
            ->header('Detail')
            ->description('description')
            ->body($this->detail($id));
    }

    /**
     * Edit interface.
     *
     * @param mixed $id
     * @param Content $content
     * @return Content
     */
    public function edit($id, Content $content)
    {
        return $content
            ->header('Edit')
            ->description('description')
            ->body($this->form()->edit($id));
    }

    /**
     * Create interface.
     *
     * @param Content $content
     * @return Content
     */
    public function create(Content $content)
    {
        return $content
            ->header('Create')
            ->description('description')
            ->body($this->form());
    }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        $grid = new Grid(new WebsiteProjectImage);

        $grid->id('Id');
        $grid->project_id('Project id');
        $grid->image('Image');
        $grid->description('Description');
        $grid->optimized_and_watermarked('Optimized and watermarked');

        return $grid;
    }

    /**
     * Make a show builder.
     *
     * @param mixed $id
     * @return Show
     */
    protected function detail($id)
    {
        $show = new Show(WebsiteProjectImage::findOrFail($id));

        $show->id('Id');
        $show->project_id('Project id');
        $show->image('Image');
        $show->description('Description');
        $show->optimized_and_watermarked('Optimized and watermarked');

        return $show;
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        $form = new Form(new WebsiteProjectImage);
        if(request('project_id' , false)){
            $form->hidden('project_id')->default(request('project_id'));
        }else{
            $form->number('project_id', 'Project id');
        }
        $form->image('image', 'Image')->move('uploads/project/'.WebsiteProject::find(request('project_id'))->user_id.'/'.request('project_id'))->required();
        $form->textarea('description', 'Description')->required();
        $form->hidden('optimized_and_watermarked')->default(0);

        return $form;
    }
}
