<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WebsiteArticles
 * @package App
 *
 * @mixin \Eloquent
 */
class WebsiteArticles extends Model {

	protected $fillable = [ "user_id", "image", "title", "category_id", "body", "repost","optimized" ];

	public function category() {
		return $this->belongsTo(WebsiteArticlesCategory::class, 'category_id', 'id');
	}

	public function user() {
		return $this->belongsTo(User::class, 'user_id', 'id');
	}

	public function timeline() {
		return $this->hasOne(WebsiteArticleTimeLine::class, 'article_id', 'id');
	}

	public function tags() {
		return $this->belongsToMany(Tag::class, 'website_articles_tags', 'article_id', 'tag_id');
	}
}
