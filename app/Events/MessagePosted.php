<?php

namespace App\Events;

use App\Message;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class MessagePosted implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $message_id;
    public $oMessage;
    public $user;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($message,User $user)
    {
        $this->user = $user;
        $this->oMessage = Message::with('user', 'user_to','attachments')->where(function ($query) use ($message,$user) {
            $query->where('user_id', '=', $user->id)->where('id', '=', $message);
        })->get()->toArray();
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        Log::info('Chat.'.$this->oMessage[0]['user_id_to']);
        return new PresenceChannel('Chat.'.$this->oMessage[0]['user_id_to']);
    }
}
