<?php

namespace App\Events;

use App\Comment;
use App\Comment_reply;
use App\Post;
use App\Notification;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Support\Facades\Log;

class NotificationEvent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $user;
    public $post;
    public $commentedPost;
    public $nameOfUser;
    public $postOriginalUserId;
    public $text;
    public $resource_id;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($oUser,$oPostId,$type)
    {
        Log::info('Payload :'.json_encode($oPostId));
        switch ($type){
            case 'follow' :
                $this->postOriginalUserId = $oPostId;
                $this->resource_id = $oUser->id;
                break;
            case 'replies':
                $oComment = Comment::where([['id','=',$oPostId->comment_id]])->get()->first();
                $this->postOriginalUserId = $oComment->user_id;
                $this->resource_id = $oComment->resource_id;
                break;
            case 'comment' :
                $this->postOriginalUserId = Post::where([['id','=',$oPostId->resource_id]])->get()->first()->user_id;
                $this->resource_id = $oPostId->resource_id;
            case 'love':
                if($oPostId->resource_type == 'post'){
                    $this->postOriginalUserId = Post::where([['id','=',$oPostId->resource_id]])->get()->first()->user_id;
                    $this->resource_id = $oPostId->resource_id;
                }else if($oPostId->resource_type == 'comment'){
                    $oComment = Comment::where([['id','=',$oPostId->resource_id]])->get()->first();
                    $this->postOriginalUserId = $oComment->user_id;
                    $this->resource_id = $oComment->resource_id;
                }
                else if($oPostId->resource_type == 'reply'){
                    $oCommentR = Comment_reply::where([['id','=',$oPostId->resource_id]])->get()->first();
                    $oComment = Comment::where([['id','=',$oCommentR->comment_id]])->get()->first();
                    $this->postOriginalUserId = $oCommentR->user_id;
                    $this->resource_id = $oComment->resource_id;
                }
        }

        $this->nameOfUser = $oUser->name;
        $this->text = $this->selectText($type);
        if($type == 'love'){
            $this->text .= ' '.ucfirst($oPostId->resource_type);
        }
        if($this->postOriginalUserId != $oUser->id) {
            Notification::create([
                'user_id' => $this->postOriginalUserId,
                'user_id_from' => $oUser->id,
                'notification_type' => $type,
                'notification_text' => $this->text,
                'is_seen' => false,
                'resource_id' => $this->resource_id,
            ]);
        }
    }
    public function selectText($type){
        switch ($type){
            case 'comment' :
                $type = 'Commented on your post';
                break;
            case 'replies' :
                $type = 'Replied to your comment';
                break;
            case 'love' :
                $type = 'Loved Your';
                break;
            case 'follow' :
                $type = 'Is Now Following You';

        }
        return $type;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new PresenceChannel('Notification.'.$this->postOriginalUserId);
    }
}
