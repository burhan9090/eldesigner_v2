<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

/**
 * Class Post
 * @package App
 * @mixin \Eloquent
 */
class Post extends Model
{
    /**
     * Get the comments for the post.
     */
    protected $fillable = [
        'user_id', 'type','type_id','post_text'
    ];
    public function comments($post_id)
    {
        return Comment::where([
            'resource_id' => $post_id,
            'resource_type' => 'post',
        ])->get();
    }
    public function medias($post_id)
    {
        return Media::where([
            'resource_id' => $post_id,
            'resource_type' => 'post',
        ])->get();
    }
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public static function addPostByType($sType,$typeId,$text){
        $post =     Post::create([
            'user_id' => Auth::user()->id,
            'post_text' => $text,
            'type' => $sType,
            'type_id' => $typeId,
        ]);
        if($post){
            $sCopayToFilePath = base_path().'/public/user_media/';
            if ($sType == 'article'){
                $oArticles = WebsiteArticles::Where('id',$typeId)->get()->first();
                if($oArticles){
                    $sFilePath = base_path() . '/public/' . $oArticles->image;
                    $fileExplod = explode('/',$oArticles->image);
                    $namePosition = count($fileExplod);
                    $imageName =  time().'_'.$fileExplod[($namePosition - 1)];
                    $fileCopied = \File::copy($sFilePath,$sCopayToFilePath.$imageName);
                    if($fileCopied){
                        UserMedia::create([
                            'resource_id' => $post->id,
                            'resource_type' => 'post',
                            'file_name' => $imageName,
                            'media_type' => 'image',
                        ]);
                    }

                }
            }else if($sType == 'project'){
                $oProjectImages = WebsiteProjectImage::Where('project_id',$typeId)->get()->toArray();
                $patchInsertImage = [];
                if(!empty($oProjectImages)){
                    foreach ($oProjectImages as $image){
                        $sFilePath = base_path() . '/public/' . $image['image'];
                        $fileExplod = explode('/',$image['image']);
                        $namePosition = count($fileExplod);
                        $imageName =  time().'_'.$fileExplod[($namePosition - 1)];
                        $fileCopied = \File::copy($sFilePath,$sCopayToFilePath.$imageName);
                        if($fileCopied){
                            $patchInsertImage[] = [
                                'resource_id' => $post->id,
                                'resource_type' => 'post',
                                'file_name' => $imageName,
                                'media_type' => 'image',
                            ];
                        }
                    }
                    if(!empty($patchInsertImage)){
                        DB::table('user_media')->insert($patchInsertImage);
                    }
                }
            }
        }
    }
    public static function deletePostByType($sType,$iTypeId){
        Log::info('First inter '.$sType);
        $oPost = Post::Where('type',$sType)->where('type_id',$iTypeId)->get()->first();
        if($oPost){
            Log::info('found the post');
//            first delete all images
            $aPostUserMedia = UserMedia::Where('resource_id',$oPost->id)->get();
            if($aPostUserMedia){
                $aFileToDelete = [];
                $idsToDelete = [];
                $sFilePath = base_path().'/public/user_media/';
                foreach ($aPostUserMedia as $userMedia){
                    $aFileToDelete[] = $sFilePath.$userMedia->file_name;
                    $idsToDelete[] = $userMedia->id;
                }
                if(!empty($aFileToDelete)){
                    Log::info('Deleation Files'.json_encode($aFileToDelete));

                    \File::delete($aFileToDelete);
                    if(!empty($idsToDelete)) {
                        Log::info('Deleation UserMedia Db '.json_encode($idsToDelete));
                        DB::table('user_media')->whereIn('id', $idsToDelete)->delete();
                    }
                }

            }

            //delete comment and comment replies
            $oComments = Comment::Where('resource_id',$oPost->id)->get()->toArray();
            $commentIdToDelete = [];
            $aRepliesCommentDelete = [];
            if(!empty($oComments)){
                foreach ($oComments as $comment){
                    $commentIdToDelete [] = $comment['id'];
                    $oCommentReplies = Comment_reply::select('id')->where('comment_id',$comment['id'])->get()->toArray();
                    if(!empty($oCommentReplies)){
                        foreach ($oCommentReplies as $commentsReplies)
                        $aRepliesCommentDelete[] = $commentsReplies['id'];

                    }
                }
            }
            if(!empty($aRepliesCommentDelete)){
                Log::info('Deleation comment_replies Db '.json_encode($aRepliesCommentDelete));
                DB::table('comment_replies')->whereIn('id', $aRepliesCommentDelete)->delete();
            }
            if(!empty($commentIdToDelete)){
                Log::info('Deleation comments Db '.json_encode($commentIdToDelete));
                DB::table('comments')->whereIn('id', $commentIdToDelete)->delete();
            }

            //finally delete the post
            $oPost->delete();
            Log::info('POST DELEATION');


        }
    }
}
