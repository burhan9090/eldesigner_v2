<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment_reply extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'comment_replies';

    public function comment(){
        return $this->belongsTo(Comment::class,'comment_id');
    }
}
