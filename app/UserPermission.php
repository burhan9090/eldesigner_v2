<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 2019-03-04
 * Time: 00:11
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class UserPermission extends Model
{
    protected $table = 'user_permission';
    protected $fillable = ['user_id', 'plans_id','expiration_timestamp','is_deleted'];
    public static function checkPermission($iOptionName,$iUserId = null){

        $bAutraized = false;
        if ($iUserId === null){
            $iUserId = Auth::user()->id;
        }

        $userPermission = UserPermission::Where('user_id',$iUserId)->where('is_deleted',false)->get()->first();

        if($userPermission){

            $userPlan = $userPermission->plans_id;

            $planExpirationTimeStamp = $userPermission->expiration_timestamp;

            if(!empty($planExpirationTimeStamp) && time() < $planExpirationTimeStamp ){
                $currentOption = PlansOptions::Where('plans_options_name',$iOptionName)->get()->first();
                if($currentOption) {
                    $selectedPlans = PlansOptionsSelected::where('plans_id', $userPlan)->where('plans_options_id', $currentOption->plans_options_id)->get()->first();
                   if($selectedPlans){
                       $bAutraized = true;
                   }
                }

            }
        }
        return $bAutraized;
    }
    public static function checkExist(){
        $bAutraized = false;
        $iUserId = Auth::user()->id;
        $userPermission = UserPermission::Where('user_id',$iUserId)->where('is_deleted',false)->get()->first();
        if($userPermission){
            $bAutraized = true;
        }
        return $bAutraized;
    }
    public static function checkProjectsAndArticles($isArticle = false){
        $period = 0;
        $iUserId = Auth::user()->id;
        $userPermission = UserPermission::Where('user_id',$iUserId)->where('is_deleted',false)->get()->first();
        if($userPermission){
            $userPlan = $userPermission->plans_id;
            $planExpirationTimeStamp = $userPermission->expiration_timestamp;
            if(!empty($planExpirationTimeStamp) && time() < $planExpirationTimeStamp ){
               if ($userPlan == 2 || $userPlan == 3 || $userPlan == 4){
                   $period = 'unlimited';
               }else if ($userPlan == 1){
                   if(!$isArticle){
                       $period = 40;
                   }else{
                       $period = 20;
                   }
               }
            }
        }
        if ($period == 0 ){
            if(Auth::user()->profile_type == 'company'){
                $period = 20;
            }else{
                $period = 15;
            }
        }
        return $period;
    }
    public function plan(){
       return $this->belongsTo(Plans::class , 'plans_id' , 'plans_id');
    }
}