<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Tag
 * @package App
 *
 * @mixin \Eloquent
 */
class Tag extends Model
{
	public $timestamps = false;
    protected $fillable = ["text"];
}
