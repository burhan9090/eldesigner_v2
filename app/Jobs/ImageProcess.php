<?php

namespace App\Jobs;

use App\UserMedia;
use App\UserPermission;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\App;
use Intervention\Image\ImageManager;
use SebastianBergmann\CodeCoverage\Report\PHP;

class ImageProcess implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $oUser;
    private $oImage;
    private $sFile;
    private $manager;
    private $sOperationType;
    private $iMinimumFileSize;
    private $sType;
    private $aNoSaveActions;
    public function __construct($oUser, $oImage, $sFile, $sOperationType,$sType='')
    {
        $this->oUser = $oUser;
        $this->oImage = $oImage;
        $this->sFile = $sFile;
        $this->sOperationType = $sOperationType;
        $this->iMinimumFileSize = 10; // Size in KB
        $this->sType = $sType;
        $this->aNoSaveActions = ['WELCOME_USER_PROFILE','WELCOME_LOGO','WELCOME_BG'];
    }
    private function getObjectByType(){
        switch ($this->sType){
            case 'USER_MEDIA' :
                $this->oImage = UserMedia::Where('resource_id',$this->oImage)->where('file_name',$this->sFile)->get()->first();
                if(empty($this->oImage)){
                    throw new \Exception("Object User Media Not Exist");
                }
                $this->sFile = 'user_media/'.$this->sFile;
                break;
            case 'COMMENT' :
                $this->sFile = 'user_media_comments/'.$this->sFile;
                break;
            case 'REPLIES' :
                $this->sFile = 'user_media_replies/'.$this->sFile;
                break;
        }
    }
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        print "=============================== Start Processing Image " . date('Y-m-d H:i:s') . " ===============================" . PHP_EOL;

        try {
            $this->manager = App::make('imagick');
            print "Operation Type : " . $this->sOperationType . PHP_EOL;
            switch ($this->sOperationType) {
                case 'OPTIMIZE|WATERMARK':
                    $this->processWaterMarkAndOptimization();
                    break;
                case 'OPTIMIZE' :
                    $this->getObjectByType();
                    $this->processOptimization();
                    break;
                default :
                    print "This Operation : " . $this->sOperationType . " Is Not Supported" . PHP_EOL;
                    break;
            }
            print "=============================== End Processing Image " . date('Y-m-d H:i:s') . " ===============================" . PHP_EOL;
        } catch (\Exception  $e) {
            print $e->getMessage();
        }

    }

    private function processOptimization()
    {
        $sFilePath = $this->getFilePath($this->sFile);
        $image = $this->manager->make($sFilePath);
        $fileSizeKb = (($image->filesize() / 1000));
        print "Image Size Before Processing : ".($fileSizeKb/1000).' MB'.PHP_EOL;
        $isSavedImage = false;
        $Qulity = $this->getBestQuality($fileSizeKb);
        if ($fileSizeKb > $this->iMinimumFileSize) {
            print "Image Saved  With The New Quality : ".$Qulity.PHP_EOL;
            $isSavedImage = $image->save($sFilePath, $Qulity);
            $fileAfter = $this->manager->make($sFilePath);
            print "Image Size After Processing : ".(($fileAfter->filesize()/1000)/1000).' MB'.PHP_EOL;
        }
        if ($isSavedImage && !in_array($this->sType,$this->aNoSaveActions)) {
            if($this->sType == 'USER_LOGO'){
                $this->oUser->optimized_logo = true;
                $this->oUser->save();
                print "Updating DATABASE USER_LOGO ".PHP_EOL;
            }elseif ($this->sType == 'USER_COVER'){
                $this->oUser->optimized_cover = true;
                $this->oUser->save();
                print "Updating DATABASE USER_COVER ".PHP_EOL;
            }else{
                $this->oImage->optimized = true;
                $this->oImage->save();
                print "Updating DATABASE".PHP_EOL;
            }

        }
    }

    private function processWaterMarkAndOptimization()
    {
        $sFilePath = $this->getFilePath($this->sFile);
        $isWaterMarkEdited = false;
        //create image object
        $image = $this->manager->make($sFilePath);
        $fileSizeKb = (($image->filesize() / 1000));
        print "Image Size Before Processing : ".(($fileSizeKb)/1000).' MB'.PHP_EOL;
        $isSavedImage = false;
        //if file size in kb greater than 1000 kb

        $Qulity = $this->getBestQuality($fileSizeKb);
        if ($this->oUser->watermark_text_active == true && !empty($this->oUser->watermark_text)) {
            print "Text WaterMark " . $this->oUser->watermark_text . PHP_EOL;
            $sWaterMarkText = $this->oUser->watermark_text;
            $sWaterMarkTextLength = strlen($sWaterMarkText);
            print "Image Width : ".$image->width().PHP_EOL;
            print "Image Height : ".$image->height().PHP_EOL;
            $IimageWidth = ($image->width() / 2);
            $IimageHeight = ($image->height() / 2);
            $sFont = $this->getFontPath();
            $image->text($sWaterMarkText, $IimageWidth, $IimageHeight, function ($font) use ($sFont) {
                $font->file($sFont);
                $font->size(300);
                $font->color(array(255, 255, 255, 0.4));
                $font->align('center');
                $font->valign('top');
                $font->angle(45);
            });
            $isWaterMarkEdited = true;
        }
        if ($this->oUser->watermark_file_active == true && !empty($this->oUser->watermark_file) &&  UserPermission::checkPermission('image_protection',$this->oUser->id)) {
            print "Start Image WaterMark".PHP_EOL;
            try {
                $sFilePathWaterMark = $this->getFilePath($this->oUser->watermark_file);
                $imageWaterMark = $this->manager->make($sFilePathWaterMark);
                $imageWaterMark->resize(150, 150);
                $image->insert($imageWaterMark, 'bottom-left', 10, 10);
                $isWaterMarkEdited = true;
            } catch (\Exception  $e) {
                print "++++++++++++++ Error While Processing WaterMark Images ++++++++++++++++";
                print $e->getMessage().PHP_EOL;
                print "++++++++++++++ Error While Processing WaterMark Images ++++++++++++++++";
            }
        }

        if ($fileSizeKb > $this->iMinimumFileSize) {
            print "Updating Image With Quality : ".$Qulity.PHP_EOL;
            $isSavedImage = $image->save($sFilePath, $Qulity);
            $fileAfter = $this->manager->make($sFilePath);
            print "Image Size After Processing : ".(($fileAfter->filesize()/1000)/1000).' MB'.PHP_EOL;
        } elseif ($isWaterMarkEdited) {
            print "Updating Image With WaterMark".PHP_EOL;
            $isSavedImage = $image->save($sFilePath);
        }
        if ($isSavedImage) {
            print "Updating optimized_and_watermarked".PHP_EOL;
            $this->oImage->optimized_and_watermarked = true;
            $this->oImage->save();
        }


    }

    public function getFontPath()
    {
        $sFontPath = base_path() . '/public/image_font/OpenSans-Regular.ttf';
        print "Font Path : " . $sFontPath . PHP_EOL;
        if (!file_exists($sFontPath)) {
            throw new \Exception("Font " . $sFontPath . " Not Exist");
        }
        return $sFontPath;
    }

    public function getFilePath($imageLink)
    {
        {
            $sFilePath = base_path() . '/public/' . $imageLink;
            print "File Path : " . $sFilePath . PHP_EOL;
            if (!file_exists($sFilePath)) {
                throw new \Exception("File " . $sFilePath . " Not Exist");
            }
            return $sFilePath;
        }
    }

    //quality of image depends on file size [0-100]
    public function getBestQuality($fileSizeKb)
    {
        $iQulitySize = 0;
        switch (true) {
            case ($fileSizeKb >= 1000 && $fileSizeKb <= 2000):
                $iQulitySize = 60;
                break;
            case ($fileSizeKb >= 2000 && $fileSizeKb <= 3000):
                $iQulitySize = 50;
                break;

            case ($fileSizeKb >= 3000 && $fileSizeKb <= 4000):
                $iQulitySize = 40;
                break;
            case ($fileSizeKb >= 4000 && $fileSizeKb <= 5000):
                $iQulitySize = 30;
                break;
            default :
                $iQulitySize = 40;
                break;

        }
        return $iQulitySize;
    }
}
