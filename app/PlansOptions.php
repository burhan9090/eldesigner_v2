<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 2019-03-04
 * Time: 00:24
 */

namespace App;

use Illuminate\Database\Eloquent\Model;
class PlansOptions extends Model{
    protected $table = 'plans_options';
    protected $fillable = ['plans_options_id', 'plans_options_name','plans_options_view_name'];

    public function plans_selected(){
        return $this->belongsTo(PlansOptionsSelected::class);
    }

}