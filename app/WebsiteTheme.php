<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteTheme extends Model {

	protected $fillable = [
		"name", "folder", "description", "type", "created_at", "updated_at"
	];

	public function getImage() {
		if(file_exists(public_path("themes/{$this->folder}/config.json"))){
			$config = file_get_contents(public_path("themes/{$this->folder}/config.json"));

			$config = json_decode($config, 1);

			if(isset($config['logo'])){
				return "themes/{$this->folder}/{$config['logo']}";
			}
		}

		return null;
	}

	public function getJSON() {
		if(file_exists(public_path("themes/{$this->folder}/config.json"))){
			$config = file_get_contents(public_path("themes/{$this->folder}/config.json"));

			$config = json_decode($config, 1);

			if(isset($config['settings'])){
				return $config['settings'];
			}
		}

		return null;
	}
}
