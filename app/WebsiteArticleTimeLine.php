<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteArticleTimeLine extends Model
{
	protected $table="website_article_time_line";
    protected $fillable = ["user_id", "article_id"];


    public function article(){
    	return $this->belongsTo(WebsiteArticles::class, 'article_id');
    }
}
