<?php

namespace App\Console\Commands;

use App\UserPermission;
use Illuminate\Console\Command;

class DeleteExpiredSubscriptions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'delete:subscription';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this command to delete subscription monthly';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo 'Job Started at : '.date('m/d/Y h:i:s a').PHP_EOL;
        $allDeletedUserCount = 0;
        $allUserIdDeleted = [];
        //get all subscribed users and not deleted
        $oAllSubscripedUsers = UserPermission::Where('is_deleted',false)->get()->all();
        foreach ($oAllSubscripedUsers as $subscripedUser){
            $iExpirationTime = intval($subscripedUser->expiration_timestamp);
            if(time() >= $iExpirationTime){
                echo '=========================================[ Deleting User ]=============================================['.date('m/d/Y h:i:s a').']'.PHP_EOL;
                $subscripedUser->is_deleted = true;
                $subscripedUser->save();
                echo 'Deleting User id : '. $subscripedUser->user_id.PHP_EOL;
                echo 'Plans Id : '. $subscripedUser->plans_id.PHP_EOL;
                $allDeletedUserCount++;
                $allUserIdDeleted[] = $subscripedUser->user_id;
                echo '=========================================[ Deleting User ]=============================================['.date('m/d/Y h:i:s a').']'.PHP_EOL;
            }

        }
        echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++".PHP_EOL;
        echo 'Number Of Deleted Subscriptions : '. $allDeletedUserCount.PHP_EOL;
        if(count($allUserIdDeleted) > 0){
            echo 'List Of User Id For Deleted subscriptions : '.PHP_EOL;
            print json_encode($allUserIdDeleted).PHP_EOL;;
        }
        echo "+++++++++++++++++++++++++++++++++++++++++++++++++++++++".PHP_EOL;

    }
}
