<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Love extends Model
{
    protected $table = 'likes';

    public static function resourceLoves($resource_id , $resource_type){
        return Love::where([
            'resource_id' => $resource_id,
            'resource_type' => $resource_type
        ])->get();
    }
    public static function isCurrentLoved($resource_id , $resource_type){
        return Love::where([
            'resource_id' => $resource_id,
            'resource_type' => $resource_type,
            'user_id' => \Auth::user()->id
        ])->first();
    }
}
