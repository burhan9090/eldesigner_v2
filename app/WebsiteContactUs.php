<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WebsiteContactUs
 * @package App
 *
 * @mixin \Eloquent
 */
class WebsiteContactUs extends Model
{
	protected $table = "website_contact_us";

    protected $fillable = [ "user_id", "show_tab", "email_notification", "title", "message" ];

}
