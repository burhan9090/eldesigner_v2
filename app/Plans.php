<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 2019-03-23
 * Time: 22:10
 */
namespace App;
use Illuminate\Database\Eloquent\Model;
class Plans extends Model{
    protected $table = 'plans';
    protected $fillable = ['plans_id', 'plans_name','plans_price','description'];
}