<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WebsiteProject
 * @package App
 *
 * @mixin \Eloquent
 */
class WebsiteProject extends Model
{
    protected $fillable = [
    	"user_id", "title", "description", "repost",
	    "project_count", "category_id", "is_demo"
    ];

    public function image(){
    	return $this->hasOne(WebsiteProjectImage::class, 'project_id', 'id');
    }

    public function images(){
    	return $this->hasMany(WebsiteProjectImage::class, 'project_id', 'id');
    }

    public function tags(){
    	return $this->belongsToMany(Tag::class, 'website_project_tags', 'project_id', 'tag_id');
    }

    public function timeline(){
    	return $this->hasOne(WebsiteProjectTimeLine::class, 'project_id');
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

	public function category() {
		return $this->belongsTo(Service::class, 'category_id');
    }
}
