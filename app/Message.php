<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message', 'user_id', 'user_id_to','have_attachments','conversation_id'
    ];
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function user_to(){
        return $this->belongsTo(User::class,'user_id_to');
    }
    public function attachments(){
        return $this->hasMany(Attachment::class);
    }
}
