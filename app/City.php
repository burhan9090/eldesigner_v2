<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $timestamps=false;
	public $fillable=['country_id', 'text', 'code'];

	public function country(){
		return $this->belongsTo(Country::class);
	}
    public function user(){
        return $this->belongsTo(User::class);
    }

}
