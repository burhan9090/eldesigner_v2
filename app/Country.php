<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
	public $fillable=['text', 'code'];

	public function cities(){
		return $this->hasMany(City::class);
	}
}
