<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 2019-02-17
 * Time: 20:22
 */

namespace App;
use Illuminate\Foundation\Auth\User as Authenticatable;

class UserMedia extends Authenticatable{
    protected $table = 'user_media';
    protected $fillable = [
        'resource_id','resource_type','file_name','media_type','optimized'
    ];
}