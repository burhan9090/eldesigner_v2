<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ContactUs
 * @package App
 *
 * @mixin \Eloquent
 */

class ContactUs extends Model
{
	public $table='contact_us';
	public $fillable=[ 'user_id', 'title', 'message', 'is_read' ];

}
