<?php
//** Helpers File , Here's you'll find custom helpers functions  that autoload-ed by composer **/

use Intervention\Image\ImageManagerStatic as Image;

if (! function_exists('uploadImage')) {
    function uploadImage($file_array , $saved_path , $width , $height){
        try{
            $image_name = time() . $file_array . $file_array['type'];
            $image_path = $saved_path . $image_name;
            $image = Image::make($file_array['image']['tmp_name'])->resize($width, $height);
            $image->save($image_path);
            return $image_path;
        }catch (Exception $e){
            return false;
        }
    }
}
if (! function_exists('outJson')) {
    function outJson($dataOut){
        echo json_encode($dataOut);
        die();
    }
}
