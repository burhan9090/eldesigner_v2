<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteProjectImage extends Model
{
	public $timestamps=false;
	protected $fillable = ["project_id", "image", "description","optimized_and_watermarked"];

	public function tags(){
		return $this->belongsToMany(
			Tag::class,
			'website_project_image_tags',
			'image_id',
			'tag_id'
			);
	}
}
