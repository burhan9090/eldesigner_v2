<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteArticlesCategory extends Model
{
	protected $fillable= ['text', 'category_id'];


	public function parent(){
		return $this->belongsTo(Static::class, 'category_id', 'id');
	}

	public function children(){
		return $this->hasMany(Static::class, 'category_id', 'id');
	}

	public function articles(){
		return $this->hasMany(WebsiteArticles::class, 'category_id', 'id');
	}
}
