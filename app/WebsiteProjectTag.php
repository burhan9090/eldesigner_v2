<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WebsiteProjectTag extends Model
{
	public $timestamps=false;
	protected $fillable = ["project_id", "tag_id"];

}
