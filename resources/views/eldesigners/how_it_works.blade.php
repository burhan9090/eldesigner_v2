@extends('layouts.eldesigners')
@section('content')
    <style>
        .pricing-cover{
            background-image: url(../website/Pricing-Cover.svg);
            width: 100%;
            background-repeat: no-repeat;
            /* background-attachment: fixed; */
            background-position: unset;
            background-size: cover;
        }

        .bg-primary-opacity {
            background-color: rgba(251, 251, 251, 0);
        }
    </style>
    <div class="stunning-header bg-primary-opacity">

        @include('eldesigners.top_menu')
        <div class="header-spacer--standard"></div>

        <div class="stunning-header-content">
            <h1 class="stunning-header-title" style="font-weight: 500">How it works</h1>
            <ul class="breadcrumbs">
                <li class="breadcrumbs-item">
                    <a href="#"></a>
                </li>
                <li class="breadcrumbs-item active">
                    <span> <a href="{{ route('home') }}" style="color: white">Sign up now for free!</a></span>
                </li>
            </ul>
        </div>

        <div class="content-bg-wrap pricing-cover"></div>
    </div>
    <style>

        .cat-list__item a:hover {
            background-color: #1bb5cc !important
        }

        .cat-list__item.active {
            background-color: #1de0fd !important
        }
    </style>

    <div class="">
        <section class="medium-padding100" style="padding-top: 0px">

            <div class="container">
                <div class="row" >
                    <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 mb-5">
                        <img src="{{ asset('website/icon-fly.png') }}" alt="screen">
                    </div>

                    <div class="col col-xl-6 col-lg-6 m-auto col-md-12 col-sm-12 col-12">
                        <div class="crumina-module crumina-heading">
                            <h2 class="heading-title" style="color:#303445;font-weight: 500;">By Designers
                                <span class="c-primary" style="color: #06b4d2">For Designers</span></h2>
                            <p class="heading-text" style="font-size: 16px">elDesigners is a powerful solution to end all the time-consuming tasks designers face.
                                Like keeping portfolio updated, building and maintaining a network, running a website, identifying customers trends, and finding & communicating with clients.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section></div>

    <section class="medium-padding100" style="background-color: #f7f9ff">
        <div class="container">
            <div class="row">
                <div class="col col-xl-6 col-lg-6 m-auto col-md-12 col-sm-12 col-12">
                    <div class="crumina-module crumina-heading">
                        <h2 class="heading-title" style="color:#303445;font-weight: 500;">Get a website<span style="color: #06b4d2"> immediately.</span></h2>
                        <p class="heading-text" style="font-size: 16px">Too much to handle while trying to run a vibrant website? elDesigners technology will rebuild designers’ portfolios, data, and interactions into a website in one click only!
                            Designers can change the theme, disable/enable tabs, edit colors and read analytics.
                            Everything is synced in real time and accessible without any need for coding or web development experience.</div>
                </div>

                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <img src="{{ asset('website/HomePageStep22.png') }}" alt="screen" style="width: 579px; height: 334px">
                </div>
            </div>
        </div>
    </section>


    <section class="medium-padding100">
        <div class="container">
            <div class="row">
                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <img src="{{ asset('website/HomePageStep33.png') }}" alt="screen">
                </div>

                <div class="col col-xl-6 col-lg-6 m-auto col-md-12 col-sm-12 col-12">
                    <div class="crumina-module crumina-heading">
                        <h2 class="heading-title" style="color:#303445;font-weight: 500;">An empowering <span style="color: #06b4d2">set of tools.</span></h2>
                        <p class="heading-text" style="font-size: 16px">Tools such as design requests, image protections, unique domain buyer, professional email services and many other tools can be added to website & online-portfolio.
                            These tools are built to empower designers to interact with clients, sell online, build a followers base and protect their work from plagiarism.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="medium-padding100" style="background-color: #f7f9ff">
        <div class="container">
            <div class="row">
                <div class="col col-xl-6 col-lg-6 m-auto col-md-12 col-sm-12 col-12">
                    <div class="crumina-module crumina-heading">
                        <h2 class="heading-title" style="color:#303445;font-weight: 500;">Share, Engage and<span style="color: #06b4d2"> Get inspired.</span></h2>
                        <p class="heading-text" style="font-size: 16px">When a project is uploaded to the online portfolio, the project will be displayed as well in the generated website, published to a public design library and shared to an internal social network between designers. Automatically!
                            Get ready to engage, join a community and expose your work to an international audience of designers, users, and clients.  </p>
                    </div>
                </div>

                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <img src="{{ asset('website/HomePageStep55.png') }}" alt="screen">
                </div>
            </div>
        </div>
    </section>
    <section class="medium-padding100">
        <div class="container">
            <div class="row">
                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <img src="{{ asset('website/HomePageStep44.png') }}" alt="screen">
                </div>

                <div class="col col-xl-6 col-lg-6 m-auto col-md-12 col-sm-12 col-12">
                    <div class="crumina-module crumina-heading">
                        <h2 class="heading-title" style="color:#303445;font-weight: 500;">Why think local?<span style="color: #06b4d2"> Think Global.</span></h2>
                        <p class="heading-text" style="font-size: 16px">The crowdsourced designs and articles library is a great way for designers to share their work with a global audience.
                            Using the tools and features on elDesigners, designers can engage & acquire clients from all over al the world and interact with them online, overcoming any physical barriers. Not only clients, designers as well can build a fan base of international designers and interested users.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="medium-padding80" style="padding-bottom: 0px">
        <div class="container">
            <div class="row">
                <div class="col col-xl-6 col-lg-6 m-auto col-md-12 col-sm-12 col-12">
                    <div class="crumina-module crumina-heading">
                        <h2 class="heading-title" style="color:#303445;font-weight: 500;">Are You<span style="color: #06b4d2"> Ready?</span></h2>
                        <p class="heading-text" style="font-size: 16px">We built the technology to eliminate the hassle of running a design business to allow designers to do what they actually love.
                            Start today using the FOREVER FREE account, designers can upgrade/downgrade anytime. </p>
                        <a href="{{ route('home') }}" class="btn btn-primary btn-lg"  style="background-color: #FF82C8">Join For Free Now!</a>

                    </div>
                </div>

                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <img src="{{ asset('website/HomePageStep66.png') }}" alt="screen">
                </div>
            </div>
        </div>
    </section>


@endsection
