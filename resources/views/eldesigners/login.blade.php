@extends('layouts.eldesigners')
@section('content')
    <style>
        .registration-login-form{
            padding-left : unset !important;
        }
        @media (max-width:480px) {

            .background-artx {
                background-image: url(../website/HomePage-BackgroundArt2-Mobile-blue.svg);
            }
            .info-box-title {
                padding-top: 20px;
            }
            .mobile-centerx {
                text-align: center;
            }
            .bg-landing {
                background-image: url(../website/background-mobile-blue.jpg);
                background-repeat: space;
            }
            .mobile-landing1 {
                text-align: center;
            }
        }
    </style>
    <div class="main-header main-header-fullwidth main-header-has-header-standard">


        @include('eldesigners.top_menu' , ['sHeaderFontColor' => "black"])
        <div class="header-spacer--standard"></div>

        <div class="content-bg-wrap bg-landing" style="	background-repeat: no-repeat"></div>

        <div class="container">
            <div class="row display-flex">
                <div class="col col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12">
                    <div class="landing-content mobile-landing1">


                        <br>
                        <h1 style="font-weight: 700; font-size: 50px">ONLINE GATEWAY<br> FOR DESIGNERS!</h1>
                        <p>Everything designers need in one place, synchronized in real time, available anywhere at any
                            time, on the cloud.</p>
                        <h6>Start today on our forever free plan!</h6>

                        <a href="#" class="btn btn-md btn-border c-white">Register Now!</a>
                    </div>
                </div>

                <div class="col col-xl-5 ml-auto col-lg-6 col-md-12 col-sm-12 col-12">


                    <!-- Login-Registration Form  -->

                    <div class="registration-login-form">
                        <!-- Nav tabs -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="profile" role="tabpanel" data-mh="log-tab">
                                <div class="title h6">Login to your Account</div>
                                <form class="content" method="POST" action="{{ route('login') }}">
                                    {{ csrf_field() }}
                                    <div class="row">
                                        <div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                            <div class="form-group label-floating is-empty {{ $errors->has('email') ? ' has-error' : '' }}">
                                                <label class="control-label">Your Email</label>
                                                <input class="form-control" name="email" placeholder="" type="email"
                                                       value="{{ old('email') }}" required autofocus>
                                                @if ($errors->has('email'))
                                                    <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                            <div class="form-group label-floating is-empty {{ $errors->has('password') ? ' has-error' : '' }}"
                                                 required>
                                                <label class="control-label">Your Password</label>
                                                <input class="form-control" name="password" placeholder="" type="password">
                                                @if ($errors->has('password'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>

                                            <div class="remember">

                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox"
                                                               name="remember" {{ old('remember') ? 'checked' : '' }}>
                                                        Remember Me
                                                    </label>
                                                </div>
                                                <a href="#" class="forgot">Forgot my Password</a>
                                            </div>

                                            <button type="submit" class="btn btn-lg full-width" style="background-color: #303445">Login</button>

                                            <div class="or"></div>

                                            {{--<a href="#" class="btn btn-lg bg-facebook full-width btn-icon-left"><i class="fab fa-facebook-f" aria-hidden="true"></i>Login with Facebook</a>--}}

                                            {{--<a href="#" class="btn btn-lg bg-twitter full-width btn-icon-left"><i class="fab fa-twitter" aria-hidden="true"></i>Login with Twitter</a>--}}


                                            <p>Don’t you have an account? <a href="{{ route('home') }}">Register Now!</a> it’s really simple and you can start enjoing all the benefits!</p>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <!-- ... end Login-Registration Form  -->
                </div>
            </div>
        </div>

    </div>

@endsection