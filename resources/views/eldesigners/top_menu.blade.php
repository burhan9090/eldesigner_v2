<!-- Header Standard Landing  -->
<style>
    .nav-link{
        color: {{ isset($sHeaderFontColor)?$sHeaderFontColor:'white' }} !important;
    }
</style>
<div class="header--standard header--standard-landing" id="header--standard" style="<?= isset($sHeaderBg)?'background: '.$sHeaderBg.'':'' ?>">
    <div class="container">
        <div class="header--standard-wrap">

            <a href="{{ route('home') }}" class="logo">
                <div class="img-wrap">
                    <img src="{{ asset('website/Logo-blue.png') }}" alt="elDesigners" style="width: 70px">
                </div>
                <div class="title-block">
                    <h3 class="logo-title">elDESIGNERS</h3>
                    <p class="logo-title" style="text-align: center;">Design without limits</p>
                </div>
            </a>

            <a href="#" class="open-responsive-menu js-open-responsive-menu">
                <svg class="olymp-menu-icon">
                    <use xlink:href="/svg-icons/sprites/icons.svg#olymp-menu-icon"></use>
                </svg>
            </a>

            <div class="nav nav-pills nav1 header-menu expanded-menu">
                <div class="mCustomScrollbar">
                    <ul>
                        <li class="nav-item">
                            <a href="{{route('home.discover-designs')}}" class="nav-link" >Discover Designs</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('home.discover-friends')}}" class="nav-link" >Designers</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('home.home_blog')}}" class="nav-link" >Blog</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('home.how_it_works') }}" class="nav-link" >How it works</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('home.pricing') }}" class="nav-link" >Pricing</a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('home.login') }}" class="nav-link" >Login</a>
                        </li>
                        <li class="close-responsive-menu js-close-responsive-menu">
                            <svg class="olymp-close-icon d-lg-none d-m-none d-block">
                                <use xlink:href="/svg-icons/sprites/icons.svg#olymp-close-icon"></use>
                            </svg>
                        </li>
                        <li class="nav-item js-expanded-menu d-lg-none d-m-none d-block">
                            <a href="#" class="nav-link">
                                <svg class="olymp-menu-icon">
                                    <use xlink:href="/svg-icons/sprites/icons.svg#olymp-menu-icon"></use>
                                </svg>
                                <svg class="olymp-close-icon d-none">
                                    <use xlink:href="/svg-icons/sprites/icons.svg#olymp-close-icon"></use>
                                </svg>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ... end Header Standard Landing  -->