<style>
    .registration-login-form {
        padding-left: unset !important;
    }
</style>
<!-- Tab panes -->
<div class="tab-content">
    <div class="tab-pane active" id="home" role="tabpanel" data-mh="log-tab">
        <div class="title h6">Join elDesigners</div>
        <form class="content" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}

            <div class="row">
                <div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group label-floating is-empty">
                        <label class="control-label">Your Full Name</label>
                        <input id="name" type="text" class="form-control" name="name"
                               value="{{ old('name') }}" required autofocus>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                        @endif
                    </div>
                    <div class="form-group label-floating is-empty">
                        <label class="control-label">Your User Name</label>
                        <input id="user_name" type="text" class="form-control" name="user_name"
                               value="{{ old('user_name') }}" required autofocus>
                        @if ($errors->has('user_name'))
                            <span class="help-block">
                                                        <strong>{{ $errors->first('user_name') }}</strong>
                                                    </span>
                        @endif
                    </div>
                </div>
                <div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group label-floating is-empty">
                        <label class="control-label">Your Email</label>
                        <input id="email" type="email" class="form-control" name="email"
                               value="{{ old('email') }}" required autofocus>

                        @if ($errors->has('email'))
                            <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                        @endif
                    </div>
                    <div class="form-group label-floating is-empty">
                        <label class="control-label">Your Password</label>
                        <input id="password" type="password" class="form-control" name="password"
                               required>

                        @if ($errors->has('password'))
                            <span class="help-block">
                                                     <strong>{{ $errors->first('password') }}</strong>
                                                 </span>
                        @endif

                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Main Design Service</label>
                        <select class="selectpicker form-control">
                            <option value="RO">Photography</option>
                            <option value="RO">Illustration</option>
                            <option value="RO">Motion</option>
                            <option value="RO">Interior Design</option>
                            <option value="RO">Graphic Design</option>
                            <option value="RO">Architecture Design</option>
                        </select>
                    </div>
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Profile Type</label>
                        <select class="selectpicker form-control" name="profile_type">
                            <option value="freelancer" id="freelancer">Personal</option>
                            <option value="company" id="company">Business</option>
                        </select>
                    </div>
                    <div class="form-group label-floating is-empty">
                        <label class="control-label">Promo Code</label>
                        <input id="code" type="text" class="form-control" name="code"
                               value="{{ old('code') }}" autofocus>

                        @if ($errors->has('code'))
                            <span class="help-block">
                                <strong>{{ $errors->first('code') }}</strong>
                            </span>
                        @endif
                    </div>

                    {{--<div class="form-group label-floating is-empty align-center">--}}
                    {{--<div id="gender_options" class="btn-group btn-group-toggle "--}}
                    {{--data-toggle="buttons">--}}
                    {{--<label class="btn btn-md btn-secondary active bababab" >--}}
                    {{--<input type="radio" name="profile_type" id="freelancer" autocomplete="off"--}}
                    {{--checked value="freelancer"> Personal--}}
                    {{--</label>--}}
                    {{--<label class="btn btn-md btn-secondary bababab ">--}}
                    {{--<input type="radio" name="profile_type" id="company" autocomplete="off"--}}
                    {{--value="company"> Business--}}
                    {{--</label>--}}
                    {{--</div>--}}
                    {{--</div>--}}

                    <div class="remember">
                        <div class="checkbox">
                            <label>
                                <input name="optionsCheckboxes" type="checkbox" required>
                                I accept the <a href="#">Terms and Conditions</a> of the website
                            </label>
                        </div>
                    </div>

                    <button type="submit" class="btn btn-lg full-width" style="background-color: #303445">Complete
                        Registration!
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>