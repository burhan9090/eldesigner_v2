@extends('layouts.eldesigners')
@section('content')
    <div class="main-header main-header-fullwidth main-header-has-header-standard">

      @include('eldesigners.top_menu' , ['sHeaderFontColor' => "black"])
        <div class="header-spacer--standard"></div>

        <div class="content-bg-wrap bg-landing" style="	background-repeat: no-repeat"></div>

        <div class="container">
            <div class="row display-flex">
                <div class="col col-xl-5 col-lg-5 col-md-12 col-sm-12 col-12 mt-m-7" style="margin-bottom: 20% !important;">
                    <div class="landing-content mobile-landing1">
                        <h1 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s"  style="font-weight: 700; font-size: 50px">ONLINE GATEWAY<br> FOR DESIGNERS!</h1>
                        <p class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.6s" >Everything designers need in one place, synchronized in real time, available anywhere at any
                            time, on the cloud.</p>
                        <h6 class="wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.7s">Start today on our forever free plan!</h6>

                        <a href="#" class="btn btn-md btn-border c-white wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.8s">Register Now!</a>
                    </div>
                </div>

                <div class="col col-xl-5 ml-auto col-lg-6 col-md-12 col-sm-12 col-12">


                    <!-- Login-Registration Form  -->

                    <div class="registration-login-form wow fadeInRight" data-wow-duration="1.2s" data-wow-delay="0.5s">
                        <!-- Nav tabs -->
                        @include('eldesigners.home_registration')
                    </div>

                    <!-- ... end Login-Registration Form  -->
                </div>
            </div>
        </div>

    </div>

    <section class="medium-padding counters">
        <div class="container">
            <div class="row">


                <div class="crumina-module crumina-counter-item col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6 wow fadeInUp" data-wow-duration="1s" data-wow-delay="0.4s">
                    <div class="counter-numbers counter h2">
                        @php $views = \App\User::sum('profile_count');@endphp
                        <span data-speed="2000" data-refresh-interval="3" data-to="{{ $views }}" data-from="2" style="color: #303445">{{ $views }}</span>
                        <div class="units"><div style="color: #48A5E8">+</div></div>
                    </div>
                    <div class="counter-title">Profile/Website Views</div>
                </div>
                <!-- Counter Item -->
                @php $projects = \App\WebsiteProject::count('id');@endphp
                <div class="crumina-module crumina-counter-item col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6 wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.5s">
                    <div class="counter-numbers counter h2">
                        <span data-speed="2000" data-refresh-interval="3" data-to="{{ $projects }}" data-from="2" style="color: #303445">{{ $projects }}</span>
                        <div class="units"><div style="color: #FF82C8">+</div></div>
                    </div>
                    <div class="counter-title">Projects Uploaded</div>
                </div>

                <!-- ... end Counter Item -->


                <!-- Counter Item -->
                @php $websites = \App\User::count('id');@endphp
                <div class="crumina-module crumina-counter-item col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6 wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="0.6s">
                    <div class="counter-numbers counter h2">
                        <span data-speed="2000" data-refresh-interval="3" data-to="{{ $websites }}" data-from="2" style="color: #303445">{{ $websites }}</span>
                        <div class="units"><div style="color: #FF82C8">+</div></div>
                    </div>
                    <div class="counter-title">Websites Generated</div>
                </div>

                <!-- ... end Counter Item -->


                <!-- Counter Item -->
                @php $cities = \App\User::count(DB::raw('DISTINCT city_id'));;@endphp
                <div class="crumina-module crumina-counter-item col-xl-3 col-lg-3 col-md-6 col-sm-6 col-6 wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="0.6s">
                    <div class="counter-numbers counter h2">
                        <span data-speed="2000" data-refresh-interval="3" data-to="{{ $cities }}" data-from="2" style="color: #303445">{{ $cities }}</span>
                        <div class="units"><div style="color: #FF82C8">+</div></div>

                    </div>
                    <div class="counter-title">Cities</div>
                </div>

                <!-- ... end Counter Item -->


                <!-- Counter Item -->



                <!-- ... end Counter Item -->

            </div>
        </div>
    </section>







    <div class="background-artx" style="background-size: cover; background-repeat: no-repeat">
        <section class="medium-padding120" style="margin-top: 0px">

            <div class="container">


                <div class="mobile-centerx">
                    <section class="medium-padding20">
                        <div class="container">
                            <div class="row">
                                <div class="col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12 wow fadeInLeft" data-wow-duration="1.3s" data-wow-delay="0.6s">
                                    <div class="crumina-module crumina-heading" style="margin-bottom: 40px">
                                        <h2 class="heading-title" style="font-weight: 700; margin-bottom: 12px">Awesome<span class="c-primary" style="color: #07b1d2;"> Features</span></h2>
                                        <p class="heading-text">We built the technology so you can focus on what you do best!<br>elDesigners provides a wide verity of tools and features.</p>

                                    </div>
                                </div>

                                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                                </div>
                            </div>
                        </div>
                    </section>
                </div>


                <div class="info-box-wrap">
                    <div class="row">
                        <div class="col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 wow fadeInUp" data-wow-duration="1.1s" data-wow-delay="0.3s">
                            <!-- Info Box  -->
                            <div class="crumina-module crumina-info-box" data-mh="box--classic" style="margin-bottom: 60px">
                                <div class="info-box-image">
                                    <img src="{{ asset('website/Portfolio.svg') }}" alt="icon" style="width: 110px">
                                </div>
                                <div class="info-box-content" style="color: white; margin-top: -30px; ">
                                    <h3 class="info-box-title">Profile Page</h3>
                                </div>
                            </div>
                            <!-- ... end Info Box  -->
                        </div>
                        <div class="col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 wow fadeInUp" data-wow-duration="1.2s" data-wow-delay="0.4s">
                            <!-- Info Box  -->
                            <div class="crumina-module crumina-info-box" data-mh="box--classic" style="margin-bottom: 60px">
                                <div class="info-box-image">
                                    <img src="{{ asset('website/Website-Generator.svg') }}" alt="icon" style="width: 110px">
                                </div>
                                <div class="info-box-content" style="color: white; margin-top: -30px; ">
                                    <h3 class="info-box-title">Website Generater</h3>
                                </div>
                            </div>

                            <!-- ... end Info Box  -->

                        </div>
                        <div class="col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="0.5s">


                            <!-- Info Box  -->

                            <div class="crumina-module crumina-info-box" data-mh="box--classic" style="margin-bottom: 60px">
                                <div class="info-box-image">
                                    <img src="{{ asset('website/Social-Network.svg') }}" alt="icon" style="width: 110px">
                                </div>
                                <div class="info-box-content" style="color: white; margin-top: -30px; ;">
                                    <h3 class="info-box-title">Social Network</h3>
                                </div>
                            </div>

                            <!-- ... end Info Box  -->

                        </div>
                        <div class="col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 wow fadeInUp" data-wow-duration="1.4s" data-wow-delay="0.6s">


                            <!-- Info Box  -->

                            <div class="crumina-module crumina-info-box" data-mh="box--classic" style="margin-bottom: 60px">
                                <div class="info-box-image">
                                    <img src="{{ asset('website/Crowd-sourced-content.svg') }}" alt="icon" style="width: 110px">
                                </div>
                                <div class="info-box-content" style="color: white; margin-top: -30px; ;">
                                    <h3 class="info-box-title">Crowdsourced Content</h3>
                                </div>
                            </div>

                            <!-- ... end Info Box  -->

                        </div>
                        <div class="col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 wow fadeInUp" data-wow-duration="1.5s" data-wow-delay="0.7s">


                            <!-- Info Box  -->

                            <div class="crumina-module crumina-info-box" data-mh="box--classic" style="margin-bottom: 60px">
                                <div class="info-box-image">
                                    <img src="{{ asset('website/Image-Tagging.svg') }}" alt="icon" style="width: 110px">
                                </div>
                                <div class="info-box-content">
                                    <h3 class="info-box-title">Image Tagging</h3>
                                </div>
                            </div>

                            <!-- ... end Info Box  -->

                        </div>



                        <div class="col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 wow fadeInUp" data-wow-duration="1.55s" data-wow-delay="0.8s">


                            <!-- Info Box  -->

                            <div class="crumina-module crumina-info-box" data-mh="box--classic" style="margin-bottom: 60px">
                                <div class="info-box-image">
                                    <img src="{{ asset('website/DRSI-Icon.svg') }}" alt="icon" style="width: 110px">
                                </div>
                                <div class="info-box-content">
                                    <h3 class="info-box-title">Design Requests (DRVSI)</h3>
                                </div>
                            </div>

                            <!-- ... end Info Box  -->

                        </div>

                        <div class="col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 wow fadeInUp" data-wow-duration="1.6s" data-wow-delay="0.9s">


                            <!-- Info Box  -->



                            <!-- Info Box  -->

                            <div class="crumina-module crumina-info-box" data-mh="box--classic" style="margin-bottom: 60px">
                                <div class="info-box-image">
                                    <img src="{{ asset('website/Unique-domain.svg') }}" alt="icon" style="width: 110px">
                                </div>
                                <div class="info-box-content">
                                    <h3 class="info-box-title">Unique Domains</h3>
                                </div>
                            </div>

                            <!-- ... end Info Box  -->

                        </div>

                        <!-- ... end Info Box  -->



                        <div class="col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12 wow fadeInUp" data-wow-duration="1.66s" data-wow-delay="1s">


                            <!-- Info Box  -->



                            <!-- Info Box  -->

                            <div class="crumina-module crumina-info-box" data-mh="box--classic">
                                <div class="info-box-image">
                                    <img src="{{ asset('website/Pro-Mail.svg') }}" alt="icon" style="width: 110px">
                                </div>
                                <div class="info-box-content">
                                    <h3 class="info-box-title">Professional Email</h3>
                                </div>
                            </div>

                            <!-- ... end Info Box  -->

                        </div>

                        <!-- ... end Info Box  -->



                    </div>
                </div>





            </div>
            <div class="mb30">
                <div class="col col-xl-6 col-lg-6 m-auto col-md-12 col-sm-12 col-12">
                    <div class="crumina-module crumina-heading align-center">
                        <a href="{{ route('home.how_it_works') }}" class="btn btn-primary btn-lg">   Learn More   </a>


                    </div>
                </div>
            </div>

        </section>
    </div>

    <!-- Section Img Scale Animation -->
    <section class="medium-padding20">
        <div class="container">
            <div class="row">
                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                    <div class="crumina-module crumina-heading">
                        <h2 class="heading-title wow fadeInLeft" data-wow-duration="1.3s" data-wow-delay="0.6s" style="font-weight: 700">Are You <span class="c-primary"
                                                                                         style="color: #07b1d2">Ready?</span>
                        </h2>
                        <p class="heading-text wow fadeInLeft" data-wow-duration="1.3s" data-wow-delay="0.7s">We built the technology to eliminate the hassle of running a design
                            business to allow designers to do what they actually love.
                            Start today on our forever free plan! </p>
                        <a href="#" class="btn btn-primary btn-lg scroll-top-arrow wow fadeInUp" data-wow-duration="1.3s" data-wow-delay="0.7s" style="background-color: #07b1d2; border: none">Join
                            For Free Now!</a>
                    </div>
                </div>
                <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12 wow fadeInRight" data-wow-duration="1.3s" data-wow-delay="0.6s">
                    <img src="{{ asset('website/HomePageStep66.png') }}" alt="screen">
                </div>
            </div>
        </div>
    </section>
    <!-- Planer Animation -->
@endsection
@section('script')
<script>
    var wow = new WOW(
    {
        boxClass:     'wow',      // animated element css class (default is wow)
        animateClass: 'animated', // animation css class (default is animated)
        offset:       0,          // distance to the element when triggering the animation (default is 0)
        mobile:       false       // trigger animations on mobile devices (true is default)
    }
    );
    wow.init();
</script>