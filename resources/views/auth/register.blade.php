@extends('layouts.auth')

@section('content')
    <style>
        .input-group-addon {
            position: absolute;
            /* right: 0; */
            top: 14px !important;;
            font-size: 33px !important;
            right: auto !important;
        }

        .fb_link {
            color: #2f5b9d !important;
        }

        .tw_link {
            color: #38bff1;
        !important;
        }

        .ig_link {
            color: #f8460b;
        !important;
        }

        .pin_link {
            color: #CB2027;
        !important;
        }

        .input_pad {
            padding-left: 50px !important;
            font-size: 17px;
        }

        .group_radio {
            margin-top: 39px;
            margin-left: 174px;
        }

        .btn-secondary:not(:disabled):not(.disabled).active, .btn-secondary:not(:disabled):not(.disabled):active, .show > .btn-secondary.dropdown-toggle {
            background-color: #F27A51 !important;
        }
    </style>

    <div class="content-bg-wrap"></div>
    <div class="container">
        <div class="row display-flex">
            <div class="col col-xl-12 col-lg-8 col-md-12 col-sm-12 col-12">
                <div class="registration-login-form"
                     style="padding-left:0 !important;    margin-top: 70px;min-height: auto">

                    <div class="tab-content">
                        <div class="tab-pane active" id="home" role="tabpanel" data-mh="log-tab">
                            <div class="title h6">Register to Olympus</div>
                            <form class="content" method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col col-6 col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                        <label for="user_name" class="col-md-4 control-label">User Name</label>


                                        <input id="user_name" type="text" class="form-control" name="user_name"
                                               value="{{ old('user_name') }}" required autofocus>

                                        @if ($errors->has('user_name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('user_name') }}</strong>
                                        </span>
                                        @endif


                                    </div>
                                    <div class="col col-6 col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                        <label for="full_name" class="col-md-4 control-label">Full Name</label>


                                        <input id="full_name" type="text" class="form-control" name="full_name"
                                               value="{{ old('name') }}" required>

                                        @if ($errors->has('name'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                        @endif

                                    </div>
                                    <div class="col col-6 col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                        <label for="email" class="col-md-4 control-label">E-Mail Address</label>


                                        <input id="email" type="email" class="form-control" name="email"
                                               value="{{ old('email') }}" required>

                                        @if ($errors->has('email'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                        @endif

                                    </div>
                                    <div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                        <label for="gender_options" class="col-md-4 control-label">Select Gender</label>
                                        <br>
                                        <div id="gender_options" class="btn-group btn-group-toggle "
                                             data-toggle="buttons">
                                            <label class="btn btn-lg btn-secondary active">
                                                <input type="radio" name="gender" id="gender1" autocomplete="off"
                                                       checked value="male"> Male
                                            </label>
                                            <label class="btn btn-lg btn-secondary">
                                                <input type="radio" name="gender" id="gender2" autocomplete="off"
                                                       value="female"> Female
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col col-6 col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                        <label for="password" class="col-md-4 control-label">Password</label>


                                        <input id="password" type="password" class="form-control" name="password"
                                               required>

                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                        @endif

                                    </div>
                                    <div class="col col-6 col-xl-6 col-lg-6 col-md-6 col-sm-6">
                                        <label for="password-confirm" class="col-md-4 control-label">Confirm
                                            Password</label>


                                        <input id="password-confirm" type="password" class="form-control"
                                               name="password_confirmation" required>
                                    </div>


                                </div>
                                <hr />
                                <button class="btn btn-purple btn-lg full-width">Complete Registration!</button>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
