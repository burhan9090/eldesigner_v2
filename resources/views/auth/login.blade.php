<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 12/1/18
 * Time: 11:44 PM
 */
?>

@extends('layouts.auth')
@section('content')

    <div class="content-bg-wrap"></div>

    <div class="container">
        <div class="row display-flex">
            <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="landing-content">
                    <h1>{{  __('general.welcome') }}</h1>
                    <p>We are the best and biggest social network with 5 billion active users all around the world.
                        Share you thoughts, write blog posts, show your favourite music via Stopify, earn badges and much more!
                    </p>
                    <a href="register/" class="btn btn-md btn-border c-white">Register Now!</a>
                </div>
            </div>

            <div class="col col-xl-5 col-lg-6 col-md-12 col-sm-12 col-12">

                <!-- Login-Registration Form  -->

                <div class="registration-login-form">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#home" role="tab">
                                <svg class="olymp-login-icon">
                                    <use xlink:href="/svg-icons/sprites/icons.svg#olymp-login-icon"></use>
                                </svg>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#profile" role="tab">
                                <svg class="olymp-register-icon">
                                    <use xlink:href="/svg-icons/sprites/icons.svg#olymp-register-icon"></use>
                                </svg>
                            </a>
                        </li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="home" role="tabpanel" data-mh="log-tab">
                            <div class="title h6">Register to Olympus</div>
                            <form class="content" method="POST" action="{{ route('register') }}">
                                {{ csrf_field() }}

                                <div class="row">

                                    <div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label">Your User Name</label>
                                            <input id="user_name" type="text" class="form-control" name="user_name"
                                                   value="{{ old('user_name') }}" required autofocus>

                                            @if ($errors->has('user_name'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('user_name') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label">Your Full Name</label>
                                            <input id="name" type="text" class="form-control" name="name"
                                                   value="{{ old('name') }}" required autofocus>

                                            @if ($errors->has('name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label">Your Email</label>
                                            <input id="email" type="email" class="form-control" name="email"
                                                   value="{{ old('email') }}" required autofocus>

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group label-floating is-empty align-center">
                                            <div id="gender_options" class="btn-group btn-group-toggle "
                                                 data-toggle="buttons">
                                                <label class="btn btn-md btn-secondary active">
                                                    <input type="radio" name="gender" id="gender1" autocomplete="off"
                                                           checked value="male"> Male
                                                </label>
                                                <label class="btn btn-md btn-secondary ">
                                                    <input type="radio" name="gender" id="gender2" autocomplete="off"
                                                           value="female"> Female
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group label-floating is-empty">
                                            <label class="control-label">Your Password</label>
                                            <input id="password" type="password" class="form-control" name="password"
                                                   required>

                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                     <strong>{{ $errors->first('password') }}</strong>
                                                 </span>
                                            @endif

                                        </div>

                                        <div class="remember">
                                            <div class="checkbox">
                                                <label>
                                                    By clicking Sign Up, you agree to our <a href="#">Terms and
                                                        Conditions</a> of the website
                                                </label>
                                            </div>
                                        </div>

                                        <button href="#" class="btn btn-purple btn-lg full-width">Sign Up</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div class="tab-pane" id="profile" role="tabpanel" data-mh="log-tab">
                            <div class="title h6">Login to your Account</div>
                            <form class="content" method="POST" action="{{ route('login') }}">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                                        <div class="form-group label-floating is-empty {{ $errors->has('email') ? ' has-error' : '' }}">
                                            <label class="control-label">Your Email</label>
                                            <input class="form-control" name="email" placeholder="" type="email"
                                                   value="{{ old('email') }}" required autofocus>
                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <div class="form-group label-floating is-empty {{ $errors->has('password') ? ' has-error' : '' }}"
                                             required>
                                            <label class="control-label">Your Password</label>
                                            <input class="form-control" name="password" placeholder="" type="password">
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                        <div class="remember">

                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"
                                                           name="remember" {{ old('remember') ? 'checked' : '' }}>
                                                    Remember Me
                                                </label>
                                            </div>
                                            <a href="#" class="forgot">Forgot my Password</a>
                                        </div>

                                        <button type="submit" class="btn btn-lg btn-primary full-width">Login</button>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- ... end Login-Registration Form  -->        </div>
        </div>
    </div>

@endsection