@extends('layouts.profile')
@section('content_profile')
  <div class="row">
    <div class="col-md-2 hidden-sm"></div>
    <div class="col-md-8 col-sm-12">
      <div class="ui-block">
        <div class="ui-block-title">
          <h6 class="title">Payment</h6>
        </div>
        <div class="ui-block-content">
          <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="form-group label-floating">
                <label class="control-label">Mount</label>
                <input class="form-control" readonly=""  type="text" autocomplete="off" value="{{ $payment->getDataItem('amount') }}">
              </div>
            </div>
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="form-group label-floating">
                <label class="control-label">Description</label>
                <textarea class="form-control" readonly="" autocomplete="off">{{ $payment->getDataItem('description') }}</textarea>
              </div>
            </div>
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="form-group label-floating {{ $payment->isFail()? 'has-error': '' }}">
                <label class="control-label">Status</label>
                <input class="form-control " readonly=""  type="text" autocomplete="off" value="{{ $payment->getResponseMessage() }}">
              </div>
            </div>
        </div>
      </div>
    </div>
  </div>
@endsection
