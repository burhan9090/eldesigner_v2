@extends('layouts.profile')
@section('content_profile')
  <div class="row">
    <div class="col-md-2 hidden-sm"></div>
    <div class="col-md-8 col-sm-12">
      <div class="ui-block">
        <div class="ui-block-title">
          <h6 class="title">Payment</h6>
          <div class="lock_icn" style="height: 50px;width: 50px; float: right;">
            <img style="height: 50px;width: 50px" src="{{ asset('img/lock_icn.png') }}">
          </div>
        </div>
        <div class="ui-block-content">

          <form >
            {{ csrf_field() }}
            {{--<div class="row">--}}
              {{--<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">--}}
                {{--<div class="lock_icn" style="height: 50px;width: 50px">--}}
                  {{--<img style="height: 50px;width: 50px" src="{{ asset('img/lock_icn.png') }}">--}}
                {{--</div>--}}

              {{--</div>--}}
            {{--</div>--}}
            <div class="row">
              <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="form-group label-floating">
                  <label class="control-label">Mount</label>
                  <input class="form-control" readonly=""  type="text" autocomplete="off" value="{{ $payment->getDataItem('amount') }}">
                </div>
              </div>

              @if($payment->getDataItem('description'))
                <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                  <div class="form-group label-floating">
                    <label class="control-label">Description</label>
                    <textarea class="form-control" readonly="" autocomplete="off">{{ $payment->getDataItem('description') }}</textarea>
                  </div>
                </div>
              @endif


              <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">

                <div id="paypal-button-container"></div>
                @if(URL::previous() and request()->url()!=URL::previous())
                <a href="{{ URL::previous() }}" class="btn btn-breez btn-lg ">Cancel</a>
                @endif
              </div>
            </div>
          </form>

          <!-- ... end Change Password Form -->
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script>
    $('.number').keypress(function (e) {
      var key = e.which || e.keyCode;
      var char = String.fromCharCode(key);
      var allowChar = '0123456789';
      if ($(this).is('.decimal')) {
        allowChar += '.';
      }

      if (allowChar.indexOf(char) == -1) {
        return false;
      }

      if (String($(this).val()).indexOf('.') > -1 && char == '.') {
        return false;
      }
    });
    var amount_usd  = "{{ $payment->getDataItem('amount') }}";
    var bundle_name  = "{{ $payment->getDataItem('bundle_name') }}";
    var secure_hashed  = '{{ $payment->getDataItem('secure_hashed') }}';
  </script>
@endsection