@extends('layouts.eldesigners')
@section('content')
    <style>
        .bg-landing, .w-action {
            background-image: url(/website/Pricing-Cover.svg);
            width: 100%;
            background-repeat: no-repeat;
            /* background-attachment: fixed; */
            background-position: unset;
            background-size: cover;
        }

        .bg-primary-opacity {
            background-color: rgba(251, 251, 251, 0);
        }
    </style>
    <div class="main-header main-header-fullwidth main-header-has-header-standard">
    @include('eldesigners.top_menu')
    <!-- ... end Header Standard Landing  -->
        <div class="header-spacer--standard"></div>

        <div class="stunning-header-content">
            <div class="main-header-content">
                <h1 class="stunning-header-title" style="color: white;font-weight: 500;">ARTICLES BY OUR DESIGNERS</h1>
            </div>
        </div>

        <div class="content-bg-wrap bg-landing"></div>
    </div>


    <!-- ... end Main BlogV1 -->




    <div class="container">
        @yield('content2')
    </div>

@endsection