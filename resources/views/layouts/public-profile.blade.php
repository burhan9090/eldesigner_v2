<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 12/1/18
 * Time: 8:01 PM
 */
?>
@extends('layouts.application')
@section('content')

    <!-- Top Header-Profile -->

    <div class="container">
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 fixed-top-head">
                <div class="ui-block">
                    <div class="top-header">
                        <div class="top-header-thumb cover_photo">
                            @php
                                if(!isset($user_info)){
                                $user_info = auth()->user();
                                }
                            @endphp
                            @if($user_info->cover)
                                <img src="{{ asset($user_info->cover) }}" alt="nature">
                            @else
                                <img src="{{ asset('/img/top-header2.jpg') }}" alt="nature">
                            @endif
                        </div>
                        <div class="profile-section">
                            <div class="row">
                                <div class="col col-lg-5 col-md-5 col-sm-12 col-12">
                                    <ul class="profile-menu">
                                        @if($user_info->id == Auth::user()->id)
                                            <li>
                                                <a href="{{ route('profile.show') }}" class="active">Profile</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('user.about', [$user_info->id]) }}">About</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('projects.index') }}">Projects</a>
                                            </li>
                                        @else
                                            <li>

                                            </li>
                                            <li>
                                                <a href="/profile/{{$user_info->user_name}}">Profile</a>
                                            </li>
                                            <li>
                                                <a href="/profile/about/{{$user_info->user_name}}">About</a>
                                            </li>
                                        @endif
                                    </ul>
                                </div>
                                @if($user_info->id == Auth::user()->id)
                                    <div class="col col-lg-5 ml-auto col-md-5 col-sm-12 col-12">
                                        <ul class="profile-menu">
                                            <li>
                                                <a href="{{ route('profile.followers-following') }}">Followers/Following</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('dashboard.index') }}" {!! strpos(request()->route()->uri, 'website_dashboard')!==false? 'class="active"': ''  !!}>Website
                                                    Dashboard</a>
                                            </li>
                                            <li>
                                                <a href="{{ route('statistics.index') }}">Statistics</a>
                                            </li>
                                            <li>
                                                <div class="more">
                                                    <svg class="olymp-three-dots-icon">
                                                        <use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                                    </svg>
                                                    <ul class="more-dropdown more-with-triangle">
                                                        <li>
                                                            <a href="#">Networks</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Edit profile</a>
                                                        </li>
                                                        <li>
                                                            <a href="#">Report an issue</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                @else
                                    <div class="col col-lg-5 ml-auto col-md-5 col-sm-12 col-12">
                                        <ul class="profile-menu">
                                            <li>                        <a href="/profile/projects/{{$user_info->user_name}}">Projects</a>
                                            </li>
                                            <li>
                                                <a href="/profile/articles/{{$user_info->user_name}}">Articles</a>
                                            </li>
                                            <li>

                                            </li>
                                            <li>

                                            </li>
                                        </ul>
                                    </div>
                                @endif
                            </div>

                            <div class="control-block-button">
                                @if(Auth::user()->id != $user_info->id)
                                    <a href="#" class="btn btn-control bg-grey-light accept-request request-del">
                    <span class="icon-minus">
                      <svg class="olymp-happy-face-icon"><use
                                  xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-happy-face-icon') }}"></use></svg>
                    </span>
                                    </a>


                                    <a href="#" class="btn btn-control bg-purple">
                                        <svg class="olymp-chat---messages-icon">
                                            <use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-chat---messages-icon') }}"></use>
                                        </svg>
                                    </a>
                                @else

                                    <div class="btn btn-control bg-primary more">
                                        <svg class="olymp-settings-icon">
                                            <use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-settings-icon') }}"></use>
                                        </svg>
                                        @endif
                                        <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                                            <li>
                                                <a href="{{ route('welcome.index') }}">Account Settings</a>
                                            </li>
                                        </ul>
                                    </div>
                            </div>
                        </div>
                        <div class="top-header-author user_logo">
                            <a href="{{ asset('/profile/'.$user_info->user_name) }}" class="author-thumb">
                                @if($user_info->logo)
                                    <img src="{{ asset($user_info->logo) }}" alt="author">
                                @else
                                    <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                                @endif
                            </a>
                            <div class="author-content">
                                <a href="#" class="h4 author-name">{{$user_info->name}}</a>
                                <div class="country">{{$user_info->user_name}}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ... end Top Header-Profile -->
    <div class="container">
        @yield('content_profile')
    </div>
@endsection


