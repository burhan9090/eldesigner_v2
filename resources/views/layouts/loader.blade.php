<link rel="stylesheet" href="{{ asset('css/spinner.css') }}">

<div class="modal" id="spinner-loader" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
        <div class="container-spinner">
          <div class="shape shape-1"></div>
          <div class="shape shape-2"></div>
          <div class="shape shape-3"></div>
          <div class="shape shape-4"></div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  showLoader= function(){
    $('#spinner-loader').modal('show');
  };

  hideLoader= function(){
    $('#spinner-loader').modal('hide');
  };
</script>
