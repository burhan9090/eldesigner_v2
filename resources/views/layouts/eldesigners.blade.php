<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 12/1/18
 * Time: 7:43 PM
 */
?>
        <!doctype html>
<html class = "fontawesome-i2svg-active fontawesome-i2svg-complete wf-roboto-n3-active wf-roboto-n4-active wf-roboto-n5-active wf-roboto-n7-active wf-active" lang="{{ app()->getLocale() }}">
<head>

    @include('includes.head')
</head>
<body class="body-bg-white">
<div>
    @yield('content')
</div>

<footer>
    @include('includes.full_footer')
    @yield('script')
</footer>
</footer>
@include('includes.embedded-script')
</body>
</html>

