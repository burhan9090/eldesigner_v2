<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 12/1/18
 * Time: 8:01 PM
 */
?>
@extends('layouts.application')
@section('content')

  <!-- Top Header-Profile -->
  @php
    if(!isset($user_info)){
    $user_info = auth()->user();
    }
    if(!isset($oUserInfo)){
    $oUserInfo = $user_info;
    }
  @endphp
  <div class="container">
    <div class="row">
      <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 fixed-top-head">
        <div class="ui-block">
          <div class="top-header">
            @if(Auth::user()->id == $user_info->id)
            <div class="top-header-thumb cover_photo">
              @if($user_info->cover)
                <img src="{{ asset($user_info->cover) }}" alt="nature" style="object-fit: cover">
              @else
                <img src="{{ asset('uploads/user/dummy_cover.png') }}" class="fit-image" alt="nature" style="object-fit: cover">
              @endif
            </div>
            <div class="top-header-author user_logo" style="padding: 35px 0;top: 26px;width: 200px;height: 245px;border-radius: 8px;background: #212121a3; color: white;border-color: black;">
              <a href="{{ asset('/profile/'.$user_info->user_name) }}" class="author-thumb">
                @if($user_info->logo)
                  <img src="{{ asset($user_info->logo) }}" alt="author" style="object-fit: cover">
                @else
                  <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author" style="object-fit: cover">
                @endif
              </a>
              <div class="author-content">
                <a href="#" class="h4 author-name" style="color: white">{{$user_info->name}}</a>
                <div class="country">{{$user_info->user_name}}</div>
              </div>
            </div>
            @else
              <div class="top-header-thumb cover_photo">
                @if($oUserInfo->cover)
                  <img src="/{{$oUserInfo->cover}}" alt="nature">
                @else
                  <img src="{{ asset('uploads/user/dummy_cover.png') }}" alt="nature">
                @endif

                <div class="top-header-author">
                  @if($oUserInfo->logo)
                    <div class="author-thumb user_logo">
                      <img src="/{{ $oUserInfo->logo }}" alt="author">
                    </div>
                  @else
                    <div class="author-thumb user_logo">
                      <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                    </div>
                  @endif
                  <div class="author-content">
                    <a href="#" class="h3 author-name">{{ $oUserInfo->name }}</a>
                    @php
                      $user_country = \App\Country::find($oUserInfo->country_id);
                      $user_city = \App\City::find($oUserInfo->city_id)
                    @endphp
                    <div class="country">{{ $user_country?$user_country->text:'' }}  |  {{ $user_city? $user_city->text:'' }}</div>
                  </div>
                </div>
              </div>
              <div class="profile-section">
                <div class="row">
                  <div class="col col-xl-8 m-auto col-lg-8 col-md-12">
                    <ul class="profile-menu">

                      <li>
                        <a href="/profile/{{$oUserInfo->user_name}}" class="@php if(strpos($_SERVER['REQUEST_URI'],'/about') != -1) { echo 'active';} @endphp" >Timeline</a>
                      </li>

                        <li>

                          <a class="@php if(strpos($_SERVER['REQUEST_URI'],'/about') != -1) { echo 'active';} @endphp" href="/profile/{{$oUserInfo->user_name}}/about">About</a>
                        </li>
                        <li>
                          <a class="@php if(strpos($_SERVER['REQUEST_URI'],'/projects') != -1) { echo 'active';} @endphp" href="/profile/{{$oUserInfo->user_name}}/projects">Projects</a>
                        </li>
                        <li>
                          <a class="@php if(strpos($_SERVER['REQUEST_URI'],'/articles') != -1) { echo 'active';} @endphp" href="/profile/{{$oUserInfo->user_name}}/articles">Articles</a>
                        </li>
                        <li>
                          @if($oUserInfo->domain)
                            <a class="" href="{{ prep_url($oUserInfo->domain) }}">Visit My Website</a>
                          @else
                            <a class="" href="/{{$oUserInfo->user_name}}">Visit My Website</a>
                          @endif
                        </li>


                    </ul>
                  </div>
                </div>

                <div class="control-block-button">

                  <input type="hidden" id="request_id" value="{{$oUserInfo->id}}">
                  <a href="/profile/{{$oUserInfo->user_name}}" class="btn btn-control bg-grey-light friend_delete">
                  <span class="d-lg-none d-block follow">Follow</span>
                  <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                    viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                  <g>
                    <g>
                      <path d="M437.02,330.98c-27.883-27.882-61.071-48.523-97.281-61.018C378.521,243.251,404,198.548,404,148
                        C404,66.393,337.607,0,256,0S108,66.393,108,148c0,50.548,25.479,95.251,64.262,121.962
                        c-36.21,12.495-69.398,33.136-97.281,61.018C26.629,379.333,0,443.62,0,512h40c0-119.103,96.897-216,216-216s216,96.897,216,216
                        h40C512,443.62,485.371,379.333,437.02,330.98z M256,256c-59.551,0-108-48.448-108-108S196.449,40,256,40
                        c59.551,0,108,48.448,108,108S315.551,256,256,256z"/>
                    </g>
                  </g>  
                 </svg>
                  </a>

                  <a href="/website_dashboard/messages" class="btn btn-control bg-purple">
                     <span class="d-lg-none d-block message">Message</span>
                     <svg viewBox="0 -26 512 512" xmlns="http://www.w3.org/2000/svg"><path d="m256 100c-5.519531 0-10 4.480469-10 10s4.480469 10 10 10 10-4.480469 10-10-4.480469-10-10-10zm0 0"/><path d="m90 280c5.519531 0 10-4.480469 10-10s-4.480469-10-10-10-10 4.480469-10 10 4.480469 10 10 10zm0 0"/><path d="m336 0c-90.027344 0-163.917969 62.070312-169.632812 140.253906-85.738282 4.300782-166.367188 66.125-166.367188 149.746094 0 34.945312 13.828125 68.804688 39 95.632812 4.980469 20.53125-1.066406 42.292969-16.070312 57.296876-2.859376 2.859374-3.714844 7.160156-2.167969 10.898437 1.546875 3.734375 5.191406 6.171875 9.238281 6.171875 28.519531 0 56.003906-11.183594 76.425781-30.890625 19.894531 6.78125 45.851563 10.890625 69.574219 10.890625 90.015625 0 163.898438-62.054688 169.628906-140.222656 20.9375-.929688 42.714844-4.796875 59.945313-10.667969 20.421875 19.707031 47.90625 30.890625 76.425781 30.890625 4.046875 0 7.691406-2.4375 9.238281-6.171875 1.546875-3.738281.691407-8.039063-2.167969-10.898437-15.003906-15.003907-21.050781-36.765626-16.070312-57.296876 25.171875-26.828124 39-60.6875 39-95.632812 0-86.886719-86.839844-150-176-150zm-160 420c-23.601562 0-50.496094-4.632812-68.511719-11.800781-3.859375-1.539063-8.269531-.527344-11.078125 2.539062-12.074218 13.199219-27.773437 22.402344-44.878906 26.632813 9.425781-18.058594 11.832031-39.347656 6.097656-59.519532-.453125-1.589843-1.292968-3.042968-2.445312-4.226562-22.6875-23.367188-35.183594-53.066406-35.183594-83.625 0-70.46875 71.4375-130 156-130 79.851562 0 150 55.527344 150 130 0 71.683594-67.289062 130-150 130zm280.816406-186.375c-1.152344 1.1875-1.992187 2.640625-2.445312 4.226562-5.734375 20.171876-3.328125 41.460938 6.097656 59.519532-17.105469-4.226563-32.804688-13.433594-44.878906-26.632813-2.808594-3.0625-7.21875-4.078125-11.078125-2.539062-15.613281 6.210937-37.886719 10.511719-58.914063 11.550781-2.921875-37.816406-21.785156-73.359375-54.035156-99.75h130.4375c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10h-161.160156c-22.699219-11.554688-48.1875-18.292969-74.421875-19.707031 5.746093-67.164063 70.640625-120.292969 149.582031-120.292969 84.5625 0 156 59.53125 156 130 0 30.558594-12.496094 60.257812-35.183594 83.625zm0 0"/><path d="m256 260h-126c-5.523438 0-10 4.476562-10 10s4.476562 10 10 10h126c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10zm0 0"/><path d="m256 320h-166c-5.523438 0-10 4.476562-10 10s4.476562 10 10 10h166c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10zm0 0"/><path d="m422 100h-126c-5.523438 0-10 4.476562-10 10s4.476562 10 10 10h126c5.523438 0 10-4.476562 10-10s-4.476562-10-10-10zm0 0"/></svg>
                  </a>


                </div>
              </div>
            @endif
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- ... end Top Header-Profile -->
  <div class="container">
    @yield('content_profile')
  </div>

@endsection


