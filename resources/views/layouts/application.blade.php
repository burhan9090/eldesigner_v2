<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 12/1/18
 * Time: 7:43 PM
 */
?>
        <!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>

    @include('includes.head')

    @yield('style')
    <style>
        .tab-container{
            display: flex;
        }
        .tab-element{
            padding: 10px 7px !important;
            margin-left: 14px !important;
            width: 100% !important;
        }
        .custom-active{
            background: #00bbde;
        }
        .custom-activ a{
            color: #fff;
        }
        .top-header-thumb:after{
            box-shadow: none;
        }
        .cat-list__item a:hover, .cat-list__item.active{
            background-color: #29aed6;
        }
        .left-menu .left-menu-icon{
            fill: #020202;
        }
        .left-menu a:hover svg {
            fill: black;
        }
        /*.tab-element a:hover {*/
            /*background-color: #36d9d3;*/
        /*}*/
    </style>
</head>
<body class="fuelux">
<div>
    <header class="header" id="site-header">
        @include('includes.header')
    </header>
    @include('includes.responsive_header')
    <div>
        @include('includes.sidebar')
    </div>
        <div class="header-spacer"></div>

    <div>

        @yield('content')
    </div>

    <footer>
        @include('includes.footer')

        @yield('script')
    </footer>
    <div class="appended-modals">
        <div class="edit_post_modal_container">

        </div>
        <div class="open_post_modal_container">

        </div>
    </div>
</div>
@include('includes.embedded-script')
</body>
</html>

