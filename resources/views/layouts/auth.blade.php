<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 12/1/18
 * Time: 7:43 PM
 */
?>
        <!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>

    @include('includes.head')

</head>
<body class="landing-page">
<div>
    @yield('content')

</div>

<footer>
    @include('includes.footer')
</footer>
@include('includes.embedded-script')
</body>
</html>

