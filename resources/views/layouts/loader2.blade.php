<link rel="stylesheet" href="{{ asset('css/loader2.css') }}">

<div class="modal" id="spinner-loader" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body" style="height: 185px;background: #39add0;">
                <div class="row">
                    <div class="banter-loader">
                        <div class="banter-loader__box"></div>
                        <div class="banter-loader__box"></div>
                        <div class="banter-loader__box"></div>
                        <div class="banter-loader__box"></div>
                        <div class="banter-loader__box"></div>
                        <div class="banter-loader__box"></div>
                        <div class="banter-loader__box"></div>
                        <div class="banter-loader__box"></div>
                        <div class="banter-loader__box"></div>
                    </div>
                </div>
                <p style="color:white;text-align: center">Generating your website ... </p>
            </div>
        </div>
    </div>
</div>

<script>
    showLoader2= function(){
        $('#spinner-loader').modal('show');
    };

    hideLoader2= function(){
        $('#spinner-loader').modal('hide');
    };
</script>
