@extends('layouts.application')
@section('content')
    <style>
        .bg-landing, .w-action {
            background-image: url(../website/Pricing-Cover.svg);
            width: 100%;
            background-repeat: no-repeat;
            /* background-attachment: fixed; */
            background-position: unset;
            background-size: cover;
        }

        .bg-primary-opacity {
            background-color: rgba(251, 251, 251, 0);
        }
    </style>
    <div class="main-header main-header-fullwidth main-header-has-header-standard">
    <!-- ... end Header Standard Landing  -->
        <div class="header-spacer--standard"></div>

        <div class="stunning-header-content">
            <div class="main-header-content">
                <h1 class="stunning-header-title" style="color: white;font-weight: 500;">ARTICLES BY OUR DESIGNERS</h1>
            </div>
        </div>

        <div class="content-bg-wrap bg-landing"></div>
    </div>


    <!-- ... end Main BlogV1 -->


    <div class="container">
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="ui-block responsive-flex1200">
                    <div class="ui-block-title">
                        <div class="w-select">
                            <div class="title">Filter By:</div>

                        </div>

                        <div class="w-select">
                            <fieldset class="form-group">
                                <select class="selectpicker form-control" >
                                    <option value="ASC">Date (Descending)</option>
                                    <option value="DESC">Date (Ascending)</option>
                                </select>
                            </fieldset>
                        </div>

                        <a href="#" data-toggle="modal" data-target="#create-photo-album"
                           class="btn btn-primary btn-md-2">Filter</a>

                        <form class="w-search" method="get" action="{{route('home.home_blog')}}">
                            <div class="form-group with-button">
                                <input class="form-control" name="searchblog" type="text"
                                       placeholder="Search Blog Posts......">
                                <button>
                                    <svg class="olymp-magnifying-glass-icon">
                                        <use xlink:href="/svg-icons/sprites/icons.svg#olymp-magnifying-glass-icon"></use>
                                    </svg>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="container">
        <div class="row">
            @if (!$articles->isEmpty())
                <div class="col col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
                    <div class="ui-block">


                        <!-- Post -->

                        @foreach($articles as $art)
                            <article class="hentry blog-post blog-post-v3">

                                <div class="post-thumb">
                                    <a href="{{ route("article.show", $art->id) }}"><img src="{{asset($art->image)}}"
                                                                                         alt="photo"></a>
                                    <a href="#" class="post-category bg-blue-light">ELDESIGNERS</a>
                                </div>

                                <div class="post-content">

                                    <div class="author-date">
                                        by
                                        <a class="h6 post__author-name fn" href="#">{{$art->user['name']}}</a>
                                        <div class="post__date">
                                            <time class="published" datetime="2017-03-24T18:18">
                                                {{$art->user['created_at']}}
                                            </time>
                                        </div>
                                    </div>

                                    <a href="{{ route("article.show", $art->id) }}"
                                       class="h3 post-title">{{$art->title}} </a>

                                    <p>{{str_limit(strip_tags($art->body),100)}}
                                    </p>

                                    <div class="post-additional-info inline-items">






                                    </div>
                                </div>

                            </article>
                    @endforeach




                    <!-- ... end Post -->

                    </div>

                    <!-- Pagination -->

                    <nav aria-label="Page navigation" class="justify-content-center">
                        <div class="justify-content-center">
                            {{$articles->links('vendor.pagination.bootstrap-4')}}

                        </div>
                    </nav>

                    <!-- ... end Pagination -->

                </div>
            @else
                <div class="col col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12" style="    margin-top: 45px !important;">
                    <div class="ui-block">
                        <div class="ui-block-title ">
                            <p class="h6 title center_text">No Blogs Found</p>
                        </div>
                    </div>
                </div>
            @endif
            <div class="col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
                <aside>

                    <div class="ui-block">
                        <div class="ui-block-title">
                            <h6 class="title">Featured Posts</h6>
                        </div>
                    </div>

                    @foreach($top as $top_top)
                        <div class="ui-block">


                            <!-- Post -->

                            <article class="hentry blog-post blog-post-v3 featured-post-item">

                                <div class="post-thumb">
                                    <a href="{{ route("article.show", $top_top->id) }}"><img
                                                src="{{asset($top_top->image)}}" alt="photo"></a>
                                    <a href="#" class="post-category bg-purple">INSPIRATION</a>
                                </div>

                                <div class="post-content">

                                    <div class="author-date">
                                        by
                                        <a class="h6 post__author-name fn"
                                           href="/profile/{{$top_top->user['user_name']}}">{{$top_top->user['name']}}</a>
                                        <div class="post__date">
                                            <time class="published" datetime="2017-03-24T18:18">
                                                - {{$top_top->created_at}}
                                            </time>
                                        </div>
                                    </div>

                                    <a href="{{ route("article.show", $top_top->id) }}" class="h4 post-title">{{str_limit(strip_tags($top_top->title),100)}}</a>

                                    <div class="post-additional-info inline-items">


                                    </div>
                                </div>

                            </article>

                            <!-- ... end Post -->

                        </div>
                    @endforeach

                </aside>
            </div>

        </div>

    </div>

@endsection