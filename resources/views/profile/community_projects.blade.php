@extends('layouts.application')
@section('content')
    <style>
        .bg-landing, .w-action {
            background-image: url(../website/Pricing-Cover.svg);
            width: 100%;
            background-repeat: no-repeat;
            /* background-attachment: fixed; */
            background-position: unset;
            background-size: cover;
        }

        .bg-primary-opacity {
            background-color: rgba(251, 251, 251, 0);
        }
        .fit-image{
            object-fit: cover;
            height: 419px;
        }
    </style>
    <div class="main-header main-header-fullwidth main-header-has-header-standard">
    <!-- ... end Header Standard Landing  -->
        <div class="header-spacer--standard"></div>

        <div class="stunning-header-content">
            <h1 class="stunning-header-title" style="color: white;font-weight: 500;">DESIGNS ACROSS THE GLOBE</h1>
        </div>

        <div class="content-bg-wrap bg-landing"></div>
    </div>

    <!-- End Stunning header -->



    <section>
        @if ($project_count > 0)
            <div class="container">
                <input type="hidden" id="count" value="{{$project_count}}">
                <input type="hidden" id="type" value="{{$type}}">
                <div class="row">
                    <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="ui-block responsive-flex1200">
                            <form method="get" action="{{ route('community.projects.list') }}">
                                <input type="hidden" name="type" value="{{$type}}">
                                <div class="ui-block-title">

                                    <div class="w-select">
                                        <div class="title">Filter By:</div>
                                        <fieldset class="form-group">
                                            <select name="filterby" class="selectpicker form-control" id="filterby">
                                                <option value="AD">All Design</option>
                                                <option value="MP">Most popular</option>
                                                <option value="HR">Highest Rated</option>
                                            </select>
                                        </fieldset>
                                    </div>

                                    <div class="w-select">
                                        <fieldset class="form-group">
                                            <select name="sort" class="selectpicker form-control" id="sort">
                                                <option value="ASC">Date (Descending)</option>
                                                <option value="DES">Date (Ascending)</option>
                                            </select>
                                        </fieldset>
                                    </div>

                                    <button type="submit" class="btn btn-blue btn-md-2">Filter Products</button>

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </section>
    <div class="row" style="padding: 75px">

        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="clients-grid">

                <ul class="cat-list-bg-style align-center sorting-menu">
                    <li class="cat-list__item  @if (empty($type)) active @endif" data-filter="*"><a href="{{route('community.projects.list')}}" class="">All Designers</a></li>
                    <li class="cat-list__item @if ($type == 'arch') active @endif" data-filter=".community"><a href="?type=arch" class="">Architects</a></li>
                    <li class="cat-list__item @if ($type == 'intd') active @endif" data-filter=".news"><a href="?type=intd" class="">Interior Designers</a></li>
                    <li class="cat-list__item @if ($type == 'gd') active @endif" data-filter=".inspiration"><a href="?type=gd" class="">Graphic Designers</a>
                    </li>
                    <li class="cat-list__item @if ($type == 'fd') active @endif" data-filter=".inspiration"><a href="?type=fd" class="">Fashion Design</a>
                    </li>
                    <li class="cat-list__item @if ($type == 'pg') active @endif" data-filter=".inspiration"><a href="?type=pg" class="">Photography</a></li>
                    <li class="cat-list__item @if ($type == 'ill') active @endif" data-filter=".inspiration"><a href="?type=ill" class="">Illustration</a></li>
                    <li class="cat-list__item @if ($type == 'mo') active @endif" data-filter=".inspiration"><a href="?type=mo" class="">Motion</a>
                    </li>


                </ul>


            </div>
        </div>
    </div>

    <section class="pt20">
        <div class="container">
            <div class="des_con row">
                @if ($project_count > 0)


                    @foreach($projects as $pro)
                        <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">


                            <!-- Product Item -->

                            <div class="shop-product-item">
                                <div class="product-thumb">
                                    <a href="{{ route('projects.show', $pro['pid']) }}">
                                        <img src="{{asset($pro['image'])}}" class="fit-image" alt="product">
                                    </a>
                                </div>
                                <div class="product-content">
                                    <div class="block-title">
                                        @if ($pro['profile_setting'] == 'private')
                                            <a href="/profile/{{$pro['user_name']}}"
                                               class="product-category">{{$pro['name']}}</a>
                                        @else
                                            <a href="/home/profile/{{$pro['user_name']}}"
                                               class="product-category">{{$pro['name']}}</a>
                                        @endif
                                        <a href="{{ route('projects.show', $pro['pid']) }}" class="h5 title">{{$pro['title']}}</a>
                                    </div>
                                    <div class="block-price">
                                        {{--<ul class="rait-stars">--}}
                                        {{--<li>--}}
                                        {{--<i class="fa fa-star star-icon c-primary" aria-hidden="true"></i>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                        {{--<i class="fa fa-star star-icon c-primary" aria-hidden="true"></i>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                        {{--<i class="fa fa-star star-icon c-primary" aria-hidden="true"></i>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                        {{--<i class="fa fa-star star-icon c-primary" aria-hidden="true"></i>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                        {{--<i class="far fa-star star-icon" aria-hidden="true"></i>--}}
                                        {{--</li>--}}
                                        {{--</ul>--}}
                                        {{--<div class="product-price">{{$pro['project_count']}} View</div>--}}


                                    </div>
                                </div>
                            </div>

                            <!-- ... end Product Item -->

                        </div>
                    @endforeach
                @else
                    <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="    margin-top: 45px !important;">
                        <div class="ui-block">
                            <div class="ui-block-title ">
                                <p class="h6 title center_text">No Designs Found</p>
                            </div>
                        </div>
                    </div>
            </div>

            @endif
        </div>
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12  align-center pb120 pt120">


                <!-- Pagination -->

                <nav aria-label="Page navigation">
                    <ul class="pagination justify-content-center" id="project-pagination"></ul>
                </nav>

                <!-- ... end Pagination -->

            </div>
        </div>
        </div>
    </section>


@endsection