@extends('layouts.profile')
@section('content_profile')
    <div class="row ui-block">
        <div class="row"  style="text-align: center;margin: auto;">

            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="clients-grid">
                    @if(in_array(request()->route()->getName(), ['welcome.index', 'setting.seo', 'services.index', 'skills', 'members', 'user_services', 'contact_us', 'themes.index', 'style.index']))
                        <ul class="cat-list-bg-style align-center sorting-menu">

                            <li class="cat-list__item {!! request()->route()->getName() == 'welcome.index'?'active':'' !!}">
                                <a href="{{ route('welcome.index') }}"
                                   class="{!! request()->route()->getName()=='welcome.index'? 'active': "" !!}">Welcome</a>
                            </li>
                            <li class="cat-list__item {!! request()->route()->getName() == 'setting.seo'?'active':'' !!}">
                                <a href="{{ route('setting.seo') }}"
                                   class="{!! request()->route()->getName()=='setting.seo'? 'active': "" !!}">SEO</a>
                            </li>
                            <li class="cat-list__item {!! request()->route()->getName() == 'services.index'?'active':'' !!}">
                                <a href="{{ route('services.index') }}"
                                   class="{!! request()->route()->getName()=='services.index'? 'active': "" !!}">Services
                                    and Information</a>
                            </li>
                            @if(auth()->user()->profile_type=='company')
                                <li class="cat-list__item {!! request()->route()->getName() == 'members'?'active':'' !!}">
                                    <a href="{{ route('members') }}"
                                       class="{!! request()->route()->getName()=='members'? 'active': "" !!}">Company
                                        Members</a>
                                </li>
                            @else
                                <li class="cat-list__item {!! request()->route()->getName() == 'skills'?'active':'' !!}">
                                    <a href="{{ route('skills') }}"
                                       class="{!! request()->route()->getName()=='skills'? 'active': "" !!}">Freelancer
                                        Skills</a>
                                </li>

                            @endif
                            <li class="cat-list__item {!! request()->route()->getName() == 'user_services'?'active':'' !!}">
                                <a href="{{ route('user_services') }}"
                                   class="{!! request()->route()->getName()=='user_services'? 'active': "" !!}">Services
                                    Types</a>
                            </li>
                            <li class="cat-list__item {!! request()->route()->getName() == 'contact_us'?'active':'' !!}">
                                <a href="{{ route('contact_us') }}" {!! request()->route()->getName()=='contact_us'? 'class="active"': "" !!}>Contact
                                    tab</a>
                            </li>
                            <li class="cat-list__item {!! request()->route()->getName() == 'themes.index'?'active':'' !!}">
                                <a href="{{ route('themes.index') }}" {!! request()->route()->getName()=='themes.index'? 'class="active"': "" !!}>Theme</a>
                            </li>
                            <li class="cat-list__item {!! request()->route()->getName() == 'style.index'?'active':'' !!}">
                                <a href="{{ route('style.index') }}" {!! request()->route()->getName()=='style.index'? 'class="active"': "" !!}>Style</a>
                            </li>
                        </ul>
                    @endif
                    @if(in_array(request()->route()->getName(), ['projects.index' , 'projects.library']))
                        <ul class="cat-list-bg-style align-center sorting-menu">
                            <li class="cat-list__item {!! request()->route()->getName() == 'projects.index'?'active':'' !!}">
                                <a href="{{ route('projects.index') }}" {!! request()->route()->getName()=='projects.index'? 'class="active"': "" !!}>Projects</a>
                            </li>
                            <li class="cat-list__item {!! request()->route()->getName() == 'projects.library'?'active':'' !!}">
                                <a href="{{ route('projects.library') }}" {!! request()->route()->getName()=='projects.library'? 'class="active"': "" !!}>Community
                                    Projects</a>
                            </li>
                        </ul>
                    @endif
                    @if(strpos(request()->route()->uri, 'article')!==false)
                        <ul class="cat-list-bg-style align-center sorting-menu">
                            <li class="cat-list__item {!! request()->route()->getName() == 'add_article'?'active':'' !!}">
                                <a href="{{ route('add_article') }}" {!! request()->route()->getName()=='add_article'? 'class="active"': "" !!}>Add
                                    an article</a>
                            </li>
                            <li class="cat-list__item {!! request()->route()->getName() == 'article.index'?'active':'' !!}">
                                <a href="{{ route('article.index') }}" {!! request()->route()->getName()=='article.index'? 'class="active"': "" !!}>Articles
                                    Library</a>
                            </li>
                            <li class="cat-list__item {!! request()->route()->getName() == 'article.view'?'active':'' !!}">
                                <a href="{{ route('article.view') }}" {!! request()->route()->getName()=='article.view'? 'class="active"': "" !!}>Community
                                    Articles</a>
                            </li>
                        </ul>
                    @endif
                    @if(in_array(request()->route()->getName(), ['tool.quiz', 'tools.water_mark', 'tools.domain.index']))
                        <ul class="cat-list-bg-style align-center sorting-menu">
                            <li class="cat-list__item {!! request()->route()->getName() == 'tool.quiz'?'active':'' !!}">
                                <a class="{!! request()->route()->getName()=='tool.quiz'? 'active': "" !!}"
                                   href="{{ route('tool.quiz') }}">Design Request Via Structured Images</a>
                            </li>
                            <li class="cat-list__item {!! request()->route()->getName() == 'tools.water_mark'?'active':'' !!}">
                                <a class="{!! request()->route()->getName()=='tools.water_mark'? 'active': "" !!}"
                                   href="{{ route('tools.water_mark') }}">Image Protection</a>
                            </li>
                            <li class="cat-list__item {!! request()->route()->getName() == 'tools.domain.index'?'active':'' !!}">
                                <a class="{!! request()->route()->getName()=='tools.domain.index'? 'active': "" !!}"
                                   href="{{ route('tools.domain.index') }}">Buy a Domain</a>
                            </li>
                        </ul>
                    @endif
                </div>
            </div>
        </div>

        <div class="col col-xl-12 order-xl-2 col-lg-12 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12" style="margin-top: 10px">
            <div class="ui-block">
                <!-- Single Post -->
                @if(isset($header))
                    <article class="hentry blog-post single-post single-post-v3">

                        <div class="more" style="float: right" id="toggle-content">
                            <svg class="olymp-three-dots-icon">
                                <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use>
                            </svg>
                            {{--<ul class="more-dropdown more-with-triangle">--}}
                            {{--<li>--}}
                            {{--<a href="#">Hide This Message</a>--}}
                            {{--</li>--}}

                            {{--</ul>--}}
                        </div>


                        <div class="post-content-wrap">
                            <div class="post-header">
                                <h3 style="text-align: center">{{ $title }} </h3>
                            </div>
                            <div class="post-content" id="content-container" style="display: none;">

                                <div class="video-thumb bg-violet">
                                    <img src="{{ asset('img/video-thumb.png') }}" alt="video">
                                    <a href="{{ $header->video }}" class="play-video">
                                        <svg class="olymp-play-icon">
                                            <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-play-icon') }}"></use>
                                        </svg>
                                    </a>
                                    <div class="overlay overlay-dark"></div>
                                </div>

                                <p>{{ $header->description }}</p>
                            </div>
                        </div>
                    </article>
                @endif
            </div>

            @yield('profile_content')

        </div>
    </div>
@endsection