<div class="container">
  <div class="row">
    <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
      <div class="ui-block">
        <div class="top-header">
          <div class="top-header-thumb">
            <img src="{{ asset(auth()->user()->cover?: 'img/top-header1.jpg') }}" alt="nature">
          </div>
          <div class="profile-section">
            <div class="row">
              <div class="col col-lg-5 col-md-5 col-sm-12 col-12">
                <ul class="profile-menu">
                  <li>
                    <a href="02-ProfilePage.html" class="active">Profile</a>
                  </li>

                  <li>
                    <a href="05-ProfilePage-About.html">About</a>
                  </li>
                  <li>
                    <a href="06-ProfilePage.html">Projects</a>
                  </li>
                </ul>
              </div>
              <div class="col col-lg-5 ml-auto col-md-5 col-sm-12 col-12">
                <ul class="profile-menu">
                  <li>
                    <a href="{{ route('add_article') }}" {!! strpos(request()->route()->uri, 'website_dashboard')!==false? 'class="active"': ''  !!}>Website Dashboard</a>
                  </li>
                  <li>
                    <a href="09-ProfilePage-Videos.html">Statistics</a>
                  </li>
                  <li>
                    <div class="more">
                      <svg class="olymp-three-dots-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use></svg>
                      <ul class="more-dropdown more-with-triangle">
                        <li>
                          <a href="#">Networks</a>
                        </li>
                        <li>
                          <a href="#">Edit profile</a>
                        </li>
                        <li>
                          <a href="#">Report an issue</a>
                        </li>
                      </ul>
                    </div>
                  </li>
                </ul>
              </div>
            </div>

            <div class="control-block-button">
              <a href="35-YourAccount-FriendsRequests.html" class="btn btn-control bg-blue">
                <svg class="olymp-happy-face-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-happy-face-icon') }}"></use></svg>
              </a>

              <a href="#" class="btn btn-control bg-purple">
                <svg class="olymp-chat---messages-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-chat---messages-icon') }}"></use></svg>
              </a>

              <div class="btn btn-control bg-primary more">
                <svg class="olymp-settings-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-settings-icon') }}"></use></svg>

                <ul class="more-dropdown more-with-triangle triangle-bottom-right">
                  <li>
                    <a href="#" data-toggle="modal" data-target="#update-header-photo">Update Profile Photo</a>
                  </li>
                  <li>
                    <a href="#" data-toggle="modal" data-target="#update-header-photo">Update Header Photo</a>
                  </li>
                  <li>
                    <a href="29-YourAccount-AccountSettings.html">Account Settings</a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="top-header-author user_logo">
            <a href="{{ route('profile.show', auth()->user()->user_name) }}" class="author-thumb">
              <img src="{{ asset(auth()->user()->logo?: 'img/author-main1.jpg') }}" alt="author">
            </a>
            <div class="author-content">
              <a href="02-ProfilePage.html" class="h4 author-name">Designer's Name</a>
              <div class="country">City, Country</div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>