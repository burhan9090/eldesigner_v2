<div class="container">
    <div class="row">
        <div class="col col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12 responsive-display-none">
            <div class="ui-block">

                <!-- Your Profile  -->

                <div class="your-profile">
                    <div class="ui-block-title ui-block-title-small">
                        <h6 class="title">Your PROFILE</h6>
                    </div>
                    <div class="ui-block-title">
                        <a href="33-YourAccount-Notifications.html" class="h6 title">Website dashboard</a>
                        <a href="#" class="items-round-little bg-primary">8</a>
                    </div>
                    <div id="content-block" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header" role="tab" id="content-nav">
                                <h6 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#content-block" href="#content-collapse" aria-expanded="true"
                                       aria-controls="content-collapse" {{ in_array(request()->route()->getName(), ['services.index', 'contact_us', 'projects.index'])? '': 'class="collapsed"'  }}>
                                        Content
                                        <svg class="olymp-dropdown-arrow-icon">
                                            <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon') }}"></use>
                                        </svg>
                                    </a>
                                </h6>
                            </div>

                            <div id="content-collapse" class="collapse {{ in_array(request()->route()->getName(), ['services.index', 'contact_us', 'projects.index'])? 'show': ''  }}" role="tabpanel" aria-labelledby="content-nav">
                                <ul class="your-profile-menu">
                                    <li>
                                        <a href="{{ route('welcome.index') }}">Welcoming Screen</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('projects.index') }}">Projects</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('services.index') }}">Services and Information</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('contact_us') }}">Contact tab</a>
                                    </li>
                                    <li>
                                        <a href="32-YourAccount-EducationAndEmployement.html">Style</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div id="articles-block" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header" role="tab" id="article-nav">
                                <h6 class="mb-0">
                                    <a data-toggle="collapse" data-parent="#articles-block" href="#article-collapse" aria-expanded="true"
                                       aria-controls="article-collapse" {{ strpos(request()->route()->uri, 'article')!==false? '': 'class="collapsed"'  }}>
                                        Articles
                                        <svg class="olymp-dropdown-arrow-icon">
                                            <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon') }}"></use>
                                        </svg>
                                    </a>
                                </h6>
                            </div>

                            <div id="article-collapse" class="collapse {{ strpos(request()->route()->uri, 'article')!==false? 'show': ''  }}" role="tabpanel" aria-labelledby="article-nav">
                                <ul class="your-profile-menu">
                                    <li>
                                        <a href="{{ route('add_article') }}">Add an article</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('article.index') }}">Articles Library</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('article.view') }}">Community Articles</a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="ui-block-title">
                        <a href="{{ route('personal_information.index') }}" class="h6 title">Personal Information</a>
                    </div>

                    <div class="ui-block-title">
                        <a href="{{ route('user_history.index') }}" class="h6 title">Education & Employment</a>
                    </div>

                    <div class="ui-block-title">
                        <a href="34-YourAccount-ChatMessages.html" class="h6 title">Tools</a>
                    </div>
                    <div class="ui-block-title">
                        <a href="35-YourAccount-FriendsRequests.html" class="h6 title">Orders</a>
                        <a href="#" class="items-round-little bg-blue">4</a>
                    </div>

                    <div class="ui-block-title">
                        <a href="36-FavPage-SettingsAndCreatePopup.html" class="h6 title">Statistics</a>
                    </div>
                    <div class="ui-block-title">
                        <a href="36-FavPage-SettingsAndCreatePopup.html" class="h6 title">Settings</a>
                    </div>
                </div>

                <!-- ... end Your Profile  -->


            </div>
        </div>

        <div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
            <div class="ui-block">
                <!-- Single Post -->
                @if(isset($header))
                    <article class="hentry blog-post single-post single-post-v3">

                        <div class="more" style="float: right">
                            <svg class="olymp-three-dots-icon">
                                <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use>
                            </svg>
                            <ul class="more-dropdown more-with-triangle">
                                <li>
                                    <a href="#">Hide This Message</a>
                                </li>

                            </ul>
                        </div>


                        <div class="post-content-wrap">
                            <div class="post-content">
                                <h3 style="text-align: center">{{ $title }} </h3>

                                <div class="video-thumb bg-violet">
                                    <img src="{{ asset('img/video-thumb.png') }}" alt="video">
                                    <a href="{{ $header->video }}" class="play-video">
                                        <svg class="olymp-play-icon">
                                            <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-play-icon') }}"></use>
                                        </svg>
                                    </a>
                                    <div class="overlay overlay-dark"></div>
                                </div>

                                <p>{{ $header->description }}</p>
                            </div>
                        </div>
                    </article>
            @endif

            <!-- ... end Single Post -->
            </div>

            @yield('profile_content')

        </div>
    </div>
</div>
