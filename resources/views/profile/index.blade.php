<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 12/1/18
 * Time: 8:01 PM
 */
?>
@extends('layouts.profile')
@section('content_profile')
    <div class="row" id="profile">

        <!-- Main Content -->

        <div class="col col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">
            <div id="newsfeed-items-grid">

                @auth()
                    @if(auth()->id()==$user_info->id)
                <div class="ui-block" style="border-width: 1px; border-color:#44aec8">

                    <!-- Post -->

                    <article class="hentry post video">

                        <div class="post__author author vcard inline-items">
                            <img src="{{ asset('website/Logo-blue.png') }}" alt="author">

                            <div class="author-date">
                                <a class="h6 post__author-name fn">Help Center</a>

                            </div>
                        </div>

                        <p>Welcome to elDesigners - Here's a video tutorial to guide you through you profile and
                            dashboard</p>

                        <div class="post-video">
                            <iframe width="100%" height="315" src="https://www.youtube.com/embed/MCK5FrtSvDk?rel=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                        </div>
                    </article>

                    <!-- .. end Post -->
                </div>
                    @endif
                @endauth
                @include('timeline.post')
                <input type="hidden" id="last_page" value="{{ $posts->lastPage() }}">
                <div style="display: none; text-align: center;" id="load_more_posts">
                    <img class="center-block" src="/img/post_loader.gif"
                         style="text-align: center;height: 30px;width: 30px;" alt="Loading..."/>
                </div>
                <div style="text-align: center;">
                    <button class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color" id="btn-load-more"
                            style="display: none;">
                        Load More
                    </button>
                </div>
            </div>

            <a id="load-more-button" href="#" class="btn btn-control btn-more" data-load-link="items-to-load.html"
               data-container="newsfeed-items-grid">
                <svg class="olymp-three-dots-icon">
                    <use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                </svg>
            </a>
        </div>

        <!-- ... end Main Content -->


        <!-- Left Sidebar -->

        <div class="col col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-12">

            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">Overview</h6>
                    <!-- This shoud be a clickable link to take the designer to edit his profile -->

                </div>
                <div class="ui-block-content">

                    <!-- W-Personal-Info -->

                    <ul class="widget w-personal-info item-block">
                        <li>
                            <span class="title">Bio</span>
                            <span class="text">{{$user_info->description}}</span>
                        </li>
                        <li>
                            <span class="title">Working Fields</span>
                            <span class="text">{{$user_info->profile_type}}</span>
                        </li>
                        <li>
                            <span class="title">Technicals</span>
                            <span class="text">Autodesk Max, Autodesk Revit, Photoshop</span>
                        </li>
                    </ul>

                    <!-- .. end W-Personal-Info -->
                    <!-- W-Socials -->

                    <div class="widget w-socials">
                        <h6 class="title">Other Social Networks:</h6>
                        @if(!empty($user_info->fb_link))
                            <a href="{{$user_info->fb_link}}" class="social-item bg-facebook">
                                <i class="fab fa-facebook-f" aria-hidden="true"></i>
                                Facebook
                            </a>
                        @endif
                        @if(!empty($user_info->tw_link))
                            <a href="{{$user_info->tw_link}}" class="social-item bg-twitter">
                                <i class="fab fa-twitter" aria-hidden="true"></i>
                                Twitter
                            </a>
                        @endif
                        @if(!empty($user_info->website))
                            <a href="{{$user_info->website}}" class="social-item bg-dribbble">
                                <i class="fab fa-dribbble" aria-hidden="true"></i>
                                Website
                            </a>
                        @endif
                    </div>


                    <!-- ... end W-Socials -->
                </div>
            </div>

            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">Designers You May Follow</h6>
                    <a href="#" class="more">
                        <svg class="olymp-three-dots-icon">
                            <use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                        </svg>
                    </a>
                </div>


                <!-- W-Action -->

                <friend-suggestions :friend_suggestions="friend_suggestions"
                                    v-on:requestfriend="requestFriend"></friend-suggestions>

                <!-- ... end W-Action -->
            </div>

            <!-- ... end W-Build-Fav -->

        </div>

        <!-- ... end Left Sidebar -->


        <!-- Right Sidebar -->

        <div class="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12">

            <div class="ui-block">
                <div class="ui-block-title">
                    <!-- Here it will show the last designs uploaded regardless if they were images or projects, If the designers
                has both designs and projects, only show the cover photo of the project + other project + Images -->

                    <h6 class="title">Your Recent Designs</h6>
                </div>
                <div class="ui-block-content">

                    <!-- W-Latest-Photo -->

                    <ul class="widget w-last-photo ">
                        @if (empty($recent_designs))
                            @foreach($recent_designs as $des)
                                <li>
                                    <a href="/website_dashboard/projects/{{$des['project_id']}}">
                                        <img src="/{{$des['image']}}" alt="photo">
                                    </a>
                                </li>
                            @endforeach
                        @else
                            <div style="justify-content: center;text-align: center">
                                No Recent Designs
                            </div>
                        @endif

                    </ul>


                    <!-- .. end W-Latest-Photo -->
                </div>
            </div>


            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">Followers</h6>
                </div>
                <div class="ui-block-content">

                    <!-- W-Faved-Page -->

                    <ul class="widget w-faved-page">
                        @foreach($followers as $f)
                            <li>
                                <a href="/profile/{{$f->userfollower['user_name']}}">
                                    @if($f->userfollower['logo'])
                                        <img src="/{{$f->userfollower['logo']}}" alt="author"
                                             title="{{$f->userfollower['name']}}">
                                    @else
                                        <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author"
                                             title="{{$f->userfollower['name']}}">
                                    @endif
                                </a>
                            </li>
                        @endforeach
                        @if($followers_count > 14)
                            <li class="all-users">
                                <a href="{{ route('profile.followers-following') }}">+{{$followers_count}}</a>
                            </li>
                        @endif
                    </ul>

                    <!-- .. end W-Faved-Page -->
                </div>
            </div>
            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">Recent Articles</h6>
                </div>
                <!-- W-Blog-Posts -->

                <ul class="widget w-blog-posts">
                    @foreach($recent_art as $art)
                        <li>
                            <article class="hentry post">

                                <a href="/website_dashboard/article/{{$art->id}}" class="h4">{{$art->title}}</a>

                                <p> @php
                                        echo str_limit(strip_tags($art->body),100);
                                    @endphp
                                </p>

                                <div class="post__date">
                                    <time class="published" datetime="2017-03-24T18:18">
                                        {{$art->created_at}}
                                    </time>
                                </div>

                            </article>
                        </li>
                    @endforeach

                </ul>

                <!-- .. end W-Blog-Posts -->
            </div>

            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">Companies You Follow</h6>
                </div>

                <!-- W-Friend-Pages-Added -->

                <ul class="widget w-friend-pages-added notification-list friend-requests">
                    @if(!$following_company->isEmpty())
                        @foreach($following_company as $fc)
                            <li class="inline-items">
                                <div class="author-thumb header_logo">
                                    @if($fc->userfollower['logo'])
                                        <img src="{{ asset('uploads/user/' . $fc->user['logo']) }}" alt="author" title="{{$fc->user['name']}}">
                                    @else
                                        <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author"
                                             title="{{$fc->user['name']}}">
                                    @endif
                                </div>
                                <div class="notification-event">
                                    <a href="/profile/{{$fc->user['user_name']}}"
                                       class="h6 notification-friend">{{$fc->user['name']}}</a>
                                    <span class="chat-message-item">{{$fc->user['profile_type']}}</span>
                                </div>

                                </span>

                            </li>
                        @endforeach
                    @endif
                </ul>

                <!-- .. end W-Friend-Pages-Added -->
            </div>


        </div>

        <!-- ... end Right Sidebar -->

    </div>
@endsection
