<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 12/1/18
 * Time: 8:01 PM
 */
?>
@extends('layouts.profile')
@section('content_profile')

    <div class="row" id="app">
  
        <div class="col col-xl-12 order-xl-1 col-lg-12 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">Chat / Messages</h6>
                    <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
                </div>

                <div class="row" v-if="side_message.length > 0">
                    <div class="col col-xl-4 col-lg-4 col-md-12 col-sm-12  padding-r-0">
                        <!-- Notification List Chat Messages -->
                                <chat-side-menu v-on:openchat="openChatMessage" :message_side_data="side_message" :selected_friend="friend_id" :selected_conv_id="query_conv_id" v-on:updatesidemenu="updateSideMenu"></chat-side-menu>

                        <!-- ... end Notification List Chat Messages -->
                    </div>

                    <div class="col col-xl-8 col-lg-8 col-md-12 col-sm-12  padding-l-0" v-if="friend_id != ''" >


                        <!-- Chat Field -->
                        <div >
                        <chat-panel  :messages="messages"></chat-panel>
                        </div>
                        <form>
                            <chat-composer v-on:messagesent="addMessage" :friend_id="friend_id" v-on:typingmessage="typingMessageFriend" :user="{{Auth::user()}}"></chat-composer>
                        </form>
                        <!-- ... end Chat Field -->

                    </div>
                </div>
                
                <div class="row" v-else>
                <div class="col col-xl-12 order-xl-12 col-lg-12 order-lg-12 col-md-12 order-md-1 col-sm-12 col-12">
                <ul class="notification-list chat-message align-center">
                    <li>
                    You Have No New Messages
                    </li>
                </ul>
                
                </div>
                    
                </div>
                
            </div>


            <!-- Pagination -->

            <!-- <nav aria-label="Page navigation">
                <ul class="pagination justify-content-center">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" tabindex="-1">Previous</a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1<div class="ripple-container"><div class="ripple ripple-on ripple-out" style="left: -10.3833px; top: -16.8333px; background-color: rgb(255, 255, 255); transform: scale(16.7857);"></div></div></a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">...</a></li>
                    <li class="page-item"><a class="page-link" href="#">12</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#">Next</a>
                    </li>
                </ul>
            </nav> -->

            <!-- ... end Pagination -->

        </div>
@endsection
