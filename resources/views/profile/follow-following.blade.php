@extends('layouts.profile')

@section('content_profile')


    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item" style="width: 50%">
            <a class="nav-link active" data-toggle="tab" href="#home" role="tab">
                Followers
            </a>
        </li>
        <li class="nav-item" style="width: 50%">
            <a class="nav-link" data-toggle="tab" href="#profile" role="tab">
                Following
            </a>
        </li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="home" role="tabpanel" data-mh="log-tab" style="min-height: 300px">
            <div class="infinite-scroll_two">
            <div class="row" style="margin-top: 10px">
                @if(!$followers->isEmpty())
                    @foreach ($followers as $user)
                        <div class="col col-xl-3 col-lg-6 col-md-6 col-sm-6 col-6">
                            <div class="ui-block">

                                <!-- Friend Item -->

                                <div class="friend-item">
                                    <div class="friend-header-thumb " style="height: 95px !important;">
                                        @if($user->userfollower['cover'])
                                            <img src="/{{$user->userfollower['cover']}}" alt="friend">
                                        @else
                                            <img src="img/friend9.jpg" alt="friend">
                                        @endif
                                    </div>

                                    <div class="friend-item-content">


                                        <div class="friend-avatar">
                                            <div class="author-thumb discover_logo">
                                                @if($user->userfollower['logo'])
                                                    <img src="/{{$user->userfollower['logo']}}" alt="friend">
                                                @else
                                                    <img src="img/avatar16.jpg" alt="author">
                                                @endif                            </div>
                                            <div class="author-content">
                                                <a href="/profile/{{ $user->userfollower['user_name'] }}"
                                                   class="h5 author-name">{{ str_limit($user->userfollower['name'],20,' ...') }}</a>
                                                <div class="country">{{ $user->userfollower['user_name'] }}</div>
                                            </div>
                                        </div>

                                        <div class="swiper-container">
                                            <div class="swiper-wrapper">
                                                <div class="swiper-slide">
                                                    <p class="friend-about" data-swiper-parallax="-500">
                                                        {{str_limit($user->userfollower['description'],150,' ...')}}
                                                    </p>
                                                    <div class="control-block-button" data-swiper-parallax="-100">
                                                        <a href="/profile/{{ $user->userfollower['user_name'] }}"
                                                           data-toggle="modal"
                                                           data-target="#blog-post-popup"
                                                           class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color"
                                                           style="background-color: #38a9ff; color: white">View
                                                            <div class="ripple-container"></div>
                                                        </a>

                                                        <a href="#"
                                                           @click.prevent="openChatDiscoverFriends({{$user->userfollower['id']}})"
                                                           data-toggle="modal" data-target="#blog-post-popup"
                                                           class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color">Message
                                                            <div class="ripple-container"></div>
                                                        </a>


                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                                <!-- ... end Friend Item -->                    </div>
                        </div>
                    @endforeach
                        <div class="two">
                        {{ $followers->links() }}
                        </div>
                @else
                    <div class="container">
                        <div class="row">
                            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="ui-block">
                                    <div class="ui-block-title ">
                                        <p class="h6 title center_text">No Result Found</p>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>

                @endif
            </div>
            </div>
        </div>
        <div class="tab-pane" id="profile" role="tabpanel" data-mh="log-tab" style="min-height: 300px">
            <div class="infinite-scroll">
                <div class="row" style="margin-top: 10px">
                    @if(!$following->isEmpty())

                        @foreach ($following as $user)
                            <div class="col col-xl-3 col-lg-6 col-md-6 col-sm-6 col-6">
                                <div class="ui-block">

                                    <!-- Friend Item -->

                                    <div class="friend-item">
                                        <div class="friend-header-thumb " style="height: 95px !important;">
                                            @if($user->user['cover'])
                                                <img src="/{{$user->user['cover']}}" alt="friend">
                                            @else
                                                <img src="img/friend9.jpg" alt="friend">
                                            @endif
                                        </div>

                                        <div class="friend-item-content">


                                            <div class="friend-avatar">
                                                <div class="author-thumb discover_logo">
                                                    @if($user->user['logo'])
                                                        <img src="/{{$user->user['logo']}}" alt="friend">
                                                    @else
                                                        <img src="img/avatar16.jpg" alt="author">
                                                    @endif                            </div>
                                                <div class="author-content">
                                                    <a href="/profile/{{ $user->user['user_name'] }}"
                                                       class="h5 author-name">{{ str_limit($user->user['name'],20,' ...') }}</a>
                                                    <div class="country">{{ $user->user['user_name'] }}</div>
                                                </div>
                                            </div>

                                            <div class="swiper-container">
                                                <div class="swiper-wrapper">
                                                    <div class="swiper-slide">
                                                        <p class="friend-about" data-swiper-parallax="-500">
                                                            {{str_limit($user->user['description'],150,' ...')}}
                                                        </p>
                                                        <div class="control-block-button" data-swiper-parallax="-100">
                                                            <a href="/profile/{{ $user->user['user_name'] }}"
                                                               data-toggle="modal"
                                                               data-target="#blog-post-popup"
                                                               class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color"
                                                               style="background-color: #38a9ff; color: white">View
                                                                <div class="ripple-container"></div>
                                                            </a>

                                                            <a href="#"
                                                               @click.prevent="openChatDiscoverFriends({{$user->user['id']}})"
                                                               data-toggle="modal" data-target="#blog-post-popup"
                                                               class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color">Message
                                                                <div class="ripple-container"></div>
                                                            </a>


                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                    <!-- ... end Friend Item -->                    </div>
                            </div>
                        @endforeach
                        <div class="one">
                        {{ $following->links() }}
                        </div>
                    @else
                        <div class="container">
                            <div class="row">
                                <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                    <div class="ui-block">
                                        <div class="ui-block-title ">
                                            <p class="h6 title center_text">No Result Found</p>
                                        </div>

                                    </div>
                                </div>

                            </div>

                        </div>

                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
