@extends('profile.not_friend_profile_head')
@section('content_not_friend')

    <div class="row profile-view-look">
        <div class="col col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-sm-12 col-12">
            <div id="newsfeed-items-grid">
                    @if(!$posts->isEmpty() && $oUserInfo->profile_setting != 'private')
                        @include('timeline.post')
                    <input type="hidden" id = "last_page"  value="{{ $posts->lastPage() }}">
                    <div style="display: none; text-align: center;" id = "load_more_posts">
                        <img class="center-block" src="/img/post_loader.gif" style="text-align: center;height: 30px;width: 30px;" alt="Loading..." />
                    </div>
                    <div style="text-align: center;">
                        <button class = "btn btn-md-2 btn-border-think c-grey btn-transparent custom-color" id="btn-load-more" style="display: none;">
                            Load More
                        </button>
                    </div>
                    @else
                    <div class="ui-block" style="text-align: center;">
                        <div class="ui-block-title">
                            <p>No recent Post to show</p>
                        </div>
                    </div>
                    @endif

            </div>
        </div>

        <div class="col col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-12">
            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">Page Intro</h6>
                    <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
                </div>
                <div class="ui-block-content">

                    <!-- W-Personal-Info -->

                    <ul class="widget w-personal-info item-block">
                        <li>
                            <span class="text">{{$oUserInfo->description}}</span>
                        </li>
                        <li>
                            <span class="title">Created:</span>
                            <span class="text">{{$oUserInfo->created_at}}</span>
                        </li>
                        @if (isset($oUserInfo->country['text']) && isset($oUserInfo->city['text'] ))
                        <li>
                            <span class="title">Based in:</span>
                            <span class="text">{{$oUserInfo->city['text']}}, {{$oUserInfo->country['text']}}</span>
                        </li>
                        @endif
                        <li>
                            <span class="title">Contact:</span>
                            <a href="#" class="text">{{$oUserInfo->email}}</a>
                        </li>
                        <li>
                            <span class="title">Website:</span>
                            <a href="#" class="text">{{$oUserInfo->website}}</a>
                        </li>
                    </ul>

                    <!-- ... end W-Personal-Info -->
                    <!-- W-Socials -->

                    <div class="widget w-socials">
                        <h6 class="title">Other Social Networks:</h6>
                        @if(!empty($oUserInfo->fb_link))
                            <a href="{{$oUserInfo->fb_link}}" class="social-item bg-facebook">
                                <i class="fab fa-facebook-f" aria-hidden="true"></i>
                                Facebook
                            </a>
                        @endif
                        @if(!empty($oUserInfo->tw_link))
                            <a href="{{$oUserInfo->tw_link}}" class="social-item bg-twitter">
                                <i class="fab fa-twitter" aria-hidden="true"></i>
                                Twitter
                            </a>
                        @endif
                        @if(!empty($oUserInfo->website))
                            <a href="{{$oUserInfo->website}}" class="social-item bg-dribbble">
                                <i class="fab fa-dribbble" aria-hidden="true"></i>
                                Website
                            </a>
                        @endif
                    </div>


                    <!-- ... end W-Socials -->				</div>
            </div>


        </div>

        <div class="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12">
            <div class="ui-block">
                <div class="ui-block-title">
                    <!-- Here it will show the last designs uploaded regardless if they were images or projects, If the designers
                has both designs and projects, only show the cover photo of the project + other project + Images -->

                    <h6 class="title">Your Recent Designs</h6>
                </div>
                <div class="ui-block-content">

                    <!-- W-Latest-Photo -->

                    <ul class="widget w-last-photo ">
                        @if (empty($recent_designs))
                            @foreach($recent_designs as $des)
                                <li>
                                    <a href="/website_dashboard/projects/{{$des['project_id']}}">
                                        <img src="/{{$des['image']}}" alt="photo">
                                    </a>
                                </li>
                            @endforeach
                        @else
                            <div style="justify-content: center;text-align: center">
                                No Recent Designs
                            </div>
                        @endif

                    </ul>


                    <!-- .. end W-Latest-Photo -->
                </div>
            </div>
        </div>
    </div>

@endsection