<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 2019-01-08
 * Time: 21:02
 */
?>
@extends('layouts.application')
@section('content')

    <div class="container">
        <input type="hidden" id="request_id" value="{{$oUserInfo->id}}">
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 fixed-top-head">
                <div class="ui-block">
                    <div class="top-header top-header-favorit">
                        <div class="top-header-thumb cover_photo">
                            @if($oUserInfo->cover)
                                <img src="/{{$oUserInfo->cover}}" alt="nature">
                            @else
                                <img src="{{ asset('uploads/user/dummy_cover.png') }}" alt="nature">
                            @endif

                            <div class="top-header-author">
                                @if($oUserInfo->logo)
                                <div class="author-thumb user_logo">
                                    <img src="/{{ $oUserInfo->logo }}" alt="author">
                                </div>
                                @else
                                    <div class="author-thumb user_logo">
                                        <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                                    </div>
                                @endif
                                <div class="author-content">
                                    <a href="#" class="h3 author-name">{{ $oUserInfo->name }}</a>
                                    @php
                                        $user_country = \App\Country::find($oUserInfo->country_id);
                                        $user_city = \App\City::find($oUserInfo->city_id)
                                    @endphp
                                    <div class="country">{{ $user_country?$user_country->text:'' }}  |  {{ $user_city? $user_city->text:'' }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="profile-section">
                            <div class="row">
                                <div class="col col-xl-8 m-auto col-lg-8 col-md-12">
                                    <ul class="profile-menu">

                                        <li>
                                            <a href="/profile/{{$oUserInfo->user_name}}" class="@php if(strpos($_SERVER['REQUEST_URI'],'/about') !== false) { echo 'active';} @endphp" >Timeline</a>
                                        </li>
                                        @if($oUserInfo->profile_setting != 'private')
                                        <li>

                                            <a class="@php if(strpos($_SERVER['REQUEST_URI'],'/about') !== false) { echo 'active';} @endphp" href="/profile/{{$oUserInfo->user_name}}/about">About</a>
                                        </li>
                                        <li>
                                            <a class="@php if(strpos($_SERVER['REQUEST_URI'],'/projects') !== false) { echo 'active';} @endphp" href="/profile/{{$oUserInfo->user_name}}/projects">Projects</a>
                                        </li>
                                        <li>
                                            <a class="@php if(strpos($_SERVER['REQUEST_URI'],'/articles') !== false) { echo 'active';} @endphp" href="/profile/{{$oUserInfo->user_name}}/articles">Articles</a>
                                        </li>
                                        <li>
                                        @if($oUserInfo->domain)
                                            <a class="" href="{{ prep_url($oUserInfo->domain) }}">Visit My Website</a>
                                        @else
                                            <a class="" href="/{{$oUserInfo->user_name}}">Visit My Website</a>
                                        @endif
                                        </li>
                                        @endif

                                    </ul>
                                </div>
                            </div>

                            <div class="control-block-button d-lg-block d-none">


                                    @if ($request_friend_waiting === 2)
                                    <a href="#" class="btn btn-control bg-facebook  friend_request_accept accept-request " style="    padding-top: 13px;">
                                        <span class="icon-add without-text">
                                                <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 512 512"><defs><style>.cls-1{fill:url(#linear-gradient);}</style><linearGradient id="linear-gradient" x1="-126.2" y1="588.24" x2="370.32" y2="588.24" gradientTransform="translate(834.99 136.57) rotate(90)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#00f2fe"/><stop offset="0.02" stop-color="#03effe"/><stop offset="0.29" stop-color="#24d2fe"/><stop offset="0.55" stop-color="#3cbdfe"/><stop offset="0.8" stop-color="#4ab0fe"/><stop offset="1" stop-color="#4facfe"/></linearGradient></defs><title>zzzzzz</title><path class="cls-1" d="M224.23,446.6H63.42a18.47,18.47,0,0,1-18.54-18.43,18.23,18.23,0,0,1,.89-5.72C64,365.65,119.75,326,181.26,326a138.31,138.31,0,0,1,79.53,25,18.49,18.49,0,0,0,21.14-30.33,175.15,175.15,0,0,0-36.86-19.72,94.11,94.11,0,0,0,30.61-62.08A139.12,139.12,0,0,1,452.09,224.4a18.49,18.49,0,1,0,21.28-30.24l-.12-.08a174.94,174.94,0,0,0-36.88-19.62A94.27,94.27,0,1,0,303.21,168q3,3.27,6.23,6.25c-2.58,1-5.14,2-7.72,3.16a175.78,175.78,0,0,0-32.55,18.69A94.26,94.26,0,1,0,118,300.54C67.83,319.29,27.18,359.67,10.62,411.13a55.46,55.46,0,0,0,52.8,72.44H224.23a18.49,18.49,0,0,0,0-37ZM374.06,46.11A57.47,57.47,0,1,1,316.6,103.6v0A57.53,57.53,0,0,1,374.06,46.11ZM182.19,173a57.46,57.46,0,1,1-57.46,57.46h0A57.52,57.52,0,0,1,182.19,173ZM418.45,371.9V431H316.11c-19.49,0-35.29-13.22-35.29-29.54s15.8-29.54,35.29-29.54H418.45m0,0h31.8c19.49,0,35.29,13.23,35.29,29.54S469.74,431,450.25,431h-31.8m-5.73,7.94H353.64V336.58c0-19.49,13.22-35.29,29.54-35.29s29.54,15.8,29.54,35.29V438.92m0,0v31.8c0,19.49-13.23,35.29-29.54,35.29s-29.54-15.8-29.54-35.29v-31.8"/></svg>
                                        </span>
                                    </a>
                                    @elseif ($request_friend_waiting === 1)
                                    <a href="#" class="btn btn-control bg-grey-light friend_request_cancel">
                                        <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 512 512"><defs><style>.cls-1{fill:url(#linear-gradient);}</style><linearGradient id="linear-gradient" x1="-126.2" y1="588.24" x2="347.84" y2="588.24" gradientTransform="translate(834.99 136.57) rotate(90)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#00f2fe"/><stop offset="0.02" stop-color="#03effe"/><stop offset="0.29" stop-color="#24d2fe"/><stop offset="0.55" stop-color="#3cbdfe"/><stop offset="0.8" stop-color="#4ab0fe"/><stop offset="1" stop-color="#4facfe"/></linearGradient></defs><title>unfollow</title><path class="cls-1" d="M224.23,446.6H63.42a18.47,18.47,0,0,1-18.54-18.43,18.23,18.23,0,0,1,.89-5.72C64,365.65,119.75,326,181.26,326a138.31,138.31,0,0,1,79.53,25,18.49,18.49,0,0,0,21.14-30.33,175.15,175.15,0,0,0-36.86-19.72,94.11,94.11,0,0,0,30.61-62.08A139.12,139.12,0,0,1,452.09,224.4a18.49,18.49,0,1,0,21.28-30.24l-.12-.08a174.94,174.94,0,0,0-36.88-19.62A94.27,94.27,0,1,0,303.21,168q3,3.27,6.23,6.25c-2.58,1-5.14,2-7.72,3.16a175.78,175.78,0,0,0-32.55,18.69A94.26,94.26,0,1,0,118,300.54C67.83,319.29,27.18,359.67,10.62,411.13a55.46,55.46,0,0,0,52.8,72.44H224.23a18.49,18.49,0,0,0,0-37ZM374.06,46.11A57.47,57.47,0,1,1,316.6,103.6v0A57.53,57.53,0,0,1,374.06,46.11ZM182.19,173a57.46,57.46,0,1,1-57.46,57.46h0A57.52,57.52,0,0,1,182.19,173ZM418.45,371.9V431H316.11c-19.49,0-35.29-13.22-35.29-29.54s15.8-29.54,35.29-29.54H418.45m0,0h31.8c19.49,0,35.29,13.23,35.29,29.54S469.74,431,450.25,431h-31.8"/></svg>
                                    </a>
                                    @else:
                                            @if($oUserInfo->follow_setting == 'private')
                                                <a href="#" class="btn btn-control bg-facebook friend_request">
                                                    <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 512 512"><defs><style>.cls-1{fill:url(#linear-gradient);}</style><linearGradient id="linear-gradient" x1="-126.2" y1="588.24" x2="370.32" y2="588.24" gradientTransform="translate(834.99 136.57) rotate(90)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#00f2fe"/><stop offset="0.02" stop-color="#03effe"/><stop offset="0.29" stop-color="#24d2fe"/><stop offset="0.55" stop-color="#3cbdfe"/><stop offset="0.8" stop-color="#4ab0fe"/><stop offset="1" stop-color="#4facfe"/></linearGradient></defs><title>zzzzzz</title><path class="cls-1" d="M224.23,446.6H63.42a18.47,18.47,0,0,1-18.54-18.43,18.23,18.23,0,0,1,.89-5.72C64,365.65,119.75,326,181.26,326a138.31,138.31,0,0,1,79.53,25,18.49,18.49,0,0,0,21.14-30.33,175.15,175.15,0,0,0-36.86-19.72,94.11,94.11,0,0,0,30.61-62.08A139.12,139.12,0,0,1,452.09,224.4a18.49,18.49,0,1,0,21.28-30.24l-.12-.08a174.94,174.94,0,0,0-36.88-19.62A94.27,94.27,0,1,0,303.21,168q3,3.27,6.23,6.25c-2.58,1-5.14,2-7.72,3.16a175.78,175.78,0,0,0-32.55,18.69A94.26,94.26,0,1,0,118,300.54C67.83,319.29,27.18,359.67,10.62,411.13a55.46,55.46,0,0,0,52.8,72.44H224.23a18.49,18.49,0,0,0,0-37ZM374.06,46.11A57.47,57.47,0,1,1,316.6,103.6v0A57.53,57.53,0,0,1,374.06,46.11ZM182.19,173a57.46,57.46,0,1,1-57.46,57.46h0A57.52,57.52,0,0,1,182.19,173ZM418.45,371.9V431H316.11c-19.49,0-35.29-13.22-35.29-29.54s15.8-29.54,35.29-29.54H418.45m0,0h31.8c19.49,0,35.29,13.23,35.29,29.54S469.74,431,450.25,431h-31.8m-5.73,7.94H353.64V336.58c0-19.49,13.22-35.29,29.54-35.29s29.54,15.8,29.54,35.29V438.92m0,0v31.8c0,19.49-13.23,35.29-29.54,35.29s-29.54-15.8-29.54-35.29v-31.8"/></svg>
                                                </a>
                                            @elseif($oUserInfo->follow_setting == 'public')
                                                <a href="#" class="btn btn-control bg-facebook public_friend_request">
                                                    <svg id="Capa_1" data-name="Capa 1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 512 512"><defs><style>.cls-1{fill:url(#linear-gradient);}</style><linearGradient id="linear-gradient" x1="-126.2" y1="588.24" x2="370.32" y2="588.24" gradientTransform="translate(834.99 136.57) rotate(90)" gradientUnits="userSpaceOnUse"><stop offset="0" stop-color="#00f2fe"/><stop offset="0.02" stop-color="#03effe"/><stop offset="0.29" stop-color="#24d2fe"/><stop offset="0.55" stop-color="#3cbdfe"/><stop offset="0.8" stop-color="#4ab0fe"/><stop offset="1" stop-color="#4facfe"/></linearGradient></defs><title>zzzzzz</title><path class="cls-1" d="M224.23,446.6H63.42a18.47,18.47,0,0,1-18.54-18.43,18.23,18.23,0,0,1,.89-5.72C64,365.65,119.75,326,181.26,326a138.31,138.31,0,0,1,79.53,25,18.49,18.49,0,0,0,21.14-30.33,175.15,175.15,0,0,0-36.86-19.72,94.11,94.11,0,0,0,30.61-62.08A139.12,139.12,0,0,1,452.09,224.4a18.49,18.49,0,1,0,21.28-30.24l-.12-.08a174.94,174.94,0,0,0-36.88-19.62A94.27,94.27,0,1,0,303.21,168q3,3.27,6.23,6.25c-2.58,1-5.14,2-7.72,3.16a175.78,175.78,0,0,0-32.55,18.69A94.26,94.26,0,1,0,118,300.54C67.83,319.29,27.18,359.67,10.62,411.13a55.46,55.46,0,0,0,52.8,72.44H224.23a18.49,18.49,0,0,0,0-37ZM374.06,46.11A57.47,57.47,0,1,1,316.6,103.6v0A57.53,57.53,0,0,1,374.06,46.11ZM182.19,173a57.46,57.46,0,1,1-57.46,57.46h0A57.52,57.52,0,0,1,182.19,173ZM418.45,371.9V431H316.11c-19.49,0-35.29-13.22-35.29-29.54s15.8-29.54,35.29-29.54H418.45m0,0h31.8c19.49,0,35.29,13.23,35.29,29.54S469.74,431,450.25,431h-31.8m-5.73,7.94H353.64V336.58c0-19.49,13.22-35.29,29.54-35.29s29.54,15.8,29.54,35.29V438.92m0,0v31.8c0,19.49-13.23,35.29-29.54,35.29s-29.54-15.8-29.54-35.29v-31.8"/></svg>
                                                </a>
                                            @endif
                                    @endif


                                <a href="#" class="btn btn-control bg-purple">
                                    <svg class="olymp-chat---messages-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-chat---messages-icon"></use></svg>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        @yield('content_not_friend')
    </div>

@endsection

