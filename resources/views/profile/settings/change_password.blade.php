@extends('profile.layout.main')
@section('profile_content')

  <div class="ui-block">
    <div class="ui-block-title">
      <h6 class="title">Change Password</h6>
    </div>
    <div class="ui-block-content">


      <!-- Change Password Form -->

      <form action="{{ route('user.change_password.save') }}" method="post">
        {{ csrf_field() }}

        <div class="row">
          <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Confirm Current Password</label>
              <input class="form-control" placeholder="" name="current_password" type="password" value="">
            </div>
          </div>

          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <div class="form-group label-floating is-empty">
              <label class="control-label">Your New Password</label>
              <input class="form-control" placeholder="" name="new_password" type="password">
            </div>
          </div>
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <div class="form-group label-floating is-empty">
              <label class="control-label">Confirm New Password</label>
              <input class="form-control" placeholder="" name="confirm_password" type="password">
            </div>
          </div>

          <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <button class="btn btn-primary btn-lg full-width">Change Password Now!</button>
          </div>
        </div>
      </form>

      <!-- ... end Change Password Form -->
    </div>
  </div>

@endsection
