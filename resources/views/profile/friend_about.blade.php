<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 2019-02-03
 * Time: 14:11
 */
?>
@extends('layouts.profile')
@section('content_profile')
    <div class="ui-block">
        <div class="ui-block-title">
            <h6 class="title">Personal Information</h6>
        </div>
        <div class="ui-block-content">

            <!-- Personal Information Form  -->

            <div class="row">

                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="form-group label-floating">
                        <label class="control-label">First Name</label>
                        <label class="form-control">{{ $user->name }}</label>
                    </div>

                    <div class="form-group label-floating">
                        <label class="control-label">Your Email</label>
                        <label class="form-control">{{ $user->email }}</label>
                    </div>

                    <div class="form-group date-time-picker label-floating">
                        <label class="control-label">Your Birthday</label>
                        <label class="form-control">{{ $user->bod }}</label>
                        <span class="input-group-addon">
                <svg class="olymp-month-calendar-icon icon"><use
                            xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-month-calendar-icon') }}"></use></svg>
              </span>
                    </div>
                </div>

                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="form-group label-floating">
                        <label class="control-label">Last Name</label>
                        <label class="form-control">{{ $user->last_name }}</label>
                    </div>

                    <div class="form-group label-floating">
                        <label class="control-label">Your Website</label>
                        <label class="form-control">{{ $user->website }}</label>
                    </div>


                    <div class="form-group label-floating is-empty">
                        <label class="control-label">Your Phone Number</label>
                        <label class="form-control">{{ $user->phone }}</label>
                    </div>
                </div>

                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Your Country</label>
                        <label class="form-control">{{ optional(optional(optional($user->city()->first())->country())->first())->text }}</label>
                    </div>
                </div>
                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="form-group label-floating is-select">
                        <label class="control-label">Your City</label>
                        <label class="form-control">{{ optional($user->city()->first())->text }}</label>
                    </div>
                </div>
                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                    <div class="form-group label-floating">
                        <label class="control-label">Write a little description about you</label>
                        <pre class="form-control">{{ $user->description }}</pre>
                    </div>
                </div>
                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">

                    <div class="form-group label-floating is-select">
                        <label class="control-label">Your Gender</label>
                        <label class="form-control">{{ $user->gender=='male'? 'Male': 'Female' }}</label>
                    </div>

                    <div class="form-group label-floating">
                        <label class="control-label">Your Occupation</label>
                        <label class="form-control">{{ $user->occupation }}</label>
                    </div>
                </div>
            </div>
            @if($user->profile_type=='freelancer')
                @php
                    $a=[];
                @endphp
                @foreach($user->history as $row)
                    @if(!in_array($row->type, $a))
                        @php
                            $a[]=$row->type;
                        @endphp

                        <hr />
                        <div class="row">
                            <div class="col col-12 h3">{{ $row->type==\App\UserHistory::TYPE_EDUCATION? "Education": "Employment" }}</div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="form-group label-floating ">
                                <label class="control-label">Title or Place</label>
                                <label class="form-control">{{ $row->title }}</label>
                            </div>
                        </div>
                        <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                            <div class="form-group label-floating ">
                                <label class="control-label">Period of Time</label>
                                <label class="form-control">{{ $row->period }}</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-md-12 col-sm-12 col-12">
                            <div class="form-group label-floating ">
                                <label class="control-label">Description</label>
                                <label class="form-control">{{ $row->description }}</label>
                            </div>
                        </div>
                    </div>
            @endforeach
        @endif
        {{--<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">--}}
        {{--<div class="form-group with-icon label-floating">--}}
        {{--<label class="control-label">Your Facebook Account</label>--}}
        {{--<label class="form-control" type="text" name="fb_link" value="{{ $user->fb_link }}">--}}
        {{--<i class="fab fa-facebook-f c-facebook" aria-hidden="true"></i>--}}
        {{--</div>--}}
        {{--<div class="form-group with-icon label-floating">--}}
        {{--<label class="control-label">Your Twitter Account</label>--}}
        {{--<label class="form-control" type="text" name="tw_link" value="{{ $user->tw_link }}">--}}
        {{--<i class="fab fa-twitter c-twitter" aria-hidden="true"></i>--}}
        {{--</div>--}}
        {{--<div class="form-group with-icon label-floating">--}}
        {{--<label class="control-label">Your Dribbble Account</label>--}}
        {{--<label class="form-control" type="text" name="ig_link" value="{{ $user->ig_link }}">--}}
        {{--<i class="fab fa-dribbble c-dribbble" aria-hidden="true"></i>--}}
        {{--</div>--}}
        {{--<div class="form-group with-icon label-floating is-empty">--}}
        {{--<label class="control-label">Your Spotify Account</label>--}}
        {{--<label class="form-control" type="text" name="pin_link" value="{{ $user->pin_link }}">--}}
        {{--<i class="fab fa-spotify c-spotify" aria-hidden="true"></i>--}}
        {{--</div>--}}
        {{--</div>--}}

        <!-- ... end Personal Information Form  -->
        </div>
    </div>
@endsection
