@extends('profile.layout.main')

@section('profile_content')

  <div class="ui-block">
    <div class="ui-block-title">
      <h6 class="title">Services</h6>
    </div>
    <div class="ui-block-content">
      <form id="service_form">
        <div class="description-toggle">
          <div class="description-toggle-content">
            <div class="h6">View services section</div>
            <p>If you enable this, the services section will appear on your website. (recommended)</p>
          </div>

          <div class="togglebutton">
            <label>
              <input type="checkbox" name="show_service" {{ $service->show_service? 'checked=""': '' }}>
            </label>
          </div>
        </div>

        @php
          $serviceArr = json_decode($service->service_text, 1);

          if(!$serviceArr){
            $serviceArr = [];
          }
          
          $serviceIcon = json_decode($service->icon, 1);

          if(!$serviceIcon){
            $serviceIcon = [];
          }
        @endphp

        <div class="row">
          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Service Title 1</label>
              <textarea class="form-control" placeholder=""
                        name="service_text[title][0]">{{ isset($serviceArr['title'][0])? $serviceArr['title'][0]: '' }}</textarea>
            </div>
          </div>

          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Services 1 description</label>
              <textarea class="form-control" placeholder=""
                        name="service_text[description][0]">{{ isset($serviceArr['description'][0])? $serviceArr['description'][0]: '' }}</textarea>
            </div>
          </div>

          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <div class="file-upload">
                <label for="icon1" class="file-upload__label">Services 1 icon [32 X 32]</label>
                <input id="icon1" class="file-upload__input" type="file" name="icon[0]">
                @if($serviceIcon[0] ?? false)
                  <img style="max-height: 64px; max-width: 64px;" src="{{ asset($serviceIcon[0]) }}">
                @endif
              </div>
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Service Title 2</label>
              <textarea class="form-control" placeholder=""
                        name="service_text[title][1]">{{ isset($serviceArr['title'][1])? $serviceArr['title'][1]: '' }}</textarea>
            </div>
          </div>

          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Services 2 description</label>
              <textarea class="form-control" placeholder=""
                        name="service_text[description][1]">{{ isset($serviceArr['description'][1])? $serviceArr['description'][1]: '' }}</textarea>
            </div>
          </div>

          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <div class="file-upload">
                <label for="icon2" class="file-upload__label">Services 2 icon [32 X 32]</label>
                <input id="icon2" class="file-upload__input" type="file" name="icon[1]">
                @if($serviceIcon[1] ?? false)
                  <img style="max-height: 64px; max-width: 64px;" src="{{ asset($serviceIcon[1]) }}">
                @endif
              </div>
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Service Title 3</label>
              <textarea class="form-control" placeholder=""
                        name="service_text[title][2]">{{ isset($serviceArr['title'][2])? $serviceArr['title'][2]: '' }}</textarea>
            </div>
          </div>

          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Services 3 description</label>
              <textarea class="form-control" placeholder=""
                        name="service_text[description][2]">{{ isset($serviceArr['description'][2])? $serviceArr['description'][2]: '' }}</textarea>
            </div>
          </div>

          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <div class="file-upload">
                <label for="icon3" class="file-upload__label">Services 3 icon [32 X 32]</label>
                <input id="icon3" class="file-upload__input" type="file" name="icon[2]">
                @if($serviceIcon[2] ?? false)
                  <img style="max-height: 64px; max-width: 64px;" src="{{ asset($serviceIcon[2]) }}">
                @endif
              </div>
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Service Title 4</label>
              <textarea class="form-control" placeholder=""
                        name="service_text[title][3]">{{ isset($serviceArr['title'][3])? $serviceArr['title'][3]: '' }}</textarea>
            </div>
          </div>

          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Services 4 description</label>
              <textarea class="form-control" placeholder=""
                        name="service_text[description][3]">{{ isset($serviceArr['description'][3])? $serviceArr['description'][3]: '' }}</textarea>
            </div>
          </div>

          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <div class="file-upload">
                <label for="icon4" class="file-upload__label">Services 4 icon [32 X 32]</label>
                <input id="icon4" class="file-upload__input" type="file" name="icon[3]">
                @if($serviceIcon[3] ?? false)
                  <img style="max-height: 64px; max-width: 64px;" src="{{ asset($serviceIcon[3])  }}">
                @endif
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button class="btn btn-secondary btn-lg full-width">Cancel</button>
          </div>
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button id="submit" type="button" class="btn btn-primary btn-lg full-width">Save all Changes</button>
          </div>
        </div>

      </form>
      <!-- ... end Form Hobbies and Interests -->
    </div>
  </div>

  @include('layouts.loader')

@endsection

@section('script')
  <script>
    $('#submit').click(function () {
      var $this = $(this);

      $this.addClass('disabled');
      showLoader();
      $.ajax({
        url: '{{ route('services.save') }}',
        data: new FormData($('#service_form')[0]),
        processData: false,
        method: 'POST',
        contentType: false,
        success: function (d) {
          if (d.status) {
            swal("Saved", "", "success");
          }
          else {

            $msg = '';
            for (i in d.message) {
              for (j in d.message[i]) {
                $msg += i + ': ' + d.message[i][j] + "\n";
              }
            }
            swal("Something goes wrong!", $msg, "info");
          }
        }
      }).always(function () {
        $this.removeClass('disabled');
        hideLoader();
      });
    });


    $('.file-upload__input').change(function () {
      var readerUpload = new FileReader();
      var $this = $(this);

      readerUpload.onload = function (e) {
        $this.parent().find('img').remove();
        $this.after($('<img>').css({"max-height": 64, "max-width": 64, "display": "none"}));
        $this.parent().find('img').attr('src', e.target.result).after('<br>');
        $this.parent().find('img').show();
      };

      if (this.files && this.files[0]) {
        readerUpload.readAsDataURL(this.files[0]);
      }
    });
  </script>
@endsection