@extends('profile.layout.main')

@section('profile_content')

  <div class="ui-block">


    <div class="ui-block-title">
      <h6 class="title">Contact Tab</h6>
    </div>


    <div class="ui-block-content">

      <form id="contactUs_form">
        <div class="description-toggle">
          <div class="description-toggle-content">
            <div class="h6">Show Contact Tab</div>
            <p>Enable the contact me tab on your website. (recommended)</p>
          </div>

          <div class="togglebutton">
            <label>
              <input type="checkbox" name="show_tab" {{ $contactUs->show_tab? 'checked=""': '' }}>
            </label>
          </div>
        </div>

        <div class="description-toggle">
          <div class="description-toggle-content">
            <div class="h6">Email Notifications</div>
            <p>You'll recieve all the messages on your communications tab. To be notified via email, turn this option
              on.</p>
          </div>

          <div class="togglebutton">
            <label>
              <input type="checkbox" name="email_notification" {{ $contactUs->email_notification? 'checked=""': '' }}>
            </label>
          </div>
        </div>

        <!-- Personal Information Form  -->

        <div class="row">

          <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Title</label>
              <input class="form-control" placeholder="" type="text" name="title" value="{{ $contactUs->title }}">
            </div>
          </div>

          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">

          </div>

          <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Message</label>
              <textarea class="form-control" name="message" placeholder="">{{ $contactUs->message }}</textarea>
            </div>
          </div>

          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button type="reset" class="btn btn-secondary btn-lg full-width">Restore all Attributes</button>
          </div>
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button type="button" id="submit" class="btn btn-primary btn-lg full-width">Save all Changes</button>
          </div>

        </div>
      </form>

      <!-- ... end Personal Information Form  -->
    </div>
  </div>

@endsection

@section('script')
  <script>
    $('#submit').click(function () {
      var $this = $(this);

      $this.addClass('disabled');

      $.post('{{ route('contact_us.save') }}', $('#contactUs_form').serialize())
        .done(function (d) {
          if (d.status) {
            swal("Saved", "", "success");
          }
          else {

            $msg = '';
            for (i in d.message) {
              for (j in d.message[i]) {
                $msg += i + ': ' + d.message[i][j] + "\n";
              }
            }

            swal("Something goes wrong!", $msg, "info");
          }
        })
        .always(function () {
          $this.removeClass('disabled');
        });
    });
  </script>
@endsection