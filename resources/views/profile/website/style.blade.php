@extends('profile.layout.main')

@section('profile_content')

  <form action="{{ route('style.save') }}" method="post">
    {{ csrf_field() }}
    @foreach($style as $key=>$row)
      @if(!is_array($row))
        @continue
      @endif
      <div class="ui-block">
        <div class="ui-block-title">
          <h6 class="title">{{ unslug($key) }}</h6>
        </div>
        <div class="ui-block-content">
          @foreach($row as $name=>$val)
            <div class="row">
              <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                <div class="form-group label-floating">
                  <label class="control-label">{{ unslug($name) }}</label>
                </div>
              </div>
              <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                <input name="{{ $key.'['.$name.']' }}" data-value="#{{ trim($val, '#') }}"
                       value="@php
                       $v = '#'.trim($val, '#');
                        if(isset($user[$key])){
                          if(isset($user[$key][$name])){
                            $v = '#'.trim($user[$key][$name], '#');
                          }
                        }
                        echo $v;
                        @endphp"
                       type="color" style="width: 50px; padding: unset;"/>
              </div>
            </div>
            <hr/>
          @endforeach
        </div>
      </div>
    @endforeach

    <div class="ui-block">
      <div class="ui-block-content">
        <!-- Form Hobbies and Interests -->
        <div class="row">
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button id="reset" type="button" class="btn btn-secondary btn-lg full-width">Reset Values</button>
          </div>
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button id="submit" class="btn btn-primary btn-lg full-width">Save all Changes</button>
          </div>
        </div>
      </div>
    </div>


  </form>

@endsection

@section('script')
  <script>
    $('#reset').click(function () {
      $('input[data-value]').each(function () {
        var $this = $(this);

        $this.val($this.data('value'));
      });
    });
  </script>
@endsection