@extends('profile.layout.main')
@section('profile_content')
  <style>
    .remove-row {
      border: none;
      background: none;
    }

    .remove-row:hover {
      border: solid 1px;
    }
  </style>

  <div class="ui-block">
    <div class="ui-block-title">
      <h6 class="title">Your Education History</h6>
    </div>
    <div class="ui-block-content">


      <!-- Education History Form -->

      <form>
        <input name="type" type="hidden" value="{{ \App\UserHistory::TYPE_EDUCATION }}">
        <div id="edu">
        @foreach($histories as $key=>$history)
          @if($history->type!=\App\UserHistory::TYPE_EDUCATION)
            @continue
          @endif
          <div class="row">
            <div class="col-md-12 text-right"><button type="button" class="remove-row">&times;</button><br/><br/></div>
            <input class="form-control" name="id[{{ $key }}]" type="hidden" value="{{ $history->id }}">

            <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
              <div class="form-group label-floating">
                <label class="control-label">Title or Place</label>
                <input class="form-control" name="title[{{ $key }}]" type="text" value="{{ $history->title }}">
              </div>
            </div>

            <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
              <div class="form-group label-floating">
                <label class="control-label">Period of Time</label>
                <input class="form-control" name="period[{{ $key }}]" type="text" value="{{ $history->period }}">
              </div>
            </div>

            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="form-group label-floating">
                <label class="control-label">Description</label>
                <textarea class="form-control" name="description[{{ $key }}]">{{ $history->description }}</textarea>
              </div>
            </div>
          </div>
          <hr>
        @endforeach
        </div>

        <div class="row">
          <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <a href="javascript:addRow('edu');" class="add-field">
              <svg class="olymp-plus-icon">
                <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-plus-icon') }}"></use>
              </svg>
              <span>Add Education Field</span>
            </a>
          </div>

          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
          </div>

          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button class="btn btn-primary btn-lg full-width">Save all Changes</button>
          </div>
        </div>
      </form>

      <!-- ... end Education History Form -->
    </div>
  </div>


  <div class="ui-block">
    <div class="ui-block-title">
      <h6 class="title">Your Employement History</h6>
    </div>
    <div class="ui-block-content">


      <!-- Employement History Form -->

      <form data-id="emp">
        <input name="type" type="hidden" value="{{ \App\UserHistory::TYPE_EMPLOYMENT }}">
        <div id="emp">
        @foreach($histories as $key=>$history)
          @if($history->type!=\App\UserHistory::TYPE_EMPLOYMENT)
            @continue
          @endif
          <div class="row">
            <div class="col-md-12 text-right"><button type="button" class="remove-row">&times;</button><br/><br/></div>
            <input class="form-control" name="id[{{ $key }}]" type="hidden" value="{{ $history->id }}">

            <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
              <div class="form-group label-floating">
                <label class="control-label">Title or Place</label>
                <input class="form-control" name="title[{{ $key }}]" type="text" value="{{ $history->title }}">
              </div>
            </div>

            <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
              <div class="form-group label-floating">
                <label class="control-label">Period of Time</label>
                <input class="form-control" name="period[{{ $key }}]" type="text" value="{{ $history->period }}">
              </div>
            </div>

            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="form-group label-floating">
                <label class="control-label">Description</label>
                <textarea class="form-control" name="description[{{ $key }}]">{{ $history->description }}</textarea>
              </div>
            </div>
          </div>
          <hr>
        @endforeach
        </div>

        <div class="row">
          <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <a href="javascript:addRow('emp');" class="add-field">
              <svg class="olymp-plus-icon">
                <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-plus-icon') }}"></use>
              </svg>
              <span>Add Education Field</span>
            </a>
          </div>

          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
          </div>

          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button class="btn btn-primary btn-lg full-width">Save all Changes</button>
          </div>
        </div>
      </form>

      <!-- ... end Employement History Form -->
    </div>
  </div>

@endsection

@section('script')
  <script>

    function addRow(elem) {
      var newID = parseInt(Math.random() * 10000);


      $('#' + elem).append(
        $('<div class="row">')
        // .append('<input class="form-control" name="id['.newID.']" type="text" value="">')
          .append('<div class="col-md-12 text-right"><button type="button" class="remove-row">&times;</button><br/><br/></div>')
          .append(
            $('<div class="col col-lg-6 col-md-6 col-sm-12 col-12">').append(
              $('<div class="form-group label-floating">')
                .append('<label class="control-label">Title or Place</label>')
                .append('<input class="form-control" name="title[' + newID + ']" type="text" >')
            )
          )
          .append(
            $('<div class="col col-lg-6 col-md-6 col-sm-12 col-12">').append(
              $('<div class="form-group label-floating">')
                .append('<label class="control-label">Period of Time</label>')
                .append('<input class="form-control" name="period[' + newID + ']" type="text" >')
            )
          )
          .append(
            $('<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">').append(
              $('<div class="form-group label-floating">')
                .append('<label class="control-label">Description</label>')
                .append('<textarea class="form-control" name="description[' + newID + ']">')
            )
          ))
          .append($('<hr>'));

    }

    $('.remove-row').click(function () {
      var newID = parseInt(Math.random() * 10000);
      var $div = $(this).closest('div.row');
      $(this).closest('form').append('<input class="form-control" name="delete['+newID+']" type="hidden" value="'+$div.find('input[name*="id"]').val()+'">');


      $div.next().remove();
      $div.remove();
    });

    $('form').submit(function (e) {
      var $this = $(this);

      e.preventDefault();

      $.post('{{ route('user_history.save') }}', $(this).serialize())
        .done(function (d) {
          if(d.status){
            for(i in d.data){
              if($this.find('[name="id['+i+']"]').length==0){
                $this.append('<input class="form-control" name="id['+i+']" type="hidden" value="'+d.data[i]+'">');
              }
            }
            swal("Saved!", "", "success");
          }
          else {
            swal("Error!", "Something goes wrong", "info");
          }
        });

      return false;
    });

  </script>
@endsection
