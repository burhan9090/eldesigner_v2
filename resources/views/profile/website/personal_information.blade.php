@extends('profile.layout.main')
@section('profile_content')

  <div class="ui-block">
    <div class="ui-block-title">
      <h6 class="title">Personal Information</h6>
    </div>
    <div class="ui-block-content">

      <!-- Personal Information Form  -->

      <form id="personalInformationForm" method="post" action="{{ route("personal_information.save") }}">
        {{ csrf_field() }}
        <div class="row">

          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">First Name</label>
              <input class="form-control" placeholder="" type="text" name="name" value="{{ $user->name }}">
            </div>

            <div class="form-group label-floating">
              <label class="control-label">@if(auth()->user()->profile_type=='company')
                  Company
                 @else
                  Your
                  @endif
                Email</label>
              <input class="form-control" placeholder="" type="email" readonly="" value="{{ $user->email }}">
            </div>
            @if(auth()->user()->profile_type=='freelancer')
            <div class="form-group date-time-picker label-floating">
              <label class="control-label">Your Birthday</label>
              <input name="bod" value="{{ $user->bod }}"/>
              <span class="input-group-addon">
                <svg class="olymp-month-calendar-icon icon"><use
                      xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-month-calendar-icon') }}"></use></svg>
              </span>
            </div>
            @endif
          </div>

          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            @if(auth()->user()->profile_type=='freelancer')
            <div class="form-group label-floating">
              <label class="control-label">Last Name</label>
              <input class="form-control" placeholder="" type="text" name="last_name" value="{{ $user->last_name }}">
            </div>
            @endif
            <div class="form-group label-floating">
              <label class="control-label">Your Website</label>
              <input class="form-control" placeholder="" type="email" name="website" value="{{ $user->website }}">
            </div>


            <div class="form-group label-floating is-empty">
              <label class="control-label">Your Phone Number</label>
              <input class="form-control" placeholder="" type="text" name="phone" value="{{ $user->phone }}">
            </div>
          </div>

          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <div class="form-group label-floating is-select">
              <label class="control-label">Your Country</label>
              <select class="selectpicker form-control" name="country_id" id="country_id">
                @php
                  $city_id = $user->city()->first();
                  $country_id = $city_id? $city_id->country_id: 0;
                @endphp
                @foreach(\App\Country::all() as $country)
                  <option
                      value="{{ $country->id }}" {{ ($country->id==$country_id)? 'selected="selected"': '' }}>{{ $country->text }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <div class="form-group label-floating is-select">
              <label class="control-label">Your City</label>
              <select class="selectpicker form-control" name="city_id" id="city_id">
                @foreach(\App\City::where('country_id', $user->city_id?:1)->get() as $city)
                  <option
                      value="{{ $city->id }}" {{ ($city->id==$user->city_id)? 'selected="selected"': '' }}>{{ $city->text }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Write a little description about @if(auth()->user()->profile_type=='company')
                  your company
                @else
                  you
                @endif</label>
              <textarea class="form-control" placeholder="" name="description">{{ $user->description }}</textarea>
            </div>
          </div>
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            @if(auth()->user()->profile_type=='freelancer')

            <div class="form-group label-floating is-select">
              <label class="control-label">Your Gender</label>
              <select class="selectpicker form-control" name="gender">
                <option {{ $user->gender=='male'? 'selected="selected"': '' }} value="male">Male</option>
                <option {{ $user->gender=='female'? 'selected="selected"': '' }} value="female">Female</option>
              </select>
            </div>

            <div class="form-group label-floating">
              <label class="control-label">Your Occupation</label>
              <input class="form-control" placeholder="" type="text" name="occupation" value="{{ $user->occupation }}">
            </div>
            @endif
          </div>
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <div class="col col-12">
              <div class="row">
                <div class="file-upload">
                  <label for="logo" class="file-upload__label">Upload Logo</label>
                  <input id="logo" class="file-upload__input" type="file" name="logo">
                </div>
              </div>
              <div class="row image">
                @if($user->logo)
                  <img src="{{ asset($user->logo) }}" style="max-width: 100%; max-height: 200px"/>
                @endif
              </div>
            </div>
          </div>
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <div class="col-12">
              <div class="row">
                <div class="file-upload">
                  <label for="cover" class="file-upload__label">Upload Cover</label>
                  <input id="cover" class="file-upload__input" type="file" name="cover">
                </div>
              </div>
              <div class="row image">
                @if($user->cover)
                  <img src="{{ asset($user->cover) }}" style="max-width: 100%; max-height: 200px"/>
                @endif
              </div>
            </div>
          </div>
          <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 mt-3">
            <div class="form-group with-icon label-floating">
              <label class="control-label">Your Facebook Account</label>
              <input class="form-control" type="text" name="fb_link" value="{{ $user->fb_link }}">
              <i class="fab fa-facebook-f c-facebook" aria-hidden="true"></i>
            </div>
            <div class="form-group with-icon label-floating">
              <label class="control-label">Your Twitter Account</label>
              <input class="form-control" type="text" name="tw_link" value="{{ $user->tw_link }}">
              <i class="fab fa-twitter c-twitter" aria-hidden="true"></i>
            </div>
            <div class="form-group with-icon label-floating">
              <label class="control-label">Your Instagram Account</label>
              <input class="form-control" type="text" name="ig_link" value="{{ $user->ig_link }}">
              <i class="fab fa-instagram c-dribbble" aria-hidden="true"></i>
            </div>
            <div class="form-group with-icon label-floating is-empty">
              <label class="control-label">Your Pinterest Account</label>
              <input class="form-control" type="text" name="pin_link" value="{{ $user->pin_link }}">
              <i class="fab fa-pinterest c-orange" aria-hidden="true"></i>
            </div>
          </div>
          <div class="col col-lg-offset-6 col-md-offset-6 col-lg-6 col-md-6 col-sm-12 col-12">
            <button class="btn btn-primary btn-lg full-width">Save all Changes</button>
          </div>

        </div>
      </form>

      <!-- ... end Personal Information Form  -->
    </div>
  </div>

@endsection

@section('script')
  <script>
    $('#country_id').change(function () {
      var city_id = $('#city_id');

      city_id.empty();

      $.get('{{ route('ajax.city') }}', {"country_id": $(this).val()})
        .done(function (d) {
          for (var i in d) {
            city_id.append($('<option>').html(d[i].text).val(i));
          }
          city_id.selectpicker('refresh');
        })
    });

    /** Logo preview  */
    var readerUploadLogo = new FileReader();

    readerUploadLogo.onload = function (e) {
      var $this = $('#logo').parent().parent().parent();

      $this.find('img').remove();
      $this.find('.image').append($('<img>').css({"max-height": 200, "max-width": "100%", "display": "none"}));
      $this.find('img').attr('src', e.target.result).after('<br>');
      $this.find('img').show();
    };

    $('#logo').change(function () {
      if (this.files && this.files[0]) {
        readerUploadLogo.readAsDataURL(this.files[0]);
      }
    });


    /** Cover preview  */
    var readerUploadCover = new FileReader();

    readerUploadCover.onload = function (e) {
      var $this = $('#cover').parent().parent().parent();

      $this.find('img').remove();
      $this.find('.image').append($('<img>').css({"max-height": 200, "max-width": "100%", "display": "none"}));
      $this.find('img').attr('src', e.target.result).after('<br>');
      $this.find('img').show();
    };

    $('#cover').change(function () {
      if (this.files && this.files[0]) {
        readerUploadCover.readAsDataURL(this.files[0]);
      }
    });

    $('#personalInformationForm').submit(function (e) {
      e.preventDefault();

      $.ajax({
        url: $(this).attr('action'),
        data: new FormData(this),
        processData: false,
        method: 'POST',
        contentType: false,
        success: function ( d ) {
          if(d.status){
            swal("Saved!", "", "success");
            window.history.go(0);
          }
          else {
            swal("Error!", "Something goes wrong", "info");
          }
        }
      });
    });

  </script>
@endsection