@extends('profile.layout.main')

@section('profile_content')


  <div class="container">
    <div class="row">
      <div class="col col-md-12 col-12">

        <div class="container">
          <div class="row">
            <div class="col col-lg-12 col-sm-12 col-12">
              <div class="ui-block responsive-flex">
                <div class="ui-block-title">
                  <div class="h6 title">Monthly Bar Graphic</div>
                  {{--<select class="selectpicker form-control without-border">--}}
                    {{--<option value="LY">LAST YEAR (2016)</option>--}}
                    {{--<option value="CUR">CURRENT YEAR (2017)</option>--}}
                  {{--</select>--}}
                  {{--<a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use></svg></a>--}}
                </div>

                <div class="ui-block-content">
                  <div class="chart-js chart-js-one-bar">
                    <canvas id="hits-by-months" width="1400" height="380"></canvas>
                  </div>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="container">
          <div class="row">


            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="ui-block">
                <div class="ui-block-title">
                  <div class="h6 title">Worldwide Statistics</div>
                  <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use></svg></a>
                </div>

                <div class="ui-block-content">
                  <div class="world-statistics">
                    <div class="world-statistics-img">
                      <img src="{{ asset('img/world-map.png') }}" alt="map">
                    </div>

                    <ul class="country-statistics">
                      @foreach($countriesHit as $hit)
                      <li>
{{--                        <img src="{{ asset('img/flag1.jpg') }}" alt="flag">--}}
                        <span class="country">{{ $hit->text?: 'Unknown' }}</span>
                        <span class="count-stat">{{ $hit->hits }}</span>
                      </li>
                      @endforeach
                    </ul>

                  </div>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col col-lg-12 col-sm-12 col-12">
              <div class="ui-block responsive-flex">
                <div class="ui-block-title">
                  <div class="h6 title">Yearly Line Graphic</div>
                  {{--<select class="selectpicker form-control without-border">--}}
                    {{--<option value="LY">LAST YEAR (2016)</option>--}}
                    {{--<option value="2">CURRENT YEAR (2017)</option>--}}
                  {{--</select>--}}
                  {{--<a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use></svg></a>--}}
                </div>

                <div class="ui-block-content">
                  <div class="chart-js chart-js-line-chart">
                    <canvas id="hits-line-chart" width="1400" height="380"></canvas>
                  </div>
                </div>
                <hr>
                <div class="ui-block-content display-flex content-around">
                  <div class="chart-js chart-js-small-pie">
                    <canvas id="pie-small-chart" width="90" height="90"></canvas>
                  </div>

                  <div class="points points-block">

									<span>
										<span class="statistics-point bg-breez"></span>
										Word wide users
									</span>

                    <span>
										<span class="statistics-point bg-yellow"></span>
                      {{ config('app.name') }} users
									</span>

                  </div>

                  @foreach($deviceTypeHit as $row)
                  <div class="text-stat">
                    <div class="count-stat">{{ $row->hits }}</div>
                    <div class="title">{{ $row->type }}</div>
                    {{--<div class="sub-title">This Year</div>--}}
                  </div>
                  @endforeach
                </div>
              </div>
            </div>

          </div>


        </div>
      </div>
    </div>
  </div>

@endsection

@section('script')
  <script>
    var oneBarChart = document.getElementById("hits-by-months");
    if (null !== oneBarChart) var ctx_ob = oneBarChart.getContext("2d"), data_ob = {
      labels: {!! json_encode(array_column($monthsHit, 'date_hit')) !!},
      datasets: [{
        backgroundColor: "#38a9ff",
        data: {{ json_encode(array_column($monthsHit, 'hits')) }}
      }]
    }, oneBarEl = new Chart(ctx_ob, {
      type: "bar",
      data: data_ob,
      options: {
        deferred: {delay: 200},
        tooltips: {enabled: !1},
        legend: {display: !1},
        responsive: !0,
        scales: {
          xAxes: [{stacked: !0, barPercentage: .6, gridLines: {display: !1}, ticks: {fontColor: "#888da8"}}],
          yAxes: [{stacked: !0, gridLines: {color: "#f0f4f9"}, ticks: {beginAtZero: !0, fontColor: "#888da8"}}]
        }
      }
    });


    var lineChart = document.getElementById("hits-line-chart");
    if (null !== lineChart) var ctx_lc = lineChart.getContext("2d"), data_lc = {
      labels: {!! json_encode(array_values(array_unique(array_column($usersHit, 'date_hit'))), 1) !!},
      datasets: [{
        label: " - Word wide users",
        borderColor: "#ffdc1b",
        borderWidth: 4,
        pointBorderColor: "#ffdc1b",
        pointBackgroundColor: "#fff",
        pointBorderWidth: 4,
        pointRadius: 6,
        pointHoverRadius: 8,
        fill: !1,
        lineTension: 0,
        data: {!! json_encode(array_column(array_filter($usersHit, function($val){ return $val->hit_type=='out';}), 'hits'), 1) !!}
      }, {
        label: " - {{ config('app.name') }} users",
        borderColor: "#08ddc1",
        borderWidth: 4,
        pointBorderColor: "#08ddc1",
        pointBackgroundColor: "#fff",
        pointBorderWidth: 4,
        pointRadius: 6,
        pointHoverRadius: 8,
        fill: !1,
        lineTension: 0,
        data: {!! json_encode(array_column(array_filter($usersHit, function($val){ return $val->hit_type=='in';}), 'hits'), 1) !!}
      }]
    }, lineChartEl = new Chart(ctx_lc, {
      type: "line",
      data: data_lc,
      options: {
        legend: {display: !1},
        responsive: !0,
        scales: {
          xAxes: [{ticks: {fontColor: "#888da8"}, gridLines: {color: "#f0f4f9"}}],
          yAxes: [{gridLines: {color: "#f0f4f9"}, ticks: {beginAtZero: !0, fontColor: "#888da8"}}]
        }
      }
    });
  </script>
@endsection
