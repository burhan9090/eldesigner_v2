@extends('profile.layout.main')

@section('profile_content')

  <section class="blog-post-wrap medium-padding80">
    <div class="container">
      <div class="row">

        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
          <div class="ui-block responsive-flex">
            <div class="ui-block-title">
              <div class="h6 title">Articles You Published</div>
            </div>
          </div>
        </div>

        @foreach($articles as $article)
          <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
            <div class="ui-block">
              <article class="hentry blog-post">

                @if($article->image)
                  <div class="post-thumb" style="height: 250px; text-align: center;">
                    <img style="max-height: 250px; width: auto;" src="{{ asset($article->image) }}" alt="photo">
                  </div>
                @endif

                <div class="post-content">
                  {{--<a href="#" class="post-category bg-blue-light">YOUR ARTICLES</a>--}}
                  <a href="{{ route("article.show", $article->id) }}" class="h4 post-title">{{ $article->title }}</a>
                  <p>{{ str_limit(strip_tags($article->body))  }}</p>

                  <div class="author-date">
                    by
                    <a class="h6 post__author-name fn" href="#">{{ $article->user? $article->user->name : ''}}</a>
                    <div class="post__date">
                      <time class="published" datetime="{{ $article->created_at }}">
                        {{ \Carbon\Carbon::parse($article->created_at )->diffForHumans() }}
                      </time>
                    </div>
                  </div>
                </div>
                <div class="align-center">
                  <a href="javascript:void(0);" data-id="{{ $article->id }}" onclick="hideArticle(this);"
                     class="btn btn-md-2 mb-0 btn-border-think custom-color c-grey btn-block" {!! $article->timeline()->where('user_id', auth()->id())->first()? '': "style='display: none;'"  !!} >Hide
                    article</a>
                  <a href="javascript:void(0);" data-id="{{ $article->id }}" onclick="publishArticle(this);"
                     class="btn btn-blue btn-md-2 mb-0 btn-block" {!! $article->timeline()->where('user_id', auth()->id())->first()? "style='display: none;'": ""  !!} >Republish</a>
                @if($mine)
                  <a href="{{ route('add_article.edit', $article->id) }}" data-id="{{ $article->id }}" class="btn btn-md-2 mb-0 btn-border-think btn-block btn-purple">Edit article</a>
                  <a href="javascript:void(0);" data-id="{{ $article->id }}" onclick="deleteArticle(this);" class="btn btn-md-2 mb-0 btn-border-think btn-block btn-danger">Delete
                    article</a>
                </div>
                @endif
              </article>
            </div>
          </div>
        @endforeach

        @if ($articles->hasPages())
          <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="ui-block responsive-flex">
              <div class="ui-block-title" style="padding: 0 25px 0;">
                {{ $articles->render('layouts.paginator') }}
              </div>
            </div>
          </div>
        @endif
      </div>
    </div>
  </section>

@endsection

@section('script')
  <script>
    function hideArticle(elem) {
      var $this = $(elem);
      var $id = $this.data('id');

      $this.addClass('disabled');

      $.post('{{ route('article.hide') }}', {article_id: $id})
        .done(function (d) {
          if (d.status) {
            $('a[data-id=' + $id + ']').each(function () {
              $(this).toggle();
            });
            swal("Article has hidden", "", "success");
          }
          else {
            swal("Something goes wrong!", "", "info");
          }
        })
        .always(function () {
          $this.removeClass('disabled');
        });
    }

    function publishArticle(elem) {
      var $this = $(elem);
      var $id = $this.data('id');

      $this.addClass('disabled');

      $.post('{{ route('article.publish') }}', {article_id: $id})
        .done(function (d) {
          if (d.status) {
            $('a[data-id=' + $id + ']').each(function () {
              $(this).toggle();
            });
            swal("Article has published", "", "success");
          }
          else {
            swal("Something goes wrong!", "", "info");
          }
        })
        .always(function () {
          $this.removeClass('disabled');
        });
    }

    function deleteArticle(elem) {
      var $this = $(elem);
      var $id = $this.data('id');

      $this.addClass('disabled');

      swal({
        title: "Are you sure?",
        text: "You are going to delete This Article!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
      })
        .then((willDelete) => {
          if (willDelete) {
            $.post('{{ route('article.delete') }}', {article_id: $id})
              .done(function (d) {
                if (d.status) {
                  swal("Article has deleted!", "", "success");
                  $this.parent().parent().parent().parent().hide('slow', function(){$(this).remove();});
                  history.go(0);
                }
                else {
                  swal("Something goes wrong!", "", "info");
                }
              })
              .always(function () {
                $this.removeClass('disabled');
              });
          }
        });
    }

  </script>
@endsection