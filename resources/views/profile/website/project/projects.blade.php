<div class="ui-block">
    <article class="hentry blog-post single-post single-post-v3">
        <div class="post-content-wrap">
            <div style="text-align: right;">
                Created:
                <time class="published" datetime="{{ $project->created_at }}">
                    {{ \Carbon\Carbon::parse($project->created_at )->diffForHumans() }}
                </time>
            </div>
            <h3 style="text-align: center">{{ $project->title }} </h3>

            <p>{{ $project->description }}</p>
        </div>
    </article>
</div>

<div class="row post-block-photo js-zoom-gallery">
    @foreach($project->images as $key=>$image)
        <a href="{{ asset($image->image) }}" class="col col-xl-4 col-lg-12 col-md-12 col-sm-12 col-12">
            <img src="{{ asset($image->image) }}" alt="photo" style="object-fit: cover; height: 300px; object-position: center;">
        </a>
    @endforeach

</div>