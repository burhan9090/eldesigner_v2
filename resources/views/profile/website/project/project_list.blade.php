@extends('profile.layout.main')

@section('profile_content')

  <div class="ui-block responsive-flex">
    <div id="message_alert" style="display: none">

    </div>
    <div class="ui-block-title">
      <div class="h6 title">Your project's gallery</div>
      <input type="hidden" id="project_count" value="{{$projects_count}}">
      <div class="block-btn align-right">
        <a href="#" class="btn btn-primary btn-md-2 add_new_project">
          Add new project +</a>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <!-- Tab panes -->
        <div class="tab-content">

          <div class="tab-pane active" id="album-page" role="tabpanel">
            <div class="photo-album-wrapper">
              <div class="row">
              <div class="photo-album-item-wrap col-3-width">
                <div class="photo-album-item create-album" data-mh="album-item">
                  <a href="#" class="full-block add_new_project"></a>
                  <div class="content">
                    <a href="#" class="btn btn-control bg-primary add_new_project" >
                      <svg class="olymp-plus-icon">
                        <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-plus-icon') }}"></use>
                      </svg>
                    </a>

                    <a href="#" class="title h5 add_new_project "  >Add new project +
                    </a>
                    <span class="sub-title">When you add a new project, it will appear on your website, profile and on the newsfeed.</span>
                  </div>
                </div>
              </div>
              @foreach($projects as $project)
                <div class="photo-album-item-wrap col-3-width">
                  <div class="photo-album-item" data-mh="album-item">
                    <div class="photo-item" style="height: 211px;">
                      <img class="project-logo-image" src="@php
                        if(count($project->image)){
                          echo e(asset($project->image->image));
                        }
                        else {
                          echo e(asset('img/photo-item2.jpg'));
                        }
                      @endphp" alt="photo">
                      <div class="overlay overlay-dark"></div>
                      {{--<a href="javascript:void(0)" class="more edit" data-id="{{ $project->id }}">--}}
                        {{--<svg class="olymp-three-dots-icon">--}}
                          {{--<use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use>--}}
                        {{--</svg>--}}
                      {{--</a>--}}
                      {{--<a href="#" class="post-add-icon">
                        <svg class="olymp-heart-icon">
                          <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-heart-icon') }}"></use>
                        </svg>
                        <span>324</span>
                      </a>--}}
                      <a href="{{ route('projects.show', $project->id) }}" data-toggle="modal" data-target="#open-photo-popup-v2" class="full-block"></a>
                    </div>

                    <div class="content">
                      <a href="{{ route('projects.show', $project->id) }}" class="title h5">{{ str_limit($project->title, 50) }}</a>
                      <span class="sub-title">category: {{ optional($project->category)->service_name }}</span>
                      <span class="sub-title">Last Added:
                        {{ $project->created_at->diffForHumans() }}</span>

                      <div class="swiper-container">
                        <div class="swiper-wrapper">
                          <div class="swiper-slide">
                            <div class="togglebutton">
                              @if(!$project->is_demo)
                                <label>Show on website and timeline
                                  <input class="project-toggle" type="checkbox"
                                         data-id="{{ $project->id }}" {!! $project->timeline?'checked=""': '' !!}>
                                </label>
                              @endif
                            </div>
{{--                            <div class="togglebutton" >--}}
{{--                              <button {!! $project->repost? 'style="display: none;': ''  !!} class="btn btn-primary repost-project" data-id="{{ $project->id }}">Allow Repost</button>--}}
{{--                              <button {!! $project->repost? '': 'style="display: none;'  !!} class="btn btn-warning repost-project-dis" data-id="{{ $project->id }}">Disallow Repost</button>--}}
{{--                            </div>--}}
                            <div class="togglebutton">
                              @if(!$project->is_demo)
                                <a href="javascript:void(0)" class="btn btn-purple edit" data-id="{{ $project->id }}">
                                  Edit
                                </a>
                              @endif
                              <button class="btn btn-danger delete-project" data-id="{{ $project->id }}">Delete</button>
                            </div>
                          </div>

                          {{--<div class="swiper-slide">--}}
                            {{--<div class="friend-count" data-swiper-parallax="-500">--}}
                              {{--<a href="#" class="friend-count-item">--}}
                                {{--<div class="h6">24</div>--}}
                                {{--<div class="title">Photos</div>--}}
                              {{--</a>--}}
                              {{--<a href="#" class="friend-count-item">--}}
                                {{--<div class="h6">86</div>--}}
                                {{--<div class="title">Comments</div>--}}
                              {{--</a>--}}
                              {{--<a href="#" class="friend-count-item">--}}
                                {{--<div class="h6">16</div>--}}
                                {{--<div class="title">Share</div>--}}
                              {{--</a>--}}
                            {{--</div>--}}
                          {{--</div>--}}
                        </div>

                        <!-- If we need pagination -->
                        <div class="swiper-pagination"></div>
                      </div>
                    </div>

                  </div>
                </div>
              @endforeach
              </div>
            </div>
          </div>

          @if ($projects->hasPages())
            <div class="ui-block responsive-flex">
              <div class="ui-block-title" style="padding: 0 25px 0;">
                {{ $projects->render('layouts.paginator') }}
              </div>
            </div>
          @endif

        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="create-photo-album" tabindex="-1" role="dialog" aria-labelledby="create-photo-album"
       aria-hidden="true">
    <div class="modal-dialog window-popup create-photo-album" role="document">
      <div class="modal-content">
        <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
          <svg class="olymp-close-icon">
            <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use>
          </svg>
        </a>

        <div class="modal-header">
          <h6 class="title">Add new project</h6>
        </div>

        <div class="modal-body">
          <form id="add-project">
            <div class="form-group">
              <label class="control-label">Project title</label>
              <input class="form-control" placeholder="Modern Living Room Design" type="text"
                     name="title">
            </div>
            <div class="form-group">
              <label class="control-label">Description</label>
              <input class="form-control" placeholder="General info about the project" type="text"
                     name="description">
            </div>
            <div class="form-group">
              <label class="control-label">Category</label>
              <select class="form-control" name="category_id" size="">
                @foreach($categories as $category)
                <option value="{{ $category->id }}">{{ $category->service_name }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label class="control-label">Tags</label>
              <input class="form-control"
                     placeholder="Add tags such as: living room, home, modern and separate them using a comma."
                     type="text" name="tags">
            </div>
            <div class="photo-album-wrapper images">
              <div class="photo-album-item-wrap col-3-width">
                <div class="photo-album-item create-album" data-mh="album-item">
                  <div class="content">
                    <a href="#" class="btn btn-control bg-primary add-image">
                      <svg class="olymp-plus-icon">
                        <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-plus-icon') }}"></use>
                      </svg>
                    </a>

                    <a href="#" class="title h5 add-image">Add images...</a>
                  </div>
                </div>
              </div>

            </div>

            <button class="btn btn-secondary btn-lg btn--half-width" data-dismiss="modal"
                    aria-label="Close">Discard Everything
            </button>
            <a href="#" id="submit-project" class="btn btn-primary btn-lg btn--half-width">Post Project</a>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="edit-photo-album" tabindex="-1" role="dialog" aria-labelledby="edit-photo-album"
       aria-hidden="true">
    <div class="modal-dialog window-popup create-photo-album" role="document">
      <div class="modal-content">
        <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
          <svg class="olymp-close-icon">
            <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use>
          </svg>
        </a>

        <div class="modal-header">
          <h6 class="title">Edit project</h6>
        </div>

        <div class="modal-body">
          <form id="edit-project" action="{{ route('projects.update') }}" method="post" enctype="multipart/form-data">
            {{ csrf_field() }}
            <input name="id" type="hidden" />
            <div class="form-group">
              <label class="control-label">Project title</label>
              <input class="form-control" placeholder="Modern Living Room Design" type="text" name="title">
            </div>
            <div class="form-group">
              <label class="control-label">Description</label>
              <input class="form-control" placeholder="General info about the project" type="text" name="description">
            </div>
            <div class="form-group">
              <label class="control-label">Category</label>
              <select class="form-control" name="category_id" size="">
                @foreach($categories as $category)
                  <option value="{{ $category->id }}">{{ $category->service_name }}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label class="control-label">Tags</label>
              <input class="form-control" placeholder="Add tags such as: living room, home, modern and separate them using a comma." type="text" name="tags">
            </div>
            <div class="photo-album-wrapper images">
              <div class="photo-album-item-wrap col-3-width">
                <div class="photo-album-item create-album" data-mh="album-item">
                  <div class="content">
                    <a href="#" class="btn btn-control bg-primary add-image">
                      <svg class="olymp-plus-icon">
                        <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-plus-icon') }}"></use>
                      </svg>
                    </a>

                    <a href="#" class="title h5 add-image">Add images...</a>
                  </div>
                </div>
              </div>

            </div>

            <button class="btn btn-secondary btn-lg btn--half-width" data-dismiss="modal" aria-label="Close">Discard Everything
            </button>
            <button type="submit" class="btn btn-primary btn-lg btn--half-width">Post Project</button>
          </form>
        </div>
      </div>
    </div>
  </div>

  {{--<div class="modal fade" id="loading" tabindex="-1" role="dialog" aria-labelledby="loading" aria-hidden="true">--}}
    {{--<div class="modal-dialog window-popup create-photo-album" role="document">--}}
      {{--<div class="modal-content">--}}
        {{--<div class="modal-header">--}}
          {{--<h6 class="title">Loading</h6>--}}
        {{--</div>--}}

        {{--<div class="modal-body">--}}
          {{--<div class="form-group text-center">--}}
            {{--<img src="{{ asset('img/spinner.gif') }}" />--}}
          {{--</div>--}}
        {{--</div>--}}
      {{--</div>--}}
    {{--</div>--}}
  {{--</div>--}}

  @include('layouts.loader')
@endsection

@section('style')
  <style>
    .cover-image {
      height: 159px;
      object-position: center;
      object-fit: cover;
    }

    .project-logo-image {
      height: 211px;
      object-position: center;
      object-fit: cover;
    }
  </style>
@endsection

@section('script')
  <script>

    $('.add-image').click(function () {

      var $id = parseInt(Math.random() * 10000)*-1;

      var $form = $(this).closest('form');

      var $row = $('<div class="photo-album-item-wrap col-3-width item" style="display: none;">')
        .append(
          $('<div class="photo-album-item" data-mh="album-item">')
            .append(
              $('<div class="form-group">')
                .append('<input type="file" name="picture-file[' + $id + ']" style="display: none;" />')
                .append('<img alt="photo" class="cover-image" />')
                .append('<textarea class="form-control" name="picture[' + $id + '][description]" placeholder="Write an individual description for each one, else the overall description will appear here...">')
            )
            .append('<input class="form-control" name="picture[' + $id + '][tags]" placeholder="individual tags" type="text">')
            .append('<button type="button" class="btn btn-danger btn-block m-0 remove-image">Remove</button>')
        );


      $(this).closest('.create-album').parent().after($row);

      $row.find('input[type="file"]').change(function () {
        loadImage(this, $row);
      })
        .click();
    });

    $('.delete-project').click(function () {
      var $this = $(this);

      $this.addClass('disabled');

      showLoader();
      $.get('{{ route('projects.delete') }}', {id: $this.data('id')})
        .done(function (d) {
          if (d.status) {
            swal(d.message, "", "success").then(() => history.go(0));
          } else {
            swal("Something goes wrong!", d.message, "info");
          }
        })
        .always(function () {
          $this.removeClass('disabled');
          hideLoader();
        });
    });

    loadImage = function (elem, $row) {
      if (elem.files && elem.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
          $row.find('img').attr('src', e.target.result);
          $row.show();
        };

        reader.readAsDataURL(elem.files[0]);
      } else {
        $row.remove()
      }
    };

    $('.images').on('click', '.remove-image', function () {
      $(this).closest('.photo-album-item-wrap').remove();
    });

    $('#submit-project').click(function () {
      var $this = $(this);

      $this.addClass('disabled');

      showLoader();
      $.ajax({
        url: '{{ route('projects.save') }}',
        data: new FormData($('#add-project')[0]),
        processData: false,
        method: 'POST',
        contentType: false,
        success: function (d) {
          if (d.status) {
            // $form = $('#add-project');

            // $form.find('input[name="title"]').val('');
            // $form.find('[name="description"]').val('');
            // $form.find('input[name="tags"]').val('');
            // $form.find('.images>.item').remove();

            // $('#create-photo-album').modal('hide');

            swal("Project has added", "", "success").then(()=> history.go(0));

          } else {

            $msg = '';
            for (i in d.message) {
              for (j in d.message[i]) {
                $msg += i + ': ' + d.message[i][j] + "\n";
              }
            }

            swal("Something goes wrong!", $msg, "info");
          }
        }
      })
        .always(function () {
          $this.removeClass('disabled');
          hideLoader();
        });

    });

    $('.project-toggle').change(function () {
      var $this = $(this);

      if($this.prop('checked')){
        publishProject($this);
      }
      else {
        hideProject($this);
      }

    });

    $('.repost-project, .repost-project-dis').click(function(){
      var $this = $(this);
      var $id = $this.data('id');

      showLoader();
      $.post('{{ route('projects.repost') }}', {project_id: $id})
        .done(function (d) {
          if (d.status) {
            $('.repost-project, .repost-project-dis').each(function () {
              if($(this).data('id')!=$id){
                return;
              }
              $(this).toggle();
            });
            swal("Project has hidden", "", "success");
          }
          else {
            swal("Something goes wrong!", "", "info");
          }
        })
        .always(function () {
          $this.removeClass('disabled');
          hideLoader();
        });
    });

    function hideProject($this) {
      var $id = $this.data('id');

      $this.addClass('disabled');

      showLoader();
      $.post('{{ route('projects.hide') }}', {project_id: $id})
        .done(function (d) {
          if (d.status) {
            $('a[data-id=' + $id + ']').each(function () {
              $(this).toggle();
            });
            swal("Project has hidden", "", "success");
          }
          else {
            swal("Something goes wrong!", "", "info");
          }
        })
        .always(function () {
          $this.removeClass('disabled');
          hideLoader();
        });
    }

    function publishProject($this) {
      var $id = $this.data('id');

      $this.addClass('disabled');

      showLoader();
      $.post('{{ route('projects.publish') }}', {project_id: $id})
        .done(function (d) {
          if (d.status) {
            $('a[data-id=' + $id + ']').each(function () {
              $(this).toggle();
            });
            swal("Project has published", "", "success");
          }
          else {
            swal("Something goes wrong!", "", "info");
          }
        })
        .always(function () {
          $this.removeClass('disabled');
          hideLoader();
        });
    }

    $("#edit-photo-album").on("shown.bs.modal", function () {
      $("body").addClass("modal-open");
    }).on("hidden.bs.modal", function () {
      $("body").removeClass("modal-open")
    });

    $('.edit').click(function(){

      var $id = $(this).data('id');

      $('#spinner-loader').off('shown.bs.modal').on('shown.bs.modal', function() {


        $.get('{{ route('projects.get_project_ajax') }}', {id: $id})
          .done(function (d) {
            hideLoader();

            if(d.data){
              var editPhoto = $('#edit-photo-album');
              var tags = [];
              editPhoto.find('[name="id"]').val(d.data.id);
              editPhoto.find('[name="title"]').val(d.data.title);
              editPhoto.find('[name="description"]').val(d.data.description);

              if(d.data.category) {
                editPhoto.find('[name="category_id"]').val(d.data.category.id);
              }

              for(i=0; tag=d.data.tags[i]; i++){
                tags.push(tag.text);
              }

              editPhoto.find('[name="tags"]').val(tags.join(', '));

              for(i=0; image=d.data.images[i]; i++) {
                tags=[];

                for(j=0; tag=image.tags[j]; j++){
                  tags.push(tag.text);
                }

                var $row = $('<div class="photo-album-item-wrap col-3-width item">')
                  .append(
                    $('<div class="photo-album-item" data-mh="album-item">')
                      .append(
                        $('<div class="form-group">')
                          .append(
                            $('<div>')
                              .css({
                                width: "100 %",
                                height: 159,
                                "vertical-align": "middle",
                                display: "table-cell"
                              })
                              // .append('<input type="file" name="picture-file[' + image.id + ']"  style="display: none;" />')
                              .append($('<img alt="photo" style="max-height: 159px;">').attr('src', image.image))
                          )
                          .append($('<textarea class="form-control" name="picture[' + image.id + '][description]" placeholder="Write an individual description for each one, else the overall description will appear here...">').val(image.description))
                      )
                      .append($('<input class="form-control" name="picture[' + image.id + '][tags]" placeholder="individual tags" type="text">').val(tags.join(', ')))
                      .append('<button type="button" class="btn btn-danger btn-block m-0 remove-image">Remove</button>')
                  );


                editPhoto.find('.images').append($row);
              }


              $('#edit-photo-album').modal('show');
            }
            else {
              swal("Something goes wrong!", '', "info");
            }
          })
          .always(function(){
            hideLoader();
          });

      });

      showLoader();
    });
  </script>
@endsection