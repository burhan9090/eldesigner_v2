@extends(auth()->check()?'profile.layout.main': 'layouts.eldesigners_with_header')

@section(auth()->check()?'profile_content': 'content2')

  @include('profile.website.project.projects')
@endsection