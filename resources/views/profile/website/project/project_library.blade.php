@extends('profile.layout.main')

@section('profile_content')

  <div class="ui-block responsive-flex">
    <div class="ui-block-title">
      <div class="h6 title">Community projects</div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <!-- Tab panes -->
        <div class="tab-content">

          <div class="tab-pane active" id="album-page" role="tabpanel">
            <div class="photo-album-wrapper">
              <div class="row">
              @foreach($projects as $project)
                <div class="photo-album-item-wrap col-3-width">
                  <div class="photo-album-item" data-mh="album-item">
                    <div class="photo-item" style="height: 211px;">
                      <img style="max-height: 211px;" src="@php
                        if(count($project->image)){
                          echo e(asset($project->image->image));
                        }
                        else {
                          echo e(asset('img/photo-item2.jpg'));
                        }
                      @endphp" alt="photo">
                      <div class="overlay overlay-dark"></div>
                      <a href="{{ route('projects.show', $project->id) }}" class="more">
                        <svg class="olymp-three-dots-icon">
                          <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use>
                        </svg>
                      </a>
                      {{--<a href="#" class="post-add-icon">
                        <svg class="olymp-heart-icon">
                          <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-heart-icon') }}"></use>
                        </svg>
                        <span>324</span>
                      </a>--}}
                      <a href="{{ route('projects.show', $project->id) }}" data-toggle="modal"
                         data-target="#open-photo-popup-v2" class="full-block"></a>
                    </div>

                    <div class="content">
                      <a href="{{ route('projects.show', $project->id) }}"
                         class="title h5">{{ str_limit($project->title, 50) }}</a>
                      <span class="sub-title">Last Added:
                        {{ \Carbon\Carbon::parse($project->created_at )->diffForHumans() }}</span>

                      <div class="swiper-container">
                        <div class="swiper-wrapper">
                          <div class="swiper-slide">
                            <div class="togglebutton">
                              @if($project->user_id==auth()->id())
                              <label>Show on website
                                <input class="project-toggle" type="checkbox"
                                       data-id="{{ $project->id }}" {!! $project->timeline?'checked=""': '' !!}>
                              </label>
                              @endif
                            </div>
                          </div>

                          {{--<div class="swiper-slide">--}}
                          {{--<div class="friend-count" data-swiper-parallax="-500">--}}
                          {{--<a href="#" class="friend-count-item">--}}
                          {{--<div class="h6">24</div>--}}
                          {{--<div class="title">Photos</div>--}}
                          {{--</a>--}}
                          {{--<a href="#" class="friend-count-item">--}}
                          {{--<div class="h6">86</div>--}}
                          {{--<div class="title">Comments</div>--}}
                          {{--</a>--}}
                          {{--<a href="#" class="friend-count-item">--}}
                          {{--<div class="h6">16</div>--}}
                          {{--<div class="title">Share</div>--}}
                          {{--</a>--}}
                          {{--</div>--}}
                          {{--</div>--}}
                        </div>

                        <!-- If we need pagination -->
                        <div class="swiper-pagination"></div>
                      </div>
                    </div>

                  </div>
                </div>
              @endforeach
              </div>
            </div>
          </div>

          @if ($projects->hasPages())
            <div class="ui-block responsive-flex">
              <div class="ui-block-title" style="padding: 0 25px 0;">
                {{ $projects->render('layouts.paginator') }}
              </div>
            </div>
          @endif

        </div>
      </div>
    </div>
  </div>


  <div class="modal fade" id="create-photo-album" tabindex="-1" role="dialog" aria-labelledby="create-photo-album"
       aria-hidden="true">
    <div class="modal-dialog window-popup create-photo-album" role="document">
      <div class="modal-content">
        <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
          <svg class="olymp-close-icon">
            <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use>
          </svg>
        </a>

        <div class="modal-header">
          <h6 class="title">Add new project</h6>
        </div>

        <div class="modal-body">
          <form id="add-project">
            <div class="form-group">
              <label class="control-label">Project title</label>
              <input class="form-control" placeholder="Modern Living Room Design" type="text"
                     name="title">
            </div>
            <div class="form-group">
              <label class="control-label">Description</label>
              <input class="form-control" placeholder="General info about the project" type="text"
                     name="description">
            </div>
            <div class="form-group">
              <label class="control-label">Tags</label>
              <input class="form-control"
                     placeholder="Add tags such as: living room, home, modern and separate them using a comma."
                     type="text" name="tags">
            </div>
            <div class="photo-album-wrapper images">
              <div class="photo-album-item-wrap col-3-width">
                <div class="photo-album-item create-album" data-mh="album-item">
                  <div class="content">
                    <a href="#" class="btn btn-control bg-primary add-image">
                      <svg class="olymp-plus-icon">
                        <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-plus-icon') }}"></use>
                      </svg>
                    </a>

                    <a href="#" class="title h5 add-image">Add images...</a>
                  </div>
                </div>
              </div>

            </div>

            <button class="btn btn-secondary btn-lg btn--half-width" data-dismiss="modal"
                    aria-label="Close">Discard
              Everything
            </button>
            <a href="#" id="submit-project" class="btn btn-primary btn-lg btn--half-width">Post Project</a>
          </form>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('script')
  <script>

    $('.project-toggle').change(function () {
      var $this = $(this);

      if ($this.prop('checked')) {
        publishProject($this);
      }
      else {
        hideProject($this);
      }

    });

    function hideProject($this) {
      var $id = $this.data('id');

      $this.addClass('disabled');

      $.post('{{ route('projects.hide') }}', {project_id: $id})
        .done(function (d) {
          if (d.status) {
            $('a[data-id=' + $id + ']').each(function () {
              $(this).toggle();
            });
            swal("Project has hidden", "", "success");
          }
          else {
            swal("Something goes wrong!", "", "info");
          }
        })
        .always(function () {
          $this.removeClass('disabled');
        });
    }

    function publishProject($this) {
      var $id = $this.data('id');

      $this.addClass('disabled');

      $.post('{{ route('projects.publish') }}', {project_id: $id})
        .done(function (d) {
          if (d.status) {
            $('a[data-id=' + $id + ']').each(function () {
              $(this).toggle();
            });
            swal("Project has published", "", "success");
          }
          else {
            swal("Something goes wrong!", "", "info");
          }
        })
        .always(function () {
          $this.removeClass('disabled');
        });
    }

  </script>
@endsection