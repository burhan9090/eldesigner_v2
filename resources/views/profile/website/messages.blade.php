@extends('profile.layout.main')
@section('profile_content')

  <div class="ui-block">


    <section class="">
      <div class="container">
        <div class="row">
          <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12  m-auto">


            <ul class="table-careers">
              <li class="head">
                <span>DATE</span>
                <span>TITLE</span>
                <span></span>
                <span></span>
              </li>

              @foreach($messages as $message)
                <li class="{{ boolval($message->is_read)?'' :'unread' }}">
                  <span class="date">{{ $message->created_at->diffInWeeks()>1? $message->created_at->format('Y-m-d'): $message->created_at->diffForHumans() }}</span>
                  <span class="position bold">{{ $message->title }}</span>
                  <span></span>
                  <span><a href="{{ route('messages.show', $message->id) }}" class="btn btn-primary btn-sm full-width" >View Message</a></span>
                </li>
              @endforeach


            </ul>
          </div>
        </div>
      </div>
    </section>
  </div>


  <div class="row">
    @if ($messages->hasPages())
      <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="ui-block responsive-flex">
          <div class="ui-block-title" style="padding: 0 25px 0;">
            {{ $messages->render('layouts.paginator') }}
          </div>
        </div>
      </div>
    @endif
  </div>

@endsection
