@extends('profile.layout.main')

@section('profile_content')

  <div class="ui-block">
    <div class="ui-block-title">
      <h6 class="title">Team Members</h6>
    </div>
    <div class="ui-block-content">
      <form id="member_form">

        @php
          $memberArr = json_decode($members->member, 1);

          if(!$memberArr){
            $memberArr = [];
          }
        @endphp

        <div class="row">
          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <div class="file-upload">
                <label class="file-upload__label">Member Image 1
                  <input class="file-upload__input" type="file" name="image[0]">
                </label>
                @if($memberArr[0]['image'] ?? false)
                  <img style="max-height: 80px; max-width: 80px;" src="{{ asset($memberArr[0]['image']) }}">
                @endif
              </div>
            </div>
          </div>

          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Member Name 1</label>
              <input type="text" class="form-control" name="member[0][title]"
                     value="{{ $memberArr[0]['title'] ?? '' }}"/>
            </div>
          </div>

          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Member description 1</label>
              <textarea class="form-control" placeholder=""
                        name="member[0][description]">{{ $memberArr[0]['description'] ?? '' }}</textarea>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <div class="file-upload">
                <label class="file-upload__label">Member Image 2
                  <input class="file-upload__input" type="file" name="image[1]">
                </label>
                @if($memberArr[1]['image'] ?? false)
                  <img style="max-height: 80px; max-width: 80px;" src="{{ asset($memberArr[1]['image']) }}">
                @endif
              </div>
            </div>
          </div>

          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Member Name 2</label>
              <input type="text" class="form-control" name="member[1][title]"
                     value="{{ $memberArr[1]['title'] ?? '' }}"/>
            </div>
          </div>

          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Member description 2</label>
              <textarea class="form-control" placeholder=""
                        name="member[1][description]">{{ $memberArr[1]['description'] ?? '' }}</textarea>
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <div class="file-upload">
                <label class="file-upload__label">Member Image 3
                  <input class="file-upload__input" type="file" name="image[2]">
                </label>
                @if($memberArr[2]['image'] ?? false)
                  <img style="max-height: 80px; max-width: 80px;" src="{{ asset($memberArr[2]['image']) }}">
                @endif
              </div>
            </div>
          </div>

          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Member Name 3</label>
              <input type="text" class="form-control" name="member[2][title]"
                     value="{{ $memberArr[2]['title'] ?? '' }}"/>
            </div>
          </div>

          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Member description 3</label>
              <textarea class="form-control" placeholder=""
                        name="member[2][description]">{{ $memberArr[2]['description'] ?? '' }}</textarea>
            </div>
          </div>
        </div>


        <div class="row">
          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <div class="file-upload">
                <label class="file-upload__label">Member Image 4
                  <input class="file-upload__input" type="file" name="image[3]">
                </label>
                @if($memberArr[3]['image'] ?? false)
                  <img style="max-height: 80px; max-width: 80px;" src="{{ asset($memberArr[3]['image']) }}">
                @endif
              </div>
            </div>
          </div>

          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Member Name 4</label>
              <input type="text" class="form-control" name="member[3][title]"
                     value="{{ $memberArr[3]['title'] ?? '' }}"/>
            </div>
          </div>

          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Member description 4</label>
              <textarea class="form-control" placeholder=""
                        name="member[3][description]">{{ $memberArr[3]['description'] ?? '' }}</textarea>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button class="btn btn-secondary btn-lg full-width">Cancel</button>
          </div>
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button id="submit" type="button" class="btn btn-primary btn-lg full-width">Save all Changes</button>
          </div>
        </div>

      </form>
    </div>
  </div>

  @include('layouts.loader')

@endsection

@section('script')
  <script>
    $('#submit').click(function () {
      var $this = $(this);

      $this.addClass('disabled');
      showLoader();
      $.ajax({
        url: '{{ route('members.save') }}',
        data: new FormData($('#member_form')[0]),
        processData: false,
        method: 'POST',
        contentType: false,
        success: function (d) {
          if (d.status) {
            swal("Saved", "", "success");
          }
          else {

            $msg = '';
            for (i in d.message) {
              for (j in d.message[i]) {
                $msg += i + ': ' + d.message[i][j] + "\n";
              }
            }
            swal("Something goes wrong!", $msg, "info");
          }
        }
      }).always(function () {
        $this.removeClass('disabled');
        hideLoader();
      });
    });


    $('.file-upload__input').change(function () {
      var readerUpload = new FileReader();
      var $this = $(this).parent();

      readerUpload.onload = function (e) {
        $this.parent().find('img').remove();
        $this.after($('<img>').css({"max-height": 80, "max-width": 80, "display": "none"}));
        $this.parent().find('img').attr('src', e.target.result).after('<br>');
        $this.parent().find('img').show();
      };

      if (this.files && this.files[0]) {
        readerUpload.readAsDataURL(this.files[0]);
      }
    });
  </script>
@endsection