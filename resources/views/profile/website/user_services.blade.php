@extends('profile.layout.main')

@section('profile_content')

  <div class="ui-block">
    <div class="ui-block-title">
      @if(auth()->user()->profile_type=='company')
      <h6 class="title">Company's Fields of services</h6>
      @else
      <h6 class="title">Fields of services</h6>
      @endif
    </div>
    <div class="ui-block-content">
      <form id="service_form">
        <div class="description-toggle">
          <div class="description-toggle-content">
            <div class="h6">View services section</div>
            <p>If you enable this, the services section will appear on your website. (recommended)</p>
          </div>
        </div>

        <div class="row">
          <div class="col col-md-12 col-sm-12 col-12">
            <div class="form-group label-floating is-select">
              <label class="control-label">Select your Services</label>
              <select class="selectpicker form-control" multiple="" id="user_service" name="user_service[]" size="4">
                @foreach($services as $service)
                  <option value="{{ $service->id }}" {{ in_array($service->id, $userServices)? 'selected="selected"': '' }}>{{ $service->service_name }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <div class="col col-md-12 col-sm-12 col-12">
            <span class="alert-danger" id="msg"></span>
          </div>
        </div>

        <div class="row">
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button class="btn btn-secondary btn-lg full-width">Cancel</button>
          </div>
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button id="submit" type="button" class="btn btn-primary btn-lg full-width">Save all Changes</button>
          </div>
        </div>

      </form>
    </div>
  </div>

  @include('layouts.loader')

@endsection

@section('script')
  <script>
    $('#submit').click(function () {
      var $this = $(this);

      var $msg = $('#msg');
      $msg.text('');

      if($('#user_service :selected').length>4){
        $msg.text('You can chose 4 Services at maximum.');
        return;
      }

      $this.addClass('disabled');
      showLoader();
      $.ajax({
        url: '{{ route('user_services.save') }}',
        data: new FormData($('#service_form')[0]),
        processData: false,
        method: 'POST',
        contentType: false,
        success: function (d) {
          if (d.status) {
            swal("Saved", "", "success");
          }
          else {

            $msg = '';
            for (i in d.message) {
              for (j in d.message[i]) {
                $msg += i + ': ' + d.message[i][j] + "\n";
              }
            }
            swal("Something goes wrong!", $msg, "info");
          }
        }
      }).always(function () {
        $this.removeClass('disabled');
        hideLoader();
      });
    });

  </script>
@endsection