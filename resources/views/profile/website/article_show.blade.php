{{--@extends('layouts.profile')--}}
@extends(auth()->check()?'layouts.application': 'layouts.eldesigners_with_header')

@section(auth()->check()?'content': 'content2')
  <div class="container">
    <div class="row mt50">

      <div class="col col-xl-8 col-lg-8 col-md-12 col-sm-12 col-12">
        <div class="ui-block">


          <!-- Single Post -->

          <article class="hentry blog-post single-post single-post-v3">

            <a href="#" class="post-category bg-primary">{{ optional($article->category)->text }}</a><br/>
            @foreach($article->tags as $tag)
              <a href="javascript:void(0);" class="post-category bg-blue">{{ $tag->text }}</a>
            @endforeach

            <h1 class="post-title">{{ $article->title }}</h1>

            <div class="author-date">
              <div class="author-thumb header_logo">
                <img alt="author" src="{{ asset(optional($article->user)->image?: 'uploads/user/dummy_logo.png') }}"
                     class="avatar">
              </div>
              by
              <a class="h6 post__author-name fn" href="#">{{ optional($article->user)->name }}</a>
              <div class="post__date">
                <time class="published" datetime="{{ $article->created_at }}">
                  - {{ \Carbon\Carbon::parse($article->created_at )->diffForHumans() }}
                </time>
              </div>
            </div>
            @if($article->image)
              <div class="post-thumb">
                <img src="{{ asset($article->image) }}" alt="author">
              </div>
            @endif

            <div class="post-content-wrap">
              {{--
              <div class="control-block-button post-control-button">
                <a href="#" class="post-add-icon inline-items">
                  <svg class="olymp-speech-balloon-icon">
                    <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-speech-balloon-icon') }}"></use>
                  </svg>
                  <span>105</span>
                </a>

                <a href="#" class="btn btn-control has-i bg-facebook">
                  <i class="fab fa-facebook-f" aria-hidden="true"></i>
                </a>

                <a href="#" class="btn btn-control has-i bg-twitter">
                  <i class="fab fa-twitter" aria-hidden="true"></i>
                </a>
              </div>
  --}}
              <div class="post-content">
                {!! $article->body !!}
              </div>
            </div>
          </article>
        </div>
      </div>


      <div class="col col-xl-4 col-lg-4 col-md-12 col-sm-12 col-12">
        <aside>
          <div class="ui-block">
            <div class="ui-block-title">
              <h6 class="title">Featured Posts</h6>
            </div>
          </div>

          @foreach($related as $row)
            <div class="ui-block">
              <article class="hentry blog-post blog-post-v3 featured-post-item">

                <div class="post-thumb">
                  @if($row->image)
                    <img src="{{ asset($row->image) }}" alt="photo">
                  @endif
                  <a href="#"
                     class="post-category bg-purple">{{ optional($row)->category_text?: optional(optional($row)->category)->text }}</a>
                </div>

                <div class="post-content">

                  <div class="author-date">
                    by
                    <a class="h6 post__author-name fn"
                       href="#">{{ optional($row)->user_first_name?: optional(optional($row)->user)->name }}</a>
                    <div class="post__date">
                      <time class="published" datetime="{{ $row->created_at }}">
                        - {{ \Carbon\Carbon::parse($row->created_at )->diffForHumans() }}
                      </time>
                    </div>
                  </div>

                  <a href="{{ route('article.show', $row->id) }}" class="h4 post-title">{{ $row->title }}</a>

                  {{--
                  <div class="post-additional-info inline-items">

                    <ul class="friends-harmonic">
                      <li>
                        <a href="#">
                          <img src="img/icon-chat26.png" alt="icon">
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img src="img/icon-chat27.png" alt="icon">
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img src="img/icon-chat2.png" alt="icon">
                        </a>
                      </li>
                    </ul>
                    <div class="names-people-likes">
                      206
                    </div>

                    <div class="comments-shared">
                      <a href="#" class="post-add-icon inline-items">
                        <svg class="olymp-speech-balloon-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use></svg>
                        <span>97</span>
                      </a>
                    </div>
                  </div>
                  --}}
                </div>

              </article>
            </div>
          @endforeach

        </aside>
      </div>
    </div>
  </div>
@endsection