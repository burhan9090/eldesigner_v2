@extends('profile.layout.main')

@section('profile_content')

  <div class="ui-block">
      <div class="ui-block-title bg-blue">
        <h6 class="title c-white">Publish An Article</h6>
      </div>
      <div class="ui-block-content">

        <form action="{{ route('add_article.update') }}" method="post" enctype="multipart/form-data">
          {{ csrf_field() }}
          <input type="hidden" name="id" value="{{ $article->id }}">
          <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="form-group label-floating">
                <input class="form-control" type="text" name="title"
                       placeholder="Title | For example: Modern architecture designs in the MENA region" value="{{ $article->title }}">
              </div>
            </div>

            <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
              <div class="form-group label-floating is-select">
                <label class="control-label">Select Category</label>
                <select class="selectpicker form-control" id="category">
                  @foreach($categories as $category)
                  <option value="{{ $category->id }}" data-children="{{ json_encode($category->children->pluck('text', 'id')) }}" {{ optional(optional($article->category)->parent)->id==$category->id ? 'selected=""': '' }}>{{ $category->text }}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
              <div class="form-group label-floating is-select">
                <label class="control-label">Select Subcategory</label>
                <select class="selectpicker form-control" name="category_id" id="subcategory">
                  @if(count($categories))
                  @foreach(array_first($categories)->children as $category)
                    <option value="{{ $category->id }}"  {{ $article->category_id==$category->id ? 'selected=""': '' }}>{{ $category->text }}</option>
                  @endforeach
                  @endif
                </select>
              </div>
            </div>

            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="form-group label-floating">
                <label class="control-label">Article body</label>

                <textarea class="form-control" name="body" id="body" style="min-height: 240px; display: none;">{{ $article->body }}</textarea>
              </div>
            </div>

            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

              <div class="form-group">
                <input class="form-control" type="text" name="tags" value="{{ implode(', ', $article->tags->pluck('text')->toArray()) }}"
                       placeholder="Add tags to enable users to find your article easier. and separate them using a comma.">
              </div>
              <div class="description-toggle">
                <div class="description-toggle-content">
                  <div class="h6">Allow reposting by others</div>
                  <p>This option allows other designers to use the article on their websites. <br>
                    The article will still show your name as the publisher along with a link to your profile.</p>
                </div>

                <div class="togglebutton">
                  <label>
                    <input type="checkbox" {{ $article->repost ? 'checked=""': '' }} name="repost">
                  </label>
                </div>
              </div>
              <div class="file-upload">
                <label for="upload" class="file-upload__label">Article Cover Photo</label>
                <input id="upload" class="file-upload__input" type="file" name="image">
                @if($article->image)
                <img src="{{ asset($article->image) }}" style="max-height: 200px; max-width: 200px;">
                @endif
              </div>
            </div>

            <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
              <a href="#" class="btn btn-secondary btn-lg full-width">Cancel</a>
            </div>

            <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
              <button type="submit" class="btn btn-blue btn-lg full-width">Post Article</button>
            </div>
          </div>
        </form>
      </div>
    </div>

@endsection

@section('script')

  <script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>
  <script>
    var $ed;
    ClassicEditor
      .create( document.querySelector( '#body' ) )
      .then( editor => {
         $ed = editor;
      } )
      .catch( error => {
        console.error( error );
      } );


    $('#category').change(function(){
      $this = $(this).find('option:selected');
      $('#subcategory').empty().append(function() {
        var $data = $this.data('children');
        var options = [];

        for(i in $data){
          options.push($('<option>').text($data[i]).val(i))
        }

        return options;
      }).selectpicker('refresh');
    });


    var readerUpload = new FileReader();

    readerUpload.onload = function (e) {
      var $this = $('#upload');

      $this.parent().find('img').remove();
      $this.after($('<img>').css({"max-height": 200, "max-width":200, "display": "none"}));
      $this.parent().find('img').attr('src', e.target.result).after('<br>');
      $this.parent().find('img').show();

    };

    $('#upload').change(function(){
      if (this.files && this.files[0]) {
        readerUpload.readAsDataURL(this.files[0]);
      }
    });
  </script>
@endsection

@section('style')
  <style>
    .ck-rounded-corners .ck.ck-editor__main>.ck-editor__editable, .ck.ck-editor__main>.ck-editor__editable.ck-rounded-corners {
      min-height: 200px;
    }
  </style>
@endsection