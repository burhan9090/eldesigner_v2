@extends('profile.layout.main')

@section('profile_content')

  <div class="ui-block">
    <div class="ui-block-title">
      <h6 class="title">Skills</h6>
    </div>
    <div class="ui-block-content">
      <form id="skills_form">
        @php
          $skillsArr = json_decode($skills->skills ?? '[]', 1);

          if(!$skillsArr){
            $skillsArr = [];
          }

        @endphp

        <div class="row">
          <div class="col col-lg-8 col-md-8 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Skill 1</label>
              <input class="form-control" placeholder="" type="text"
                     name="skill[title][0]" value="{{ $skillsArr['title'][0] ?? '' }}" />
            </div>
          </div>

          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Skill 1 Weight</label>
              <input class="form-control" placeholder="" type="number" min="0" max="100"
                     name="skill[weight][0]" value="{{ $skillsArr['weight'][0]?? 0}}" />
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col col-lg-8 col-md-8 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Skill 2</label>
              <input class="form-control" placeholder="" type="text"
                     name="skill[title][1]" value="{{ $skillsArr['title'][1] ?? ''}}" />
            </div>
          </div>

          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Skill 2 Weight</label>
              <input class="form-control" placeholder="" type="number" min="0" max="100"
                     name="skill[weight][1]" value="{{ $skillsArr['weight'][1]?? 0}}" />
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col col-lg-8 col-md-8 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Skill 3</label>
              <input class="form-control" placeholder="" type="text"
                     name="skill[title][2]" value="{{ $skillsArr['title'][2] ?? ''}}" />
            </div>
          </div>

          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Skill 3 Weight</label>
              <input class="form-control" placeholder="" type="number" min="0" max="100"
                     name="skill[weight][2]" value="{{ $skillsArr['weight'][2]?? ''}}" />
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col col-lg-8 col-md-8 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Skill 4</label>
              <input class="form-control" placeholder="" type="text"
                     name="skill[title][3]" value="{{ $skillsArr['title'][3] ?? ''}}" />
            </div>
          </div>

          <div class="col col-lg-4 col-md-4 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Skill 4 Weight</label>
              <input class="form-control" placeholder="" type="number" min="0" max="100"
                     name="skill[weight][3]" value="{{ $skillsArr['weight'][3]?? 0}}" />
            </div>
          </div>
        </div>





        <div class="row">
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button class="btn btn-secondary btn-lg full-width">Cancel</button>
          </div>
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button id="submit" type="button" class="btn btn-primary btn-lg full-width">Save all Changes</button>
          </div>
        </div>

      </form>
      <!-- ... end Form Hobbies and Interests -->
    </div>
  </div>

  @include('layouts.loader')

@endsection

@section('script')
  <script>
    $('#submit').click(function () {
      var $this = $(this);

      $this.addClass('disabled');
      showLoader();
      $.ajax({
        url: '{{ route('skills.save') }}',
        data: new FormData($('#skills_form')[0]),
        processData: false,
        method: 'POST',
        contentType: false,
        success: function (d) {
          if (d.status) {
            swal("Saved", "", "success");
          }
          else {

            $msg = '';
            for (i in d.message) {
              for (j in d.message[i]) {
                $msg += i + ': ' + d.message[i][j] + "\n";
              }
            }
            swal("Something goes wrong!", $msg, "info");
          }
        }
      }).always(function () {
        $this.removeClass('disabled');
        hideLoader();
      });
    });


    $('.file-upload__input').change(function () {
      var readerUpload = new FileReader();
      var $this = $(this);

      readerUpload.onload = function (e) {
        $this.parent().find('img').remove();
        $this.after($('<img>').css({"max-height": 64, "max-width": 64, "display": "none"}));
        $this.parent().find('img').attr('src', e.target.result).after('<br>');
        $this.parent().find('img').show();
      };

      if (this.files && this.files[0]) {
        readerUpload.readAsDataURL(this.files[0]);
      }
    });
  </script>
@endsection