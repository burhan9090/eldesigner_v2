@extends('profile.layout.main')

@section('profile_content')

  <div class="ui-block">
    <div class="scroll_alert"></div>
    @if($articles_permission != 'unlimited' && $articles_count >= $articles_permission )
      <div class="alert alert-danger" role="alert">
        <button type="button" class="close" data-dismiss="alert">x</button>
        <p>You only can have up to {{$articles_permission}} articles , Please upgrade your account <a href="/pricing/bundles">Here</a> to create more articles</p>
      </div>
    @endif
      <div class="ui-block-title bg-blue">
        <h6 class="title c-white">Publish An Article</h6>
      </div>

      <div class="ui-block-content">

        <form id="add_article">
          {{ csrf_field() }}
          <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="form-group label-floating">
                <input class="form-control" type="text" name="title"
                       placeholder="Title | For example: Modern architecture designs in the MENA region" value="">
              </div>
            </div>

            <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
              <div class="form-group label-floating is-select">
                <label class="control-label">Select Category</label>
                <select class="selectpicker form-control" id="category">
                  @foreach($categories as $category)
                  <option value="{{ $category->id }}" data-children="{{ json_encode($category->children->pluck('text', 'id')) }}">{{ $category->text }}</option>
                  @endforeach
                </select>
              </div>
            </div>

            <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
              <div class="form-group label-floating is-select">
                <label class="control-label">Select Subcategory</label>
                <select class="selectpicker form-control" name="category_id" id="subcategory">
                  @if(count($categories))
                  @foreach(array_first($categories)->children as $category)
                    <option value="{{ $category->id }}">{{ $category->text }}</option>
                  @endforeach
                  @endif
                </select>
              </div>
            </div>

            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="form-group label-floating">
                <label class="control-label">Article body</label>

                <textarea class="form-control" name="body" id="body" style="min-height: 240px; display: none;"></textarea>
              </div>
            </div>

            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

              <div class="form-group">
                <input class="form-control" type="text" name="tags"
                       placeholder="Add tags to enable users to find your article easier. and separate them using a comma.">
              </div>
              <div class="description-toggle">
                <div class="description-toggle-content">
                  <div class="h6">Allow reposting by others</div>
                  <p>This option allows other designers to use the article on their websites. <br>
                    The article will still show your name as the publisher along with a link to your profile.</p>
                </div>

                <div class="togglebutton">
                  <label>
                    <input type="checkbox" checked="" name="repost">
                  </label>
                </div>
              </div>
              <div class="file-upload">
                <label for="upload" class="file-upload__label">Article Cover Photo</label>
                <input id="upload" class="file-upload__input" type="file" name="image">
              </div>
            </div>

            <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
              <a href="#" class="btn btn-secondary btn-lg full-width">Cancel</a>
            </div>

            <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
              @if($articles_permission != 'unlimited' && $articles_count >= $articles_permission )
                <a disabled class="btn btn-blue btn-lg full-width article_disabled">Post Article</a>
              @else
              <a href="javascript:void(0);" onclick="submitForm()" class="btn btn-blue btn-lg full-width">Post Article</a>
              @endif
            </div>
          </div>
        </form>
      </div>
    </div>

@endsection

@section('script')

  <script src="https://cdn.ckeditor.com/ckeditor5/11.2.0/classic/ckeditor.js"></script>
  <script>
    var $ed;
    ClassicEditor
      .create( document.querySelector( '#body' ) )
      .then( editor => {
         $ed = editor;
      } )
      .catch( error => {
        console.error( error );
      } );


    $('#category').change(function(){
      $this = $(this).find('option:selected');
      $('#subcategory').empty().append(function() {
        var $data = $this.data('children');
        var options = [];

        for(i in $data){
          options.push($('<option>').text($data[i]).val(i))
        }

        return options;
      }).selectpicker('refresh');
    });

    function submitForm(){
      $('#body').val($ed.getData());


      $.ajax({
        url: '{{ route('add_article.save') }}',
        data: new FormData($('#add_article')[0]),
        processData: false,
        method: 'POST',
        contentType: false,
        success: function ( d ) {
          if(d.status){
            $form = $('#add_article');
            $form.find('input[name="title"]').val('');
            $form.find('[name="body"]').val('');
            $form.find('input[name="tags"]').val('');
            $form.find('#category').find('option').first().prop('selected', true);
            $form.find('#category').change();
            $form.find('input[name="repost"]').prop('checked', true);
            $form.find('input[name="image"]').val('');

            swal("Article has added", "", "success").then(e=> location.href='{{ route('article.index') }}');
          } else {

            $msg='';
            for(i in d.message){
              for(j in d.message[i]){
                $msg += i+': '+d.message[i][j]+"\n";
              }
            }

            swal("Something goes wrong!", $msg, "info");
          }
        }
      });
    }

    var readerUpload = new FileReader();

    readerUpload.onload = function (e) {
      var $this = $('#upload');

      $this.parent().find('img').remove();
      $this.after($('<img>').css({"max-height": 200, "max-width":200, "display": "none"}));
      $this.parent().find('img').attr('src', e.target.result).after('<br>');
      $this.parent().find('img').show();

    };

    $('#upload').change(function(){
      if (this.files && this.files[0]) {
        readerUpload.readAsDataURL(this.files[0]);
      }
    });
  </script>
@endsection

@section('style')
  <style>
    .ck-rounded-corners .ck.ck-editor__main>.ck-editor__editable, .ck.ck-editor__main>.ck-editor__editable.ck-rounded-corners {
      min-height: 200px;
    }
  </style>
@endsection