@extends('profile.layout.main')

@section('profile_content')


  <div class="ui-block">
    @if(!$has_permission)
      <div class="alert alert-danger" role="alert">
        <p>Generating website is disabled , Please upgrade your account <a href="/pricing/bundles">Here</a> to start generating a new website</p>
      </div>
    @endif

    <!-- Single Post -->

    <article class="hentry blog-post single-post single-post-v3">
      <h6 class="post-title" style="text-align: center; font-size: 25px">Use the "generate" button to build your website in one
        click!</h6>



      <h6 style="text-align: center">Your account status
      </h6>
      <p style="text-align: center">To be able to create an awesome looking website, you need to have all the data
        ready. <br>
        If you added a project, info or any other information to your profile, it will appear here automaticly.</p>

      <div class="ui-block">


        <div class="birthday-item inline-items badges">
          <div class="author-thumb">
            <img src="{{ asset('svg-icons/custom_icons/welcoming-screen.jpeg') }}" alt="elDesigners Welcoming Screen" style="height: 50px">
            <div class="label-avatar bg-primary">2</div>
          </div>
          <div class="birthday-author-name">
            <a href="#" class="h6 author-name">Welcoming screen</a>
            <div class="birthday-date">Welcoming text, and background images</div>
          </div>

          {{--<div class="skills-item">--}}
            {{--<div class="skills-item-meter">--}}
              {{--<span class="skills-item-meter-active" style="width: @php--}}
                {{--$setting = \App\WebsiteWelcome::where('user_id', auth()->id())->first();--}}
                {{--$wlc = 0;--}}
                {{--if($setting){--}}
                {{--$wlc += $setting->background? 25:0;--}}
                {{--$wlc += ($setting->logo||$setting->user_picture)? 25:0;--}}
                {{--$wlc += $setting->main? 25:0;--}}
                {{--$wlc += $setting->description? 25:0;--}}
                {{--}--}}

                {{--echo $wlc;--}}

              {{--@endphp%"></span>--}}
            {{--</div>--}}
          {{--</div>--}}

        </div>

      </div>

      <div class="ui-block">


        <div class="birthday-item inline-items badges">
          <div class="author-thumb">
            <img src="{{ asset('svg-icons/custom_icons/projects-icon.jpeg') }}" alt="elDesigners Projects" style="height: 50px">
          </div>
          <div class="birthday-author-name">
            <a href="#" class="h6 author-name">Projects</a>
            <div class="birthday-date">We recommend you add 4 projects or more.</div>
          </div>

          {{--<div class="skills-item">--}}
            {{--<div class="skills-item-meter">--}}
              {{--<span class="skills-item-meter-active" style="width: @php--}}
                {{--$projects = \App\WebsiteProjectTimeLine::where('user_id', auth()->id())->count();--}}
                {{--$projects = min($projects, 4);--}}
                {{--$projects = $projects*25;--}}

                {{--echo $projects;--}}

              {{--@endphp%"></span>--}}
            {{--</div>--}}
          {{--</div>--}}

        </div>

      </div>

      {{--@if(optional($services = \App\WebsiteService::where('user_id', auth()->id())->first())->show_service)--}}
      {{----}}
      {{--@endif--}}
      <div class="ui-block">
        <div class="birthday-item inline-items badges">
          <div class="author-thumb">
            <img src="{{ asset('svg-icons/custom_icons/services-icon.jpeg') }}" alt="elDesingers Services" style="height: 50px">
            <div class="label-avatar bg-blue">4</div>
          </div>
          <div class="birthday-author-name">
            <a href="#" class="h6 author-name">Services and information</a>
            <div class="birthday-date">The kind of services you offer and information about your work</div>
          </div>

          {{--<div class="skills-item">--}}
          {{--<div class="skills-item-meter">--}}
          {{--<span class="skills-item-meter-active" style="width: @php--}}
          {{--try{--}}
          {{--$servicesjson = json_decode($services->service_text, 1);--}}
          {{--$services=0;--}}
          {{--if($servicesjson['title']){--}}
          {{--foreach ($servicesjson['title'] as $row){--}}
          {{--if($row){--}}
          {{--$services++;--}}
          {{--}--}}
          {{--}--}}
          {{--}--}}
          {{--} catch (Exception $e){--}}
          {{--$services=0;--}}
          {{--}--}}

          {{--$services=$services*25;--}}

          {{--echo $services;--}}
          {{--@endphp%"></span>--}}
          {{--</div>--}}
          {{--</div>--}}
        </div>
      </div>


      <div class="ui-block">


        <div class="birthday-item inline-items badges">
          <div class="author-thumb">
            <img src="{{ asset('svg-icons/custom_icons/contact-icon.jpeg') }}" alt="elDesigners Contact Info" style="height: 50px">
          </div>
          <div class="birthday-author-name">
            <a href="#" class="h6 author-name">Contact details</a>
            <div class="birthday-date">Add your contact info to enable users to contact you using your website</div>
          </div>

          {{--<div class="skills-item">--}}
            {{--<div class="skills-item-meter">--}}
              {{--<span class="skills-item-meter-active" style="width: @php--}}
                {{--$contactUs = \App\WebsiteContactUs ::where('user_id', auth()->id())->first();--}}

                {{--$cnactus = 0;--}}
                {{--if($contactUs){--}}
                  {{--if($contactUs->show_tab){--}}
                    {{--if($contactUs->title){--}}
                      {{--$cnactus += 50;--}}
                    {{--}--}}
                    {{--if($contactUs->message){--}}
                      {{--$cnactus += 50;--}}
                    {{--}--}}
                  {{--}--}}
                  {{--else{--}}
                    {{--$cnactus = 100;--}}
                  {{--}--}}
                {{--}--}}


                {{--echo $cnactus;--}}

              {{--@endphp%"></span>--}}
            {{--</div>--}}
          {{--</div>--}}

        </div>

      </div>

    </article>

    {{--@if($wlc==100&&$projects>=100&&$services==100&&$cnactus==100)--}}
      {{--@if(auth()->user()->theme_name)--}}
        {{--@php--}}
          {{--if(auth()->user()->domain){--}}
            {{--$domain = 'http://'.auth()->user()->domain.route('inner.website.index', auth()->user()->user_name, [], false);--}}
          {{--}--}}
          {{--else{--}}
            {{--$domain = route('inner.website.index', auth()->user()->user_name);--}}
          {{--}--}}
        {{--@endphp--}}
      {{----}}
      {{--@endif--}}
    {{--@endif--}}
    @php
      if(auth()->user()->domain){
        $domain = 'http://'.auth()->user()->domain.route('inner.website.index', auth()->user()->user_name, [], false);
      }
      else{
        $domain = route('inner.website.index', auth()->user()->user_name);
      }
    @endphp
    <div class="crumina-module crumina-heading align-center">
      @if($has_permission)
      <a href="javascript:void(0);" onclick="generateWebsite()" class="btn btn-blue btn-lg">GENERATE WEBSITE</a>
      @else
        <a  disabled class="btn btn-blue btn-lg article_generation">GENERATE WEBSITE</a>
      @endif
      <h2 id="linkg" style="display: none;"><a href="{{ $domain }}" target="_blank">{{ $domain }}</a></h2>
    </div>
  </div>
  @include('layouts.loader2')

@endsection
@section('script')
  <script>
    function generateWebsite(){
        showLoader2();
        setTimeout(function(){
            hideLoader2();
            $('#linkg').show();
            swal('Congratulation' , 'Your website generated successfully' , 'success')
        }, 5000)

    }
  </script>
@endsection