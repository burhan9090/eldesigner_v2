@extends('profile.layout.main')
@section('profile_content')

  @if($current_theme)
    <div class="ui-block features-video">
      <div class="video-player">
        <img src="{{ asset($current_theme->getImage()) }}" alt="photo">
        <div class="overlay"></div>
      </div>

      <div class="features-video-content">
        <article class="hentry post">
          <div class="post__author author vcard inline-items">
            <div class="author-date">{{ $current_theme->name }}</div>
          </div>

          <p>{{ $current_theme->description }}</p>
        </article>
      </div>
    </div>
  @endif

  <div class="ui-block">
    <div class="ui-block-title">
      <div class="h6 title" style="text-align: center">Theme Selections</div>
    </div>
  </div>

  <div class="container">
    <form action="{{ route('themes.save') }}" method="post">
      <div class="row">
        @foreach($themes as $theme)
          {{ csrf_field() }}
          <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
            <div class="ui-block video-item">
              <div class="video-player" style="height: 200px;">
                <img src="{{ asset($theme->getImage()?: 'img/icon-no-image.svg') }}"
                     style="max-height: 200px;width: 100%;" alt="photo">
              </div>

              <div class="ui-block-content video-content">
                <div class="align-center">
                  <a href="javascript:void(0);" class="h6 title">{{ $theme->name }}</a><br>
                  @if($theme->id != optional($current_theme)->id)
                    <button type="button" class="btn btn-primary btn-md-2 select-theme" data-id="{{ $theme->id }}" >Select this theme</button>
                  @endif
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </form>
    @if ($themes->hasPages())
      <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
        <div class="ui-block responsive-flex">
          <div class="ui-block-title" style="padding: 0 25px 0;">
            {{ $themes->render('layouts.paginator') }}
          </div>
        </div>
      </div>
    @endif
  </div>
@endsection

@section('script')
  <script>
    $('.select-theme').click(function () {
      $(this)
        .after($('<input name="id" type="hidden">').val($(this).data('id')))
        .closest('form')
        .submit();
    });

  </script>
@endsection