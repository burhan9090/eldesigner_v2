@extends('profile.layout.main')
@section('profile_content')
  @if($hasDomain)
  <div class="ui-block">
    <div class="ui-block-title">
      <h6 class="title">Account Settings</h6>
    </div>
    <div class="ui-block-content">
      <div class="row">
          <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="description-toggle">
              <table class="table table-responsive table-striped">
                <thead>
                <tr>
                  <th colspan="2">Domain</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td>{{ auth()->user()->domain }}</td>
                  <td><span class="text-white bg-badges p-2 pl-5 pr-5" style="border-radius: 20px;">Booked</span></td>
                </tr>
                </tbody>
              </table>
            </div>
          </div>
      </div>
      <div class="row">
        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
          <small style="color: red;">*** Domain registration may take up to 48 hours to be completed stay tuned, we will notify you when it's done :)</small>
        </div>
      </div>
    </div>
  </div>

  @else
  <form class="{{ !$has_permission?'disabledbutton':'' }}">
    @if(!$has_permission)
      <div class="alert alert-danger" role="alert">
        <p>Buying a unique domain is disabled , Please upgrade your account <a href="/pricing/bundles">Here</a> to making a new unique domains</p>
      </div>
    @endif
    <div class="ui-block">
      <div class="ui-block-title">
        <h6 class="title">Account Settings</h6>
      </div>
      <div class="ui-block-content">
        <div class="row">

          <div class="col col-lg-10 col-md-10 col-sm-10 col-10">
            <div class="form-group label-floating">
              <label class="control-label">Find Domain</label>
              <input class="form-control" placeholder="" type="text" value="{{ request('domain', '') }}" name="domain">
            </div>
          </div>

          <div class="col col-lg-2 col-md-2 col-sm-2 col-2">
            <div class="form-group label-floating">
              <button class="btn btn-purple mt-2">Find</button>
            </div>
          </div>

        </div>

        @if($domains)
          <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="description-toggle">
                <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                  <hr/>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
              <div class="description-toggle">
                <table class="table table-responsive table-striped">
                  <thead>
                  <tr>
                    <th colspan="3">Domain</th>
                  </tr>
                  </thead>
                  <tbody>
                  @if(!in_array(request('domain', ''), $domains))
                    <tr>
                      <td>{{ request('domain', '') }}</td>
                      <td><span class="text-danger">Not Available</span></td>
                      <td></td>
                    </tr>
                    @else
                    <tr>
                      <td>{{ request('domain', '') }}</td>
                      <td><span class="text-success">Available</span></td>
                      <td style="width: 100px;"><a href="{{ route('tools.domain.select', [request('domain')]) }}" class="btn btn-sm btn-purple domainBuy" data-domain="{{ request('domain', '') }}">Select</a></td>
                    </tr>
                  @endif


                  @foreach($domains as $domain)
                    @if($domain!= request('domain', ''))
                      <tr>
                        <td colspan="2">{{ $domain }}</td>
                        <td style="width: 100px;"><a href="{{ route('tools.domain.select', [$domain]) }}" class="btn btn-sm btn-purple domainBuy" data-domain="{{ $domain }}">Select</a></td>
                      </tr>
                    @endif
                  @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        @endif
      </div>
    </div>

    <div class="ui-block">
      <div class="ui-block-content">
        <div class="row">
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button type="reset" class="btn btn-secondary btn-lg full-width">Restore all Attributes</button>
          </div>
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button type="button" id="submit" class="btn btn-primary btn-lg full-width">Save all Changes</button>
          </div>
        </div>
      </div>
    </div>

  </form>
  @endif

@endsection
