@extends('profile.layout.main')
@section('profile_content')

  <form id="toolsForm">
    <div class="ui-block">
      <div class="ui-block-title">
        <h6 class="title">Account Settings</h6>
      </div>
      <div class="ui-block-content">
        <div class="row">

          <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="description-toggle">
              <div class="description-toggle-content">
                <div class="h6">Watermark (Text Mark)</div>
                <p>We'll add text-watermark on your future upload pictures in project section</p>
              </div>

              <div class="togglebutton">
                <label>
                  <input type="checkbox" name="watermark_text_active" value="1" {{ $user->watermark_text_active ? 'checked=""': '' }}>
                </label>
              </div>
            </div>
          </div>

          <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Text Mark</label>
              <input class="form-control" placeholder="" type="text" value="{{ $user->watermark_text }}" name="watermark_text">
            </div>
          </div>

          <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
            <hr />
            @if(!$has_permission)
              <div class="alert alert-danger" role="alert">
                <p>Image watermark is disabled , Please upgrade your account <a href="/pricing/bundles">Here</a> to use watermark (Image Mark)</p>
              </div>
            @endif
          </div>


          <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 @if(!$has_permission)  disabledbutton @endif">
            <div class="description-toggle">
              <div class="description-toggle-content">
                <div class="h6">Watermark (Image Mark)</div>
                <p>We'll add image-watermark on your future upload pictures in project section</p>
              </div>

              <div class="togglebutton">
                <label>
                  <input type="checkbox" name="watermark_file_active" value="1" {{ $user->watermark_file_active ? 'checked=""': '' }}>
                </label>
              </div>
            </div>
          </div>


          <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 @if(!$has_permission)  disabledbutton @endif">
            <div class="file-upload">
              <label for="watermark_file" class="file-upload__label">Upload watermark picture</label>
              <input id="watermark_file" class="file-upload__input" type="file" name="watermark_file">
              @if($user->watermark_file)
                <img style="max-height: 300px; max-width: 300px;" src="{{ asset($user->watermark_file) }}"/>
              @endif
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="ui-block">
      <div class="ui-block-content">
        <div class="row">
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button type="reset" class="btn btn-secondary btn-lg full-width">Restore all Attributes</button>
          </div>
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button type="button" id="submit" class="btn btn-primary btn-lg full-width">Save all Changes</button>
          </div>
        </div>
      </div>
    </div>

  </form>

@endsection

@section('script')
  <script>
    $('#submit').click(function () {

      $(this).addClass('disabled');
      $.ajax({
        url: '{{ route('tools.water_mark.save') }}',
        data: new FormData($('#toolsForm')[0]),
        processData: false,
        method: 'POST',
        contentType: false,
        success: function (d) {
          if (d.status) {
            swal("Settings has Saved", "", "success");
          } else {

            $msg = '';
            for (i in d.message) {
              for (j in d.message[i]) {
                $msg += i + ': ' + d.message[i][j] + "\n";
              }
            }

            swal("Something goes wrong!", $msg, "info");
          }
        },
        error: function () {
          swal("Something goes wrong!", $msg, "info");
        }
      })
        .always(function () {
          $(this).removeClass('disabled');

        });
    });

    show_image=function(e, $this){
      $this.parent().find('img').remove();
      $this.after($('<img>').css({"max-height": 300, "max-width": 300, "display": "none"}));
      $this.parent().find('img').attr('src', e.target.result);
      $this.parent().find('img').show();
    };


    var readerWatermark = new FileReader();

    readerWatermark.onload = function (e) {
      show_image(e, $('#watermark_file'));
    };

    $('#watermark_file').change(function () {
      if (this.files && this.files[0]) {
        readerWatermark.readAsDataURL(this.files[0]);
      }
    });

  </script>
@endsection