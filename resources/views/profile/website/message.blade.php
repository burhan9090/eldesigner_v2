@extends('profile.layout.main')
@section('profile_content')

  <div class="ui-block">
    <div class="ui-block-title">
      <div class="container">
        <div class="row">
          <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 ">
            <h3>{{ $message->title }}</h3>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="ui-block">
    <div class="ui-block-title">
      <div class="container">
        <div class="row">
          <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 pt-4" style="min-height: 300px" >
            <span class="h6">{{ $message->message }}</span>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="ui-block">
    <div class="ui-block-title">
      <div class="container">
        <div class="row">
          <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 text-right">
            <span><a href="{{ URL::previous(route('messages.index')) }}" class="btn btn-primary" >Back Messages</a></span>
          </div>
        </div>
      </div>
    </div>
  </div>

@endsection