@extends('profile.layout.main')
@section('profile_content')

  <form id="settingsForm">
    <div class="ui-block">
      <div class="ui-block-title">
        <h6 class="title">Account Settings</h6>
      </div>
      <div class="ui-block-content">
        <div class="row">

          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <div class="form-group label-floating is-select">
              <label class="control-label">Who can follow you?</label>
              <select class="selectpicker form-control" name="follow_setting">
                <option {{ $user->follow_setting=='public'? 'selected="': '' }} value="public">Public</option>
                <option {{ $user->follow_setting=='private'? 'selected="': '' }} value="private">Private/Requests</option>
                <option {{ $user->follow_setting=='none'? 'selected="': '' }} value="none">None</option>
              </select>
            </div>
          </div>

          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <div class="form-group label-floating is-select">
              <label class="control-label">Profile Setting?</label>
              <select class="selectpicker form-control" name="profile_setting">
                <option {{ $user->profile_setting=='public'? 'selected="': '' }} value="public">Public</option>
                <option {{ $user->profile_setting=='private'? 'selected="': '' }} value="private">Private</option>
              </select>
            </div>
          </div>

          <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="description-toggle">
              <div class="description-toggle-content">
                <div class="h6">Notifications Email</div>
                <p>We’ll send you an email to your account each time you receive a new activity notification</p>
              </div>

              <div class="togglebutton">
                <label>
                  <input type="checkbox" name="email_notification" value="1" {{ $user->email_notification ? 'checked=""': '' }}>
                </label>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>

    <div class="ui-block">
      <div class="ui-block-content">
        <div class="row">
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button type="reset" class="btn btn-secondary btn-lg full-width">Restore all Attributes</button>
          </div>
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button type="button" id="submit" class="btn btn-primary btn-lg full-width">Save all Changes</button>
          </div>
        </div>
      </div>
    </div>

  </form>

@endsection

@section('script')
  <script>
    $('#submit').click(function () {

      $(this).addClass('disabled');
      $.ajax({
        url: '{{ route('setting.main.save') }}',
        data: new FormData($('#settingsForm')[0]),
        processData: false,
        method: 'POST',
        contentType: false,
        success: function (d) {
          if (d.status) {
            swal("Settings has Saved", "", "success");
          } else {

            $msg = '';
            for (i in d.message) {
              for (j in d.message[i]) {
                $msg += i + ': ' + d.message[i][j] + "\n";
              }
            }

            swal("Something goes wrong!", $msg, "info");
          }
        },
        error: function () {
          swal("Something goes wrong!", $msg, "info");
        }
      })
        .always(function () {
          $(this).removeClass('disabled');

        });
    });

    show_image=function(e, $this){
      $this.parent().find('img').remove();
      $this.after($('<img>').css({"max-height": 300, "max-width": 300, "display": "none"}));
      $this.parent().find('img').attr('src', e.target.result);
      $this.parent().find('img').show();
    };

    var readerBackground = new FileReader();
    var readerLogo = new FileReader();
    var readerWatermark = new FileReader();

    readerBackground.onload = function (e) {
      show_image(e, $('#background'));
    };

    readerWatermark.onload = function (e) {
      show_image(e, $('#watermark_file'));
    };

    readerLogo.onload = function (e) {
      show_image(e, $('#logo'));
    };

    $('#background').change(function () {
      if (this.files && this.files[0]) {
        readerBackground.readAsDataURL(this.files[0]);
      }
    });

    $('#watermark_file').change(function () {
      if (this.files && this.files[0]) {
        readerBackground.readAsDataURL(this.files[0]);
      }
    });

    $('#logo').change(function () {
      if (this.files && this.files[0]) {
        readerLogo.readAsDataURL(this.files[0]);
      }
    });
  </script>
@endsection