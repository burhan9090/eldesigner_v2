@extends('profile.layout.main')
@section('profile_content')

  <form id="welcomeForm">
    <div class="ui-block">
      <div class="ui-block-title">
        <h6 class="title">Welcoming screen</h6>
      </div>

      <div class="ui-block-content">
        <div class="file-upload">
          <label for="background" class="file-upload__label">Upload Background</label>
          <input id="background" class="file-upload__input" type="file" name="background">
          @if($websiteWelcome->background)
            <img style="max-height: 200px; max-width: 200px;" src="{{ asset($websiteWelcome->background) }}"/>
          @endif
        </div>
        <br>
        <br>
        @if(auth()->user()->profile_type=='company')
        <div class="file-upload">
          <label for="logo" class="file-upload__label">Upload Website Logo </label>
          <input id="logo" class="file-upload__input" type="file" name="logo">
          @if($websiteWelcome->logo)
            <img style="max-height: 200px; max-width: 200px;" src="{{ asset($websiteWelcome->logo) }}"/>
          @endif
        </div>
        <br>
        <br>
        @else
        <div class="file-upload">
          <label for="user_picture" class="file-upload__label">Upload User Picture</label>
          <input id="user_picture" class="file-upload__input" type="file" name="user_picture">
          @if($websiteWelcome->user_picture )
            <img style="max-height: 200px; max-width: 200px;" src="{{ asset($websiteWelcome->user_picture) }}"/>
          @endif
        </div>
        <br>
        <br>
        @endif

        <div class="row">
          <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Main welcoming text</label>
              <input class="form-control" placeholder="" type="text" value="{{ $websiteWelcome->main }}" name="main">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Write a little description about you</label>
              <textarea class="form-control" placeholder=""
                        name="description">{{ $websiteWelcome->description }}</textarea>
            </div>
          </div>
        </div>
      </div>
    </div>


    <div class="ui-block">
      <div class="ui-block-content">
        <div class="row">
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button type="reset" class="btn btn-secondary btn-lg full-width">Restore all Attributes</button>
          </div>
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button type="button" id="submit" class="btn btn-primary btn-lg full-width">Save all Changes</button>
          </div>
        </div>
      </div>
    </div>

  </form>

@endsection

@section('script')
  <script>
    $('#submit').click(function () {

      $(this).addClass('disabled');
      $.ajax({
        url: '{{ route('welcome.save') }}',
        data: new FormData($('#welcomeForm')[0]),
        processData: false,
        method: 'POST',
        contentType: false,
        success: function (d) {
          if (d.status) {
            swal("Settings has Saved", "", "success");
          } else {

            $msg = '';
            for (i in d.message) {
              for (j in d.message[i]) {
                $msg += i + ': ' + d.message[i][j] + "\n";
              }
            }

            swal("Something goes wrong!", $msg, "info");
          }
        },
        error: function () {
          swal("Something goes wrong!", $msg, "info");
        }
      })
        .always(function () {
          $(this).removeClass('disabled');

        });
    });

    show_image=function(e, $this){
      $this.parent().find('img').remove();
      $this.after($('<img>').css({"max-height": 300, "max-width": 300, "display": "none"}));
      $this.parent().find('img').attr('src', e.target.result);
      $this.parent().find('img').show();
    };

    var readerBackground = new FileReader();
    var readerLogo = new FileReader();
    var readerWatermark = new FileReader();

    readerBackground.onload = function (e) {
      show_image(e, $('#background'));
    };

    readerWatermark.onload = function (e) {
      show_image(e, $('#watermark_file'));
    };

    readerLogo.onload = function (e) {
      show_image(e, $('#logo'));
    };

    $('#background').change(function () {
      if (this.files && this.files[0]) {
        readerBackground.readAsDataURL(this.files[0]);
      }
    });

    $('#watermark_file').change(function () {
      if (this.files && this.files[0]) {
        readerBackground.readAsDataURL(this.files[0]);
      }
    });

    $('#logo').change(function () {
      if (this.files && this.files[0]) {
        readerLogo.readAsDataURL(this.files[0]);
      }
    });
  </script>
@endsection