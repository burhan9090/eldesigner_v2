@extends('profile.layout.main')
@section('profile_content')

  <form id="seoForm">

    <div class="ui-block">
      <div class="ui-block-title">
        <h6 class="title">SEO Settings</h6>
      </div>

      <div class="ui-block-content">

        <div class="row">
          <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Website Title</label>
              <input class="form-control" placeholder="" type="text" value="{{ $websiteWelcome->title }}" name="title">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Meta Keyword</label>
              <input class="form-control"
                     placeholder="Add keyword to enable users to find your article easier. and separate them using a comma."
                     type="text" value="{{ $websiteWelcome->keyword }}" name="keyword">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Meta Description</label>
              <textarea class="form-control" name="meta_description">{{ $websiteWelcome->meta_description }}</textarea>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Facebook Page ID/Admin ID</label>
              <input class="form-control" placeholder="" type="text" value="{{ $websiteWelcome->fb_page_id }}"
                     name="fb_page_id">
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Google Plus Account Link</label>
              <textarea class="form-control" placeholder=""
                        name="gplus">{{ $websiteWelcome->gplus }}</textarea>
            </div>
          </div>
        </div>

      </div>
    </div>


    <div class="ui-block">
      <div class="ui-block-title">
        <h6 class="title">Tracking Script</h6>
      </div>

      <div class="ui-block-content">

        <div class="row">
          <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="form-group label-floating">
              <label class="control-label">Tracking Script</label>
              <textarea class="form-control" name="tracking_script">{{ $websiteWelcome->tracking_script }}</textarea>
            </div>
          </div>
        </div>

      </div>
    </div>


    <div class="ui-block">
      <div class="ui-block-content">
        <div class="row">
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button type="reset" class="btn btn-secondary btn-lg full-width">Restore all Attributes</button>
          </div>
          <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
            <button type="button" id="submit" class="btn btn-primary btn-lg full-width">Save all Changes</button>
          </div>
        </div>
      </div>
    </div>

  </form>

@endsection

@section('script')
  <script>
    $('#submit').click(function () {

      $(this).addClass('disabled');
      $.ajax({
        url: '{{ route('setting.seo.save') }}',
        data: new FormData($('#seoForm')[0]),
        processData: false,
        method: 'POST',
        contentType: false,
        success: function (d) {
          if (d.status) {
            swal("Settings has Saved", "", "success");
          } else {

            $msg = '';
            for (i in d.message) {
              for (j in d.message[i]) {
                $msg += i + ': ' + d.message[i][j] + "\n";
              }
            }

            swal("Something goes wrong!", $msg, "info");
          }
        },
        error: function () {
          swal("Something goes wrong!", $msg, "info");
        }
      })
        .always(function () {
          $(this).removeClass('disabled');

        });
    });

    show_image=function(e, $this){
      $this.parent().find('img').remove();
      $this.after($('<img>').css({"max-height": 300, "max-width": 300, "display": "none"}));
      $this.parent().find('img').attr('src', e.target.result);
      $this.parent().find('img').show();
    };

    var readerBackground = new FileReader();
    var readerLogo = new FileReader();
    var readerWatermark = new FileReader();

    readerBackground.onload = function (e) {
      show_image(e, $('#background'));
    };

    readerWatermark.onload = function (e) {
      show_image(e, $('#watermark_file'));
    };

    readerLogo.onload = function (e) {
      show_image(e, $('#logo'));
    };

    $('#background').change(function () {
      if (this.files && this.files[0]) {
        readerBackground.readAsDataURL(this.files[0]);
      }
    });

    $('#watermark_file').change(function () {
      if (this.files && this.files[0]) {
        readerBackground.readAsDataURL(this.files[0]);
      }
    });

    $('#logo').change(function () {
      if (this.files && this.files[0]) {
        readerLogo.readAsDataURL(this.files[0]);
      }
    });
  </script>
@endsection