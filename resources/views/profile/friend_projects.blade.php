<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 2019-02-03
 * Time: 15:25
 */
?>

@extends('layouts.profile')
@section('content_profile')

    <div class="ui-block responsive-flex ">
        <div class="ui-block-title">
            <div class="h6 title">{{$user_info->name}} project's gallery</div>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <!-- Tab panes -->
                <div class="tab-content">

                    <div class="tab-pane active" id="album-page" role="tabpanel">
                        <div class="photo-album-wrapper">
                            <div class="row">
                                @if (!$projects->isEmpty())


                                @foreach($projects as $project)
                                    <div class="photo-album-item-wrap col-3-width">
                                        <div class="photo-album-item" data-mh="album-item">
                                            <div class="photo-item" style="height: 211px;">
                                                <img style="max-height: 211px;" src="@php
                                                    if(count($project->image)){
                                                      echo e(asset($project->image->image));
                                                    }
                                                    else {
                                                      echo e(asset('img/photo-item2.jpg'));
                                                    }
                                                @endphp" alt="photo">
                                                <div class=""></div>
                                                <a href="javascript:void(0)" class="more edit" data-id="{{ $project->id }}">
                                                    <svg class="olymp-three-dots-icon">
                                                        <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use>
                                                    </svg>
                                                </a>
                                                {{--<a href="#" class="post-add-icon">
                                                  <svg class="olymp-heart-icon">
                                                    <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-heart-icon') }}"></use>
                                                  </svg>
                                                  <span>324</span>
                                                </a>--}}
                                                <a href="#" data-toggle="modal" data-target="#open-photo-popup-v2" class="full-block"></a>
                                            </div>

                                            <div class="content">
                                                <a class="title h5">{{ str_limit($project->title, 50) }}</a>
                                                <span class="sub-title">Last Added:
                        {{ \Carbon\Carbon::parse($project->created_at )->diffForHumans() }}</span>


                                            </div>

                                        </div>
                                    </div>
                                @endforeach
                                    @else
                                    <div class="ui-block-title medium-padding80" style="text-align:center;">
                                        <div class="h6 title">No Project Available</div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                    @if ($projects->hasPages())
                        <div class="ui-block responsive-flex">
                            <div class="ui-block-title" style="padding: 0 25px 0;">
                                {{ $projects->render('layouts.paginator') }}
                            </div>
                        </div>
                    @endif

                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="create-photo-album" tabindex="-1" role="dialog" aria-labelledby="create-photo-album"
         aria-hidden="true">
        <div class="modal-dialog window-popup create-photo-album" role="document">
            <div class="modal-content">
                <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
                    <svg class="olymp-close-icon">
                        <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use>
                    </svg>
                </a>

                <div class="modal-header">
                    <h6 class="title">Add new project</h6>
                </div>

                <div class="modal-body">
                    <form id="add-project">
                        <div class="form-group">
                            <label class="control-label">Project title</label>
                            <input class="form-control" placeholder="Modern Living Room Design" type="text"
                                   name="title">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Description</label>
                            <input class="form-control" placeholder="General info about the project" type="text"
                                   name="description">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tags</label>
                            <input class="form-control"
                                   placeholder="Add tags such as: living room, home, modern and separate them using a comma."
                                   type="text" name="tags">
                        </div>
                        <div class="photo-album-wrapper images">
                            <div class="photo-album-item-wrap col-3-width">
                                <div class="photo-album-item create-album" data-mh="album-item">
                                    <div class="content">
                                        <a href="#" class="btn btn-control bg-primary add-image">
                                            <svg class="olymp-plus-icon">
                                                <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-plus-icon') }}"></use>
                                            </svg>
                                        </a>

                                        <a href="#" class="title h5 add-image">Add images...</a>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <button class="btn btn-secondary btn-lg btn--half-width" data-dismiss="modal"
                                aria-label="Close">Discard Everything
                        </button>
                        <a href="#" id="submit-project" class="btn btn-primary btn-lg btn--half-width">Post Project</a>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="edit-photo-album" tabindex="-1" role="dialog" aria-labelledby="edit-photo-album"
         aria-hidden="true">
        <div class="modal-dialog window-popup create-photo-album" role="document">
            <div class="modal-content">
                <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
                    <svg class="olymp-close-icon">
                        <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use>
                    </svg>
                </a>

                <div class="modal-header">
                    <h6 class="title">Edit project</h6>
                </div>

                <div class="modal-body">
                    <form id="edit-project" action="{{ route('projects.update') }}" method="post" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <input name="id" type="hidden" />
                        <div class="form-group">
                            <label class="control-label">Project title</label>
                            <input class="form-control" placeholder="Modern Living Room Design" type="text" name="title">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Description</label>
                            <input class="form-control" placeholder="General info about the project" type="text" name="description">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Tags</label>
                            <input class="form-control" placeholder="Add tags such as: living room, home, modern and separate them using a comma." type="text" name="tags">
                        </div>
                        <div class="photo-album-wrapper images">
                            <div class="photo-album-item-wrap col-3-width">
                                <div class="photo-album-item create-album" data-mh="album-item">
                                    <div class="content">
                                        <a href="#" class="btn btn-control bg-primary add-image">
                                            <svg class="olymp-plus-icon">
                                                <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-plus-icon') }}"></use>
                                            </svg>
                                        </a>

                                        <a href="#" class="title h5 add-image">Add images...</a>
                                    </div>
                                </div>
                            </div>

                        </div>

                        <button class="btn btn-secondary btn-lg btn--half-width" data-dismiss="modal" aria-label="Close">Discard Everything
                        </button>
                        <button type="submit" class="btn btn-primary btn-lg btn--half-width">Post Project</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="loading" tabindex="-1" role="dialog" aria-labelledby="loading" aria-hidden="true">
        <div class="modal-dialog window-popup create-photo-album" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="title">Loading</h6>
                </div>

                <div class="modal-body">
                    <div class="form-group text-center">
                        <img src="{{ asset('img/spinner.gif') }}" />
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
