<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 12/1/18
 * Time: 7:37 PM
 */
?>

@extends('layouts.application')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="ui-block-title ">
                        <p class="h6 title center_text">This page isn't available</p>
                    </div>
                    <div class="ui-block-title ">
                        <p class="h6 title center_text">The link you followed may be broken, or the page may have been removed.</p>
                    </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection
