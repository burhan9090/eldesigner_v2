<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 2019-02-11
 * Time: 23:09
 */
?>
@extends('layouts.profile')

@section('content_profile')
    <div id="friend_requests">
        <follow-request :follow_request_data="follow_request_data" ></follow-request>
    </div>
@endsection
