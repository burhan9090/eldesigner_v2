@extends('layouts.profile')

@section('content_profile')
    <div class="infinite-scroll">
    <div class="row" id="all_notification">
        <div class="col col-xl-12 order-xl-1 col-lg-12 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">

            <ul class="notification-list">
                @if(!empty($notification))
                @foreach ($notification as $user_notification)
                    <li  class="@if($user_notification->is_seen == false) un-read @endif open_notification" data-id="{{$user_notification->id}}" data-type="{{$user_notification->notification_type}}" data-user_name="{{$user_notification->user['user_name']}}">
                        <div class="author-thumb header_logo">
                            @if ($user_notification->user['logo'] != null &&  $user_notification->user['logo'] != '')
                                <img src="{{asset($user_notification->user['logo'])}}" alt="author">
                                @else
                                <img v-else src="{{asset('uploads/user/dummy_logo.png')}}" alt="author">
                            @endif
                        </div>
                        <div class="notification-event">
                            <div><a href="/profile/{{$user_notification->user['user_name']}}" class="h6 notification-friend">{{$user_notification->user['name']}}</a> {{$user_notification->notification_text}} .</div>
                            <span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">{{$user_notification->created_at}}</time></span>
                        </div>
                        <span class="notification-icon">
                            <svg class="olymp-comments-post-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-comments-post-icon"></use></svg>
                        </span>
                        <div class="more">
                            <svg class="olymp-three-dots-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
                            <svg class="olymp-little-delete"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-little-delete"></use></svg>
                        </div>
                    </li>
                        {{$notification->links()}}
                @endforeach

                @else
                    <li style="text-align:center;" >
                        <div >
                            No New Notification
                        </div>
                    </li>
                @endif
            </ul>

        </div>

    </div>
    </div>

@endsection
