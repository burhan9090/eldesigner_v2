<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 2019-02-03
 * Time: 15:40
 */
?>
@extends('profile.not_friend_profile_head')
@section('content_not_friend')
        <div class="container">
            <div class="row">

                <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="ui-block responsive-flex">
                        <div class="ui-block-title">
                            <div class="h6 title">{{$oUserInfo->name}} Articles</div>
                        </div>
                    </div>
                </div>
                @if (!$articles->isEmpty())
                @foreach($articles as $article)
                    <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                        <div class="ui-block">
                            <article class="hentry blog-post">

                                @if($article->image)
                                    <div class="post-thumb" style="height: 250px; text-align: center;">
                                        <a href="{{ route("article.show", $article->id) }}">
                                        <img style="max-height: 250px; width: auto;" src="{{ asset($article->image) }}" alt="photo">
                                        </a>
                                    </div>
                                @endif

                                <div class="post-content">
                                    {{--<a href="#" class="post-category bg-blue-light">YOUR ARTICLES</a>--}}
                                    <a href="{{ route("article.show", $article->id) }}" class="h4 post-title">{{ $article->title }}</a>
                                    <p>{{ str_limit(strip_tags($article->body))  }}</p>

                                    <div class="author-date">
                                        by
                                        <a class="h6 post__author-name fn" href="#">{{ $article->user? $article->user->name : ''}}</a>
                                        <div class="post__date">
                                            <time class="published" datetime="{{ $article->created_at }}">
                                                {{ \Carbon\Carbon::parse($article->created_at )->diffForHumans() }}
                                            </time>
                                        </div>
                                    </div>
                                </div>


                            </article>
                        </div>
                    </div>
                @endforeach

                @if ($articles->hasPages())
                    <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="ui-block responsive-flex">
                            <div class="ui-block-title" style="padding: 0 25px 0;">
                                {{ $articles->render('layouts.paginator') }}
                            </div>
                        </div>
                    </div>
                @endif
                @else
                    <div class="ui-block-title medium-padding80" style="text-align:center;">
                        <div class="h6 title">No Articles Available</div>
                    </div>
                @endif
            </div>
        </div>
@endsection

