@extends('profile.layout.main')
@section('profile_content')
    <style>
        .pricing-cover{
            background-image: url(../website/Pricing-Cover.svg);
            width: 100%;
            background-repeat: no-repeat;
            /* background-attachment: fixed; */
            background-position: unset;
            background-size: cover;
        }

        .bg-primary-opacity {
            background-color: rgba(251, 251, 251, 0);
        }
    </style>
    @if ($message = Session::get('error'))
    @endif
    @if($user_permission)
    <div class="col col-xl-12 order-xl-12 col-lg-12 order-lg-12 col-md-12 col-sm-12 col-12 sorting-item personal">
        <div class="ui-block">
            <div class="ui-block-title">
                <h3 class="heading-title"
                    style="color: #29aed6;font-weight: 500; text-align: center">You'r Subscription Info</h3>
            </div>
            <div class="ui-block-content">
                <ul class="" >
                    <li ><span
                                style="color: #303445;  text-align: center; font-size: 17px" > Plan Name : <b style="color: #29aed6;">{{ucfirst(strtolower($user_permission->plan['plans_name']))}}</b> </span></li>
                    <li ><span
                                style="color: #303445;  text-align: center; font-size: 17px" > Expiration Date : <b style="color: #29aed6;">{{$expiration_datetime}}</b> </span></li>
                </ul>
            </div>
        </div>
    </div>
    @endif
    <div class="stunning-header bg-primary-opacity">

        <div class="header-spacer--standard"></div>

        <div class="stunning-header-content">
            <h1 class="stunning-header-title" style="color: #29aed6;font-weight: 500;">Plans & Pricing</h1>
        </div>

        <div class="content-bg-wrap pricing-cover"></div>
    </div>
    <style>

        .cat-list__item a:hover {
            background-color: #1bb5cc !important
        }

        .cat-list__item.active {
            background-color: #1de0fd !important
        }
    </style>
    <div class="container">
        <div class="row">

            <div class="col col-lg-12 col-md-12 col-sm-12 col-12">
                <div class="clients-grid">

                    <div class="row sorting-container" id="clients-grid-1" data-layout="masonry">
                        @if(auth()->user()->profile_type=='freelancer')
                        {{--personal--}}
                        <div class="col col-xl-4 order-xl-4 col-lg-6 order-lg-4 col-md-6 col-sm-12 col-12 sorting-item personal">

                            <div class="ui-block">
                                <div class="ui-block-title">
                                    <h3 class="heading-title"
                                        style="color: #303445; font-weight: 500; text-align: center">FREE</h3>
                                </div>
                                <div class="ui-block-content">

                                    <!-- W-Personal-Info -->

                                    <ul class="widget w-personal-info item-block" style="margin-left: 15%">
                                        <li style="padding-bottom: 5px"><h6 class="align-center" style="color: #303445">
                                                Use all the features, launch a website, build connections and share your
                                                work.></h6>

                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Access to all the features</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Upload Up to 15 projects</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Publish Up to 15 articles</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Unlimited website visits</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Free website themes only (personal)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"></i><i class="fa fa-check-circle"
                                                                               style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Baisc Statistics (Website)</span></li>
                                        <li style="padding-bottom: 3px"></i><span class="heading-title"> Multi-language RTL website (coming soon)</span>
                                        </li>

                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Image Protection (text only)</span></li>
                                        <li style="padding-bottom: 3px"><span
                                                    class="heading-title"> Unqiue domain name</span></li>
                                        <li style="padding-bottom: 3px"><span class="heading-title"> 2 Pro-Email: name@youname.com</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><span
                                                    class="heading-title"> Secure website SSL</span></li>
                                        <li style="padding-bottom: 3px"><span class="heading-title"> Web hosting</span>
                                        </li>

                                        <li style="padding-bottom: 3px"><span class="heading-title"> Design Request (Recieve orders online)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><span class="heading-title"> Image Tagging (coming soon)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><span class="heading-title"> ECommerce (Coming Soon)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Chat Support</span></li>


                                    </ul>

                                    <!-- .. end W-Personal-Info -->
                                    <!-- W-Socials -->
                                    <div class="align-center">
                                        <a href="{{ route('home') }}" class="btn btn-blue btn-lg"
                                           style="margin-top: 30px; background-color: #303445">FOREVER FREE</a>
                                    </div>


                                    <!-- ... end W-Socials -->
                                </div>
                            </div>

                        </div>
                        {{--personal--}}
                        <div class="col col-xl-4 order-xl-4 col-lg-6 order-lg-4 col-md-6 col-sm-12 col-12 sorting-item personal ">


                            <div class="ui-block">
                                <div class="ui-block-title">
                                    <h3 class="heading-title"
                                        style="color: #08ddc1;font-weight: 500; text-align: center">START</h3>
                                </div>
                                @if($user_permission->plan['plans_name'] == 'START')
                                <div class="already_subscribed">

                                </div>
                                @endif
                                <div class="ui-block-content @if($user_permission->plan['plans_name'] == 'START')  disabledbutton  @endif">

                                    <!-- W-Personal-Info -->

                                    <ul class="widget w-personal-info item-block" style="margin-left: 15%">
                                        <li style="padding-bottom: 5px"><h6 class="align-center" style="color: #303445">
                                                Boost your portfolio, recieve orders online and get an arabic version of
                                                your website.</span></h6>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Access to all the features</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Upload Up to 40 projects</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Publish Up to 20 articles</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Unlimited website visits</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Basic website themes</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Advanced Statistics (Website + Portfolio)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #b4b4b4"></i><span
                                                    class="heading-title"> Multi-language RTL website (coming soon)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Image Protection</span></li>
                                        <li style="padding-bottom: 3px"><span
                                                    class="heading-title"> Unqiue domain name</span></li>
                                        <li style="padding-bottom: 3px"><span class="heading-title"> 2 Pro-Emails: name@youname.com</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><span
                                                    class="heading-title"> Secure website SSL</span></li>
                                        <li style="padding-bottom: 3px"><span class="heading-title"> Web hosting</span>
                                        </li>


                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Design Request (Recieve orders online)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #b4b4b4"></i><span
                                                    class="heading-title"> Image Tagging (coming soon)</span></li>
                                        <li style="padding-bottom: 3px"><span class="heading-title"> ECommerce (Coming Soon)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Chat Support</span></li>


                                    </ul>

                                    <!-- .. end W-Personal-Info -->
                                    <!-- W-Socials -->

                                    <div class="align-center">
                                        <a href="/pricing/bundles/start" class="btn btn-blue btn-lg"
                                           style="margin-top: 30px;background-color: #08ddc1">7 USD/Month</a>
                                    </div>

                                    <!-- ... end W-Socials -->
                                </div>
                            </div>

                        </div>\
                        {{--personal--}}
                        <div class="col col-xl-4 order-xl-4 col-lg-6 order-lg-4 col-md-6 col-sm-12 col-12 sorting-item personal">


                            <div class="ui-block">
                                <div class="ui-block-title">
                                    <h3 class="heading-title"
                                        style="color: #48a5e8;font-weight: 500; text-align: center">GROW</h3>
                                </div>

                                @if($user_permission->plan['plans_name'] == 'GROW')
                                    <div class="already_subscribed">

                                    </div>
                                @endif
                                <div class="ui-block-content @if($user_permission->plan['plans_name'] == 'GROW')  disabledbutton  @endif">

                                    <!-- W-Personal-Info -->

                                    <ul class="widget w-personal-info item-block" style="margin-left: 15%">
                                        <li style="padding-bottom: 5px"><h6 class="align-center" style="color: #303445">
                                                Get a domain and email service, and empower it with unlimited tools to
                                                scale your work</span></h6>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Access to all the features</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Unlimited projects</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Unlimited articles</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Unlimited website visits</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> All website themes</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Advanced Statistics (Website + Portfolio)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #b4b4b4"></i><span
                                                    class="heading-title"> Multi-language RTL website (coming soon)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Image Protection (text only)</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Unique domain (yourname.com)</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> 2 Pro-Email: name@youname.com</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Secure website SSL</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Web hosting</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Design Request (Recieve orders online)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #b4b4b4"></i><span
                                                    class="heading-title"> Image Tagging (coming soon)</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #b4b4b4"></i><span
                                                    class="heading-title"> ECommerce (Coming Soon)</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Chat + phone Support</span></li>

                                    </ul>

                                    <!-- .. end W-Personal-Info -->
                                    <!-- W-Socials -->


                                    <div class="align-center">
                                        <a  href="/pricing/bundles/grow" class="btn btn-primary btn-lg"
                                           style="margin-top: 30px; background-color: #48a5e8; border: none">12
                                            USD/Month</a>
                                    </div>
                                    <!-- ... end W-Socials -->
                                </div>

                            </div>

                        </div>
                        @else
                        {{--business--}}
                        <div class="col col-xl-4 order-xl-4 col-lg-6 order-lg-4 col-md-6 col-sm-12 col-12 sorting-item personal">


                            <div class="ui-block">
                                <div class="ui-block-title">
                                    <h3 class="heading-title"
                                        style="color: #303445; font-weight: 500; text-align: center">FREE</h3>
                                </div>
                                <div class="ui-block-content">

                                    <!-- W-Personal-Info -->

                                    <ul class="widget w-personal-info item-block" style="margin-left: 15%">
                                        <li style="padding-bottom: 5px"><h6 class="align-center" style="color: #303445">
                                                Use all the features, launch a website, build connections and share your
                                                work.</span></h6>

                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Access to all the features</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Upload Up to 20 projects</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Publish Up to 20 articles</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Unlimited website visits</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Free website themes only (business)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"></i><i class="fa fa-check-circle"
                                                                               style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Baisc Statistics (Website)</span></li>
                                        <li style="padding-bottom: 3px"></i><span class="heading-title"> Multi-language RTL website (coming soon)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Image Protection (text only)</span></li>
                                        <li style="padding-bottom: 3px"><span class="heading-title"> Unique domain (yourname.com)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><span
                                                    class="heading-title"> Secure website SSL</span></li>
                                        <li style="padding-bottom: 3px"><span class="heading-title"> 5 Pro-Email: name@youname.com</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><span class="heading-title"> Web hosting</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><span class="heading-title"> Design Request (Recieve orders online)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><span class="heading-title"> Image Tagging (coming soon)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><span class="heading-title"> ECommerce (Coming Soon)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> 1 user (Admin)</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Chat Support</span></li>


                                    </ul>

                                    <!-- .. end W-Personal-Info -->
                                    <!-- W-Socials -->
                                    <div class="align-center">
                                        <a href="{{ route('home') }}" class="btn btn-blue btn-lg"
                                           style="margin-top: 30px; background-color: #303445">FOREVER FREE</a>
                                    </div>


                                    <!-- ... end W-Socials -->
                                </div>
                            </div>

                        </div>
                        {{--business--}}
                        <div class="col col-xl-4 order-xl-4 col-lg-6 order-lg-4 col-md-6 col-sm-12 col-12 sorting-item personal">


                            <div class="ui-block">
                                <div class="ui-block-title">
                                    <h3 class="heading-title"
                                        style="color: #48a5e8;font-weight: 500; text-align: center">PREMIUM</h3>
                                </div>

                                @if($user_permission->plan['plans_name'] == 'PREMIUM')
                                    <div class="already_subscribed">

                                    </div>
                                @endif
                                <div class="ui-block-content @if($user_permission->plan['plans_name'] == 'PREMIUM')  disabledbutton  @endif">

                                    <!-- W-Personal-Info -->

                                    <ul class="widget w-personal-info item-block" style="margin-left: 15%">
                                        <li style="padding-bottom: 5px"><h6 class="align-center" style="color: #303445">
                                                Get a domain and email service, and empower it with unlimited tools to
                                                scale your work</span></h6>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Access to all the features</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Unlimited projects</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Unlimited articles</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Unlimited website visits</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> All website themes (business)</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Advanced Statistics (Website + Portfolio)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #b4b4b4"></i><span
                                                    class="heading-title"> Multi-language RTL website (coming soon)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Image Protection (text or logo)</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Unique domain (yourname.com)</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Secure website SSL</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> 5 Pro-Email: name@youname.com</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Web hosting</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Design Request (Recieve orders online)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #b4b4b4"></i><span
                                                    class="heading-title"> Image Tagging (coming soon)</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #b4b4b4"></i><span
                                                    class="heading-title"> ECommerce (Coming Soon)</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> 3 user (1 Admin + 2 contributors)</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Chat + phone Support</span></li>

                                    </ul>

                                    <!-- .. end W-Personal-Info -->
                                    <!-- W-Socials -->


                                    <div class="align-center">
                                        <a  href="/pricing/bundles/premuim" class="btn btn-primary btn-lg"
                                           style="margin-top: 30px; background-color: #48a5e8; border: none">25
                                            USD/Month</a>
                                    </div>
                                    <!-- ... end W-Socials -->
                                </div>

                            </div>

                        </div>
                        {{--business--}}
                        <div class="col col-xl-4 order-xl-4 col-lg-6 order-lg-4 col-md-6 col-sm-12 col-12 sorting-item personal">


                            <div class="ui-block">
                                <div class="ui-block-title">
                                    <h3 class="heading-title"
                                        style="color: #08ddc1;font-weight: 500; text-align: center">ENTREPRISE</h3>
                                </div>

                                @if($user_permission->plan['plans_name'] == 'ENTREPRISE')
                                    <div class="already_subscribed">

                                    </div>
                                @endif
                                <div class="ui-block-content @if($user_permission->plan['plans_name'] == 'ENTREPRISE')  disabledbutton  @endif">

                                    <!-- W-Personal-Info -->

                                    <ul class="widget w-personal-info item-block" style="margin-left: 15%">
                                        <li style="padding-bottom: 5px"><h6 class="align-center" style="color: #303445">
                                                Get a domain and email service, and empower it with unlimited tools to
                                                scale your work</span></h6>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Access to all the features</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> All the features in premium plan</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Mail integerations</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Web tools integerations</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Customised website themes (business)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Real time Advanced Statistics</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Multi-language unlimited (coming soon)</span>
                                        </li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Sub-domain and SEO enhancment</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> +Unique domain (yourname.com)</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Secure website SSL</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> + 5 Pro-Email: name@youname.com</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Extended Web hosting</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Custom Design Request </span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Social connect</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Web optimisation</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> +3 Users</span></li>
                                        <li style="padding-bottom: 3px"><i class="fa fa-check-circle"
                                                                           style="font-size:13px; color: #38a9ff"></i><span
                                                    class="heading-title"> Extended support</span></li>

                                    </ul>

                                    <!-- .. end W-Personal-Info -->
                                    <!-- W-Socials -->


                                    <div class="align-center">
                                        <a href="{{ route('home') }}" class="btn btn-primary btn-lg"
                                           style="margin-top: 30px; background-color: #08ddc1; border: none">Contact
                                            Sales</a>
                                    </div>
                                    <!-- ... end W-Socials -->
                                </div>

                            </div>

                        </div>
                        @endif

                    </div>

                </div>
            </div>
        </div>
    </div>






    <section class="pt40" style="margin-top: 80px; background-color: ">
        <div class="container">


            <div class="row mb30">
                <div class="col col-xl-5 col-lg-5 m-auto col-md-12 col-sm-12 col-12">
                    <div class="crumina-module crumina-heading align-center">
                        <div class="heading-sup-title" style="margin-bottom: 10px; color: #515365">Get Instant Free
                            Access To All Features
                        </div>
                        <h2 class="heading-title" style="margin-bottom: 0px; font-weight: 500; color: #48a5e8">All The
                            Features On One Platfrom</h2>
                        <p class="heading-text" style="margin-top: 5px">All the features are synchronized automaticlly
                            in real time.</p>

                    </div>
                </div>
            </div>


            <div class="info-box-wrap">
                <div class="row">
                    <div class="col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <!-- Info Box  -->
                        <div class="crumina-module crumina-info-box" data-mh="box--classic">
                            <div class="info-box-image" style="margin-bottom: 0px">
                                <img src="{{ asset('website/Portfolio.svg') }}" alt="icon" style="width: 130px">
                            </div>
                            <div class="info-box-content">
                                <h3 class="info-box-title" style="margin-bottom: 20px">Profile Page</h3>
                                <p class="info-box-text">The profile page is a dedicated online portfolio for designers.
                                    Where work, project and professional experiences can be shared.</p>
                            </div>
                        </div>
                        <!-- ... end Info Box  -->
                    </div>
                    <div class="col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">
                        <!-- Info Box  -->
                        <div class="crumina-module crumina-info-box" data-mh="box--classic">
                            <div class="info-box-image" style="margin-bottom: 0px">
                                <img src="{{ asset('website/Website-Generator.svg') }}" alt="icon" style="width: 130px">
                            </div>
                            <div class="info-box-content">
                                <h3 class="info-box-title" style="margin-bottom: 20px">Website Generater</h3>
                                <p class="info-box-text">Using the magic button "Generate Website", designer can launch
                                    unique websites without any hassles or complicatations.</p>
                            </div>
                        </div>

                        <!-- ... end Info Box  -->

                    </div>
                    <div class="col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">


                        <!-- Info Box  -->

                        <div class="crumina-module crumina-info-box" data-mh="box--classic">
                            <div class="info-box-image" style="margin-bottom: 0px">
                                <img src="{{ asset('website/Social-Network.svg') }}" alt="icon" style="width: 130px">
                            </div>
                            <div class="info-box-content">
                                <h3 class="info-box-title" style="margin-bottom: 20px">Social Network</h3>
                                <p class="info-box-text">A community between designers where designers can share,
                                    interact and develop a fan base amongst the community.</p>
                            </div>
                        </div>

                        <!-- ... end Info Box  -->

                    </div>
                    <div class="col col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12">


                        <!-- Info Box  -->

                        <div class="crumina-module crumina-info-box" data-mh="box--classic">
                            <div class="info-box-image" style="margin-bottom: 0px">
                                <img src="{{ asset('website/Crowd-sourced-content.svg') }}" alt="icon" style="width: 130px">
                            </div>
                            <div class="info-box-content">
                                <h3 class="info-box-title" style="margin-bottom: 20px">Crowdsourced Content</h3>
                                <p class="info-box-text">Projects & Articles uploaded on the platform will be shared on
                                    an attributed plublic libraries available for clients and visitors.</p>
                            </div>
                        </div>

                        <!-- ... end Info Box  -->
                    </div>
                </div>
            </div>


            <div class="row mb30">
                <div class="col col-xl-5 col-lg-5 m-auto col-md-12 col-sm-12 col-12">
                    <div class="crumina-module crumina-heading align-center">
                        <div class="heading-sup-title" style="margin-bottom: 10px; color: #303445">Looking For More?
                        </div>
                        <h2 class="heading-title" style="margin-bottom: 0px; font-weight: 500;color: #48e5c7">Tools
                            Built For Designers</h2>
                        <p class="heading-text" style="margin-top: 5px">Empower your work with tools to scale, exceed
                            and grow.</p>

                    </div>
                </div>
            </div>


            <div class="info-box-wrap">
                <div class="row">
                    <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                        <!-- Info Box  -->
                        <div class="crumina-module crumina-info-box" data-mh="box--classic">
                            <div class="info-box-image" style="margin-bottom: 0px">
                                <img src="{{ asset('website/Image-Protection.svg') }}" alt="icon" style="width: 130px">
                            </div>
                            <div class="info-box-content">
                                <h3 class="info-box-title" style="margin-bottom: 20px">Image Protection</h3>
                                <p class="info-box-text">An HTML watermark to cover uploaded projects while keeping
                                    images untouched as well as disabling the save as button.</p>
                            </div>
                        </div>
                        <!-- ... end Info Box  -->
                    </div>
                    <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                        <!-- Info Box  -->
                        <div class="crumina-module crumina-info-box" data-mh="box--classic">
                            <div class="info-box-image" style="margin-bottom: 0px">
                                <img src="{{ asset('website/Unique-domain.svg') }}" alt="icon" style="width: 130px">
                            </div>
                            <div class="info-box-content">
                                <h3 class="info-box-title" style="margin-bottom: 20px">Unique Domain</h3>
                                <p class="info-box-text">For generated websites, designers can set and publish a unique
                                    domain name such as www.NameOfDesigner.com</p>
                            </div>
                        </div>
                        <!-- ... end Info Box  -->
                    </div>
                    <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                        <!-- Info Box  -->
                        <div class="crumina-module crumina-info-box" data-mh="box--classic">
                            <div class="info-box-image" style="margin-bottom: 0px">
                                <img src="{{ asset('website/Pro-Mail.svg') }}" alt="icon" style="width: 130px">
                            </div>

                            <div class="info-box-content">
                                <h3 class="info-box-title" style="margin-bottom: 20px">Professional Email</h3>
                                <p class="info-box-text">This tools allows designers to create emails using their unique
                                    domain name such as Name@NameOfDesigner.com</p>
                            </div>
                        </div>

                        <!-- ... end Info Box  -->

                    </div>
                    <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">


                        <!-- Info Box  -->

                        <div class="crumina-module crumina-info-box" data-mh="box--classic">
                            <div class="info-box-image" style="margin-bottom: 0px">
                                <img src="{{ asset('website/DRSI-Icon.svg') }}" alt="icon" style="width: 130px">
                            </div>
                            <div class="info-box-content">
                                <h3 class="info-box-title" style="margin-bottom: 20px">Design Request (DRVSI)</h3>
                                <p class="info-box-text">Fully customised design request process allowing
                                    website/profile visitors to submit design inquiries online.</p>
                            </div>
                        </div>

                        <!-- ... end Info Box  -->

                    </div>
                    <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">


                        <!-- Info Box  -->

                        <div class="crumina-module crumina-info-box" data-mh="box--classic">
                            <div class="info-box-image" style="margin-bottom: 0px">
                                <img src="{{ asset('website/Image-Tagging.svg') }}" alt="icon" style="width: 130px">
                            </div>
                            <div class="info-box-content">
                                <h3 class="info-box-title" style="margin-bottom: 20px">Image Tagging</h3>
                                <p class="info-box-text">Image tagging allows client to tag their images with their
                                    favorite features in order to deliver ideas smarter and faster.</p>
                            </div>
                        </div>

                        <!-- ... end Info Box  -->

                    </div>
                    <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">


                        <!-- Info Box  -->

                        <div class="crumina-module crumina-info-box" data-mh="box--classic">
                            <div class="info-box-image" style="margin-bottom: 0px">
                                <img src="{{ asset('website/E-Commerce.svg') }}" alt="icon" style="width: 130px">
                            </div>
                            <div class="info-box-content">
                                <h3 class="info-box-title" style="margin-bottom: 20px">ECommerce</h3>
                                <p class="info-box-text">The ECommerce tool is all in one built tool to give the
                                    designers the ability to sell products online seamlessly. (Comming Soon)</p>
                            </div>
                        </div>

                        <!-- ... end Info Box  -->

                    </div>
                </div>
            </div>

        </div>
    </section>



@endsection
@section('script')
    <script>
        $('[data-filter=".business"]').click(function (){

            $('.business').fadeIn(10)
        })
    </script>
@endsection