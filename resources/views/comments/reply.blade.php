<li class="comment-item">
    <div class="post__author author vcard">
        @if($user->logo)
            <img alt="author" src="{{asset( $user->logo)}}" class="avatar">
        @else
            <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
        @endif

        <div class="author-date">
            <p>
                <a class="h6 post__author-name fn" href="/profile/{{$user->user_name}}">{{ $user->name }}</a>
                {{ $reply->reply_text }}
            </p>
            @if($reply->image_path)
                <div style="padding: 15px">
                    <img style="max-height: 250px" src="{{ asset('user_media_replies/' . $reply->image_path) }}">
                </div>
            @endif
            <a href="#" class="post-add-icon inline-items love-reply" data-reply_id = "{{ $reply->id }}">
                <svg class="olymp-heart-icon">
                    <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-heart-icon') }}"></use>
                </svg>
                <span class ="num_of_loves_reply-{{ $reply->id }}">0</span>
            </a>
            <div class="post__date">
                <time class="published" datetime="Just now">
                    Just now
                </time>
            </div>

            @if(\Illuminate\Support\Facades\Auth::user()->id == $reply->user_id)
                <a href="#" class="more">
                    <svg class="olymp-three-dots-icon">
                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                    </svg>
                </a>
            @endif
        </div>


    </div>



</li>