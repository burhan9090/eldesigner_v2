<li class="comment-item" >
    <div class="post__author author vcard">
        @if($user->logo)
            <img alt="author" src="{{asset( $user->logo)}}" class="avatar">
        @else
            <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
        @endif

        <div class="author-date">
            <p>
                <a class="h6 post__author-name fn" href="/profile/{{$user->user_name}}">{{ $user->name }}</a>
            {{ $comment->comment_text }}
            @if($comment->image_path)
                <div style="padding: 15px">
                    <img style="max-height: 250px"
                         src="{{ asset('user_media_comments/' . $comment->image_path) }}">
                </div>
                @endif

                </p>
                <a href="#" class="post-add-icon inline-items love-comment {{ false?'loved':'' }}"
                   data-comment_id="{{ $comment->id }}"
                   style="{{ false?'fill:red;':'' }}">
                    <svg class="olymp-heart-icon">
                        <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-heart-icon') }}"></use>
                    </svg>
                    <span class="num_of_loves_comment-{{ $comment->id }}">{{ count(0)?:0 }}</span>
                </a>
                <a style="cursor: pointer;" class="reply"
                   data-form-reply="addReply-Comment-{{ $comment->id }}">Reply</a>
                <div class="post__date">
                    <time class="published" datetime="{{ $comment->created_at }}">
                        {{ \Carbon\Carbon::parse($comment->created_at )->diffForHumans() }}
                    </time>
                </div>
                @if($comment->user_id == Auth::user()->id)
                    <a class="more">
                        <svg class="olymp-three-dots-icon">
                            <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use>
                        </svg>
                        <ul class="more-dropdown">
                            <li>
                                <a href="#">Edit Comment</a>
                            </li>
                            <li>
                                <a href="#">Delete Comment</a>
                            </li>
                        </ul>
                    </a>
                @endif
        </div>

    </div>
    <div id="post_loader-reply-{{ $comment->id }}" style="text-align: center;padding: 15px;display: none;">
        <img src="{{ asset('img/post_loader.gif') }}" style="height: 30px; width: 30px;">
    </div>
    <form class="comment-form inline-items bg-white addReply-Comment-{{ $comment->id }}" data-replies-list="replies_list-{{ $comment->id }}" id = "submit-reply" style="display: none;">
        <input type="hidden" value="{{$comment->id}}" name = "comment_id">
        <div class="post__author author vcard inline-items">
            @if($user->logo)
                <img alt="author" src="{{asset($user->logo)}}" class="avatar">
            @else
                <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
            @endif
            <div class="form-group with-icon-right is-empty">
                <textarea class="form-control" name = "reply_text" placeholder=""></textarea>
                <div class="add-options-message">
                    <label for="reply-image" class="options-message" data-toggle="tooltip"
                           data-placement="top"
                           data-original-title="ADD PHOTO">
                        <svg for="reply-image" class="olymp-camera-icon">
                            <use xlink:href="/svg-icons/sprites/icons.svg#olymp-camera-icon"></use>
                        </svg>
                        <input data-preview-img="image" type="file" name="reply-image" id="reply-image"
                               data-preview-class = 'reply-image-preview-container-{{ $comment->id }}'
                               style="display: none;" accept="image/*">
                    </label>
                </div>
                <span class="material-input"></span>
            </div>
        </div>
        <div class="form-group padding40 reply-image-preview-container-{{ $comment->id }}" style="display: none;">

        </div>
        <button class="btn btn-md-2 btn-primary" type="submit">Post Comment</button>

        <button class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color">Cancel</button>

    </form>

    <ul class = "children" id = "replies_list-{{ $comment->id }}">

    </ul>
</li>
