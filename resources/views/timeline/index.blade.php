<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 12/1/18
 * Time: 7:37 PM
 */
?>
@extends('layouts.application')
@section('content')

    <!-- Main Content -->
    <div class="container" id="profile">
        <div class="row">
            <main class="col col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">

                <div class="ui-block">

                    <!-- News Feed Form  -->

                    <div class="news-feed-form">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active inline-items" data-toggle="tab" href="#home-1" role="tab"
                                   aria-expanded="true">

                                    <svg class="olymp-status-icon">
                                        <use xlink:href="/svg-icons/sprites/icons.svg#olymp-status-icon"></use>
                                    </svg>

                                    <span>Status</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link inline-items" data-toggle="tab" href="#profile-1" role="tab"
                                   aria-expanded="false">

                                    <svg class="olymp-multimedia-icon">
                                        <use xlink:href="/svg-icons/sprites/icons.svg#olymp-multimedia-icon"></use>
                                    </svg>

                                    <span>Multimedia</span>
                                </a>
                            </li>

                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="home-1" role="tabpanel" aria-expanded="true">
                                <form id="add_status_post">
                                    <div class="author-thumb post__author">
                                        @if(Auth::user()->logo)

                                            <img alt="author" src="{{ asset('/' . Auth::user()->logo) }}"
                                                 class="avatar">
                                        @else
                                            <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                                        @endif
                                    </div>
                                    <div class="form-group with-icon label-floating is-empty">
                                        <label class="control-label">Share what you are thinking here...</label>
                                        <textarea class="form-control" name="status" placeholder=""></textarea>
                                    </div>
                                    <div class="add-options-message">
                                        <button type="submit" class="btn btn-primary btn-md-2">Share</button>
                                    </div>
                                </form>
                            </div>

                            <div class="tab-pane" id="profile-1" role="tabpanel" aria-expanded="true">
                                <form id="add_media_post" enctype="multipart/form-data">
                                    <div class="author-thumb post__author">
                                        @if(Auth::user()->logo)
                                            <img alt="author" src="/{{Auth::user()->logo}}" class="avatar">
                                        @else
                                            <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                                        @endif
                                    </div>
                                    <div class="form-group with-icon label-floating is-empty">
                                        <label class="control-label">Share what you are thinking here...</label>
                                        <textarea class="form-control" id="status" name="status"
                                                  placeholder=""></textarea>
                                    </div>
                                    <div class="form-group padding40 image-preview-container" style="display: none;">

                                    </div>
                                    <div class="form-group padding40 video-preview-container" style="display: none;">
                                        <video width="100%" controls>
                                            <source id="previewVid" src="">
                                        </video>
                                    </div>
                                    <div class="add-options-message">
                                        <label for="images" class="options-message" data-toggle="tooltip"
                                               data-placement="top"
                                               data-original-title="ADD PHOTOS">
                                            <svg for="images" class="olymp-camera-icon">
                                                <use xlink:href="/svg-icons/sprites/icons.svg#olymp-camera-icon"></use>
                                            </svg>
                                            <input data-preview-img="image" type="file" name="images" id="images"
                                                   multiple style="display: none;" accept="image/*">
                                        </label>
                                        <label for="video" class="options-message" data-toggle="tooltip"
                                               data-placement="top"
                                               data-original-title="ADD VIDEO">
                                            <svg class="olymp-computer-icon">
                                                <use xlink:href="/svg-icons/sprites/icons.svg#olymp-computer-icon"></use>
                                            </svg>
                                            <input type="file" name="video" id="video" style="display: none;"
                                                   accept="video/mp4">
                                        </label>

                                        <button class="btn btn-primary btn-md-2">Share</button>
                                    </div>

                                </form>
                            </div>

                        </div>
                    </div>

                    <!-- ... end News Feed Form  -->            </div>


                @if(!$posts->isEmpty())
                    <div class="ui-block">

                        <!-- Search Result -->

                        <article class="hentry post searches-item">

                            <div class="post__author author vcard inline-items" >
                                <img src="{{ asset('website/icon-fly.png') }}" alt="author" style="width: 80px !important;">

                                <div class="author-date">
                                    <a class="h6 post__author-name fn">elDesigners Team</a>
                                    <div class="country">Support</div>
                                </div>



                                <div class="more">
                                    <svg class="olymp-three-dots-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
                                    <ul class="more-dropdown">
                                        <li>
                                            <a href="#">Hide Post</a>
                                        </li>

                                    </ul>
                                </div>

                            </div>

                            <p class="user-description">
                                <span class="title center" style="text-align: center">Welcome to elDesigners timeline!<br></span> Here you can share images, videos, posts and ideas, please note that the media or posts shared here will only appear on your timeline.<br>
                                <br><span class="title" style="color: #48e5c7">Projects</span> allow designers to upload their work as a full project, uploaded projects will appear on your portfolio, website, design library & timeline.
                                <br><br><span class="title" style="color: #48a5e8">Website Generator</span> is an innovative tool to build a website in one click, use it once and your website will be live in less than 30 seconds.
                                <br><br><span class="title" style="color: #303445">Profile Page</span> collects your posts, projects, articles and showcases your information, to edit your info use the "Personal Information" tab.

                            </p>

                            <div class="post-block-photo">
                                <a href="{{ route('projects.index') }}" class="col col-3-width"><img src="{{ asset('img/Manage-Projectsx.svg') }}" alt="photo"></a>
                                <a href="{{ route('dashboard.index') }}" class="col col-3-width"><img src="{{ asset('img/Website-Generatorx.svg') }}" alt="photo"></a>
                                <a href="{{ route('profile.show') }}" class="col col-3-width"><img src="{{ asset('img/Profile-pagex.svg') }}" alt="photo"></a>


                            </div>



                        </article>
                        <!-- ... end Search Result -->
                    </div>

                    @include('timeline.post')
                    <input type="hidden" id="last_page" value="{{ $posts->lastPage() }}">
                    <div style="display: none; text-align: center;" id="load_more_posts">
                        <img class="center-block" src="/img/post_loader.gif"
                             style="text-align: center;height: 30px;width: 30px;" alt="Loading..."/>
                    </div>
                    <div style="text-align: center;">
                        <button class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color"
                                id="btn-load-more" style="display: none;">
                            Load More
                        </button>
                    </div>
                @else
                    <div class="ui-block" style="text-align: center;">
                        <div class="ui-block-title">
                            <p>No recent Post to show</p>
                        </div>
                    </div>
                @endif
            </main>

            <!-- ... end Main Content -->


            <!-- Left Sidebar -->

            <aside class="col col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-12">

                <div class="ui-block">

                    <div class="ui-block-title">
                        <h6 class="title">Activity Feed</h6>
                        <a href="#" class="more">
                            <svg class="olymp-three-dots-icon">
                                <use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                            </svg>
                        </a>
                    </div>


                    <!-- W-Activity-Feed -->
                    <activity-feed :notification_data="notification_data"></activity-feed>


                    <!-- .. end W-Activity-Feed -->
                </div>
            </aside>

            <!-- ... end Left Sidebar -->


            <!-- Right Sidebar -->

            <aside class="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">Friend Suggestions</h6>
                        <a href="#" class="more">
                            <svg class="olymp-three-dots-icon">
                                <use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                            </svg>
                        </a>
                    </div>

                    <friend-suggestions :friend_suggestions="friend_suggestions"
                                        v-on:requestfriend="requestFriend"></friend-suggestions>
                    <!-- W-Action -->


                    <!-- ... end W-Action -->
                </div>


                <div class="ui-block">


                    <!-- W-Action -->

                    <div class="widget w-action"><img src="{{ asset('svg-icons/custom_icons/generator.svg') }}"
                                                      alt="elDesigners">
                        <div class="content"><h4 class="title">Generate Website</h4> <span>Launch a website in one click!</span>
                            <a href="{{ route('dashboard.index') }}" class="btn btn-bg-secondary btn-md">START</a></div>
                    </div>

                    <!-- ... end W-Action -->
                </div>
            </aside>

            <!-- ... end Right Sidebar -->

        </div>
    </div>

    <!-- ... end Window-popup Choose from my Photo -->
@endsection
