<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 2019-01-10
 * Time: 21:00
 */
?>

@extends('layouts.application')
@section('content')
    <div class="main-header">
        <div class="content-bg-wrap bg-badges"></div>
        <div class="container">
            <div class="row">
                <div class="col col-lg-8 m-auto col-md-8 col-sm-12 col-12">
                    <div class="main-header-content">
                        <h1>DISCOVER. GET INSPIRED. COMMUNICATE</h1>
                        <p>Welcome to elDesigners! Here you'll find a group of international talents in all fields of
                            design.<br>
                            Get inspired, view projects, communicate and submit orders for free!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ... end Main Header Badges -->



    <!-- Main Content Badges -->



    <!-- ... end Main Content Badges -->

    <div class="container">
        <form  method="GET" action="/discover-friends/" id="submit_search_order_by">

            <div class="row">
                <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="ui-block responsive-flex">
                        <div class="ui-block-title">
                            <div class="h6 title">DESIGNERS</div>
                            <div class="w-select">
                                <div class="title">Order By:</div>
                                <fieldset class="form-group">
                                    <select name="orderbymethod" class="selectpicker select_filtiration form-control" >

                                        @foreach($order_by_options as $option)

                                            <option @if(isset($_GET['orderbymethod']) && $_GET['orderbymethod'] == $option['value']) selected @endif value="{{$option['value']}}">{{$option['text']}}</option>
                                        @endforeach
                                    </select>
                                </fieldset>
                            </div>

                            <div class="form-group w-search with-button">
                                <input class="form-control" type="text" name="search"
                                       value="@if(isset($_GET['search']) && $_GET['search'] !== '') {{$_GET['search']}} @endif"
                                       placeholder="Search Designers...">
                                <button>
                                    <svg class="olymp-magnifying-glass-icon">
                                        <use xlink:href="/svg-icons/sprites/icons.svg#olymp-magnifying-glass-icon"></use>
                                    </svg>
                                </button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="container" id="discover_friend">

        <div class="container">
            <div class="row">
                @if(!$discover->isEmpty())
                    @foreach ($discover as $user)
                        <div class="col col-xl-3 col-lg-6 col-md-6 col-sm-6 col-6">


                            <div class="ui-block">

                                <!-- Friend Item -->

                                <div class="friend-item">
                                    <div class="friend-header-thumb discover_cover_home">
                                        @if($user->cover)
                                            <img src="/{{$user->cover}}" alt="friend">
                                        @else
                                            <img src="{{ asset('uploads/user/dummy_cover.png') }}" alt="friend">
                                        @endif
                                    </div>

                                    <div class="friend-item-content">


                                        <div class="friend-avatar">
                                            <a href="/profile/{{ $user->user_name }}">
                                            <div class="author-thumb discover_logo">
                                                @if($user->logo)
                                                    <img src="/{{$user->logo}}" alt="friend">
                                                @else
                                                    <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                                                @endif
                                            </div>
                                            </a>
                                            <div class="author-content">
                                                <a href="/profile/{{ $user->user_name }}"
                                                   class="h5 author-name">{{ $user->name }}</a>
                                                <div class="country">{{ $user->user_name }}</div>
                                            </div>
                                        </div>

                                        <div class="swiper-container" data-slide="fade" >
                                            <div class="swiper-wrapper">

                                                <div class="swiper-slide">
                                                    <p class="friend-about" data-swiper-parallax="-500"  style="height: 150px !important;">
                                                        {{str_limit($user->description,120,' ...')}}
                                                    </p>
                                                    <div class="control-block-button" data-swiper-parallax="-100">
                                                        @if ($user->follow_setting == 'public')
                                                            <a href="#" data-request_id = "{{$user->id}}"
                                                                    class="public_friend_request btn btn-md-2 btn-border-think c-grey btn-transparent custom-color"
                                                                    style="background-color: #38a9ff; color: white">Follow
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        @else
                                                            <a href="#" data-request_id = "{{$user->id}}"
                                                                    class="friend_request btn btn-md-2 btn-border-think c-grey btn-transparent custom-color"
                                                                    style="background-color: #38a9ff; color: white">Follow
                                                                <div class="ripple-container"></div>
                                                            </a>
                                                        @endif


                                                        <a href="#"
                                                           @click.prevent="openChatDiscoverFriends({{$user->id}})"
                                                           data-toggle="modal" data-target="#blog-post-popup"
                                                           class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color">Message
                                                            <div class="ripple-container"></div>
                                                        </a>


                                                    </div>

                                                </div>
                                                <div class="swiper-slide">
                                                    <div class="friend-count" data-swiper-parallax="-500">
                                                        <a href="#" class="friend-count-item">
                                                            <div class="h6">{{$user->projects_count}}</div>
                                                            <div class="title">Projects</div>
                                                        </a>
                                                        <a href="#" class="friend-count-item">
                                                            <div class="h6">{{$user->profile_count}}</div>
                                                            <div class="title">Views</div>
                                                        </a>
                                                    </div>


                                                    <div class="friend-since" data-swiper-parallax="-100">
                                                        <span></span><br>
                                                        @if (isset($user->country['text']) && isset($user->city['text'] ))
                                                            <div class="h6">{{$user->city['text']}}, {{$user->country['text']}}</div>
                                                        @endif

                                                    </div>


                                                </div>


                                            </div>

                                            <!-- If we need pagination -->
                                            <div class="swiper-pagination"></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- ... end Friend Item -->            </div>
                        </div>

                    @endforeach
                @else
                    <div class="container">
                        <div class="row">
                            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="ui-block">
                                    <div class="ui-block-title ">
                                        <p class="h6 title center_text">No Result Found</p>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
            </div>

            @endif
        </div>
        <nav aria-label="Page navigation" class="justify-content-center">
            <div class="justify-content-center">
                {{$discover->links('vendor.pagination.bootstrap-4')}}

            </div>
        </nav>
    </div>
@endsection