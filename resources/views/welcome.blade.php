<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>

    @include('includes.head')

</head>
<body>
<header class="header" id="site-header">
    @include('includes.header')
</header>
<div >
    @include('includes.sidebar')
</div>
<section class="content">
    @yield('content')
</section><!-- /.content -->
<footer>
    @include('includes.footer')
</footer>
</body>
</html>
