<!DOCTYPE html>
<html>
    <head>
        <title>Error.</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="css/main.min.css">
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #fff;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
                margin-top: 200px;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container content-bg-wrap bg-landing">
            <div class="content">
                <div class="title">An Error reported please contact website administrator.<br/>Thank You.
                    <span style="font-weight: bold; font-size:15px;"><br/>Error :<span style="color:red; ">#{{
                    $error_ref  }}</span></span> </div>
            </div>



        </div>
    </body>
</html>
