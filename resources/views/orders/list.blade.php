@extends('profile.layout.main')
@section('profile_content')
    <div class="ui-block">


    <section class="medium-padding100">
        <div class="container">
            <div class="row mb10">
                <div class="col col-xl-9 col-lg-9 col-md-12 col-sm-12  m-auto">
                    <div class="crumina-module crumina-heading align-center">
                        <div class="heading-sup-title">Website Leads</div>
                        <h2 class="heading-title">Order</h2>
                        <p class="heading-text">All the orders which was submitted through the webstie or the protfolio will be avaible here.<br>
                            Based on the package you have, you will be able to chat with the clients using the elDesigners platform.<br>
                            Else you'll have to contact them using their contact info.
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12  m-auto">


                    <ul class="table-careers">
                        <li class="head">
                            <span>DATE REQUESTED</span>
                            <span>SOURCE</span>
                            <span>CLIENT</span>
                            <span>PLACE</span>
                            <span></span>
                        </li>
                        @foreach($orders as $order)
                        <li>
                            <span class="date">{{ date('M-d-y' , strtotime($order->created_at)) }}</span>
                            <span class="position bold">Website</span>
                            <span class="type bold">{{ $order->client_name?:'Unknown Client' }}</span>
                            <span class="town-place">{{ $order->client_address?:'Unknown Address' }}</span>
                            <span><a href="#" class="btn btn-primary btn-sm full-width" data-toggle="modal" data-target="#edit-my-poll-popup">View Message</a></span>
                        </li>
                        @endforeach
                    </ul>

                </div>
            </div>
        </div>
    </section>
</div>
@endsection
