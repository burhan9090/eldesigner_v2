<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 12/1/18
 * Time: 6:23 PM
 */
?>
<style>
    .flag{
        width: 30px;
        height: 30px;
    }
    .bootstrap-select.btn-group .dropdown-menu li.selected a span{
        color: white !important;
    }
    .bootstrap-select.btn-group .dropdown-menu li a span{
        color: black !important;
    }
    .bootstrap-select > .dropdown-toggle {
        border: none !important;
    }
    .no-profile-pic {
        background: transparent;
        height: 40px;
        width: 40px;
        border-radius: 50%;
        text-align: center;
        padding: 12px;
        font-size: 15px;
        font-weight: 600;
    }
</style>
<div class="page-title">
    <a href="{{ route('timeline') }}">
        <h6 style="color: white;">Newsfeed</h6>
    </a>
</div>

<div class="header-content-wrapper" id="header_app" >
    <audio id="ChatAudio" src="{{ asset('mp3/chat_notification.mp3') }}" muted>
    </audio>
    <button type="button" @click="myClickEvent" id="audio_btn" ref="myBtn" hidden="true">
        Click Me!
    </button>
    <input type="hidden" id="user_id" value="{{ Auth::user()->id }}">
    <input type="hidden" id="user_obj" value="{{ Auth::user() }}">
    <form class="search-bar w-search notification-list friend-requests">
        <div class="form-group with-button">
            <input class="form-control" id="select-repo" placeholder="Search here Designs or Designers ..." type="text">
            <button>
                <svg class="olymp-magnifying-glass-icon"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-magnifying-glass-icon') }}"></use></svg>
            </button>
        </div>
    </form>

    {{--<select class="link-find-friend" style="width: 20%">--}}
        {{--<option selected><a href="{{ route('timeline.discover_friends') }}" class="link-find-friend">Discover Designers</a></option>--}}
        {{--<option><a href="{{ route('community.articles') }}" class="link-find-friend">Discover Articles</a></option>--}}
    {{--</select>--}}
    <div class="link-find-friend" id="accordion1" role="tablist" aria-multiselectable="false" style="width: 200px">
        <div class="card" style="background-color: #3f4257 !important; color: white">
            <div class="" role="tab" id="headingOne-1" >
                <h6 class="mb-0">
                    <a style="color: white;margin-left: 14px" data-toggle="collapse" data-parent="#accordion" href="#collapseOne-1" aria-expanded="true" aria-controls="collapseOne">
                        Discover
                    </a>
                    <icon data-toggle="collapse" data-parent="#accordion" href="#collapseOne-1" aria-expanded="true" aria-controls="collapseOne" class = "fa fa-angle-down" style = "color: white;cursor: pointer;padding-top: 2px"></icon>
                </h6>
            </div>

            <div id="collapseOne-1" class="collapse" role="tabpanel" aria-labelledby="headingOne">
                <ul class="your-profile-menu">
                    <li>
                        <a href="{{ route('timeline.discover_friends') }}" class="link-find-friend" style="color: white">Designers</a>
                    </li>
                    <li>
                        <a href="{{ route('community.articles.list') }}" class="link-find-friend" style="color: white">Articles</a>
                    </li>
                    <li>
                        <a href="{{ route('community.projects.list') }}" class="link-find-friend" style="color: white">Projects</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>


    <div class="control-block">

        <div class="control-icon more has-items">
            {{--@include('includes.lang_selector')--}}
        </div>
        <div class="control-icon more has-items">
            <svg class="olymp-happy-face-icon"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-happy-faces-icon') }}"></use></svg>
            <div class="label-avatar bg-blue" v-if="number_of_follow_requests != 0 ">@{{number_of_follow_requests}}</div>

            <div class="more-dropdown more-with-triangle triangle-top-center">
                <div class="ui-block-title ui-block-title-small">
                    <h6 class="title">FRIEND REQUESTS</h6>
                </div>

                <div class="mCustomScrollbar" data-mcs-theme="dark">
                    <follow-request :follow_request_data="follow_request_data" ></follow-request>
                </div>

                <a href="{{route('profile.friend_requests')}}" class="view-all bg-blue">Check all your Events</a>
            </div>
        </div>

        <div class="control-icon more has-items">
            <svg class="olymp-chat---messages-icon"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-chat---messages-icon') }}"></use></svg>
            <div class="label-avatar bg-purple" v-if="number_of_message_notification != 0">@{{ number_of_message_notification }}</div>

            <div class="more-dropdown more-with-triangle triangle-top-center">
                <div class="ui-block-title ui-block-title-small">
                    <h6 class="title">Chat / Messages</h6>
                </div>
                <div class="mCustomScrollbar" data-mcs-theme="dark">
                    <header-chat-panel :header_messages="header_messages"></header-chat-panel>

                </div>

                <a href="{{ url('show-chat') }}" class="view-all bg-purple">View All Messages</a>
            </div>
        </div>

        <div class="control-icon more has-items">
            <svg class="olymp-thunder-icon"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-thunder-icon') }}"></use></svg>

            <div class="label-avatar bg-primary" v-if="number_of_notification != 0">@{{number_of_notification}}</div>

            <div class="more-dropdown more-with-triangle triangle-top-center">
                <div class="ui-block-title ui-block-title-small">
                    <h6 class="title">Notifications</h6>
                    <a href="#" @click.prevent="markAllAsRead">Mark all as read</a>

                </div>

                <div class="mCustomScrollbar" data-mcs-theme="dark">
                    <notification :notification_data="notification_data" v-on:opennotification="openNotification"></notification>
                </div>

                <a href="{{ route('profile.notification') }}" class="view-all bg-primary">View All Notifications</a>
            </div>
        </div>

        <div class="author-page author vcard inline-items more">
            <div class="author-thumb header_logo">

                @if(Auth::user()->logo)
                    <img alt="author" src="/{{  Auth::user()->logo  }}" class="avatar">
                @else
                    <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                @endif
                <span class="icon-status online"></span>
                <div class="more-dropdown more-with-triangle">
                    <div class="mCustomScrollbar" data-mcs-theme="dark">
                        <div class="ui-block-title ui-block-title-small">
                            <h6 class="title">Your Account</h6>
                        </div>

                        <ul class="account-settings">
                            <li>
                                <a href="/profile/{{ Auth::user()->user_name }}">

                                    <svg class="olymp-menu-icon"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-menu-icon') }}"></use></svg>

                                    <span>Profile</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('setting.main') }}"
                                   class="{!! request()->route()->getName()=='setting.main'? 'active': "" !!}">
                                    <svg class="olymp-menu-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-settings-icon') }}"></use></svg>
                                    <span>General Settings</span>

                                </a>
                            </li>
                            <li>
                                <a href="{{ route('user.change_password') }}"
                                   class="{!! request()->route()->getName()=='user.change_password'? 'active': "" !!}">
                                    <svg class="olymp-menu-icon"><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-settings-v2-icon') }}"></use></svg>
                                    <span>Change password</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <svg class="olymp-logout-icon"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-logout-icon') }}"></use></svg>

                                    <span>Log Out</span>
                                </a>
                            </li>
                        </ul>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </div>

                </div>
            </div>
            <a href="/profile/{{ Auth::user()->user_name }}" class="author-name fn">
                <div class="author-title">
                    {{ Auth::user()->name }} <svg class="olymp-dropdown-arrow-icon"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon') }}"></use></svg>
                </div>
                <span class="author-subtitle">{{ Auth::user()->user_name }} </span>
            </a>
        </div>

    </div>
    <notifications group="foo"  position="bottom right" width="400" />
</div>
