<style>
    .HomePage-BackgroundArt-footer{
        background-image: url(../website/HomePage-BackgroundArt-footer.svg);
        background-size: cover;
    }
</style>
<!-- Footer Full Width -->

<div class="footer HomePage-BackgroundArt-footer footer-full-width" id="footer">
    <div class="container">
        <div class="row">
            <div class="col col-lg-4 col-md-4 col-sm-6 col-12">


                <!-- Widget About -->

                <div class="widget w-about">

                    <a href="{{ route('home') }}" class="logo">
                        <div class="img-wrap">
                            <img src="{{ asset('website/Logo-blue.png') }}" alt="elDesigners">
                        </div>
                        <div class="title-block">
                            <h3 class="logo-title">elDESIGNERS</h3>
                            <p class="logo-title" style="text-align: center; color: #303445">Design without limits</p>
                        </div>
                    </a>
                    <p>Everything designers need in one place, synchronized in real time, available anywhere at any time, on the cloud.</p>
                    <ul class="socials">
                        <li>
                            <a href="https://web.facebook.com/eldesigners/">
                                <i class="fab fa-facebook-square" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/el_designers">
                                <i class="fab fa-twitter" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UCIlm-6Lc3l0RynSlIiMakjQ">
                                <i class="fab fa-youtube" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company/eldesigners">
                                <i class="fab fa-linkedin" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/el_designers/">
                                <i class="fab fa-instagram" aria-hidden="true"></i>
                            </a>
                        </li>
                    </ul>
                </div>

                <!-- ... end Widget About -->

            </div>

            <div class="col col-lg-2 col-md-4 col-sm-6 col-6">


                <!-- Widget List -->

                <div class="widget w-list">
                    <h6 class="title">Public Links</h6>
                    <ul>
                        <li>
                            <a href="{{ route('home.discover-designs') }}">Discover Designs</a>
                        </li>
                        <li>
                            <a href="{{ route('home.discover-friends') }}">Designers</a>
                        </li>
                        <li>
                            <a href="{{ route('home.home_blog') }}">Articles</a>
                        </li>

                    </ul>
                </div>

                <!-- ... end Widget List -->

            </div>
            <div class="col col-lg-2 col-md-4 col-sm-6 col-6">


                <div class="widget w-list">
                    <h6 class="title">For Designers</h6>
                    <ul>
                        <li>
                            <a href="{{ route('home.how_it_works') }}">How it works</a>
                        </li>
                        <li>
                            <a href="{{ route('home.pricing') }}">Pricing</a>
                        </li>
                        <li>
                            <a data-toggle="tooltip" data-placement="right"   data-original-title="mail us at support@eldesigners.com">Support</a>
                        </li>

                    </ul>
                </div>

            </div>
            <div class="col col-lg-2 col-md-4 col-sm-6 col-6">


                <div class="widget w-list">
                    <h6 class="title">Access</h6>
                    <ul>
                        <li>
                            <a href="{{ route('home.login') }}">Login</a>
                        </li>
                        <li>
                            <a href="#">Registration</a>
                        </li>
                        <li>
                            <a data-toggle="tooltip" data-placement="right"   data-original-title="mail us at info@eldesigners.com">Feedback</a>
                        </li>

                    </ul>
                </div>

            </div>
            <div class="col col-lg-2 col-md-4 col-sm-6 col-6">


                <div class="widget w-list">
                    <h6 class="title">Social</h6>
                    <ul>
                        <li>
                            <a href="https://web.facebook.com/groups/2218633941513735/">Facebook Group</a>
                        </li>
                        <li>
                            <a href="https://www.linkedin.com/company/eldesigners">Linkedin</a>
                        </li>
                        <li>
                            <a href="https://twitter.com/el_designers">Twitter</a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/el_designers/">Instagram</a>
                        </li>
                    </ul>
                </div>

            </div>


            <div class="col col-lg-12 col-md-12 col-sm-12 col-12">


                <!-- SUB Footer -->

                <div class="sub-footer-copyright">
					<span>
						Copyright <a href="{{ route('home') }}">elDesigners </a> All Rights Reserved 2019
					</span>
                </div>

                <!-- ... end SUB Footer -->

            </div>
        </div>
    </div>
</div>

    <script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('/js/jquery-3.2.1.js')}}"></script>
    {{--<script src="{{asset('/js/jquery.appear.js')}}"></script>--}}
    {{--<script src="{{asset('/js/jquery.mousewheel.js')}}"></script>--}}
    {{--<script src="{{asset('/js/perfect-scrollbar.js')}}"></script>--}}
    {{--<script src="{{asset('/js/jquery.matchHeight.js')}}"></script>--}}
    {{--<script src="{{asset('/js/svgxuse.js')}}"></script>--}}
<script src="{{asset('/js/imagesloaded.pkgd.js')}}"></script>
<script src="{{asset('/js/Headroom.js')}}"></script>
    {{--<script src="{{asset('/js/velocity.js')}}"></script>--}}
    {{--<script src="{{asset('/js/ScrollMagic.js')}}"></script>--}}
    {{--<script src="{{asset('/js/jquery.waypoints.js')}}"></script>--}}
    {{--<script src="{{asset('/js/jquery.countTo.js')}}"></script>--}}
    {{--<script src="{{asset('/js/popper.min.js')}}"></script>--}}
<script src="{{asset('/js/material.min.js')}}"></script>
<script src="{{asset('/js/bootstrap-select.js')}}"></script>
    {{--<script src="{{asset('/js/smooth-scroll.js')}}"></script>--}}
    {{--<script src="{{asset('/js/selectize.js')}}"></script>--}}
<script src="{{asset('/js/swiper.jquery.js')}}"></script>
    {{--<script src="{{asset('/js/moment.js')}}"></script>--}}
    {{--<script src="{{asset('/js/daterangepicker.js')}}"></script>--}}
    {{--<script src="{{asset('/js/simplecalendar.js')}}"></script>--}}
    {{--<script src="{{asset('/js/fullcalendar.js')}}"></script>--}}
<script src="{{asset('/js/isotope.pkgd.js')}}"></script>
    {{--<script src="{{asset('/js/ajax-pagination.js')}}"></script>--}}
    {{--<script src="{{asset('/js/Chart.js')}}"></script>--}}
    {{--<script src="{{asset('/js/chartjs-plugin-deferred.js')}}"></script>--}}
    {{--<script src="{{asset('/js/circle-progress.js')}}"></script>--}}
    {{--<script src="{{asset('/js/loader.js')}}"></script>--}}
    {{--<script src="{{asset('/js/run-chart.js')}}"></script>--}}
    <script src="{{asset('/js/jquery.magnific-popup.js')}}"></script>
    {{--<script src="{{asset('/js/jquery.gifplayer.js')}}"></script>--}}
    {{--<script src="{{asset('/js/mediaelement-and-player.js')}}"></script>--}}
    {{--<script src="{{asset('/js/mediaelement-playlist-plugin.min.js')}}"></script>--}}
<script src="{{asset('/js/base-init.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.4.2/jquery.twbsPagination.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.js"></script>
<script src="{{asset('/js/eldesigner.js')}}"></script>
<script defer src="{{asset('/fonts/fontawesome-all.js')}}"></script>
<script src="{{asset('/Bootstrap/dist/js/bootstrap.bundle.js')}}"></script>

<script>
    const BASE_URL = '{{ env('APP_URL' , 'http://localhost/') }}';
    const CSRF_TOKEN = '{{ csrf_token() }}';

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': CSRF_TOKEN
        }
    });
</script>