
<select class="selectpicker" data-width="fit" style="padding: 10px;">
    <option class = "change_lang" {{ Auth::user()->lang == 'ar'?'selected':'' }} data-content='<img class = "flag" data-lang = "ar" src="/img/jo.svg" style = "border-radius:50%"> <span style = "color:white">Arabic</span>'></option>
    <option class = "change_lang" {{ Auth::user()->lang == 'en'?'selected':'' }} data-content='<img class = "flag" data-lang = "en" src="/img/gb.svg" style = "border-radius:50%"> <span style = "color:white">English</span>'></option>
</select>