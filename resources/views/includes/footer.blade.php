<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 12/1/18
 * Time: 6:23 PM
 */
?>

<!-- JS Scripts -->
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('/js/jquery-3.2.1.js')}}"></script>
<script src="{{asset('/js/jquery.appear.js')}}"></script>
<script src="{{asset('/js/jquery.mousewheel.js')}}"></script>
<script src="{{asset('/js/perfect-scrollbar.js')}}"></script>
<script src="{{asset('/js/jquery.matchHeight.js')}}"></script>
<script src="{{asset('/js/svgxuse.js')}}"></script>
<script src="{{asset('/js/imagesloaded.pkgd.js')}}"></script>
<script src="{{asset('/js/Headroom.js')}}"></script>
<script src="{{asset('/js/velocity.js')}}"></script>
<script src="{{asset('/js/ScrollMagic.js')}}"></script>
<script src="{{asset('/js/jquery.waypoints.js')}}"></script>
<script src="{{asset('/js/jquery.countTo.js')}}"></script>
<script src="{{asset('/js/popper.min.js')}}"></script>
<script src="{{asset('/js/material.min.js')}}"></script>
<script src="{{asset('/js/bootstrap-select.js')}}"></script>
<script src="{{asset('/js/smooth-scroll.js')}}"></script>
<script src="{{asset('/js/selectize.js')}}"></script>
<script src="{{asset('/js/swiper.jquery.js')}}"></script>
<script src="{{asset('/js/moment.js')}}"></script>
<script src="{{asset('/js/daterangepicker.js')}}"></script>
<script src="{{asset('/js/simplecalendar.js')}}"></script>
<script src="{{asset('/js/fullcalendar.js')}}"></script>
<script src="{{asset('/js/isotope.pkgd.js')}}"></script>
<script src="{{asset('/js/ajax-pagination.js')}}"></script>
<script src="{{asset('/js/Chart.js')}}"></script>
<script src="{{asset('/js/chartjs-plugin-deferred.js')}}"></script>
<script src="{{asset('/js/circle-progress.js')}}"></script>
<script src="{{asset('/js/loader.js')}}"></script>
<script src="{{asset('/js/run-chart.js')}}"></script>
<script src="{{asset('/js/jquery.magnific-popup.js')}}"></script>
<script src="{{asset('/js/jquery.gifplayer.js')}}"></script>
<script src="{{asset('/js/mediaelement-and-player.js')}}"></script>
<script src="{{asset('/js/mediaelement-playlist-plugin.min.js')}}"></script>
<script src="{{asset('/js/base-init.js')}}"></script>
<script defer src="{{asset('/fonts/fontawesome-all.js')}}"></script>
<script src="{{asset('/Bootstrap/dist/js/bootstrap.bundle.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.0/jquery.validate.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jscroll/2.4.1/jquery.jscroll.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twbs-pagination/1.4.2/jquery.twbsPagination.min.js"></script>
@if (strpos('/payment',$_SERVER['REQUEST_URI']) === 0)
        <script src="https://www.paypal.com/sdk/js?client-id={{env('PAYPAL_CLIENT_ID', 'AUn0pkEO1nyhpgn4CteHKEnlWcP9-tLkB141hE_q-juyFlf7crcqDgd7MOdG875MVhZ5O-vOXE6HEnUG')}}">
    </script>
    <script src="{{asset('/js/payment.js')}}"></script>
@endif
<script>
  /** mobile sidebar **/

  $('#sidebar-left-1-responsive').find('a[data-toggle="collapse"]').click(function(){
    var $this = $(this);
    var target = $this.attr('href');
    var tab = $this.closest('div.card').find(target);

    if(tab.hasClass('in')){
      tab.removeClass('in show');
    } else {

      tab.addClass('in show');
    }
  });
</script>

<script>
    // ** JS GLOBAL VARIABLES ** //
    const BASE_URL = '{{ env('APP_URL' , 'http://localhost/') }}';
    const CSRF_TOKEN = '{{ csrf_token() }}';

    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': CSRF_TOKEN
      }
    });

    $('#toggle-content').click(function () {
      $('#content-container').toggle('slow');
    });

    @if(request()->session()->has('save'))
    swal("", "{{ request()->session()->get('save') }}", "success");
    @endif
    @if( request()->session()->has('error') )
    swal("", "{{ request()->session()->get('error') }}", "info");
    @endif
    @php
      request()->session()->remove('save');
      request()->session()->remove('error');
    @endphp
</script>

<script src="{{asset('/include/js/main.js')}}"></script>
<script src="{{asset('js/bootstrap-notify.js')}}"></script>
<script src="{{asset('js/jquery.jscroll.min.js')}}"></script>
<script src="{{asset('js/eldesigner.js')}}"></script>


