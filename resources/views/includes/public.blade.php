<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 2019-02-08
 * Time: 16:59
 */
?>
@extends('layouts.eldesigners')
@section('content')
    <style>
        body{
            background-color: #EDF2F6 !important;
        }
        .bg-landing, .w-action {
            background-image: url(../website/HomePageBackground4.jpg);
            width: 100%;
            background-repeat: no-repeat;
            /* background-attachment: fixed; */
            background-position: unset;
            background-size: cover;
        }

        .bg-primary-opacity {
            background-color: rgba(251, 251, 251, 0);
        }
    </style>
    <div class="main-header main-header-fullwidth main-header-has-header-standard inner-fixed-header public-header">
    @include('eldesigners.top_menu' , ['sHeaderFontColor' => 'black' , 'sHeaderBg' => '#3f4257'])


        <div class="content-bg-wrap bg-landing"></div>
    </div>
    <div class="container-fluid">
        <input type="hidden" id="request_id" value="{{$oUserInfo->id}}">
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 fixed-top-head">
                <div class="ui-block public-view">
                    <div class="top-header top-header-favorit">
                        <div class="top-header-thumb cover_photo">
                            @if($oUserInfo->cover)
                                <img src="/{{$oUserInfo->cover}}" alt="nature">
                            @else
                                <img src="{{ asset('uploads/user/dummy_cover.png') }}" alt="nature">
                            @endif

                            <div class="top-header-author">
                                @if($oUserInfo->logo)
                                    <div class="author-thumb user_logo">
                                        <img src="/{{ $oUserInfo->logo }}" alt="author">
                                    </div>
                                @else
                                    <div class="author-thumb user_logo">
                                        <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                                    </div>
                                @endif
                                <div class="author-content">
                                    <a href="#" class="h3 author-name">{{ $oUserInfo->name }}</a>
                                    @php
                                        $user_country = \App\Country::find($oUserInfo->country_id);
                                        $user_city = \App\City::find($oUserInfo->city_id)
                                    @endphp
                                    <div class="country">{{ $user_country?$user_country->text:'' }}  |  {{ $user_city? $user_city->text:'' }}</div>
                                </div>
                            </div>
                        </div>
                        <div class="profile-section">
                            <div class="row">
                                <div class="col col-xl-8 m-auto col-lg-8 col-md-12">
                                    <ul class="profile-menu">

                                        <li>
                                            <a href="/home/profile/{{$oUserInfo->user_name}}" class="@php if(strpos($_SERVER['REQUEST_URI'],'/about') !== false) { echo 'active';} @endphp" >Timeline</a>
                                        </li>
                                        @if($oUserInfo->profile_setting != 'private')
                                            <li>

                                                <a class="@php if(strpos($_SERVER['REQUEST_URI'],'/about') !== false) { echo 'active';} @endphp" href="/home/profile/{{$oUserInfo->user_name}}/about">About</a>
                                            </li>
                                            <li>
                                                <a class="@php if(strpos($_SERVER['REQUEST_URI'],'/project') !== false) { echo 'active';} @endphp" href="/home/profile/{{$oUserInfo->user_name}}/project">Projects</a>
                                            </li>
                                            <li>
                                                <a class="@php if(strpos($_SERVER['REQUEST_URI'],'/articles') !== false) { echo 'active';} @endphp" href="/home/profile/{{$oUserInfo->user_name}}/articles">Articles</a>
                                            </li>
                                            <li>
                                            @if($oUserInfo->domain)
                                                <a class="" href="/{{$oUserInfo->user_name}}">Visit My Website</a>
                                            @else
                                                <a class="" href="/{{$oUserInfo->user_name}}">Visit My Website</a>
                                            @endif
                                            </li>
                                        @endif

                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        @yield('content_public')
    </div>
@endsection
