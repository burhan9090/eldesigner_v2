

<div id="side_app">
  <div  class="fixed-sidebar">
      @include('includes.sidebar-left')
  </div>
  <div class="fixed-sidebar right right_side_bar">
      @include('includes.sidebar-right')
  </div>
  <div class="fixed-sidebar fixed-sidebar-responsive">

    <div class="fixed-sidebar-left sidebar--small" id="sidebar-left-responsive">

      <a href="#" class="logo js-sidebar-open" style="background-color: #3f4159 !important;">
        <div class="img-wrap ">
          <svg class="olymp-menu-icon" >
            <use style="fill: white;" xlink:href="/svg-icons/sprites/icons.svg#olymp-menu-icon"></use>
          </svg>
        </div>
      </a>
    </div>

    <div class="fixed-sidebar-left sidebar--large" id="sidebar-left-1-responsive">
      <a href="#"  class="logo js-sidebar-open" style="background-color: #3f4159 !important;">
        <div class="img-wrap ">
          <svg class="olymp-menu-icon" >
            <use style="fill: white;" xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use>
          </svg>
        </div>
        <div class="title-block">
          <h6 class="logo-title">Eldesigners</h6>
        </div>
      </a>


      <div class="mCustomScrollbar" data-mcs-theme="dark">
        <div class="ui-block">
          <div class="your-profile">

            <div class="content-block" role="tablist" aria-multiselectable="true">
              <div class="card">
                <div class="card-header tab-container {!! in_array(request()->route()->getName(), ['profile.show','user.change_password','user.change_password'])? 'custom-active': "" !!}" role="tab" id="content-nav">
                  <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/Profile.svg') }}" >
                  {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-happy-face-icon') }}"></use></svg>--}}
                  <h6 class="mb-0 tab-element">
                    <a data-toggle="collapse" data-parent="#content-block" href="#content-collapse"
                       aria-expanded="true"
                       aria-controls="content-collapse" {!! in_array(request()->route()->getName(), ['profile.show','user.change_password','user.change_password'])? '': 'class="collapsed"' !!}>
                      {{ Auth::user()->name }}
                      <svg class="olymp-dropdown-arrow-icon" style="float: right;">
                        <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon') }}"></use>
                      </svg>
                    </a>
                  </h6>
                </div>

                <div id="content-collapse"
                     class="collapse {{ in_array(request()->route()->getName(), ['profile.show','setting.main', 'user.change_password'])? 'show': ''  }}"
                     role="tabpanel" aria-labelledby="content-nav">
                  <ul class="your-profile-menu">

                    <li>
                      <a href="{{ route('profile.show') }}"
                         class="{!! request()->route()->getName()=='profile.show'? 'active': "" !!}">Profile</a>
                    </li>
                    <li>
                      <a href="{{ route('setting.main') }}"
                         class="{!! request()->route()->getName()=='setting.main'? 'active': "" !!}">
                        General Settings
                      </a>
                    </li>
                    <li>
                      <a href="{{ route('user.change_password') }}"
                         class="{!! request()->route()->getName()=='user.change_password'? 'active': "" !!}">
                        Change password
                      </a>
                    </li>
                    <li>
                      <a href="{{ route('logout') }}"
                         onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        <span>Log Out</span>
                      </a>
                    </li>
                  </ul>
                </div>

              </div>
            </div>


            <div class="content-block">
              <div class="card">
                <div class="card-header tab-container {!! request()->route()->getName()=='timeline.discover_friends'? 'custom-active': "" !!}" role="tab" id="content-nav">
                  <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/following-followers.svg') }}" >
                  {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-happy-face-icon') }}"></use></svg>--}}
                  <h6 class="mb-0 tab-element">
                    <a href="{{ route('timeline.discover_friends') }}">
                      <span class="left-menu-title">Discover Designers</span>
                    </a>
                  </h6>
                </div>
              </div>
            </div>
            <div class="content-block">
              <div class="card">
                <div class="card-header tab-container {!! request()->route()->getName()=='community.articles.list'? 'custom-active': "" !!}" role="tab" id="content-nav">
                  <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/articles.svg') }}" >
                  {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-happy-face-icon') }}"></use></svg>--}}
                  <h6 class="mb-0 tab-element">
                    <a href="{{ route('community.articles.list') }}">
                      <span class="left-menu-title">Discover Articles</span>
                    </a>
                  </h6>
                </div>
              </div>
            </div>
            <div class="content-block">
              <div class="card">
                <div class="card-header tab-container {!! request()->route()->getName()=='community.projects.list'? 'custom-active': "" !!}" role="tab" id="content-nav">
                  <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/Projects.svg') }}" >
                  {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-happy-face-icon') }}"></use></svg>--}}
                  <h6 class="mb-0 tab-element">
                    <a href="{{ route('community.projects.list') }}">
                      <span class="left-menu-title">Discover Projects</span>
                    </a>
                  </h6>
                </div>
              </div>
            </div>
            <div class="content-block">
              <div class="card">
                <div class="card-header tab-container {!! request()->route()->getName()=='dashboard.index'? 'custom-active': "" !!}" role="tab" id="content-nav">
                  <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/generator.svg') }}" >
                  {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-manage-widgets-icon') }}"></use></svg>--}}
                  <h6 class="mb-0 tab-element">
                    <a href="{{ route('dashboard.index') }}"
                       class="h6 title {!! request()->route()->getName()=='dashboard.index'? 'active': "" !!}">Website
                      Generator</a>
                  </h6>
                </div>
              </div>
              <div class="content-block">
              <div class="card">
                <div class="card-header tab-container {!! in_array(request()->route()->getName(), ['welcome.index', 'setting.seo', 'services.index', 'skills', 'members', 'user_services', 'contact_us', 'themes.index', 'style.index'])? 'custom-active': '' !!}" role="tab" id="content-nav">
                  <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/website-data.svg') }}" >
                  {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-happy-face-icon') }}"></use></svg>--}}
                  <h6 class="mb-0 tab-element">
                    <a href="{{ route('welcome.index') }}">
                      <span class="left-menu-title">Website Data</span>
                    </a>
                  </h6>
                </div>
              </div>
            </div>
            </div>
            <div class="content-block">
              <div class="card">
                <div class="card-header tab-container {!! request()->route()->getName()=='community.projects.list'? 'custom-active': "" !!}" role="tab" id="content-nav">
                  <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/Projects.svg') }}" >
                  {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-happy-face-icon') }}"></use></svg>--}}
                  <h6 class="mb-0 tab-element">
                    <a href="{{ route('projects.index') }}">
                      <span class="left-menu-title">Projects</span>
                    </a>
                  </h6>
                </div>
              </div>
            </div>

            <div id="articles-block" role="tablist" aria-multiselectable="true">
              <div class="card">
                <div class="card-header tab-container {{ strpos(request()->route()->uri, 'article')!==false? 'custom-active': ''  }}" role="tab" id="article-nav">
                  <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/articles.svg') }}" >
                  {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-blog-icon') }}"></use></svg>--}}
                  <h6 class="mb-0 tab-element">
                  <a  href="{{ route('add_article') }}">
                      <span class="left-menu-title">Articles</span>
                    </a>
                  </h6>
                </div>
              </div>
            </div>
            <div class="content-block">
              <div class="card">
                <div class="card-header tab-container {!! request()->route()->getName()=='personal_information.index'? 'custom-active': "" !!}" role="tab" id="content-nav">
                  <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/personal-info.svg') }}" >
                  {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-stats-icon') }}"></use></svg>--}}
                  <h6 class="mb-0 tab-element">
                    <a href="{{ route('personal_information.index') }}"
                       class="h6 title {!! request()->route()->getName()=='dashboard.index'? 'active': "" !!}">
                      @if(auth()->user()->profile_type=='company')
                        Company
                      @else
                        Personal
                      @endif
                      Information
                    </a>
                  </h6>
                </div>
              </div>
            </div>

            @if(auth()->user()->profile_type=='freelancer')
              <div class="content-block">
                <div class="card">
                  <div class="card-header tab-container {!! request()->route()->getName()=='user_history.index'? 'custom-active': "" !!}" role="tab" id="content-nav">
                    <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/education.svg') }}">
                    {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-settings-icon') }}"></use></svg>--}}
                    <h6 class="mb-0 tab-element">
                      <a href="{{ route('user_history.index') }}"
                         class="h6 title {!! request()->route()->getName()=='user_history.index'? 'active': "" !!}">
                        Education
                        & Employment
                      </a>
                    </h6>
                  </div>
                </div>
              </div>
            @endif
            
            <div id="tools-block" role="tablist" aria-multiselectable="true">
            <div class="content-block">
              <div class="card">
                <div class="card-header tab-container {!! request()->route()->getName()=='community.projects.list'? 'custom-active': "" !!}" role="tab" id="content-nav">
                  <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/Projects.svg') }}" >
                  {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-happy-face-icon') }}"></use></svg>--}}
                  <h6 class="mb-0 tab-element">
                    <a href="{{ route('projects.index') }}">
                      <span class="left-menu-title">Projects</span>
                    </a>
                  </h6>
                </div>
              </div>
            </div>
              <div class="card">
                <div class="card-header tab-container {{ in_array(request()->route()->getName(), ['tool.quiz', 'tools.water_mark', 'tools.domain.index'])? 'custom-active': ''  }}" role="tab" id="tools-nav">
                  <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/tools.svg') }}" >
                  {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-heart-icon') }}"></use></svg>--}}
                  <h6 class="mb-0 tab-element">
                    <a href="{{ route('tool.quiz') }}">
                      Tools
                    </a>
                  </h6>
                </div>
              </div>
            </div>

            @if(Auth::user()->has_quiz)
              <div class="content-block">
                <div class="card">
                  <div class="card-header tab-container {!! request()->route()->getName()=='orders'? 'custom-active': "" !!}" role="tab" id="content-nav">
                    <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/order.svg') }}">
                    {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-happy-sticker-icon') }}"></use></svg>--}}
                    <h6 class="mb-0 tab-element">
                      <a href="{{ route('orders' , Auth::user()->id) }}"
                         class="h6 title">
                        Orders
                      </a>
                    </h6>
                    <a href="#" class="items-round-little bg-blue" style="margin-top: 7px;">{{ \App\Order::where('user_id' , Auth::user()->id)->count() }}</a>
                  </div>
                </div>
              </div>
            @endif
            <div class="content-block">
              <div class="card">
                <div class="card-header tab-container {!! in_array(request()->route()->getName(), ['messages.index', 'messages.show'])? 'custom-active': "" !!}" role="tab" id="content-nav">
                  <svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-chat---messages-icon') }}"></use></svg>
                  <h6 class="mb-0 tab-element">
                    <a href="{{ route('messages.index') }}"
                       class="h6 title">
                      Message
                    </a>
                  </h6>
                </div>
              </div>
            </div>
            <div class="content-block">
              <div class="card">
                <div class="card-header tab-container {!! in_array(request()->route()->getName(), ['profile.followers-following'])? 'custom-active': "" !!}" role="tab" id="content-nav">
                  <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/following-followers.svg') }}">
                  {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-happy-faces-icon') }}"></use></svg>--}}
                  <h6 class="mb-0 tab-element">
                    <a href="{{ route('profile.followers-following') }}"
                       class="h6 title">
                      Followers/Following
                    </a>
                  </h6>
                </div>
              </div>
            </div>
            <div class="content-block">
              <div class="card">
                <div class="card-header tab-container {!! in_array(request()->route()->getName(), ['statistics.index'])? 'custom-active': "" !!}" role="tab" id="content-nav">
                  <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/statistics.svg') }}">
                  {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-status-icon') }}"></use></svg>--}}
                  <h6 class="mb-0 tab-element">
                    <a href="{{ route('statistics.index') }}"
                       class="h6 title">
                      Statistics
                    </a>
                  </h6>
                </div>
              </div>
            </div>
          </div>
        </div>

        {{--<div class="profile-completion">--}}

        {{--<div class="skills-item">--}}
        {{--<div class="skills-item-info">--}}
        {{--<span class="skills-item-title">Profile Completion</span>--}}
        {{--<span class="skills-item-count"><span class="count-animate" data-speed="1000" data-refresh-interval="50" data-to="76" data-from="0"></span><span class="units">76%</span></span>--}}
        {{--</div>--}}
        {{--<div class="skills-item-meter">--}}
        {{--<span class="skills-item-meter-active bg-primary" style="width: 76%"></span>--}}
        {{--</div>--}}
        {{--</div>--}}

        {{--<span>Complete <a href="#">your profile</a> so people can know more about you!</span>--}}

        {{--</div>--}}
      </div>
    </div>

  </div>
</div>


<div class="fixed-nav-menu d-lg-none d-block">
  <div class="container">
    <div class="row">
    <div class="col l2">
      <a href="#" class="menu-open-fixed">
        <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
        <g>
          <g>
            <path d="M491.318,235.318H20.682C9.26,235.318,0,244.577,0,256s9.26,20.682,20.682,20.682h470.636
              c11.423,0,20.682-9.259,20.682-20.682C512,244.578,502.741,235.318,491.318,235.318z"/>
          </g>
        </g>
        <g>
          <g>
            <path d="M491.318,78.439H20.682C9.26,78.439,0,87.699,0,99.121c0,11.422,9.26,20.682,20.682,20.682h470.636
              c11.423,0,20.682-9.26,20.682-20.682C512,87.699,502.741,78.439,491.318,78.439z"/>
          </g>
        </g>
        <g>
          <g>
            <path d="M491.318,392.197H20.682C9.26,392.197,0,401.456,0,412.879s9.26,20.682,20.682,20.682h470.636
              c11.423,0,20.682-9.259,20.682-20.682S502.741,392.197,491.318,392.197z"/>
          </g>
        </g>
        </svg>
      </a>
    </div>
    <div class="col l2">
    <a href="{{ route('profile.show') }}" class="{!! request()->route()->getName()=='profile.show'? 'active': "" !!}">
        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
          viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
        <g>
          <g>
            <path d="M437.02,330.98c-27.883-27.882-61.071-48.523-97.281-61.018C378.521,243.251,404,198.548,404,148
              C404,66.393,337.607,0,256,0S108,66.393,108,148c0,50.548,25.479,95.251,64.262,121.962
              c-36.21,12.495-69.398,33.136-97.281,61.018C26.629,379.333,0,443.62,0,512h40c0-119.103,96.897-216,216-216s216,96.897,216,216
              h40C512,443.62,485.371,379.333,437.02,330.98z M256,256c-59.551,0-108-48.448-108-108S196.449,40,256,40
              c59.551,0,108,48.448,108,108S315.551,256,256,256z"/>
          </g>
        </g>
        </svg>
    </a>
    </div>
    <div class="col l2">
       <a href="{{ route('messages.index') }}">
          <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
              width="81.332px" height="81.332px" viewBox="0 0 81.332 81.332" style="enable-background:new 0 0 81.332 81.332;"
              xml:space="preserve">
            <g>
              <path d="M79.907,60.396v16.011l-0.109-0.001l-1.031,0.523c-0.521,0.264-8.934,4.402-23.449,4.402c-7.43,0-16.46-1.085-26.852-4.33
                l-1.104-0.346l-0.042-0.249V60.396c0-10.89,8.86-19.748,19.75-19.748h13.088C71.048,40.648,79.907,49.508,79.907,60.396z
                M27.912,49.207c-10.039-3.352-17.009-9.146-19.688-16.351c-4.389-3.632-6.799-8.45-6.799-13.615C1.425,8.632,11.828,0,24.615,0
                c9.246,0,17.241,4.515,20.963,11.024c2.343-1.435,5.088-2.277,8.036-2.277c8.519,0,15.424,6.906,15.424,15.425
                s-6.906,15.425-15.424,15.425c-5.27,0-9.918-2.646-12.699-6.679c-2.829,2.325-6.386,4.071-10.441,4.945
                c-1.065,0.229-2.167,0.396-3.295,0.496c0.583,2.276,1.482,5.193,2.726,8.843c0.193,0.567,0.048,1.196-0.375,1.621
                c-0.301,0.303-0.706,0.466-1.119,0.466C28.244,49.289,28.076,49.262,27.912,49.207z M25.875,45.081
                c-1.06-3.281-1.785-5.872-2.205-7.867c-0.097-0.459,0.016-0.938,0.308-1.306c0.292-0.367,0.731-0.586,1.201-0.597
                c1.601-0.036,3.159-0.215,4.631-0.532c3.775-0.813,7.055-2.472,9.567-4.675c-0.762-1.827-1.187-3.829-1.187-5.933
                c0-4.427,1.875-8.407,4.862-11.22c-3.056-5.753-10.168-9.797-18.437-9.797C13.568,3.154,4.58,10.371,4.58,19.24
                c0,4.32,2.117,8.38,5.96,11.432c0.235,0.187,0.413,0.437,0.511,0.721C13.029,37.12,18.242,41.908,25.875,45.081z"/>
            </g>
            </svg>
       </a>
    </div>
    <div class="col l2">
      <a href="{{ route('community.articles.list') }}">
         <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 64 64" width="512" height="512"><g id="Outline"><path d="M5.29,5.29a1.032,1.032,0,0,0-.21.33.942.942,0,0,0,0,.76.933.933,0,0,0,.21.33,1.014,1.014,0,0,0,1.42,0,1.014,1.014,0,0,0,0-1.42A1.047,1.047,0,0,0,5.29,5.29Z"/><path d="M8.62,5.08a.933.933,0,0,0-.33.21,1.014,1.014,0,0,0,0,1.42A1,1,0,0,0,10,6a1.052,1.052,0,0,0-.29-.71A1.017,1.017,0,0,0,8.62,5.08Z"/><path d="M11.62,5.08a1.032,1.032,0,0,0-.33.21,1.014,1.014,0,0,0,0,1.42,1.155,1.155,0,0,0,.33.21A1,1,0,0,0,12,7a.99.99,0,0,0,1-1,1.052,1.052,0,0,0-.29-.71A1.037,1.037,0,0,0,11.62,5.08Z"/><rect x="15" y="19" width="14" height="2"/><rect x="15" y="25" width="14" height="2"/><rect x="15" y="31" width="19" height="2"/><rect x="15" y="37" width="13" height="2"/><rect x="15" y="43" width="8" height="2"/><rect x="15" y="49" width="6" height="2"/><path d="M58.171,14a3.8,3.8,0,0,0-2.707,1.122L52,18.586V5a3,3,0,0,0-3-3H5A3,3,0,0,0,2,5V59a3,3,0,0,0,3,3H49a3,3,0,0,0,3-3V29.414l8.878-8.878A3.828,3.828,0,0,0,58.171,14ZM4,5A1,1,0,0,1,5,4H49a1,1,0,0,1,1,1V8H4ZM50,59a1,1,0,0,1-1,1H5a1,1,0,0,1-1-1V10H50V20.586l-6,6v-3L33.414,13H10V57H44V37.414l6-6ZM26.126,44.46l-2.707,8.121,8.121-2.707L34.414,47,42,39.414V55H12V15H32V25H42v3.586l-13,13ZM29,44.414,31.586,47,30.46,48.126l-3.879,1.293,1.293-3.879Zm5-28L40.586,23H34ZM33,45.586,30.414,43,54,19.414,56.586,22ZM59.464,19.122,58,20.586,55.414,18l1.464-1.464a1.829,1.829,0,1,1,2.586,2.586Z"/></g></svg>
      </a>
    </div>
    <div class="col l2">
      <a href="{{ route('community.projects.list') }}">
        <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
        viewBox="0 0 57 57" style="enable-background:new 0 0 57 57;" xml:space="preserve">
      <g>
        <path d="M5,53h47V19H5V53z M50,51H8.649l14.324-12.611l10.302,10.301c0.391,0.391,1.023,0.391,1.414,0s0.391-1.023,0-1.414
          l-4.807-4.807l9.181-10.054L50,42.44V51z M7,21h43v18.727l-10.324-9.464c-0.196-0.179-0.458-0.28-0.72-0.262
          c-0.265,0.012-0.515,0.129-0.694,0.325l-9.794,10.727l-4.743-4.743c-0.374-0.373-0.972-0.391-1.368-0.044L7,49.787V21z"/>
        <path d="M15,24c-3.071,0-5.569,2.498-5.569,5.569c0,3.07,2.498,5.568,5.569,5.568s5.569-2.498,5.569-5.568
          C20.569,26.498,18.071,24,15,24z M15,33.138c-1.968,0-3.569-1.601-3.569-3.568S13.032,26,15,26s3.569,1.602,3.569,3.569
          S16.968,33.138,15,33.138z"/>
        <path d="M49.38,15c-1.79-1.586-11.313-9.958-16.748-13.415C31.72,0.615,30.434,0,29,0s-2.72,0.615-3.632,1.585
          C19.933,5.042,10.41,13.414,8.62,15H1v42h55V15H49.38z M26.069,4.369c0.009-0.043,0.028-0.083,0.039-0.125
          c0.145-0.535,0.415-0.949,0.66-1.224c0.005-0.005,0.006-0.012,0.011-0.018C27.328,2.391,28.116,2,29,2s1.672,0.391,2.221,1.002
          c0.005,0.005,0.006,0.012,0.011,0.018c0.245,0.275,0.515,0.69,0.66,1.225c0.011,0.042,0.029,0.082,0.039,0.125
          C31.974,4.564,32,4.774,32,5c0,1.654-1.346,3-3,3s-3-1.346-3-3C26,4.774,26.026,4.564,26.069,4.369z M24,4.95
          c0,0.017,0,0.033,0,0.05c0,2.757,2.243,5,5,5s5-2.243,5-5c0-0.017,0-0.033,0-0.05c4.17,3.033,9.504,7.581,12.345,10.05H11.655
          C14.496,12.531,19.831,7.983,24,4.95z M54,55H3V17h6h40h5V55z"/>
      </g>
    </svg>
      </a>
    </div>
    </div>
  </div>
</div>
