<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 12/1/18
 * Time: 6:23 PM
 */
?>
<title>El Designers</title>
<link rel="shortcut icon" type="image/png" href="{{ asset('website/wing-favicon.ico') }}"/>
<!-- Required meta tags always come first -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="{{ asset('/Bootstrap/dist/css/bootstrap-reboot.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/Bootstrap/dist/css/bootstrap.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/Bootstrap/dist/css/bootstrap-grid.css') }}">

<!-- Main Styles CSS -->
<link rel="stylesheet" type="text/css" href="{{ asset('/css/main.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/fonts.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/eldesigner.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('/css/animate.css') }}">
{{--<link rel="stylesheet" href="//www.fuelcdn.com/fuelux/3.13.0/css/fuelux.min.css">--}}

<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Main Font -->
<script src="{{ asset('/js/webfontloader.min.js') }}"></script>
<script>
    WebFont.load({
        google: {
            families: ['Roboto:300,400,500,700:latin']
        }
    });
</script>
