<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 12/1/18
 * Time: 7:03 PM
 */
?>
    <div class="fixed-sidebar-left sidebar--small" id="sidebar-left">

        <a href="/timeline" class="logo" style="background-color: #3f4159 !important;">
            <div class="img-wrap">
                <img src="{{ asset('website/Logo-blue.png') }}" alt="Eldesigners">

            </div>
        </a>

        <div class="mCustomScrollbar" data-mcs-theme="dark" style="width: 80px;">
            <ul class="ui-block left-menu">
                <li>
                    <a href="#" class="js-sidebar-open">
                        <svg class="olymp-menu-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="OPEN MENU"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-popup-right-arrow') }}"></use></svg>
                    </a>
                </li>
                <li class="{!! request()->route()->getName()=='profile.show'? 'custom-active': "" !!}">
                    <a href="{{ route('profile.show') }}">
                        <img class="olymp-star-icon left-menu-icon" data-toggle="tooltip" data-placement="right"   data-original-title="Profile" src="{{ asset('/svg-icons/custom_icons/Profile.svg') }}">
                        {{--<svg class="olymp-star-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="Profile"><use xlink:href=""></use></svg>--}}
                    </a>
                </li>
                <li class="{!! request()->route()->getName()=='dashboard.index'? 'custom-active': "" !!}">
                    <a href="{{ route('dashboard.index') }}">
                        <img class="olymp-star-icon left-menu-icon" data-toggle="tooltip" data-placement="right"   data-original-title="Website Generator" src="{{ asset('/svg-icons/custom_icons/generator.svg') }}">
                        {{--<svg class="olymp-calendar-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="Website Generator"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-manage-widgets-icon') }}"></use></svg>--}}
                    </a>
                </li>
                <li class="{!! in_array(request()->route()->getName(), ['welcome.index', 'setting.seo', 'services.index', 'skills', 'members', 'user_services', 'contact_us', 'themes.index', 'style.index'])? 'custom-active': '' !!}">
                    <a href="#" class="js-sidebar-open">
                        <img class="olymp-star-icon left-menu-icon" data-toggle="tooltip" data-placement="right"   data-original-title="Website Data" src="{{ asset('/svg-icons/custom_icons/website-data.svg') }}">
                        {{--<svg class="olymp-calendar-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="Website Data"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-settings-v2-icon') }}"></use></svg>--}}
                    </a>
                </li>
                <li class="{!! in_array(request()->route()->getName(), ['projects.index' , 'projects.library'])? 'custom-active': '' !!}">
                    <a href="#" class="js-sidebar-open">
                        <img class="olymp-star-icon left-menu-icon" data-toggle="tooltip" data-placement="right"   data-original-title="Projects" src="{{ asset('/svg-icons/custom_icons/Projects.svg') }}">
                        {{--<svg class="olymp-calendar-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="Projects"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-multimedia-icon') }}"></use></svg>--}}
                    </a>
                </li>
                <li class="{{ strpos(request()->route()->uri, 'article')!==false? 'custom-active': ''  }}">
                    <a href="#" class="js-sidebar-open">
                        <img class="olymp-star-icon left-menu-icon" data-toggle="tooltip" data-placement="right"   data-original-title="Articles" src="{{ asset('/svg-icons/custom_icons/articles.svg') }}">
                        {{--<svg class="olymp-calendar-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="Articles"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-blog-icon') }}"></use></svg>--}}
                    </a>
                </li>
                <li class="{!! request()->route()->getName()=='personal_information.index'? 'custom-active': "" !!}">
                    <a href="{{ route('personal_information.index') }}">
                        <img class="olymp-star-icon left-menu-icon" data-toggle="tooltip" data-placement="right"   data-original-title="Personal Information" src="{{ asset('/svg-icons/custom_icons/personal-info.svg') }}">
                        {{--<svg class="olymp-badge-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="Personal Information"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-stats-icon') }}"></use></svg>--}}
                    </a>
                </li>

                @if(auth()->user()->profile_type=='freelancer')
                    <li class="{!! request()->route()->getName()=='user_history.index'? 'custom-active': "" !!}">
                        <a href="{{ route('user_history.index') }}">
                            <img class="olymp-star-icon left-menu-icon" data-toggle="tooltip" data-placement="right"   data-original-title="Education & Employment" src="{{ asset('/svg-icons/custom_icons/education.svg') }}">
                            {{--<svg class="olymp-happy-faces-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="Education & Employment"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-settings-icon') }}"></use></svg>--}}
                        </a>
                    </li>
                @endif
                <li class="{{ in_array(request()->route()->getName(), ['pricing.bundles'])? 'custom-active': ''  }}">
                    <a href="#" class="js-sidebar-open">
                        <img class="olymp-star-icon left-menu-icon" data-toggle="tooltip" data-placement="right"   data-original-title="Pricing" src="{{ asset('/svg-icons/custom_icons/pricing.svg') }}">
                        {{--<svg class="olymp-calendar-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="Tools"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-heart-icon') }}"></use></svg>--}}
                    </a>
                </li>
                <li class="{{ in_array(request()->route()->getName(), ['tool.quiz', 'tools.water_mark', 'tools.domain.index'])? 'custom-active': ''  }}">
                    <a href="#" class="js-sidebar-open">
                        <img class="olymp-star-icon left-menu-icon" data-toggle="tooltip" data-placement="right"   data-original-title="Tools" src="{{ asset('/svg-icons/custom_icons/tools.svg') }}">
                        {{--<svg class="olymp-calendar-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="Tools"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-heart-icon') }}"></use></svg>--}}
                    </a>
                </li>
                @if(Auth::user()->has_quiz)
                    <li class="{!! request()->route()->getName()=='orders'? 'custom-active': "" !!}">
                        <a href="{{ route('orders' , Auth::user()->id) }}">
                            <img class="olymp-star-icon left-menu-icon" data-toggle="tooltip" data-placement="right"   data-original-title="Orders" src="{{ asset('/svg-icons/custom_icons/order.svg') }}">
{{--                            <svg class="olymp-happy-faces-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="Orders"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-happy-sticker-icon') }}"></use></svg>--}}
                        </a>
                    </li>
                @endif
                <li class="{!! request()->route()->getName()=='messages.index'? 'custom-active': "" !!}">
                    <a href="{{ route('messages.index') }}">
                        <svg class="olymp-happy-faces-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="Messages"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-chat---messages-icon') }}"></use></svg>
                    </a>
                </li>
                <li class="{!! request()->route()->getName()=='profile.followers-following'? 'custom-active': "" !!}">
                    <a href="{{ route('profile.followers-following') }}">
                        <img class="olymp-star-icon left-menu-icon" data-toggle="tooltip" data-placement="right"   data-original-title="Followers/Following" src="{{ asset('/svg-icons/custom_icons/following-followers.svg') }}">
                        {{--<svg class="olymp-happy-faces-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="Followers/Following"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-happy-faces-icon') }}"></use></svg>--}}
                    </a>
                </li>
                <li class="{!! request()->route()->getName()=='statistics.index'? 'custom-active': "" !!}">
                    <a href="{{ route('statistics.index') }}">
                        <img class="olymp-star-icon left-menu-icon" data-toggle="tooltip" data-placement="right"   data-original-title="Statistics" src="{{ asset('/svg-icons/custom_icons/statistics.svg') }}">
                        {{--<svg class="olymp-happy-faces-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="Statistics"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-status-icon') }}"></use></svg>--}}
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="fixed-sidebar-left sidebar--large" id="sidebar-left-1">
        <a href="{{ route('timeline') }}" class="logo" style="background-color: #3f4159 !important;">
            <div class="img-wrap">
                <img style="height: 39px;" src="{{ asset('website/Logo-blue.png') }}" alt="Eldesingers">

            </div>
            <div class="title-block">
                <h6 class="logo-title">Eldesigners</h6>
            </div>
        </a>

        <div class="mCustomScrollbar" data-mcs-theme="dark">
            <div class="ui-block">
                <div class="your-profile">
                    <div class="content-block">
                        <div class="card">
                            <div class="card-header tab-container" role="tab" id="content-nav">
                            <a href="#" class="js-sidebar-open">
                            <svg id="CloseMenu" class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 25px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use></svg>
                                <h6 class="mb-0 tab-element close-menu">
            
                                        <span class="left-menu-title">Collapse Menu</span>
                                    
                                </h6>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="content-block">
                        <div class="card">
                            <div class="card-header tab-container {!! request()->route()->getName()=='profile.show'? 'custom-active': "" !!}" role="tab" id="content-nav">
                                <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/Profile.svg') }}" >
                                {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-happy-face-icon') }}"></use></svg>--}}
                                <h6 class="mb-0 tab-element">
                                    <a href="{{ route('profile.show') }}">
                                        <span class="left-menu-title">Profile</span>
                                    </a>
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div class="content-block">
                        <div class="card">
                            <div class="card-header tab-container {!! request()->route()->getName()=='dashboard.index'? 'custom-active': "" !!}" role="tab" id="content-nav">
                                <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/generator.svg') }}" >
                                {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-manage-widgets-icon') }}"></use></svg>--}}
                                <h6 class="mb-0 tab-element">
                                    <a href="{{ route('dashboard.index') }}"
                                       class="h6 title {!! request()->route()->getName()=='dashboard.index'? 'active': "" !!}">Website
                                        Generator</a>
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div id="content-block" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header tab-container {!! in_array(request()->route()->getName(), ['welcome.index', 'setting.seo', 'services.index', 'skills', 'members', 'user_services', 'contact_us', 'themes.index', 'style.index'])? 'custom-active': '' !!}" role="tab" id="content-nav">
                                <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/website-data.svg') }}" >
                                {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-settings-v2-icon') }}"></use></svg>--}}
                                <h6 class="mb-0 tab-element" >
                                <a href="{{ route('welcome.index') }}">
                                <span class="left-menu-title">Website Data</span>
                                </a>
                                </h6>
                            </div>
                        </div>
                    </div>

                    <div id="content-block" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header tab-container {!! in_array(request()->route()->getName(), ['projects.index' , 'projects.library'])? 'custom-active': '' !!}" role="tab" id="project-nav">
                                <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/Projects.svg') }}" >
                                {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-multimedia-icon') }}"></use></svg>--}}
                                <h6 class="mb-0 tab-element">
                                 <a href="{{ route('projects.index') }}">
                                    <span class="left-menu-title">Projects</span>
                                </a>
                                </h6>
                            </div>
                        </div>
                    </div>


                    <div id="articles-block" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header tab-container {{ strpos(request()->route()->uri, 'article')!==false? 'custom-active': ''  }}" role="tab" id="article-nav">
                                <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/articles.svg') }}" >
                                {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-blog-icon') }}"></use></svg>--}}
                                <h6 class="mb-0 tab-element">
                                <a  href="{{ route('add_article') }}">
                                <span class="left-menu-title">Articles</span>
                                </a>
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div class="content-block">
                        <div class="card">
                            <div class="card-header tab-container {!! request()->route()->getName()=='personal_information.index'? 'custom-active': "" !!}" role="tab" id="content-nav">
                                <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/personal-info.svg') }}" >
                                {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-stats-icon') }}"></use></svg>--}}
                                <h6 class="mb-0 tab-element">
                                    <a href="{{ route('personal_information.index') }}"
                                       class="h6 title {!! request()->route()->getName()=='dashboard.index'? 'active': "" !!}">
                                        @if(auth()->user()->profile_type=='company')
                                            Company
                                        @else
                                            Personal
                                        @endif
                                        Information
                                    </a>
                                </h6>
                            </div>
                        </div>
                    </div>

                    @if(auth()->user()->profile_type=='freelancer')
                        <div class="content-block">
                            <div class="card">
                                <div class="card-header tab-container {!! request()->route()->getName()=='user_history.index'? 'custom-active': "" !!}" role="tab" id="content-nav">
                                    <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/education.svg') }}">
                                    {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-settings-icon') }}"></use></svg>--}}
                                    <h6 class="mb-0 tab-element">
                                        <a href="{{ route('user_history.index') }}"
                                           class="h6 title {!! request()->route()->getName()=='user_history.index'? 'active': "" !!}">
                                            Education
                                            & Employment
                                        </a>
                                    </h6>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div id="pricing-block" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header tab-container {{ in_array(request()->route()->getName(), ['pricing.bundles'])? 'custom-active': ''  }}" role="tab" id="tools-nav">
                                <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/pricing.svg') }}" >
                                {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-heart-icon') }}"></use></svg>--}}
                                <h6 class="mb-0 tab-element">
                                    <a href="{{ route('pricing.bundles') }}">
                                        Pricing
                                    </a>
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div id="tools-block" role="tablist" aria-multiselectable="true">
                        <div class="card">
                            <div class="card-header tab-container {{ in_array(request()->route()->getName(), ['tool.quiz', 'tools.water_mark', 'tools.domain.index'])? 'custom-active': ''  }}" role="tab" id="tools-nav">
                                <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/tools.svg') }}" >
                                {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-heart-icon') }}"></use></svg>--}}
                                <h6 class="mb-0 tab-element">
                                    <a  href="{{ route('tool.quiz') }}">
                                        Tools
                                    </a>
                                </h6>
                            </div>
                        </div>
                    </div>

                    @if(Auth::user()->has_quiz)
                        <div class="content-block">
                            <div class="card">
                                <div class="card-header tab-container {!! request()->route()->getName()=='orders'? 'custom-active': "" !!}" role="tab" id="content-nav">
                                    <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/order.svg') }}">
                                    {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-happy-sticker-icon') }}"></use></svg>--}}
                                    <h6 class="mb-0 tab-element">
                                        <a href="{{ route('orders' , Auth::user()->id) }}"
                                           class="h6 title">
                                            Orders
                                        </a>
                                    </h6>
                                    <a href="#" class="items-round-little bg-blue" style="margin-top: 7px;">{{ \App\Order::where('user_id' , Auth::user()->id)->count() }}</a>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="content-block">
                        <div class="card">
                            <div class="card-header tab-container {!! in_array(request()->route()->getName(), ['messages.index', 'messages.show'])? 'custom-active': "" !!}" role="tab" id="content-nav">
                                <svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-chat---messages-icon') }}"></use></svg>
                                <h6 class="mb-0 tab-element">
                                    <a href="{{ route('messages.index') }}"
                                       class="h6 title">
                                        Message
                                    </a>
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div class="content-block">
                        <div class="card">
                            <div class="card-header tab-container {!! in_array(request()->route()->getName(), ['profile.followers-following'])? 'custom-active': "" !!}" role="tab" id="content-nav">
                                <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/following-followers.svg') }}">
                                {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-happy-faces-icon') }}"></use></svg>--}}
                                <h6 class="mb-0 tab-element">
                                    <a href="{{ route('profile.followers-following') }}"
                                       class="h6 title">
                                        Followers/Following
                                    </a>
                                </h6>
                            </div>
                        </div>
                    </div>
                    <div class="content-block">
                        <div class="card">
                            <div class="card-header tab-container {!! in_array(request()->route()->getName(), ['statistics.index'])? 'custom-active': "" !!}" role="tab" id="content-nav">
                                <img class="olymp-headphones-icon left-menu-icon" style="float: left;width: 25px;height: 37px;" src="{{ asset('/svg-icons/custom_icons/statistics.svg') }}">
                                {{--<svg class="olymp-headphones-icon left-menu-icon" style="float: left;width: 13px;" data-toggle="tooltip" data-placement="left"   ><use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-status-icon') }}"></use></svg>--}}
                                <h6 class="mb-0 tab-element">
                                    <a href="{{ route('statistics.index') }}"
                                       class="h6 title">
                                        Statistics
                                    </a>
                                </h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{--<div class="profile-completion">--}}

                {{--<div class="skills-item">--}}
                    {{--<div class="skills-item-info">--}}
                        {{--<span class="skills-item-title">Profile Completion</span>--}}
                        {{--<span class="skills-item-count"><span class="count-animate" data-speed="1000" data-refresh-interval="50" data-to="76" data-from="0"></span><span class="units">76%</span></span>--}}
                    {{--</div>--}}
                    {{--<div class="skills-item-meter">--}}
                        {{--<span class="skills-item-meter-active bg-primary" style="width: 76%"></span>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<span>Complete <a href="#">your profile</a> so people can know more about you!</span>--}}

            {{--</div>--}}
        </div>
    </div>
