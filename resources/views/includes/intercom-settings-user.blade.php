<script>
    window.intercomSettings = {
        app_id: "bakm66wx",
        user_id: '{{ Auth::user()->id }}',
        user_hash: '{{ Auth::user()->user_hash?Auth::user()->user_hash:hash_hmac(
            'sha256', // hash function
            Auth::user()->id, // user's id
            'uuA40syUWvUZybz1bVWmmsa9bO9hp8CzpX91BMnf' // secret key (keep safe!) intercom
        ) }}',
        email: '{{ Auth::user()->email }}',
        name: '{{ Auth::user()->name }}',
        user_profile_type: '{{ Auth::user()->profile_type }}',
        user_selected_plan: 'free',
        has_phone: '{{ Auth::user()->phone }}',
        has_about: '{{ Auth::user()->about}}',
        num_of_projects: '{{ \App\WebsiteProject::where('user_id' , Auth::user()->id)->count() }}',
        has_logo: '{{ Auth::user()->logo?1:0 }}',
        has_cover: '{{ Auth::user()->cover?1:0 }}',
        user_in_category: '{{ Auth::user()->code?:'none' }}',
        member_since: '{{ Auth::user()->created_at?:'0000-00-00' }}',
        last_update: '{{ Auth::user()->updated_at?:'0000-00-00' }}',
    }
</script>