<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 12/1/18
 * Time: 7:07 PM
 */
?>

<div class="fixed-sidebar-right sidebar--small" id="sidebar-right">

    <div class="mCustomScrollbar" data-mcs-theme="dark">
        <side-friends-menu :freiends_data="friends_data" v-on:openfrindchat="openFriendChat"></side-friends-menu>
    </div>

    <div class="search-friend inline-items">
        <a href="#" class="js-sidebar-open side_menu_friends" @click="friendMenu(true)">
            <svg class="olymp-menu-icon"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-menu-icon') }}"></use></svg>
        </a>
    </div>

    <a href="#" class="olympus-chat inline-items js-chat-open">
        <svg class="olymp-chat---messages-icon"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-chat---messages-icon') }}"></use></svg>
    </a>

</div>



<div class="fixed-sidebar-right sidebar--large" id="sidebar-right-1">

    <div class="mCustomScrollbar" data-mcs-theme="dark">
        <side-friends-menu-full :freiends_data="friends_data" v-on:openfrindchat="openFriendChat">></side-friends-menu-full>
    </div>

    <div class="search-friend inline-items">
        <div style="    width: 230px;height: 53px;">
        </div>


        <a href="#" class="js-sidebar-open" @click="friendMenu(false)">
            <svg class="olymp-close-icon" ><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use></svg>
        </a>
    </div>

    <a href="#" class="olympus-chat inline-items js-chat-open">

        <h6 class="olympus-chat-title">ELDISIGNERS</h6>
        <svg class="olymp-chat---messages-icon"><use xlink:href="{{ asset('/svg-icons/sprites/icons.svg#olymp-chat---messages-icon') }}"></use></svg>
    </a>

</div>
</div>
<!-- <chat-modal-pop-up :friends_messages="friends_messages" v-on:sidemessagesent="sideMessageSent" ></chat-modal-pop-up> -->
<pop-up :friends_messages="friends_messages" :friend_info="friend_info" v-on:sidemessagesent="sideMessageSent"></pop-up>
