<a href="#" class="fire_open_post_modal" data-toggle="modal" data-target="#open-post_modal" style="visibility: hidden;"></a>
<div class="modal fade" id="open-post_modal" tabindex="-1" role="dialog" aria-labelledby="open-post_modal" aria-hidden="true">
    <div class="modal-dialog window-popup open-photo-popup open-photo-popup-v2" role="document">
        <div class="modal-content">
            <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
                <svg class="olymp-close-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
            </a>
            @foreach($posts as $post)
                @php
                    $post_loves = \App\Love::resourceLoves($post->id , 'post');
                    $isCurrentLoved = \App\Love::isCurrentLoved($post->id , 'post');
                    $user = \App\User::find($post->user_id);
                    $comments = $post->comments($post->id);
                    $medias = $post->medias($post->id);
                    $images = array();
                    $video = "";
                foreach ($medias as $media){
                    if($media->media_type == 'image'){
                        $images[] = $media;
                    }else{
                        $video = $media->file_name;
                    }
                }
                @endphp
            <div class="modal-body">
                <div class="open-photo-thumb">
                    <div class="swiper-container" data-slide="fade">
                        <div class="swiper-wrapper">

                            <?php $count_images = count($images); ?>
                            @if($count_images)
                                @foreach($images as $image)
                                    <div class="swiper-slide">
                                        <div class="photo-item">
                                            <img src="{{ asset('user_media/' . $image['file_name']) }}" alt="photo">
                                            <div class="overlay"></div>
                                            <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
                                            <a href="#" class="tag-friends" data-toggle="tooltip" data-placement="top"   data-original-title="TAG YOUR FRIENDS">
                                                <svg class="olymp-happy-face-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-happy-face-icon"></use></svg>
                                            </a>

                                            <div class="content">
                                                <a class="h6 title"
                                                   href="/profile/{{$user['user_name'] }}">{{ $user['name'] }}</a>
                                                <time class="published" datetime="{{ $post->created_at }}">
                                                    {{ \Carbon\Carbon::parse($post->created_at )->diffForHumans() }}
                                                </time>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                            @if($video)
                                <div class="post-video">
                                    <div class="video-thumb f-none">
                                        <video style="width: 100%;height: 351px;" controls>
                                            <source src="{{ asset('user_media/' . $video) }}">
                                        </video>
                                    </div>
                                </div>
                            @endif
                        </div>

                        <!--Prev Next Arrows-->

                        <svg class="btn-next-without olymp-popup-right-arrow"><use xlink:href="svg-icons/sprites/icons.svg#olymp-popup-right-arrow"></use></svg>

                        <svg class="btn-prev-without olymp-popup-left-arrow"><use xlink:href="svg-icons/sprites/icons.svg#olymp-popup-left-arrow"></use></svg>
                    </div>
                </div>

                <div class="open-photo-content">

                    <article class="hentry post">

                        <div class="post__author author vcard inline-items">
                            @if($user['logo'])
                                <img alt="author" src="{{asset($user['logo'])}}" class="avatar">
                            @else
                                <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                            @endif
                            <div class="author-date">
                                <?php $action_text = count($images) ? ' uploaded ' . count($images) . ' new photos' : ($video ? ' shared a new video' : ' shared a new post') ?>
                                <a class="h6 post__author-name fn"
                                   href="/profile/{{$user['user_name'] }}">{{ $user['name'] }}</a><a
                                        href="#">{{ $action_text }}</a>
                                <div class="post__date">
                                    <time class="published" datetime="{{ $post->created_at }}">
                                        {{ \Carbon\Carbon::parse($post->created_at )->diffForHumans() }}
                                    </time>
                                </div>
                            </div>

                        </div>
                        <p>
                            {{ $post->post_text?:'' }}
                        </p>

                        <div class="post-additional-info inline-items">
                            <a href="#" class="post-add-icon inline-items love-post {{ $isCurrentLoved?'loved':'' }}"
                               data-post_id="{{ $post->id }}"
                               style="{{ $isCurrentLoved?'fill:red;':'' }}"
                               title="Love">
                                <svg class="olymp-heart-icon">
                                    <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-heart-icon') }}"></use>
                                </svg>
                                <span class="num_of_loves-{{ $post->id }}">{{ count($post_loves)?:0 }}</span>
                            </a>
                            <div class="comments-shared">
                                <a class="post-add-icon inline-items" title="Comment">
                                    <svg class="olymp-speech-balloon-icon">
                                        <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-speech-balloon-icon') }}"></use>
                                    </svg>
                                    <span>{{ count($comments)?:0 }}</span>
                                </a>
                                <a class="post-add-icon inline-items" title="Share">
                                    <svg class="olymp-share-icon">
                                        <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-share-icon') }}"></use>
                                    </svg>
                                    <span>0</span>
                                </a>
                            </div>
                        </div>
                        <div class="control-block-button post-control-button">

                            <a href="#" class="btn btn-control love-post {{ $isCurrentLoved?'loved':'' }}"
                               data-post_id="{{ $post->id }}"
                               style="{{ $isCurrentLoved?'fill:red;':'' }}"
                               title="Love">
                                <svg class="olymp-like-post-icon">
                                    <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-like-post-icon') }}"></use>
                                </svg>
                            </a>

                            <a class="btn btn-control" title="Comment">
                                <svg class="olymp-comments-post-icon">
                                    <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-comments-post-icon') }}"></use>
                                </svg>
                            </a>

                            <a class="btn btn-control" title="Share">
                                <svg class="olymp-share-icon">
                                    <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-share-icon') }}"></use>
                                </svg>
                            </a>

                        </div>

                    </article>

                    <div class="mCustomScrollbar" data-mcs-theme="dark" style="{{ count($comments) >= 2?'overflow-y:auto !important; max-height: 300px;':'' }}">

                        <ul class="comments-list" id="comments-list-{{ $post->id }}">
                            @foreach($comments as $comment)
                                @php
                                    $comment_loves = \App\Love::resourceLoves($comment->id , 'comment');
                                    $isCurrentLoved = \App\Love::isCurrentLoved($comment->id , 'comment');
                                    $user = \App\User::find($comment->user_id);
                                    $comment_replies = $comment->replies;
                                @endphp
                                <li class="comment-item {{ count($comment_replies)>0?'has-children':'' }}">
                                    <div class="post__author author vcard inline-items">
                                        @if($user->logo)
                                            <img alt="author" src="{{asset( $user->logo)}}" class="avatar">
                                        @else
                                            <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                                        @endif

                                        <div class="author-date">
                                            <a class="h6 post__author-name fn" href="/profile/{{$user->user_name}}">{{ $user->name }}</a>
                                            <div class="post__date">
                                                <time class="published" datetime="{{ $comment->created_at }}">
                                                    {{ \Carbon\Carbon::parse($comment->created_at )->diffForHumans() }}
                                                </time>
                                            </div>
                                        </div>
                                        @if($comment->user_id == Auth::user()->id)
                                            <div class="more">
                                                <svg class="olymp-three-dots-icon">
                                                    <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use>
                                                </svg>
                                                <ul class="more-dropdown">
                                                    <li>
                                                        <a href="#">Edit Comment</a>
                                                    </li>
                                                    <li>
                                                        <a href="#">Delete Comment</a>
                                                    </li>
                                                </ul>
                                            </div>
                                        @endif
                                    </div>

                                    <p>{{ $comment->comment_text }}</p>

                                    @if($comment->image_path)
                                        <div style="padding: 15px">
                                            <img style="max-height: 250px"
                                                 src="{{ asset('user_media_comments/' . $comment->image_path) }}">
                                        </div>
                                    @endif
                                    <a href="#" class="post-add-icon inline-items love-comment {{ $isCurrentLoved?'loved':'' }}"
                                       data-comment_id="{{ $comment->id }}"
                                       style="{{ $isCurrentLoved?'fill:red;':'' }}"
                                    >
                                        <svg class="olymp-heart-icon">
                                            <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-heart-icon') }}"></use>
                                        </svg>
                                        <span class="num_of_loves_comment-{{ $comment->id }}">{{ count($comment_loves)?:0 }}</span>
                                    </a>
                                    <div id="post_loader-reply-{{ $comment->id }}"
                                         style="text-align: center;padding: 15px;display: none;">
                                        <img src="{{ asset('img/post_loader.gif') }}"
                                             style="height: 30px; width: 30px;">
                                    </div>
                                    <a style="cursor: pointer;" class="reply"
                                       data-form-reply="addReply-Comment-{{ $comment->id }}">Reply</a>
                                    <form class="comment-form inline-items bg-white addReply-Comment-{{ $comment->id }}"
                                          data-replies-list="replies_list-{{ $comment->id }}" id="submit-reply"
                                          style="display: none;">
                                        <input type="hidden" value="{{$comment->id}}" name="comment_id">
                                        <div class="post__author author vcard inline-items">
                                            @if($user->logo)
                                                <img alt="author" src="{{asset( $user->logo)}}"
                                                     class="avatar">
                                            @else
                                                <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                                            @endif
                                            <div class="form-group with-icon-right is-empty">
                                                    <textarea class="form-control" name="reply_text"
                                                              placeholder=""></textarea>
                                                <div class="add-options-message">
                                                    <label for="reply-image" class="options-message"
                                                           data-toggle="tooltip"
                                                           data-placement="top"
                                                           data-original-title="ADD PHOTO">
                                                        <svg for="reply-image" class="olymp-camera-icon">
                                                            <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-camera-icon') }}"></use>
                                                        </svg>
                                                        <input data-preview-img="image" type="file"
                                                               name="reply-image" id="reply-image"
                                                               data-preview-class='reply-image-preview-container-{{ $comment->id }}'
                                                               style="display: none;" accept="image/*">
                                                    </label>
                                                </div>
                                                <span class="material-input"></span>
                                            </div>
                                        </div>
                                        <div class="form-group padding40 reply-image-preview-container-{{ $comment->id }}"
                                             style="display: none;">

                                        </div>
                                        <button class="btn btn-md-2 btn-primary" type="submit">Post Comment</button>

                                        <button class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color">
                                            Cancel
                                        </button>

                                    </form>

                                    <ul class="children" id="replies_list-{{ $comment->id }}">
                                        @foreach($comment_replies as $reply)
                                            @php
                                                $reply_loves = \App\Love::resourceLoves($reply->id , 'reply');
                                                $isCurrentLoved = \App\Love::isCurrentLoved($reply->id , 'reply');
                                                $user = \App\User::find($reply->user_id);
                                            @endphp
                                            <li class="comment-item">
                                                <div class="post__author author vcard inline-items">
                                                    @if($user->logo)
                                                        <img alt="author" src="{{asset($user->logo)}}"
                                                             class="avatar">
                                                    @else
                                                        <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                                                    @endif

                                                    <div class="author-date">
                                                        <a class="h6 post__author-name fn"
                                                           href="/profile/{{$user->user_name}}">{{ $user->name }}</a>
                                                        <div class="post__date">
                                                            <time class="published"
                                                                  datetime="{{ $reply->created_at }}">
                                                                {{ \Carbon\Carbon::parse($reply->created_at )->diffForHumans() }}
                                                            </time>
                                                        </div>
                                                    </div>

                                                    @if($reply->user_id == Auth::user()->id)
                                                        <div class="more">
                                                            <svg class="olymp-three-dots-icon">
                                                                <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-three-dots-icon') }}"></use>
                                                            </svg>
                                                            <ul class="more-dropdown">
                                                                <li>
                                                                    <a href="#">Edit Reply</a>
                                                                </li>
                                                                <li>
                                                                    <a href="#">Edit Reply</a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    @endif
                                                </div>

                                                <p>{{ $reply->reply_text }}</p>
                                                @if($reply->image_path)
                                                    <div style="padding: 15px">
                                                        <img style="max-height: 250px"
                                                             src="{{ asset('user_media_replies/' . $reply->image_path) }}">
                                                    </div>
                                                @endif
                                                <a href="#" class="post-add-icon inline-items love-reply {{ $isCurrentLoved?'loved':'' }}"
                                                   data-reply_id="{{ $reply->id }}"
                                                   style="{{ $isCurrentLoved?'fill:red;':'' }}"
                                                >
                                                    <svg class="olymp-heart-icon">
                                                        <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-heart-icon') }}"></use>
                                                    </svg>
                                                    <span class="num_of_loves_reply-{{ $reply->id }}">{{ count($reply_loves)?:0 }}</span>
                                                </a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endforeach
                        </ul>
                        <div id="post_loader-{{ $post->id }}"
                             style="text-align: center;padding: 15px;display: none;">
                            <img src="/img/post_loader.gif" style="height: 30px; width: 30px;">
                        </div>

                    </div>

                    <form class="comment-form inline-items" id="addComment">
                        <input type="hidden" value="{{$post->id}}" name="post_id">
                        <div class="post__author author vcard inline-items">
                            @if($user['logo'])
                                <img alt="author" src="{{asset($user['logo'])}}" class="avatar">
                            @else
                                <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                            @endif
                            <div class="form-group with-icon-right is-empty">
                                <textarea class="form-control" name="comment_text" placeholder=""></textarea>
                                <div class="add-options-message">
                                    <label for="comment-image" class="options-message" data-toggle="tooltip"
                                           data-placement="top"
                                           data-original-title="ADD PHOTO">
                                        <svg for="images" class="olymp-camera-icon">
                                            <use xlink:href="{{ asset('svg-icons/sprites/icons.svg#olymp-camera-icon') }}"></use>
                                        </svg>
                                        <input data-preview-img="image" type="file" name="comment-image"
                                               id="comment-image"
                                               style="display: none;" accept="image/*">
                                    </label>
                                </div>
                                <span class="material-input"></span>
                            </div>
                        </div>
                        <div class="form-group padding40 comment-image-preview-container"
                             style="display: none;">

                        </div>
                        <button class="btn btn-md-2 btn-primary" type="submit">Post Comment</button>

                        <button class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color">
                            Cancel
                        </button>

                    </form>

                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>
