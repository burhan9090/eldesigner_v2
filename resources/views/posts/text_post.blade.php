<style>
    .no-profile-pic {
        background: transparent;
        height: 40px;
        width: 40px;
        border-radius: 50%;
        text-align: center;
        padding: 12px;
        font-size: 15px;
        font-weight: 600;
    }
</style>
<div class="ui-block">
    <!-- Post -->
    <article class="hentry post">

        <div class="post__author author vcard inline-items">
            @if($user->logo)
                <img alt="author" src="{{asset($user->logo)}}" class="avatar">
            @else
                <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
            @endif

            <div class="author-date">
                <a class="h6 post__author-name fn" href="/profile/{{$user->user_name}}">{{ $user->name }}</a>
                <div class="post__date">
                    <time class="published" datetime="2017-03-24T18:18">
                        19 hours ago
                    </time>
                </div>
            </div>

           @if($post->user_id == $user->id)
            <div class="more">
                <svg class="olymp-three-dots-icon">
                    <use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                </svg>
                <ul class="more-dropdown">
                    <li>
                        <a href="#">Edit Post</a>
                    </li>
                    <li>
                        <a href="#">Delete Post</a>
                    </li>
                    <li>
                        <a href="#">Turn Off Notifications</a>
                    </li>
                    <li>
                        <a href="#">Select as Featured</a>
                    </li>
                </ul>
            </div>
           @endif

        </div>

        <p>
            {{ $post->post_text }}
        </p>

        <div class="post-additional-info inline-items">

            <a href="#" class="post-add-icon inline-items">
                <svg class="olymp-heart-icon">
                    <use xlink:href="/svg-icons/sprites/icons.svg#olymp-heart-icon"></use>
                </svg>
                <span>8</span>
            </a>

            <ul class="friends-harmonic">
                <li>
                    <a href="#">
                        <img src="/img/friend-harmonic7.jpg" alt="friend">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="/img/friend-harmonic8.jpg" alt="friend">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="/img/friend-harmonic9.jpg" alt="friend">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="/img/friend-harmonic10.jpg" alt="friend">
                    </a>
                </li>
                <li>
                    <a href="#">
                        <img src="/img/friend-harmonic11.jpg" alt="friend">
                    </a>
                </li>
            </ul>

            <div class="names-people-likes">
                <a href="#">Jenny</a>, <a href="#">Robert</a> and
                <br>6 more liked this
            </div>


            <div class="comments-shared">
                <a href="#" class="post-add-icon inline-items">
                    <svg class="olymp-speech-balloon-icon">
                        <use xlink:href="/svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use>
                    </svg>
                    <span>17</span>
                </a>

                <a href="#" class="post-add-icon inline-items">
                    <svg class="olymp-share-icon">
                        <use xlink:href="/svg-icons/sprites/icons.svg#olymp-share-icon"></use>
                    </svg>
                    <span>24</span>
                </a>
            </div>


        </div>

        <div class="control-block-button post-control-button">

            <a href="#" class="btn btn-control featured-post">
                <svg class="olymp-trophy-icon">
                    <use xlink:href="/svg-icons/sprites/icons.svg#olymp-trophy-icon"></use>
                </svg>
            </a>

            <a href="#" class="btn btn-control">
                <svg class="olymp-like-post-icon">
                    <use xlink:href="/svg-icons/sprites/icons.svg#olymp-like-post-icon"></use>
                </svg>
            </a>

            <a href="#" class="btn btn-control">
                <svg class="olymp-comments-post-icon">
                    <use xlink:href="/svg-icons/sprites/icons.svg#olymp-comments-post-icon"></use>
                </svg>
            </a>

            <a href="#" class="btn btn-control">
                <svg class="olymp-share-icon">
                    <use xlink:href="/svg-icons/sprites/icons.svg#olymp-share-icon"></use>
                </svg>
            </a>

        </div>

    </article>

    <!-- .. end Post -->

    <!-- Comments -->

    <ul class="comments-list">
        <li class="comment-item">
            <div class="post__author author vcard inline-items">
                <img src="/img/avatar10-sm.jpg" alt="author">

                <div class="author-date">
                    <a class="h6 post__author-name fn" href="#">Elaine Dreyfuss</a>
                    <div class="post__date">
                        <time class="published" datetime="2017-03-24T18:18">
                            5 mins ago
                        </time>
                    </div>
                </div>

                <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>

            </div>

            <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium der doloremque laudantium.</p>

            <a href="#" class="post-add-icon inline-items">
                <svg class="olymp-heart-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-heart-icon"></use></svg>
                <span>8</span>
            </a>
            <a href="#" class="reply">Reply</a>
        </li>
        <li class="comment-item has-children">
            <div class="post__author author vcard inline-items">
                <img src="/img/avatar5-sm.jpg" alt="author">

                <div class="author-date">
                    <a class="h6 post__author-name fn" href="#">Green Goo Rock</a>
                    <div class="post__date">
                        <time class="published" datetime="2017-03-24T18:18">
                            1 hour ago
                        </time>
                    </div>
                </div>

                <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>

            </div>

            <p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugiten, sed quia
                consequuntur magni dolores eos qui ratione voluptatem sequi en lod nesciunt. Neque porro
                quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur adipisci velit en lorem ipsum der.
            </p>

            <a href="#" class="post-add-icon inline-items">
                <svg class="olymp-heart-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-heart-icon"></use></svg>
                <span>5</span>
            </a>
            <a href="#" class="reply">Reply</a>

            <ul class="children">
                <li class="comment-item">
                    <div class="post__author author vcard inline-items">
                        <img src="/img/avatar8-sm.jpg" alt="author">

                        <div class="author-date">
                            <a class="h6 post__author-name fn" href="#">Diana Jameson</a>
                            <div class="post__date">
                                <time class="published" datetime="2017-03-24T18:18">
                                    39 mins ago
                                </time>
                            </div>
                        </div>

                        <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>

                    </div>

                    <p>Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>

                    <a href="#" class="post-add-icon inline-items">
                        <svg class="olymp-heart-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-heart-icon"></use></svg>
                        <span>2</span>
                    </a>
                    <a href="#" class="reply">Reply</a>
                </li>
                <li class="comment-item">
                    <div class="post__author author vcard inline-items">
                        <img src="/img/avatar2-sm.jpg" alt="author">

                        <div class="author-date">
                            <a class="h6 post__author-name fn" href="#">Nicholas Grisom</a>
                            <div class="post__date">
                                <time class="published" datetime="2017-03-24T18:18">
                                    24 mins ago
                                </time>
                            </div>
                        </div>

                        <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>

                    </div>

                    <p>Excepteur sint occaecat cupidatat non proident.</p>

                    <a href="#" class="post-add-icon inline-items">
                        <svg class="olymp-heart-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-heart-icon"></use></svg>
                        <span>0</span>
                    </a>
                    <a href="#" class="reply">Reply</a>

                </li>
            </ul>

        </li>



        <li class="comment-item">
            <div class="post__author author vcard inline-items">
                <img src="/img/avatar4-sm.jpg" alt="author">

                <div class="author-date">
                    <a class="h6 post__author-name fn" href="#">Chris Greyson</a>
                    <div class="post__date">
                        <time class="published" datetime="2017-03-24T18:18">
                            1 hour ago
                        </time>
                    </div>
                </div>

                <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>

            </div>

            <p>Dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit.</p>

            <a href="#" class="post-add-icon inline-items">
                <svg class="olymp-heart-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-heart-icon"></use></svg>
                <span>7</span>
            </a>
            <a href="#" class="reply">Reply</a>

        </li>
    </ul>

    <!-- ... end Comments -->

    <a href="#" class="more-comments">View more comments <span>+</span></a>


    <!-- Comment Form  -->

    <form class="comment-form inline-items">

        <div class="post__author author vcard inline-items">
            <img src="/img/author-page.jpg" alt="author">

            <div class="form-group with-icon-right is-empty">
                <textarea class="form-control" placeholder=""></textarea>
                <div class="add-options-message">
                    <a href="#" class="options-message" data-toggle="modal" data-target="#update-header-photo">
                        <svg class="olymp-camera-icon">
                            <use xlink:href="/svg-icons/sprites/icons.svg#olymp-camera-icon"></use>
                        </svg>
                    </a>
                </div>
                <span class="material-input"></span></div>
        </div>

        <button class="btn btn-md-2 btn-primary">Post Comment</button>

        <button class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color">Cancel</button>

    </form>

    <!-- ... end Comment Form  -->
</div>