<div class="ui-block">


    <!-- Post -->

    <article class="hentry post">

        <div class="post__author author vcard inline-items">
            @if($user->logo)
                <img alt="author" src="{{asset($user->logo)}}" class="avatar">
            @else
                <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
            @endif
            <div class="author-date">
                <?php $action_text = count($images) ? ' uploaded ' . count($images) . ' new photos' : ($video ? ' shared a new video' : ' shared a new post') ?>
                <a class="h6 post__author-name fn" href="02-ProfilePage.html">{{ $user->name }}</a><a
                        href="#">{{ $action_text }}</a>
                <div class="post__date">
                    <time class="published" datetime="Just Now">
                        Just Now
                    </time>
                </div>
            </div>

            @if($post->user_id == $user->id)
                <div class="more">
                    <svg class="olymp-three-dots-icon">
                        <use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                    </svg>
                    <ul class="more-dropdown">
                        <li>
                            <a href="#">Edit Post</a>
                        </li>
                        <li>
                            <a href="#">Delete Post</a>
                        </li>
                        <li>
                            <a href="#">Turn Off Notifications</a>
                        </li>
                        <li>
                            <a href="#">Select as Featured</a>
                        </li>
                    </ul>
                </div>
            @endif

        </div>

        <p>
            {{ $post->post_text?:'' }}
        </p>
        <?php $count_images = count($images); ?>
        @if($count_images)
            <div class="post-block-photo js-zoom-gallery open-post" data-post_id = "{{ $post->id }}">
                <?php if($count_images >= 4): ?>
                <?php $index = 1; ?>
                @foreach($images as $image)
                    @if($index == 5)
                        @break;
                    @endif
                    <a href="{{ asset('user_media/' . $image['file_name']) }}"
                       class="{{ $index == 1 || $index == 2 ? 'half-width':'col col-3-width' }}"><img
                                src="{{ asset('user_media/' . $image['file_name']) }}" alt="photo"></a>
                    <?php $index++; ?>
                @endforeach
                <?php $rest_of_images = count($images) - 4; ?>
                @if($rest_of_images > 0)
                    <a href="{{ asset('user_media/' . $images[4]['file_name'])}}" class="more-photos col-3-width">
                        <img src="{{ asset('user_media/' . $images[4]['file_name'])}}" alt="photo">
                        <span class="h2">+ {{ $rest_of_images }}</span>
                    </a>
                @endif
                <?php elseif ($count_images == 1): ?>
                <a href="{{ asset('user_media/' . $images[0]['file_name']) }}"
                   class="col col-12-width"><img
                            src="{{ asset('user_media/' . $images[0]['file_name']) }}" alt="photo"></a>
                <?php elseif ($count_images == 2): ?>
                @foreach($images as $image)
                    <a href="{{ asset('user_media/' . $image['file_name']) }}"
                       class="half-width"><img
                                src="{{ asset('user_media/' . $image['file_name']) }}" alt="photo"></a>
                @endforeach
                <?php elseif ($count_images == 3): ?>
                <?php $index = 1 ?>
                @foreach($images as $image)
                    <a href="{{ asset('user_media/' . $image['file_name']) }}"
                       class="{{ $index == 1 || $index == 2 ? 'half-width':'col col-12-width' }}"><img
                                src="{{ asset('user_media/' . $image['file_name']) }}" alt="photo"></a>
                    <?php $index++; ?>
                @endforeach
                <?php endif; ?>
            </div>
        @endif
        @if($video)
            <div class="post-video">
                <div class="video-thumb f-none">
                    <video style="width: 100%;height: 150px" controls>
                        <source src="{{ asset('user_media/' . $video) }}">
                    </video>
                </div>
            </div>
        @endif
        <div class="post-additional-info inline-items">
            <a href="#" class="post-add-icon inline-items love-post" data-post_id="{{ $post->id }}" title="Love">
                <svg class="olymp-heart-icon">
                    <use xlink:href="/svg-icons/sprites/icons.svg#olymp-heart-icon"></use>
                </svg>
                <span class="num_of_loves-{{ $post->id }}">0</span>
            </a>
            <div class="comments-shared">
                <a href="#addComment" class="post-add-icon inline-items" title="Comment">
                    <svg class="olymp-speech-balloon-icon">
                        <use xlink:href="/svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use>
                    </svg>
                    <span>0</span>
                </a>
                <a href="#" class="post-add-icon inline-items" title="Share">
                    <svg class="olymp-share-icon">
                        <use xlink:href="/svg-icons/sprites/icons.svg#olymp-share-icon"></use>
                    </svg>
                    <span>16</span>
                </a>
            </div>
        </div>

        <div class="control-block-button post-control-button">

            <a href="#" class="btn btn-control love-post" data-post_id="{{ $post->id }}" title="Love">
                <svg class="olymp-like-post-icon">
                    <use xlink:href="/svg-icons/sprites/icons.svg#olymp-like-post-icon"></use>
                </svg>
            </a>

            <a href="#" class="btn btn-control" title="Comment">
                <svg class="olymp-comments-post-icon">
                    <use xlink:href="/svg-icons/sprites/icons.svg#olymp-comments-post-icon"></use>
                </svg>
            </a>

            <a href="#" class="btn btn-control" title="Share">
                <svg class="olymp-share-icon">
                    <use xlink:href="/svg-icons/sprites/icons.svg#olymp-share-icon"></use>
                </svg>
            </a>

        </div>

    </article>

    <!-- ... end Post -->
    <ul class="comments-list" id="comments-list-{{ $post->id }}">

    </ul>
    <div id="post_loader-{{ $post->id }}" style="text-align: center;padding: 15px;display: none;">
        <img src="/img/post_loader.gif" style="height: 30px; width: 30px;">
    </div>

    <a href="#" class="more-comments">View more comments <span>+</span></a>


    <!-- Comment Form  -->

    <form class="comment-form inline-items" id="addComment">
        <input type="hidden" value="{{$post->id}}" name="post_id">
        <div class="post__author author vcard inline-items">
            @if($user->logo)
                <img alt="author" src="{{asset( $user->logo)}}" class="avatar">
            @else
                <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
            @endif
            <div class="form-group with-icon-right is-empty">
                <textarea class="form-control" name="comment_text" placeholder=""></textarea>
                <div class="add-options-message">
                    <label for="comment-image" class="options-message" data-toggle="tooltip"
                           data-placement="top"
                           data-original-title="ADD PHOTO">
                        <svg for="images" class="olymp-camera-icon">
                            <use xlink:href="/svg-icons/sprites/icons.svg#olymp-camera-icon"></use>
                        </svg>
                        <input data-preview-img="image" type="file" name="comment-image" id="comment-image"
                               style="display: none;" accept="image/*">
                    </label>
                </div>
                <span class="material-input"></span>
            </div>
        </div>
        <div class="form-group padding40 comment-image-preview-container" style="display: none;">

        </div>
        <button class="btn btn-md-2 btn-primary" type="submit">Post Comment</button>

        <button class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color">Cancel</button>

    </form>

    <!-- ... end Comment Form  -->            </div>