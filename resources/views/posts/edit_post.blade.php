<a href="#" class="fire_edit_post_modal" data-toggle="modal" data-target="#edit_post_modal" style="visibility: hidden;"></a>
<section>
    <dev class = "container">
        <div class = "row">
            <div class="modal fade" id="edit_post_modal" tabindex="-1" role="dialog" aria-labelledby="edit_post_modal"
                 aria-hidden="true">
                <div class="modal-dialog window-popup choose-from-my-photo" role="document">

                    <div class="modal-content ">
                        <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
                            <svg class="olymp-close-icon">
                                <use xlink:href="svg-icons/sprites/icons.svg#olymp-close-icon"></use>
                            </svg>
                        </a>
                        <div class="modal-body">
                            <form id="add_media_post" enctype="multipart/form-data">
                                <div class="author-thumb post__author">
                                    @if(Auth::user()->logo)
                                        <img alt="author" src="/{{Auth::user()->logo}}" class="avatar">
                                    @else
                                        <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                                    @endif
                                        <a class="h6 title"
                                           href="/profile/{{ Auth::user()->username }}">{{ Auth::user()->name }}</a>
                                        <small> since </small>
                                        <time class="published" datetime="{{ isset($post)?$post->created_at:'' }}">
                                            {{ isset($post)?\Carbon\Carbon::parse($post->created_at )->diffForHumans():'' }}
                                        </time>
                                </div>
                                <div class="form-group with-icon label-floating is-empty">
                                    <label class="control-label">Share what you are thinking here...</label>
                                    <textarea class="form-control" id="status" name="status"
                                              placeholder=""></textarea>
                                </div>
                                <div class="form-group padding40 image-preview-container" style="display: none;">

                                </div>
                                <div class="form-group padding40 video-preview-container" style="display: none;">
                                    <video width="100%" controls>
                                        <source id="previewVid" src="">
                                    </video>
                                </div>
                                <div class="add-options-message">
                                    <label for="images" class="options-message" data-toggle="tooltip"
                                           data-placement="top"
                                           data-original-title="ADD PHOTOS">
                                        <svg for="images" class="olymp-camera-icon">
                                            <use xlink:href="/svg-icons/sprites/icons.svg#olymp-camera-icon"></use>
                                        </svg>
                                        <input data-preview-img="image" type="file" name="images" id="images"
                                               multiple style="display: none;" accept="image/*">
                                    </label>
                                    <label for="video" class="options-message" data-toggle="tooltip"
                                           data-placement="top"
                                           data-original-title="ADD VIDEO">
                                        <svg class="olymp-computer-icon">
                                            <use xlink:href="/svg-icons/sprites/icons.svg#olymp-computer-icon"></use>
                                        </svg>
                                        <input type="file" name="video" id="video" style="display: none;"
                                               accept="video/mp4">
                                    </label>

                                    <button class="btn btn-primary btn-md-2">Share</button>
                                </div>

                            </form>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane active" id="home" role="tabpanel" aria-expanded="true">
                                    <div class="choose-photo-item" data-mh="choose-item">
                                        <div class="radio">
                                            <label class="custom-radio">
                                                <img src="img/choose-photo9.jpg" alt="photo">
                                                <input type="radio" name="optionsRadios">
                                            </label>
                                        </div>
                                    </div>

                                    <a href="#" class="btn btn-secondary btn-lg btn--half-width">Cancel</a>
                                    <a href="#" class="btn btn-primary btn-lg btn--half-width">Confirm Photo</a>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </dev>
</section>