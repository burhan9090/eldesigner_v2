@foreach($posts as $post)
    @php
        $post_loves = \App\Love::resourceLoves($post->id , 'post');
        $isCurrentLoved = \App\Love::isCurrentLoved($post->id , 'post');
        $user = \App\User::find($post->user_id);
        $comments = $post->comments($post->id);
        $medias = $post->medias($post->id);
        $images = array();
        $video = "";
    foreach ($medias as $media){
        if($media->media_type == 'image'){
            $images[] = $media;
        }else{
            $video = $media->file_name;
        }
    }
    @endphp
    <div class="ui-block">
        <!-- Post -->
        <article class="hentry post">

            <div class="post__author author vcard inline-items">
                @if($user['logo'])
                    <img alt="author" src="{{asset($user['logo'])}}" class="avatar">
                @else
                    <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                @endif
                <div class="author-date">
                    <?php $action_text = count($images) ? ' uploaded ' . count($images) . ' new photos' : ($video ? ' shared a new video' : ' shared a new post') ?>
                    <a class="h6 post__author-name fn"
                       href="/profile/{{$user['user_name'] }}">{{ $user['name'] }}</a><a
                            href="#">{{ $action_text }}</a>
                    <div class="post__date">
                        <time class="published" datetime="{{ $post->created_at }}">
                            {{ \Carbon\Carbon::parse($post->created_at )->diffForHumans() }}
                        </time>
                    </div>
                </div>

                @if($post->user_id == Auth::user()->id)
                    <div class="more">
                        <svg class="olymp-three-dots-icon">
                            <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                        </svg>
                        <ul class="more-dropdown">
                            <li>
                                <a href="#">Edit Post</a>
                            </li>
                            <li>
                                <a href="#">Delete Post</a>
                            </li>
                        </ul>
                    </div>
                @endif

            </div>

            <p>
                {{ $post->post_text?:'' }}
            </p>
            <?php $count_images = count($images); ?>
            @if($count_images)
                <div class="post-block-photo js-zoom-gallery">
                    <?php if($count_images >= 4): ?>
                    <?php $index = 1; ?>
                    @foreach($images as $image)
                        @if($index == 5)
                            @break;
                        @endif
                        <a href="{{ asset('user_media/' . $image['file_name']) }}"
                           class="{{ $index == 1 || $index == 2 ? 'half-width':'col col-3-width' }}"><img
                                    src="{{ asset('user_media/' . $image['file_name']) }}"
                                    alt="photo"></a>
                        <?php $index++; ?>
                    @endforeach
                    <?php $rest_of_images = count($images) - 4; ?>
                    @if($rest_of_images > 0)
                        <a href="{{ asset('user_media/' . $images[4]['file_name'])}}"
                           class="more-photos col-3-width">
                            <img src="{{ asset('user_media/' . $images[4]['file_name'])}}"
                                 alt="photo">
                            <span class="h2">+ {{ $rest_of_images }}</span>
                        </a>
                    @endif
                    <?php elseif ($count_images == 1): ?>
                    <a href="{{ asset('user_media/' . $images[0]['file_name']) }}"
                       class="col col-12-width"><img
                                src="{{ asset('user_media/' . $images[0]['file_name']) }}"
                                alt="photo"></a>
                    <?php elseif ($count_images == 2): ?>
                    @foreach($images as $image)
                        <a href="{{ asset('user_media/' . $image['file_name']) }}"
                           class="half-width"><img
                                    src="{{ asset('user_media/' . $image['file_name']) }}"
                                    alt="photo"></a>
                    @endforeach
                    <?php elseif ($count_images == 3): ?>
                    <?php $index = 1 ?>
                    @foreach($images as $image)
                        <a href="{{ asset('user_media/' . $image['file_name']) }}"
                           class="{{ $index == 1 || $index == 2 ? 'half-width':'col col-12-width' }}"><img
                                    src="{{ asset('user_media/' . $image['file_name']) }}"
                                    alt="photo"></a>
                        <?php $index++; ?>
                    @endforeach
                    <?php endif; ?>
                </div>
            @endif
            @if($video)
                <div class="post-video">
                    <div class="video-thumb f-none">
                        <video style="width: 100%;height: 150px" controls>
                            <source src="{{ asset('user_media/' . $video) }}">
                        </video>
                    </div>
                </div>
            @endif
            <div class="post-additional-info inline-items">
                <a href="#" class="post-add-icon inline-items love-post {{ $isCurrentLoved?'loved':'' }}"
                   data-post_id="{{ $post->id }}"
                   style="{{ $isCurrentLoved?'fill:red;':'' }}"
                   title="Love">
                    <svg class="olymp-heart-icon">
                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-heart-icon"></use>
                    </svg>
                    <span class="num_of_loves-{{ $post->id }}">{{ count($post_loves)?:0 }}</span>
                </a>
                <div class="comments-shared">
                    <a class="post-add-icon inline-items" title="Comment">
                        <svg class="olymp-speech-balloon-icon">
                            <use xlink:href="svg-icons/sprites/icons.svg#olymp-speech-balloon-icon"></use>
                        </svg>
                        <span>{{ count($comments)?:0 }}</span>
                    </a>
                    <a class="post-add-icon inline-items" title="Share">
                        <svg class="olymp-share-icon">
                            <use xlink:href="svg-icons/sprites/icons.svg#olymp-share-icon"></use>
                        </svg>
                        <span>0</span>
                    </a>
                </div>
            </div>

            <div class="control-block-button post-control-button">

                <a href="#" class="btn btn-control love-post {{ $isCurrentLoved?'loved':'' }}"
                   data-post_id="{{ $post->id }}"
                   style="{{ $isCurrentLoved?'fill:red;':'' }}"
                   title="Love">
                    <svg class="olymp-like-post-icon">
                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-like-post-icon"></use>
                    </svg>
                </a>

                <a class="btn btn-control" title="Comment">
                    <svg class="olymp-comments-post-icon">
                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-comments-post-icon"></use>
                    </svg>
                </a>

                <a class="btn btn-control" title="Share">
                    <svg class="olymp-share-icon">
                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-share-icon"></use>
                    </svg>
                </a>

            </div>

        </article>

        <!-- ... end Post -->
        <ul class="comments-list" id="comments-list-{{ $post->id }}">
            @foreach($comments as $comment)
                @php
                    $comment_loves = \App\Love::resourceLoves($comment->id , 'comment');
                    $isCurrentLoved = \App\Love::isCurrentLoved($comment->id , 'comment');
                    $user = \App\User::find($comment->user_id);
                    $comment_replies = $comment->replies;
                @endphp
                <li class="comment-item {{ count($comment_replies)>0?'has-children':'' }}">
                    <div class="post__author author vcard inline-items">
                        @if($user->logo)
                            <img alt="author" src="{{asset( 'img/' . $user->logo)}}" class="avatar">
                        @else
                            <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                        @endif

                        <div class="author-date">
                            <a class="h6 post__author-name fn" href="#">{{ $user->name }}</a>
                            <div class="post__date">
                                <time class="published" datetime="{{ $comment->created_at }}">
                                    {{ \Carbon\Carbon::parse($comment->created_at )->diffForHumans() }}
                                </time>
                            </div>
                        </div>
                        @if($comment->user_id == Auth::user()->id)
                            <div class="more">
                                <svg class="olymp-three-dots-icon">
                                    <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                </svg>
                                <ul class="more-dropdown">
                                    <li>
                                        <a href="#">Edit Comment</a>
                                    </li>
                                    <li>
                                        <a href="#">Delete Comment</a>
                                    </li>
                                </ul>
                            </div>
                        @endif
                    </div>

                    <p>{{ $comment->comment_text }}</p>

                    @if($comment->image_path)
                        <div style="padding: 15px">
                            <img style="max-height: 250px"
                                 src="{{ asset('user_media_comments/' . $comment->image_path) }}">
                        </div>
                    @endif
                    <a href="#" class="post-add-icon inline-items love-comment {{ $isCurrentLoved?'loved':'' }}"
                       data-comment_id="{{ $comment->id }}"
                       style="{{ $isCurrentLoved?'fill:red;':'' }}"
                    >
                        <svg class="olymp-heart-icon">
                            <use xlink:href="svg-icons/sprites/icons.svg#olymp-heart-icon"></use>
                        </svg>
                        <span class="num_of_loves_comment-{{ $comment->id }}">{{ count($comment_loves)?:0 }}</span>
                    </a>
                    <div id="post_loader-reply-{{ $comment->id }}"
                         style="text-align: center;padding: 15px;display: none;">
                        <img src="{{ asset('img/post_loader.gif') }}"
                             style="height: 30px; width: 30px;">
                    </div>
                    <a style="cursor: pointer;" class="reply"
                       data-form-reply="addReply-Comment-{{ $comment->id }}">Reply</a>
                    <form class="comment-form inline-items bg-white addReply-Comment-{{ $comment->id }}"
                          data-replies-list="replies_list-{{ $comment->id }}" id="submit-reply"
                          style="display: none;">
                        <input type="hidden" value="{{$comment->id}}" name="comment_id">
                        <div class="post__author author vcard inline-items">
                            @if($user->logo)
                                <img alt="author" src="{{asset( 'img/' . $user->logo)}}"
                                     class="avatar">
                            @else
                                <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                            @endif
                            <div class="form-group with-icon-right is-empty">
                                                    <textarea class="form-control" name="reply_text"
                                                              placeholder=""></textarea>
                                <div class="add-options-message">
                                    <label for="reply-image" class="options-message"
                                           data-toggle="tooltip"
                                           data-placement="top"
                                           data-original-title="ADD PHOTO">
                                        <svg for="reply-image" class="olymp-camera-icon">
                                            <use xlink:href="svg-icons/sprites/icons.svg#olymp-camera-icon"></use>
                                        </svg>
                                        <input data-preview-img="image" type="file"
                                               name="reply-image" id="reply-image"
                                               data-preview-class='reply-image-preview-container-{{ $comment->id }}'
                                               style="display: none;" accept="image/*">
                                    </label>
                                </div>
                                <span class="material-input"></span>
                            </div>
                        </div>
                        <div class="form-group padding40 reply-image-preview-container-{{ $comment->id }}"
                             style="display: none;">

                        </div>
                        <button class="btn btn-md-2 btn-primary" type="submit">Post Comment</button>

                        <button class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color">
                            Cancel
                        </button>

                    </form>

                    <ul class="children" id="replies_list-{{ $comment->id }}">
                        @foreach($comment_replies as $reply)
                            @php
                                $reply_loves = \App\Love::resourceLoves($reply->id , 'reply');
                                $isCurrentLoved = \App\Love::isCurrentLoved($reply->id , 'reply');
                                $user = \App\User::find($reply->user_id);
                            @endphp
                            <li class="comment-item">
                                <div class="post__author author vcard inline-items">
                                    @if($user->logo)
                                        <img alt="author" src="{{asset( 'img/' . $user->logo)}}"
                                             class="avatar">
                                    @else
                                        <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                                    @endif

                                    <div class="author-date">
                                        <a class="h6 post__author-name fn"
                                           href="#">{{ $user->name }}</a>
                                        <div class="post__date">
                                            <time class="published"
                                                  datetime="{{ $reply->created_at }}">
                                                {{ \Carbon\Carbon::parse($reply->created_at )->diffForHumans() }}
                                            </time>
                                        </div>
                                    </div>

                                    @if($reply->user_id == Auth::user()->id)
                                        <div class="more">
                                            <svg class="olymp-three-dots-icon">
                                                <use xlink:href="svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                                            </svg>
                                            <ul class="more-dropdown">
                                                <li>
                                                    <a href="#">Edit Reply</a>
                                                </li>
                                                <li>
                                                    <a href="#">Edit Reply</a>
                                                </li>
                                            </ul>
                                        </div>
                                    @endif
                                </div>

                                <p>{{ $reply->reply_text }}</p>
                                @if($reply->image_path)
                                    <div style="padding: 15px">
                                        <img style="max-height: 250px"
                                             src="{{ asset('user_media_replies/' . $reply->image_path) }}">
                                    </div>
                                @endif
                                <a href="#" class="post-add-icon inline-items love-reply {{ $isCurrentLoved?'loved':'' }}"
                                   data-reply_id="{{ $reply->id }}"
                                   style="{{ $isCurrentLoved?'fill:red;':'' }}"
                                >
                                    <svg class="olymp-heart-icon">
                                        <use xlink:href="svg-icons/sprites/icons.svg#olymp-heart-icon"></use>
                                    </svg>
                                    <span class="num_of_loves_reply-{{ $reply->id }}">{{ count($reply_loves)?:0 }}</span>
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </li>
            @endforeach
        </ul>
        <div id="post_loader-{{ $post->id }}"
             style="text-align: center;padding: 15px;display: none;">
            <img src="/img/post_loader.gif" style="height: 30px; width: 30px;">
        </div>

        <a href="#" class="more-comments">View more comments <span>+</span></a>


        <!-- Comment Form  -->

        <form class="comment-form inline-items" id="addComment">
            <input type="hidden" value="{{$post->id}}" name="post_id">
            <div class="post__author author vcard inline-items">
                @if($user['logo'])
                    <img alt="author" src="{{asset($user['logo'])}}" class="avatar">
                @else
                    <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                @endif
                <div class="form-group with-icon-right is-empty">
                    <textarea class="form-control" name="comment_text" placeholder=""></textarea>
                    <div class="add-options-message">
                        <label for="comment-image" class="options-message" data-toggle="tooltip"
                               data-placement="top"
                               data-original-title="ADD PHOTO">
                            <svg for="images" class="olymp-camera-icon">
                                <use xlink:href="svg-icons/sprites/icons.svg#olymp-camera-icon"></use>
                            </svg>
                            <input data-preview-img="image" type="file" name="comment-image"
                                   id="comment-image"
                                   style="display: none;" accept="image/*">
                        </label>
                    </div>
                    <span class="material-input"></span>
                </div>
            </div>
            <div class="form-group padding40 comment-image-preview-container"
                 style="display: none;">

            </div>
            <button class="btn btn-md-2 btn-primary" type="submit">Post Comment</button>

            <button class="btn btn-md-2 btn-border-think c-grey btn-transparent custom-color">
                Cancel
            </button>

        </form>

        <!-- ... end Comment Form  -->
    </div>
@endforeach