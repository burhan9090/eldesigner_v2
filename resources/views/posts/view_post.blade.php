<?php
/**
 * Created by PhpStorm.
 * User: mousa
 * Date: 12/1/18
 * Time: 7:37 PM
 */
?>
@extends('layouts.application')
@section('content')

    <!-- Main Content -->
    <div class="container" id="profile">
        <div class="row">
            <main class="col col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-12">

                @include('timeline.post')
                <input type="hidden" id = "last_page"  value="0">
                <div style="display: none; text-align: center;" id = "load_more_posts">
                    <img class="center-block" src="/img/post_loader.gif" style="text-align: center;height: 30px;width: 30px;" alt="Loading..." />
                </div>
                <div style="text-align: center;">
                    <button class = "btn btn-md-2 btn-border-think c-grey btn-transparent custom-color" id="btn-load-more" style="display: none;">
                        Load More
                    </button>
                </div>
            </main>

            <!-- ... end Main Content -->

            <!-- Left Sidebar -->

            <aside class="col col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-12">

                <div class="ui-block">

                    <div class="ui-block-title">
                        <h6 class="title">Activity Feed</h6>
                        <a href="#" class="more">
                            <svg class="olymp-three-dots-icon">
                                <use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                            </svg>
                        </a>
                    </div>


                    <!-- W-Activity-Feed -->
                    <activity-feed :notification_data="notification_data"></activity-feed>


                    <!-- .. end W-Activity-Feed -->
                </div>
            </aside>

            <!-- ... end Left Sidebar -->


            <!-- Right Sidebar -->

            <aside class="col col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-12">
                <div class="ui-block">
                    <div class="ui-block-title">
                        <h6 class="title">Friend Suggestions</h6>
                        <a href="#" class="more">
                            <svg class="olymp-three-dots-icon">
                                <use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use>
                            </svg>
                        </a>
                    </div>

                    <friend-suggestions :friend_suggestions="friend_suggestions"
                                        v-on:requestfriend="requestFriend"></friend-suggestions>
                    <!-- W-Action -->


                    <!-- ... end W-Action -->
                </div>


                <div class="ui-block">


                    <!-- W-Action -->

                    <div class="widget w-action"><img src="{{ asset('svg-icons/custom_icons/generator.svg') }}"
                                                      alt="elDesigners">
                        <div class="content"><h4 class="title">Generate Website</h4> <span>Launch a website in one click!</span>
                            <a href="{{ route('dashboard.index') }}" class="btn btn-bg-secondary btn-md">START</a></div>
                    </div>

                    <!-- ... end W-Action -->
                </div>
            </aside>

            <!-- ... end Right Sidebar -->
        </div>
    </div>

    @include('posts.edit_post')
@endsection
