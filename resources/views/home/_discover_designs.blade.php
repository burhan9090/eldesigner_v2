@foreach($projects as $pro)
    <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">


        <!-- Product Item -->

        <div class="shop-product-item">
            <div class="product-thumb">
                <a href="{{ route('projects.show', $pro['pid']) }}" >
                <img src="{{asset($pro['image'])}}" class="fit-image" alt="product">
                </a>
            </div>
            <div class="product-content">
                <div class="block-title">
                    @if ($pro['profile_setting'] == 'private')
                        <a href="/profile/{{$pro['user_name']}}"
                           class="product-category">{{$pro['name']}}</a>
                    @else
                        <a href="/home/profile/{{$pro['user_name']}}"
                           class="product-category">{{$pro['name']}}</a>
                    @endif
                        <a href="{{ route('projects.show', $pro['pid']) }}" class="h5 title">{{$pro['title']}}</a>
                </div>
                <div class="block-price">
{{--                    <ul class="rait-stars">--}}
{{--                        <li>--}}
{{--                            <i class="fa fa-star star-icon c-primary" aria-hidden="true"></i>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <i class="fa fa-star star-icon c-primary" aria-hidden="true"></i>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <i class="fa fa-star star-icon c-primary" aria-hidden="true"></i>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <i class="fa fa-star star-icon c-primary" aria-hidden="true"></i>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <i class="far fa-star star-icon" aria-hidden="true"></i>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
{{--                    <div class="product-price">2K View</div>--}}


                </div>
            </div>
        </div>

        <!-- ... end Product Item -->

    </div>
@endforeach
