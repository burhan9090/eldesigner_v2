<?php
/**
 * Created by PhpStorm.
 * User: burhan
 * Date: 2019-02-08
 * Time: 16:53
 */
?>
@extends('includes.public')
<div>
    @section('content_public')
    <div class="row profile-view-look">
        <div class="col col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-sm-12 col-12">
            <div id="newsfeed-items-grid">


                    <div class="ui-block" style="text-align: center;">
                        <div class="ui-block-title">
                            <p>No recent Post to show</p>
                        </div>
                    </div>


            </div>
        </div>

        <div class="col col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-12">
            <div class="ui-block">
                <div class="ui-block-title">
                    <h6 class="title">Page Intro</h6>
                    <a href="#" class="more"><svg class="olymp-three-dots-icon"><use xlink:href="/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg></a>
                </div>
                <div class="ui-block-content">

                    <!-- W-Personal-Info -->

                    <ul class="widget w-personal-info item-block">
                        <li>
                            <span class="text">{{$oUserInfo->description}}</span>
                        </li>
                        <li>
                            <span class="title">Created:</span>
                            <span class="text">{{$oUserInfo->created_at}}</span>
                        </li>
                        @if (isset($oUserInfo->country['text']) && isset($oUserInfo->city['text'] ))
                            <li>
                                <span class="title">Based in:</span>
                                <span class="text">{{$oUserInfo->city['text']}}, {{$oUserInfo->country['text']}}</span>
                            </li>
                        @endif
                        <li>
                            <span class="title">Contact:</span>
                            <a href="#" class="text">{{$oUserInfo->email}}</a>
                        </li>
                        <li>
                            <span class="title">Website:</span>
                            <a href="#" class="text">{{$oUserInfo->website}}</a>
                        </li>
                    </ul>

                    <!-- ... end W-Personal-Info -->
                    <!-- W-Socials -->

                    <div class="widget w-socials">
                        <h6 class="title">Other Social Networks:</h6>
                        @if(!empty($oUserInfo->fb_link))
                            <a href="{{$oUserInfo->fb_link}}" class="social-item bg-facebook">
                                <i class="fab fa-facebook-f" aria-hidden="true"></i>
                                Facebook
                            </a>
                        @endif
                        @if(!empty($oUserInfo->tw_link))
                            <a href="{{$oUserInfo->tw_link}}" class="social-item bg-twitter">
                                <i class="fab fa-twitter" aria-hidden="true"></i>
                                Twitter
                            </a>
                        @endif
                        @if(!empty($oUserInfo->website))
                            <a href="{{$oUserInfo->website}}" class="social-item bg-dribbble">
                                <i class="fab fa-dribbble" aria-hidden="true"></i>
                                Website
                            </a>
                        @endif
                    </div>


                    <!-- ... end W-Socials -->				</div>
            </div>


        </div>


    </div>
    @endsection
</div>
