@extends('layouts.eldesigners')
@section('content')
    <style>
        .bg-landing, .w-action {
            background-image: url(../website/Pricing-Cover.svg);
            width: 100%;
            background-repeat: no-repeat;
            /* background-attachment: fixed; */
            background-position: unset;
            background-size: cover;
        }

        .bg-primary-opacity {
            background-color: rgba(251, 251, 251, 0);
        }
    </style>
    <div class="main-header main-header-fullwidth main-header-has-header-standard">
    @include('eldesigners.top_menu')
    <!-- ... end Header Standard Landing  -->
        <div class="header-spacer--standard"></div>

        <div class="stunning-header-content">
            <div class="main-header-content">
                <h1 class="stunning-header-title" style="color: white;font-weight: 500;">DISCOVER. GET INSPIRED. COMMUNICATE</h1>
            </div>
        </div>

        <div class="content-bg-wrap bg-landing"></div>
    </div>


    <div class="container">
        <form  method="GET" action="/home/discover-friends/" id="submit_search_order_by">

            <div class="row">
                <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="ui-block responsive-flex">
                        <div class="ui-block-title">
                            <div class="h6 title">DESIGNERS</div>
                            <div class="w-select">
                                <div class="title">Order By:</div>
                                <fieldset class="form-group">
                                    <select name="orderbymethod" class="selectpicker select_filtiration form-control" >

                                        @foreach($order_by_options as $option)
                                            <option @if(isset($_GET['orderbymethod']) && $_GET['orderbymethod'] == $option['value']) selected @endif value="{{$option['value']}}">{{$option['text']}}</option>
                                        @endforeach
                                    </select>
                                </fieldset>
                            </div>

                            <div class="form-group w-search with-button">
                                <input class="form-control" type="text" name="search"
                                       value="@if(isset($_GET['search']) && $_GET['search'] !== '') {{$_GET['search']}} @endif"
                                       placeholder="Search Designers...">
                                <button>
                                    <svg class="olymp-magnifying-glass-icon">
                                        <use xlink:href="/svg-icons/sprites/icons.svg#olymp-magnifying-glass-icon"></use>
                                    </svg>
                                </button>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <div class="container" id="discover_friend">

        <div class="container px-0">
            <div class="row">
                @if(!$discover->isEmpty())
                    @foreach ($discover as $user)
                        <div class="col col-xl-3 col-lg-6 col-md-6 col-sm-6 col-6">


                            <div class="ui-block">

                                <!-- Friend Item -->

                                <div class="friend-item">
                                    <div class="friend-header-thumb discover_cover_home">
                                        @if($user->cover)
                                            <img src="/{{$user->cover}}" alt="friend">
                                        @else
                                            <img src="{{ asset('uploads/user/dummy_cover.png') }}" alt="friend">
                                        @endif
                                    </div>

                                    <div class="friend-item-content">


                                        <div class="friend-avatar">

                                            <div class="author-thumb discover_logo">
                                                @if ($user->profile_setting == 'private')
                                                    <a href="/profile/{{ $user->user_name }}">
                                                        @if($user->logo)
                                                            <img src="/{{$user->logo}}" alt="friend">
                                                        @else
                                                            <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                                                        @endif
                                                    </a>
                                                @else
                                                    <a href="/home/profile/{{ $user->user_name }}">
                                                        @if($user->logo)
                                                            <img src="/{{$user->logo}}" alt="friend">
                                                        @else
                                                            <img src="{{ asset('uploads/user/dummy_logo.png') }}" alt="author">
                                                        @endif
                                                    </a>
                                                @endif

                                            </div>
                                            <div class="author-content">
                                                @if ($user->profile_setting == 'private')
                                                    <a href="/profile/{{ $user->user_name }}"
                                                       class="h5 author-name">{{ str_limit($user->name,20,' ...') }}</a>
                                                @else
                                                    <a href="/home/profile/{{ $user->user_name }}"
                                                       class="h5 author-name">{{ str_limit($user->name,20,' ...') }}</a>
                                                @endif

                                                <div class="country">{{ $user->user_name }}</div>
                                            </div>
                                        </div>

                                        <div class="swiper-container" data-slide="fade" style="    height: 223px !important;">
                                            <div class="swiper-wrapper">

                                                <div class="swiper-slide">
                                                    <p class="friend-about" data-swiper-parallax="-500">
                                                        {{str_limit($user->description,150,' ...')}}
                                                    </p>
                                                    <div class="control-block-button" data-swiper-parallax="-100">

                                                    </div>

                                                </div>
                                                <div class="swiper-slide">
                                                    <div class="friend-count" data-swiper-parallax="-500">
                                                        <a href="#" class="friend-count-item">
                                                            @php $projectCount = $user->projects_count - 6; @endphp
                                                            <div class="h6">{{$projectCount >= 0?$projectCount:0}}</div>
                                                            <div class="title">Projects</div>
                                                        </a>
                                                        <a href="#" class="friend-count-item">
                                                            <div class="h6">{{$user->profile_count}}</div>
                                                            <div class="title">Views</div>
                                                        </a>
                                                    </div>


                                                    <div class="friend-since" data-swiper-parallax="-100">
                                                        <span></span><br>
                                                        @if (isset($user->country['text']) && isset($user->city['text'] ))
                                                            <div class="h6">{{$user->city['text']}}, {{$user->country['text']}}</div>
                                                        @endif

                                                    </div>


                                                </div>


                                            </div>

                                            <!-- If we need pagination -->
                                            <div class="swiper-pagination"></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- ... end Friend Item -->            </div>
                        </div>

                    @endforeach
                @else
                    <div class="container">
                        <div class="row">
                            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="ui-block">
                                    <div class="ui-block-title ">
                                        <p class="h6 title center_text">No Result Found</p>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>
            </div>

            @endif
        </div>
        <nav aria-label="Page navigation" class="justify-content-center">
            <div>
                {{$discover->links('vendor.pagination.bootstrap-4')}}

            </div>
        </nav>
    </div>
@endsection