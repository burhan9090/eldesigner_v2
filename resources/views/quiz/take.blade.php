@extends('layouts.eldesigners')
@section('content')
    <style>
        .skills-item-meter-active {
            top: 0px !important;
        }
        .image-fit{
            height: 350px; object-fit: cover;
        }
        .question-selected{
            border-color: #38a9ff
        }
        .pointer-event{
            cursor: pointer;
        }
    </style>
    <div class="main-header main-header-fullwidth main-header-has-header-standard">
        <div class="container" style="margin-top: 100px">
            <div class="row">
                <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                    <div class="ui-block">
                        <div class="ui-block-title" style="border-bottom: #ffff">
                            <h6 class="title" style="text-align: center">Submit Design Request | <span
                                        class="step-number">Step 1</span></h6>
                            <p class='step-desc'>Choose requested service and sub-service</p>

                        </div>
                        <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">

                            <div class="skills-item">

                                <div class="skills-item-meter mb60">
                                    <span class="skills-item-meter-active skills-animate btn-primary progress-bar-custom"
                                          style="width: {{ 100 / ($steps?:1) }}%;opacity: 1 !important;"></span>
                                </div>
                            </div>
                        </div>
                        <form id = "design_request">
                            <input type="hidden" name="process_level" id = "process_level" value="{{ $quizInfo->quiz_level }}">
                            <input type="hidden" name="user_id" id = "user_id" value="{{ $quizInfo->user_id }}">
                            <div class="container step_type">
                                <h4 class="title mb30 question" style="text-align: center">Which design service and sub
                                    service you need?</h4>

                                <section class="blog-post-wrap medium-padding60">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                                <div class="form-group label-floating is-select">
                                                    <label class="control-label">Requested Service?</label>
                                                    <select class="selectpicker form-control services">
                                                        <option value="0">Select Service</option>
                                                        @foreach($services as $service)
                                                            <option value="{{ $service->service }}"
                                                                    data-service_id="{{ $service->id  }}">{{ $service->service }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                                <div class="form-group label-floating is-select">
                                                    <label class="control-label">Requested Sub-service?</label>
                                                    <div class="sub-services-container">

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            @php $index = 1; @endphp
                            @foreach($levels as $level)
                                <div class="container step-{{ $index }}" style="display: none;">
                                    <h4 class="title mb30 question" style="text-align: center">{{ $level->question }}?</h4>
                                    <div class="row">
                                        <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <div class="ui-block pointer-event block-step-{{ $index }}"
                                                 data-answer="{{ $level->id }}" data-question="{{ $level->question }}"
                                                 data-answer_title ="{{ $level->title_1 }}"
                                                 data-answer_image ="{{ $level->image_1 }}" >


                                                <!-- Post -->

                                                <article class="hentry blog-post">

                                                    <div class="post-thumb">
                                                        <img src="{{ asset('quizzes/' . $level->image_1) }}" alt="photo" class = "image-fit">
                                                    </div>

                                                    <div class="post-content">
                                                        <a href="#" class="h4 post-title" style="text-align: center">{{ $level->title_1 }}</a>


                                                    </div>

                                                </article>

                                                <!-- ... end Post -->
                                            </div>
                                        </div>

                                        <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <div class="ui-block pointer-event block-step-{{ $index }}" data-answer="{{ $level->id }}" data-question="{{ $level->question }}"
                                                 data-answer_title ="{{ $level->title_2 }}"
                                                 data-answer_image ="{{ $level->image_2 }}">


                                                <!-- Post -->

                                                <article class="hentry blog-post">

                                                    <div class="post-thumb">
                                                        <img src="{{ asset('quizzes/' . $level->image_2) }}" alt="photo" class = "image-fit">
                                                    </div>

                                                    <div class="post-content">
                                                        <a href="#" class="h4 post-title" style="text-align: center">{{ $level->title_2 }}</a>


                                                    </div>

                                                </article>

                                                <!-- ... end Post -->
                                            </div>
                                        </div>
                                        <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                                            <div class="ui-block pointer-event block-step-{{ $index }}" data-answer="{{ $level->id }}" data-question="{{ $level->question }}"
                                                 data-answer_title ="{{ $level->title_3 }}"
                                                 data-answer_image ="{{ $level->image_3 }}">


                                                <!-- Post -->

                                                <article class="hentry blog-post">

                                                    <div class="post-thumb">
                                                        <img src="{{ asset('quizzes/' . $level->image_3) }}" alt="photo" class = "image-fit">
                                                    </div>

                                                    <div class="post-content">
                                                        <a href="#" class="h4 post-title" style="text-align: center">{{ $level->title_3 }}</a>


                                                    </div>

                                                </article>


                                                <!-- ... end Post -->
                                            </div>
                                        </div>


                                    </div>
                                </div>
                                @php $index++; @endphp
                            @endforeach
                            <div class="container client_info" style="display: none;">
                                <h4 class="title mb30 question" style="text-align: center">One step to do it, fill out the form below?</h4>

                                <section class="blog-post-wrap medium-padding60">
                                    <div class="container">
                                        <div class="row">
                                            @if($quizInfo->required_info_name)
                                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Full Name</label>
                                                        <input class="form-control" name="required_info_name" placeholder="EX : john Doe" type="text" value="" required>
                                                        <span class="material-input"></span>
                                                    </div>
                                                </div>
                                            @endif
                                            @if($quizInfo->required_info_email)
                                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Email</label>
                                                        <input class="form-control" name = "required_info_email" placeholder="EX : example@example.com" type="email" value="" required>
                                                        <span class="material-input"></span>
                                                    </div>
                                                </div>
                                            @endif
                                            @if($quizInfo->required_info_phone)
                                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Phone</label>
                                                        <input class="form-control" name = "required_info_phone" placeholder="EX : 123-456-7890" type="tel" pattern="[0-9]{3}-[0-9]{3}-[0-9]{4}" value="" required>
                                                        <span class="material-input"></span>
                                                    </div>
                                                </div>
                                            @endif
                                            @if($quizInfo->required_info_address)
                                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Address</label>
                                                        <input class="form-control" name = "required_info_address" placeholder="EX : AMMAN-JORDAN , ABC St. 11" type="text" value="" required>
                                                        <span class="material-input"></span>
                                                    </div>
                                                </div>
                                            @endif
                                            {{--@if($quizInfo->required_info_attachment)--}}
                                                {{--<div class="col col-lg-6 col-md-6 col-sm-12 col-12">--}}
                                                    {{--<div class="form-group label-floating">--}}
                                                        {{--<label class="control-label">Attachment</label>--}}
                                                        {{--<input class="form-control" name = "required_info_attachment" type="file" required>--}}
                                                        {{--<span class="material-input"></span>--}}
                                                    {{--</div>--}}
                                                {{--</div>--}}
                                            {{--@endif--}}
                                            @if($quizInfo->required_info_budget)
                                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Budget</label>
                                                        <input class="form-control" name = "required_info_budget" type="number" required>
                                                        <span class="material-input"></span>
                                                    </div>
                                                </div>
                                            @endif
                                            @if($quizInfo->required_info_location)
                                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Project Location</label>
                                                        <input class="form-control" name = "required_info_location" type="text" required>
                                                        <span class="material-input"></span>
                                                    </div>
                                                </div>
                                            @endif
                                            @if($quizInfo->required_info_overview)
                                                <div class="col col-lg-6 col-md-6 col-sm-12 col-12">
                                                    <div class="form-group label-floating">
                                                        <label class="control-label">Project Overview</label>
                                                        <textarea class="form-control" name = "required_info_overview" type="text" required></textarea>
                                                        <span class="material-input"></span>
                                                    </div>
                                                </div>
                                            @endif
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <div class="crumina-module crumina-heading align-center" id = "save" style="display: none;">
                                <button type = "submit" class="btn btn-primary btn-lg" data-current_step="client_info">LAUNCH IT!</button>
                            </div>
                        </form>
                        <div class="align-center">
                            {{--<a href="#" class="btn btn-md btn-border-think custom-color c-grey">BACK</a>--}}
                            <a href="#" class="btn btn-breez btn-md" id="next" data-current_step="step_type">NEXT</a>
                        </div>

                        <!-- Pagination -->


                        <!-- ... end Pagination -->

                        </section>


                        <!-- Section Call To Action Animation -->


                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('layouts.loader')
@section('script')
    <script>
        let levelsAnswers = {
            level_1 : {
                question : "",
                answer_title : "",
                answer_image : ""
            },
            level_2 : {
                question : "",
                answer_title : "",
                answer_image : ""
            },
            level_3 : {
                question : "",
                answer_title : "",
                answer_image : ""
            },
            level_4 : {
                question : "",
                answer_title : "",
                answer_image : ""
            }
        };
        var process_level = $('#process_level').val();
        $('#next').click(function (e) {
            e.preventDefault()
            var FinishLevels = false;
            var current_step = $(this).attr('data-current_step');
            switch (current_step) {
                case 'step_type':
                    var services = $('.services').find("option:selected").val()
                    var sub_service = $('.sub-services').find("option:selected").val()
                    console.log(services)
                    console.log(sub_service)
                    if (!services || !sub_service) {
                        alert('help the designer to know what you are really want by selecting service and sub-service')
                        return false;
                    }
                    $( ".progress-bar-custom" ).css( "width", "40%");
                    $(".step-number").html("step 2")
                    break;
                case '1':
                    levelsAnswers.level_1.question = ''+ $('.block-step-' + current_step).attr('data-question') +'';
                    levelsAnswers.level_1.answer_title = ''+ $('.block-step-' + current_step).attr('data-answer_title') +'';
                    levelsAnswers.level_1.answer_image = ''+ $('.block-step-' + current_step).attr('data-answer_image') +'';
                    $( ".progress-bar-custom" ).css( "width", "60%");
                    $(".step-number").html("step 3")
                    break;
                case '2':
                    levelsAnswers.level_2.question = ''+ $('.block-step-' + current_step).attr('data-question') +'';
                    levelsAnswers.level_2.answer_title = ''+ $('.block-step-' + current_step).attr('data-answer_title') +'';
                    levelsAnswers.level_2.answer_image = ''+ $('.block-step-' + current_step).attr('data-answer_image') +'';
                    $( ".progress-bar-custom" ).css( "width", "70%");
                    $(".step-number").html("step 4")
                    break;
                case '3':
                    levelsAnswers.level_3.question = ''+ $('.block-step-' + current_step).attr('data-question') +'';
                    levelsAnswers.level_3.answer_title = ''+ $('.block-step-' + current_step).attr('data-answer_title') +'';
                    levelsAnswers.level_3.answer_image = ''+ $('.block-step-' + current_step).attr('data-answer_image') +'';
                    if(process_level == '3'){
                        FinishLevels = true;
                    }
                    $( ".progress-bar-custom" ).css( "width", "80%");
                    $(".step-number").html("step 5")
                    break;
                case '4':
                    levelsAnswers.level_4.question = ''+ $('.block-step-' + current_step).attr('data-question') +'';
                    levelsAnswers.level_4.answer_title = ''+ $('.block-step-' + current_step).attr('data-answer_title') +'';
                    levelsAnswers.level_4.answer_image = ''+ $('.block-step-' + current_step).attr('data-answer_image') +'';
                    $( ".progress-bar-custom" ).css( "width", "90%");
                    $(".step-number").html("step 6")
                    break;
                case 'client_info':

                    break;
                default:
                    alert('somthing went wroing hit refresh and try again')
            }
            if(current_step == 'step_type'){
                $('.step_type').slideUp(300);
                if (!FinishLevels) {
                    $('.step-1').slideDown(300);
                    $(this).attr('data-current_step', "1")
                } else {
                    $('#next').fadeOut(100)
                    $('#save').fadeIn(100)
                }
            }else{
                $('.step-' + current_step).slideUp(300);
                current_step++;
                if (!FinishLevels) {
                    $('.step-' + current_step).slideDown(300);
                    $(this).attr('data-current_step', current_step)
                } else {
                    $('.client_info').slideDown(300);
                    $('#next').fadeOut(100)
                    $('#save').fadeIn(100)
                }
            }

        });
        $(document).on('submit', 'form#design_request', function (form) {
            var formData = $( this ).serializeArray();
            var services = $('.services').find("option:selected").val()
            var sub_service = $('.sub-services').find("option:selected").val()
            formData.push({name:'service' , value:services})
            formData.push({name:'sub_service' , value:sub_service})
            formData.push({name:'answers' , value:levelsAnswers})
            var payload = JSON.stringify(formData)
            console.log(payload)
            form.preventDefault();
            $.ajax({
                url: "/saveProcess",
                method: "POST",
                data: {payload : ' '+ payload +' '},
                beforeSend: function () {
                    showLoader()
                },
                success: function (response) {
                    if (response) {
                        if (response.success) {
                            alert(response.msg)
                            setTimeout(function () {
                            }, 3000);
                            location.href = '/home';
                        } else {
                            alert(response.msg)
                        }
                    } else {
                        alert("Something went wrong, try again")
                    }
                },
                error: function (jqXHR, textStatus) {
                    alert("Something went wrong, try again")
                },
                complete: function () {
                    hideLoader()
                }
            });
        })
        $('.services').on('change', function () {
            var service_id = $(this).find("option:selected").attr('data-service_id')

            if (service_id) {
                $.get('/getSubServices', {service_id: service_id}).done(function (d) {
                    var option = ''
                    let newOptions = [];
                    var selectTag = '<select class="selectpicker form-control sub-services">' +
                        '                                                <option value="0">Select service first</option>' +
                        '                                            </select>';
                    $(".sub-services-container").html(selectTag);
                    for (var i = 0; i < d.data.length; i++) {
                        newOptions.push('<option value = "' + d.data[i].sub_service_name + '">' + d.data[i].sub_service_name + '</option>');
                    }
                    $(".sub-services").html(newOptions);
                    $('.sub-services').selectpicker('refresh');
                })
            }
        })
        $('[data-answer]').click(function () {
            $('[data-answer]').each(function(){
                $(this).removeClass('question-selected')
            })
            $(this).addClass('question-selected');
        })
    </script>
@endsection