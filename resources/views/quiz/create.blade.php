@extends('profile.layout.main')
@section('profile_content')

<style>
    .selected{
        background-color: #31b1b4;
    }
    .image-upload > input
    {
        display: none;
    }

    .image-upload img
    {
        cursor: pointer;
    }
    .img-preview{
        height: 158px;
        width: 100%;
        object-fit: cover;
    }
</style>
@if(!$has_permission)
    <div class="alert alert-danger" role="alert">
        <p>Design Request is disabled , Please upgrade your account <a href="/pricing/bundles">Here</a> to start making new designs request</p>
    </div>
@endif
    <div class = "create-quiz @if(!$has_permission)  disabledbutton @endif" >
        <div id = "has_quiz_dev" style="{{ Auth::user()->has_quiz == 1?'display:block':'display: none' }}">
            <p style="text-align: center;">Congratulation you have been created your quiz successfully</p>
            <div class="align-center">

                <a href="#" class="btn btn-md-2 btn-border-think custom-color c-grey" id = "overwrite_quiz">Overwrite your quiz</a>

                <a href="{{ route('tool.quiz.take' , Auth::user()->id) }}" target="_blank" class="btn btn-blue btn-md-2 selected" id = "preview_quiz">
                    Try it now
                </a>

            </div>
        </div>
            <form id="createQuiz" style="{{ Auth::user()->has_quiz == 1?'display: none':'display: block' }}">
                <div class="step-1">
                    <div class="container" id="step_1_header">
                        <div class="row">
                            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="ui-block">
                                    <div class="ui-block-title">
                                        <div class="h6 title">Step 1 |</div>
                                        <div class="h6 title" style="color: #38a9ff">Services Categories</div>
                                        <p>Select the services catagories you offer. For example Interior,graphic design...etc.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="container" id="step_1_content">
                        <div class="row">
                            @foreach($services as $service)
                                <div class="col col-lg-3 col-md-6 col-sm-6 col-6">
                                    <div class="ui-block">
                                        <div class="available-widget">

                                            <div class="checkbox">
                                                <label>
                                                    <input name="services[]" type="checkbox" value="{{ $service->service_name }}">
                                                    {{ $service->service_name }}
                                                </label>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="step-2" style="display: none;">
                    <div class="container" id="step_2_header">
                        <div class="row">
                            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="ui-block">
                                    <div class="ui-block-title">
                                        <div class="h6 title">Step 2 |</div>
                                        <div class="h6 title" style="color: #38a9ff">Services Sub-Categories</div>
                                        <p>Please enter the sub-services you offer. For example 3D Modeling, Design...etc.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container" id="step_2_content">
                        {{--Appended Content Here--}}
                    </div>
                </div>
                <div class="step-3" style="display: none;">
                    <div class="container" id="step_3_header">
                        <div class="row">
                            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="ui-block">
                                    <div class="ui-block-title">
                                        <div class="h6 title">Step 3 |</div>
                                        <div class="h6 title" style="color: #38a9ff">Number of Steps</div>
                                        <p>Select the number of steps you would like your clients to go through.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container" id="step_3_content">
                        <input type="hidden" name="process_level" id = "process_level" value="">
                        <div class="row">
                            <div class="col col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="ui-block">
                                    <!-- Post -->
                                    <article class="hentry blog-post">
                                        <div class="post-thumb">
                                            <img src="{{ asset('design_quiz_images/Diagram5.png') }}" alt="photo">
                                        </div>
                                        <div class="post-content">
                                            <a href="#" class="post-category bg-purple">Recommended</a>
                                            <a href="#" class="h4 post-title">3-Levels Process</a>
                                            <p>Clients will go through 3 levels of selections in order to submit their
                                                requirements.</p>

                                            <div class="author-date">

                                                <a class="h6 post__author-name fn" href="#">Easy to set up</a>
                                                <div class="post__date">
                                                    <time class="published" datetime="2017-03-24T18:18">
                                                        - Requires less images.
                                                    </time>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="align-center">

                                            <a href="#" class="btn btn-md-2 btn-border-think custom-color c-grey">More info</a>


                                            <button class="btn btn-blue btn-md-2" data-level="3" id = "process_level_btn_1">
                                                Select 3-Steps
                                            </button>
                                        </div>

                                    </article>

                                    <!-- ... end Post -->
                                </div>
                            </div>
                            <div class="col col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="ui-block">


                                    <!-- Post -->

                                    <article class="hentry blog-post">

                                        <div class="post-thumb">
                                            <img src="{{ asset('design_quiz_images/Diagram4.png') }}" alt="photo">
                                        </div>

                                        <div class="post-content">
                                            <a href="#" class="post-category bg-primary">Advanced Selection</a>
                                            <a href="#" class="h4 post-title">4-Levels Process</a>
                                            <p>Clients will go through 4 levels of selections in order to submit their
                                                requirments.</p>

                                            <div class="author-date">

                                                <a class="h6 post__author-name fn" href="#">Advanced setup</a>
                                                <div class="post__date">
                                                    <time class="published" datetime="2017-03-24T18:18">
                                                        - Requires More Images
                                                    </time>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="align-center">

                                            <a href="#" class="btn btn-md-2 btn-border-think custom-color c-grey">More info</a>

                                            <button class="btn btn-blue btn-md-2" data-level="4" id = "process_level_btn_2">
                                                Select 4-Steps
                                            </button>
                                        </div>

                                    </article>

                                    <!-- ... end Post -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="step-4" style="display: none;">
                    <div class="container" id="step_4_header">
                        <div class="row">
                            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="ui-block">
                                    <div class="ui-block-title">
                                        <div class="h6 title">Step 4 |</div>
                                        <div class="h6 title" style="color: #38a9ff">Process Structure Type</div>
                                        <p>Are the images you want to appear are based on previous choices or fix?</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container" id="step_4_content">
                        <div class="row">
                            <input type="hidden" name="process_type" id = "process_type" value="NRBDS">
                            <div class="col col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="ui-block">
                                    <!-- Post -->
                                    <article class="hentry blog-post">
                                        <div class="post-thumb">
                                            <img src="{{ asset('design_quiz_images/Diagram.png') }}" alt="photo">
                                        </div>
                                        <div class="post-content">
                                            <a href="#" class="post-category bg-purple">Recommended</a>
                                            <a href="#" class="h4 post-title">Non-Related Based Diagram Structure</a>
                                            <p>The NRDS allows a set of options to appear without being dependant on previous level
                                                selections.</p>

                                            <div class="author-date">

                                                <a class="h6 post__author-name fn" href="#">Easy to set up</a>
                                                <div class="post__date">
                                                    <time class="published" datetime="2017-03-24T18:18">
                                                        - Requires less images.
                                                    </time>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="align-center">

                                            <a href="#" class="btn btn-md-2 btn-border-think custom-color c-grey">More info</a>

                                            <button class="btn btn-blue btn-md-2 selected" data-type="NRBDS" id = "process_type_btn_1">
                                                Select the NRDS
                                            </button>

                                        </div>

                                    </article>

                                    <!-- ... end Post -->
                                </div>
                            </div>
                            <div class="col col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <div class="ui-block">


                                    <!-- Post -->

                                    <article class="hentry blog-post">

                                        <div class="post-thumb">
                                            <img src="{{ asset('design_quiz_images/Diagram2.png') }}" alt="photo">
                                        </div>

                                        <div class="post-content">
                                            <a href="#" class="post-category bg-primary">Advanced Selection</a>
                                            <a href="#" class="h4 post-title">Relation Based Diagram Structure</a>
                                            <p>The RBDS is dependant based structure, the options in any level will be dependant and
                                                related to pervious selections.<br></p>

                                            <div class="author-date">

                                                <a class="h6 post__author-name fn" href="#">Advanced setup </a>
                                                <div class="post__date">
                                                    <time class="published" datetime="2017-03-24T18:18">
                                                        - Requires More Images
                                                    </time>
                                                </div>
                                            </div>


                                        </div>
                                        <div class="align-center">


                                            <button class="btn btn-blue btn-md-2" data-type="RBDS" id = "process_type_btn_2">
                                                Soon
                                            </button>

                                        </div>

                                    </article>

                                    <!-- ... end Post -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="step-5" style="display: none;">
                    <div class="container" id="step_5_header">
                        <div class="row">
                            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="ui-block">
                                    <div class="ui-block-title">
                                        <div class="h6 title">Step 5 |</div>
                                        <div class="h6 title" style="color: #38a9ff">Add Images & Set options</div>
                                        <p>Add the images and options you would like the client to go through.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container" id="step_5_content">

                        <div class="photo-album-wrapper" id="step-block-1">

                            <div class="photo-album-item-wrap col-4-width">

                                <div class="photo-album-item" data-mh="album-item" style="min-height: 336px;">
                                    <div class="photo-item">
                                    </div>
                                    <div class="content">

                                        <a href="#" class="title h5" data-toggle="modal" data-target="#create-photo-album">LEVEL 1</a>
                                        <span class="sub-title">Add a question here to be displayed prior to image selection.</span>
                                        <textarea id = "step_one_question" class="form-control" style="border-color: #38a9ff"
                                                  placeholder="Example: What is the design style you would like to have? "></textarea>


                                    </div>

                                </div>
                            </div>

                            <div class="photo-album-item-wrap col-4-width">
                                <div class="photo-album-item" data-mh="album-item">
                                    <div class="photo-item image-upload" >
                                        <label for="step_one_option_one_image">
                                            <img class = 'step_one_option_one_image img-preview' src="{{ asset('design_quiz_images/Add-icon.png') }}" alt="photo">
                                        </label>
                                        <input id = "step_one_option_one_image" type="file">
                                    </div>

                                    <div class="content">
                                        <a href="#" class="title h6">Image 1 Title</a>
                                        <p>Recommended: 350x500px</p>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Example: Modern</label>
                                            <input id = "step_one_option_one_title" class="form-control" placeholder="" type="text" value="">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="photo-album-item-wrap col-4-width">
                                <div class="photo-album-item" data-mh="album-item">
                                    <div class="photo-item image-upload" >
                                        <label for="step_one_option_two_image">
                                            <img class = 'step_one_option_two_image img-preview' src="{{ asset('design_quiz_images/Add-icon.png') }}" alt="photo">
                                        </label>
                                        <input id = "step_one_option_two_image" type="file">
                                    </div>

                                    <div class="content">
                                        <a href="#" class="title h6">Image 2 Title</a>
                                        <p>Recommended: 350x500px</p>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Example: Modern</label>
                                            <input id = "step_one_option_two_title" class="form-control" placeholder="" type="text" value="">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="photo-album-item-wrap col-4-width">
                                <div class="photo-album-item" data-mh="album-item">
                                    <div class="photo-item image-upload" >
                                        <label for="step_one_option_three_image">
                                            <img class = 'step_one_option_three_image img-preview' src="{{ asset('design_quiz_images/Add-icon.png') }}" alt="photo">
                                        </label>
                                        <input id = "step_one_option_three_image" type="file">
                                    </div>

                                    <div class="content">
                                        <a href="#" class="title h6">Image 3 Title</a>
                                        <p>Recommended: 350x500px</p>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Example: Modern</label>
                                            <input id = "step_one_option_three_title" class="form-control" placeholder="" type="text" value="">
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>
                        <div class="photo-album-wrapper" id="step-block-2">

                            <div class="photo-album-item-wrap col-4-width">

                                <div class="photo-album-item" data-mh="album-item" style="min-height: 336px;">
                                    <div class="photo-item">
                                    </div>
                                    <div class="content">

                                        <a href="#" class="title h5" data-toggle="modal" data-target="#create-photo-album">LEVEL 2</a>
                                        <span class="sub-title">Add a question here to be displayed prior to image selection.</span>
                                        <textarea id = "step_two_question" class="form-control" style="border-color: #38a9ff"
                                                  placeholder="Example: What is the design style you would like to have? "></textarea>


                                    </div>

                                </div>
                            </div>

                            <div class="photo-album-item-wrap col-4-width">
                                <div class="photo-album-item" data-mh="album-item">
                                    <div class="photo-item image-upload" >
                                        <label for="step_two_option_one_image">
                                            <img class = 'step_two_option_one_image img-preview' src="{{ asset('design_quiz_images/Add-icon.png') }}" alt="photo">
                                        </label>
                                        <input id = "step_two_option_one_image" type="file">
                                    </div>

                                    <div class="content">
                                        <a href="#" class="title h6">Image 1 Title</a>
                                        <p>Recommended: 350x500px</p>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Example: Modern</label>
                                            <input id = "step_two_option_one_title" class="form-control" placeholder="" type="text" value="">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="photo-album-item-wrap col-4-width">
                                <div class="photo-album-item" data-mh="album-item">
                                    <div class="photo-item image-upload" >
                                        <label for="step_two_option_two_image">
                                            <img class = 'step_two_option_two_image img-preview' src="{{ asset('design_quiz_images/Add-icon.png') }}" alt="photo">
                                        </label>
                                        <input id = "step_two_option_two_image" type="file">
                                    </div>

                                    <div class="content">
                                        <a href="#" class="title h6">Image 2 Title</a>
                                        <p>Recommended: 350x500px</p>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Example: Modern</label>
                                            <input id = "step_two_option_two_title" class="form-control" placeholder="" type="text" value="">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="photo-album-item-wrap col-4-width">
                                <div class="photo-album-item" data-mh="album-item">
                                    <div class="photo-item image-upload" >
                                        <label for="step_two_option_three_image">
                                            <img class = 'step_two_option_three_image img-preview' src="{{ asset('design_quiz_images/Add-icon.png') }}" alt="photo">
                                        </label>
                                        <input id = "step_two_option_three_image" type="file">
                                    </div>

                                    <div class="content">
                                        <a href="#" class="title h6">Image 3 Title</a>
                                        <p>Recommended: 350x500px</p>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Example: Modern</label>
                                            <input id = "step_two_option_three_title" class="form-control" placeholder="" type="text" value="">
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>
                        <div class="photo-album-wrapper" id="step-block-3">

                            <div class="photo-album-item-wrap col-4-width">

                                <div class="photo-album-item" data-mh="album-item" style="min-height: 336px;">
                                    <div class="photo-item">
                                    </div>
                                    <div class="content">

                                        <a href="#" class="title h5" data-toggle="modal" data-target="#create-photo-album">LEVEL 3</a>
                                        <span class="sub-title">Add a question here to be displayed prior to image selection.</span>
                                        <textarea id = "step_three_question" class="form-control" style="border-color: #38a9ff"
                                                  placeholder="Example: What is the design style you would like to have? "></textarea>


                                    </div>

                                </div>
                            </div>

                            <div class="photo-album-item-wrap col-4-width">
                                <div class="photo-album-item" data-mh="album-item">
                                    <div class="photo-item image-upload" >
                                        <label for="step_three_option_one_image">
                                            <img class = 'step_three_option_one_image img-preview' src="{{ asset('design_quiz_images/Add-icon.png') }}" alt="photo">
                                        </label>
                                        <input id = "step_three_option_one_image" type="file">
                                    </div>

                                    <div class="content">
                                        <a href="#" class="title h6">Image 1 Title</a>
                                        <p>Recommended: 350x500px</p>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Example: Modern</label>
                                            <input id = "step_three_option_one_title" class="form-control" placeholder="" type="text" value="">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="photo-album-item-wrap col-4-width">
                                <div class="photo-album-item" data-mh="album-item">
                                    <div class="photo-item image-upload" >
                                        <label for="step_three_option_two_image">
                                            <img class = 'step_three_option_two_image img-preview' src="{{ asset('design_quiz_images/Add-icon.png') }}" alt="photo">
                                        </label>
                                        <input id = "step_three_option_two_image" type="file">
                                    </div>

                                    <div class="content">
                                        <a href="#" class="title h6">Image 2 Title</a>
                                        <p>Recommended: 350x500px</p>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Example: Modern</label>
                                            <input id = "step_three_option_two_title" class="form-control" placeholder="" type="text" value="">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="photo-album-item-wrap col-4-width">
                                <div class="photo-album-item" data-mh="album-item">
                                    <div class="photo-item image-upload" >
                                        <label for="step_three_option_three_image">
                                            <img class = 'step_three_option_three_image img-preview' src="{{ asset('design_quiz_images/Add-icon.png') }}" alt="photo">
                                        </label>
                                        <input id = "step_three_option_three_image" type="file">
                                    </div>

                                    <div class="content">
                                        <a href="#" class="title h6">Image 3 Title</a>
                                        <p>Recommended: 350x500px</p>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Example: Modern</label>
                                            <input id = "step_three_option_three_title" class="form-control" placeholder="" type="text" value="">
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>
                        <div class="photo-album-wrapper" id="step-block-4">

                            <div class="photo-album-item-wrap col-4-width">

                                <div class="photo-album-item" data-mh="album-item" style="min-height: 336px;">
                                    <div class="photo-item">
                                    </div>
                                    <div class="content">

                                        <a href="#" class="title h5" data-toggle="modal" data-target="#create-photo-album">LEVEL 4</a>
                                        <span class="sub-title">Add a question here to be displayed prior to image selection.</span>
                                        <textarea id = "step_four_question" class="form-control" style="border-color: #38a9ff"
                                                  placeholder="Example: What is the design style you would like to have? "></textarea>


                                    </div>

                                </div>
                            </div>

                            <div class="photo-album-item-wrap col-4-width">
                                <div class="photo-album-item" data-mh="album-item">
                                    <div class="photo-item image-upload" >
                                        <label for="step_four_option_one_image">
                                            <img class = 'step_four_option_one_image img-preview' src="{{ asset('design_quiz_images/Add-icon.png') }}" alt="photo">
                                        </label>
                                        <input id = "step_four_option_one_image" type="file">
                                    </div>

                                    <div class="content">
                                        <a href="#" class="title h6">Image 1 Title</a>
                                        <p>Recommended: 350x500px</p>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Example: Modern</label>
                                            <input id = "step_four_option_one_title" class="form-control" placeholder="" type="text" value="">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="photo-album-item-wrap col-4-width">
                                <div class="photo-album-item" data-mh="album-item">
                                    <div class="photo-item image-upload" >
                                        <label for="step_four_option_two_image">
                                            <img class = 'step_four_option_two_image img-preview' src="{{ asset('design_quiz_images/Add-icon.png') }}" alt="photo">
                                        </label>
                                        <input id = "step_four_option_two_image" type="file">
                                    </div>

                                    <div class="content">
                                        <a href="#" class="title h6">Image 2 Title</a>
                                        <p>Recommended: 350x500px</p>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Example: Modern</label>
                                            <input id = "step_four_option_two_title" class="form-control" placeholder="" type="text" value="">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="photo-album-item-wrap col-4-width">
                                <div class="photo-album-item" data-mh="album-item">
                                    <div class="photo-item image-upload" >
                                        <label for="step_four_option_three_image">
                                            <img class = 'step_four_option_three_image img-preview' src="{{ asset('design_quiz_images/Add-icon.png') }}" alt="photo">
                                        </label>
                                        <input id = "step_four_option_three_image" type="file">
                                    </div>

                                    <div class="content">
                                        <a href="#" class="title h6">Image 3 Title</a>
                                        <p>Recommended: 350x500px</p>
                                        <div class="form-group label-floating">
                                            <label class="control-label">Example: Modern</label>
                                            <input id = "step_four_option_three_title" class="form-control" placeholder="" type="text" value="">
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>

                    </div>
                </div>
                <div class="step-6" style="display: none;">
                    <div class="container" id="step_6_header">
                        <div class="row">
                            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <div class="ui-block">
                                    <div class="ui-block-title">
                                        <div class="h6 title">Step 6 |</div>
                                        <div class="h6 title" style="color: #38a9ff">Required Information</div>
                                        <p>Select all the required information the client must fill in order to be submit an order.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container" id="step_6_content">
                        <div class="row">
                            <div class="col col-lg-3 col-md-6 col-sm-6 col-6">
                                <div class="ui-block">
                                    <div class="available-widget">

                                        <div class="checkbox">
                                            <label>
                                                <input name="requiredFields[]" type="checkbox" value="name">
                                                Name
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col col-lg-3 col-md-6 col-sm-6 col-6">
                                <div class="ui-block">
                                    <div class="available-widget">
                                        <div class="checkbox">
                                            <label>
                                                <input name="requiredFields[]" type="checkbox" value="email">
                                                Email Address
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col col-lg-3 col-md-6 col-sm-6 col-6">
                                <div class="ui-block">
                                    <div class="available-widget">
                                        <div class="checkbox">
                                            <label>
                                                <input name="requiredFields[]" type="checkbox" value="phone">
                                                Phone Number
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col col-lg-3 col-md-6 col-sm-6 col-6">
                                <div class="ui-block">
                                    <div class="available-widget">
                                        <div class="checkbox">
                                            <label>
                                                <input name="requiredFields[]" type="checkbox" value="address">
                                                Address
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col col-lg-3 col-md-6 col-sm-6 col-6">
                                <div class="ui-block">
                                    <div class="available-widget">
                                        <div class="checkbox">
                                            <label>
                                                <input name="requiredFields[]" type="checkbox" value="attachment">
                                                Attachments
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col col-lg-3 col-md-6 col-sm-6 col-6">
                                <div class="ui-block">
                                    <div class="available-widget">
                                        <div class="checkbox">
                                            <label>
                                                <input name="requiredFields[]" type="checkbox" value="budget">
                                                Budget
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col col-lg-3 col-md-6 col-sm-6 col-6">
                                <div class="ui-block">
                                    <div class="available-widget">
                                        <div class="checkbox">
                                            <label>
                                                <input name="requiredFields[]" type="checkbox" value="overview">
                                                Project Overview
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col col-lg-3 col-md-6 col-sm-6 col-6">
                                <div class="ui-block">
                                    <div class="available-widget">
                                        <div class="checkbox">
                                            <label>
                                                <input name="requiredFields[]" type="checkbox" value="location">
                                                Project Location
                                            </label>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
                <div class="crumina-module align-center" id = "next" data-current_step = "1">
                    <a href="" class="btn btn-breez btn-md">Next</a>
                </div>
                <div class="crumina-module crumina-heading align-center" id = "save" style="display: none;">
                    <button type = "submit" class="btn btn-primary btn-lg">LAUNCH IT!</button>
                </div>
            </form>
    </div>
@endsection
@include('layouts.loader')
@section('script')
<script>
    var formData = new FormData();
    $('#next').click(function (e) {
        e.preventDefault()
        var FinishConfigSteps = false;
        var current_step = $(this).attr('data-current_step');
        switch (current_step) {
            case '1':
                var index = 0;
                $("input:checkbox[name='services[]']:checked").each(function(){
                    index++;
                    var html = '<div class="ui-block">' +
                    '               <div class="ui-block-content">' +
                    '                    <div class="row">' +
                    '                        <div class="col col-lg-12 col-md-12 col-sm-12 col-12">' +
                    '                             <div class="ui-block-title">' +
                    '                                  <h6 class="title">Service '+ index +' | '+ $(this).val() +'</h6>' +
                    '                             </div>' +
                    '                         </div>' +
                    '                         <div class="col col-lg-12 col-md-12 col-sm-12 col-12">' +
                    '                              <div class="form-group label-floating has-success">' +
                    '                                  <label class="control-label"></label>' +
                    '                                  <input name = "sub_service_'+ index +'" id = "sub_service_'+ index +'" type="text" class="form-control form-control-success"' +
                    '                                      placeholder="Designer shall enter one services or multiple separated by comma">' +
                    '                              </div>' +
                    '                         </div>' +
                    '                     </div>' +
                    '                 </div>' +
                    '             </div>';
                    $('#step_2_content').append(html);
                    formData.append('services['+index+'][service]', $(this).val());
                });
                if(index == 0){
                    swal('you have to provide at least one service to set up your quiz')
                    return false;
                }
                break;
            case '2':
                var index = 0;
                var failed_block = '';
                $("input:checkbox[name='services[]']:checked").each(function(){
                    index++;
                    var sub_service = $("#sub_service_" + index);
                    if(!sub_service.val()){
                        if(failed_block){
                            failed_block += ' and ' + $(this).val();
                        }else{
                            failed_block = $(this).val();
                        }
                    }else{
                        formData.append('services['+index+'][sub_service]', sub_service.val());
                    }
                });
                if(failed_block){
                    swal(failed_block + ' service should have at least one sub-service')
                    return false;
                }
                break;
            case '3':
                var process_level = $('#process_level').val();
                if(!process_level){
                    swal('You have to select process level')
                    return false;
                }
                formData.append('process_level', process_level);
                if(process_level == '3'){
                    $('#step-block-4').fadeOut(300);
                }
                break;
            case '4':
                var process_type = $('#process_type').val();
                if(!process_type){
                    swal('You have to select process type')
                    return false;
                }
                formData.append('process_type', process_type);
                break;
            case '5':
                var process_level = $('#process_level').val();
                if(process_level == '3'){
                    var levelOne = 'step_one';
                    var levelTwo = 'step_two';
                    var levelThree = 'step_three';
                   if( !validateStepValues(levelOne , formData) || !validateStepValues(levelTwo , formData) || !validateStepValues(levelThree , formData)){
                       return false;
                   }
                   // var levelOne = 'step_one';
                   // if( !validateStepValues(levelOne , formData)){
                   //     return false;
                   // }
                }else{
                    var levelOne = 'step_one';
                    var levelTwo = 'step_two';
                    var levelThree = 'step_three';
                    var levelFour = 'step_four';
                    if( !validateStepValues(levelOne , formData) || !validateStepValues(levelTwo , formData) || !validateStepValues(levelThree , formData)
                        || !validateStepValues(levelFour , formData)){
                        return false;
                    }
                }
                break;
            case '6':
                var countRequiredParams = 0;
                $("input:checkbox[name='requiredFields[]']:checked").each(function(){
                    countRequiredParams++;
                    formData.append('requiredFields['+countRequiredParams+']', $(this).val());
                });
                console.log(countRequiredParams);
                if(countRequiredParams <= 2){
                    swal('you have to select up to 2 options as required fields in your quize')
                    return false;
                }
                FinishConfigSteps = true;
                break;
            default:
                alert('somthing went wroing hit refresh and try again')
        }
        $('#step_'+current_step+'_content').slideUp(300);
        current_step++;
        if(!FinishConfigSteps){
        $('.step-' + current_step).slideDown(300);
        $(this).attr('data-current_step' , current_step)
        }else{
            $('#next').fadeOut(100)
            $('#save').fadeIn(100)
        }
    });
     function validateStepValues(stepValues, formData){
         var stepQuestion = $('#'+ stepValues +'_question').val()
         var stepTitleOne = $('#'+ stepValues +'_option_one_title').val()
         var stepImageOne = $('#'+ stepValues +'_option_one_image')[0].files
         var stepTitleTwo = $('#'+ stepValues +'_option_two_title').val()
         var stepImageTwo = $('#'+ stepValues +'_option_two_image')[0].files
         var stepTitleThree = $('#'+ stepValues +'_option_three_title').val()
         var stepImageThree = $('#'+ stepValues +'_option_three_image')[0].files
         if((!stepImageOne || !stepTitleOne) || (!stepImageTwo || !stepTitleTwo) || (!stepImageThree || !stepTitleThree) || (!stepImageThree || !stepTitleThree )){
             swal('Their`s an issue in ' + stepValues + ' double check step values')
             return false;
         }else{
             formData.append('steps['+stepValues+'][question]', stepQuestion);
             formData.append('steps['+stepValues+'][1][title]', stepTitleOne);
             formData.append('steps['+stepValues+'][1][image]', stepImageOne[0]);
             formData.append('steps['+stepValues+'][2][title]', stepTitleTwo);
             formData.append('steps['+stepValues+'][2][image]', stepImageTwo[0]);
             formData.append('steps['+stepValues+'][3][title]', stepTitleThree);
             formData.append('steps['+stepValues+'][3][image]', stepImageThree[0]);
             return true;
         }
    }
    $('[data-level]').click(function (e) {
        $('#process_level_btn_1').removeClass('selected');
        $('#process_level_btn_2').removeClass('selected');
        e.preventDefault();
        var level = $(this).attr('data-level');
        $('#process_level').val(level);
        $(this).addClass('selected')
    })
    $('[data-type]').click(function (e) {
        e.preventDefault();
        // $('#process_type_btn_1').removeClass('selected');
        // $('#process_type_btn_2').removeClass('selected');
        // var type = $(this).attr('data-type');
        // $('#process_type').val(type);
        // $(this).addClass('selected')
    })
    $(document).on('change', "input[type = 'file']", function () {
        var file = this.files[0];
        var fileType = file["type"];
        var validImageTypes = ["image/gif", "image/jpeg", "image/png" , "image/jpg"];
        if ($.inArray(fileType, validImageTypes) < 0) {
          alert('invalid file type');
          return;
        }
        var imageTag = $(this).attr('id');
        readURL(this, $('.' + imageTag + ''), 0);
    });
    $(document).on('submit', 'form#createQuiz', function (form) {
        for (var pair of formData.entries()) {
            console.log(pair[0]+ ', ' + pair[1]);
        }
        form.preventDefault();
        formData.append('_token', CSRF_TOKEN);
        $.ajax({
            url: "/tool/quiz/save",
            method: "POST",
            data: formData,
            dataType: "JSON",
            contentType: false,
            cache: false,
            processData: false,
            beforeSend: function () {
                showLoader()
            },
            success: function (response) {
                if (response) {
                    if (response.success) {
                        setTimeout(function(){ swal(response.msg) }, 3000);
                        location.reload();
                    } else {
                        swal(response.msg)
                    }
                } else {
                    swal("Something went wrong, try again")
                }
            },
            error: function (jqXHR, textStatus) {
                alert("Something went wrong, try again")
            },
            complete: function () {
                hideLoader()
            }
        });
    })
    $('#overwrite_quiz').click(function(e){
        e.preventDefault();
        $('#has_quiz_dev').slideUp(300);
        $('#createQuiz').slideDown(300);
    });

</script>
@endsection