@extends('website.herox.layout')
@section('content')
<!--slider start-->
<section id="home" class="dark-slider p-0">
  <h2 class="d-none" aria-hidden="true">slider</h2>
  <div id="rev_slider_12_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="Herox-one"
       data-source="gallery" style="background:transparent;padding:0px;">
    <!-- START REVOLUTION SLIDER 5.4.8.1 fullscreen mode -->
    <div id="rev_slider_12_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.8.1">
      <ul>    <!-- SLIDE  -->
        <li data-index="rs-63" data-transition="crossfade" data-slotamount="default" data-hideafterloop="0"
            data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="500"
            data-rotate="0"
            data-saveperformance="off" data-title="Slide" data-param1="01" data-param2="" data-param3=""
            data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9=""
            data-param10="" data-description="">
          <!-- MAIN IMAGE -->
          <img src="{{ asset($welcome->background) }}" alt="" data-bgposition="center center"
               data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
          <!-- LAYERS -->

          <!-- LAYER NR. 1 -->
          <div class="tp-caption overlay-text tp-resizeme"
               data-x="['left','left','center','center']" data-hoffset="['35','16','0','0']"
               data-y="['top','top','middle','middle']" data-voffset="['730','685','0','-70']"
               data-fontsize="['60','40','40','40']"
               data-lineheight="['60','50','50','40']"
               data-width="none"
               data-height="none"
               data-whitespace="nowrap"

               data-type="text"
               data-responsive_offset="on"

               data-frames='[{"delay":630,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":320,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
               data-textAlign="['inherit','inherit','center','center']"
               data-paddingtop="[0,0,0,0]"
               data-paddingright="[0,0,0,0]"
               data-paddingbottom="[0,0,0,0]"
               data-paddingleft="[0,0,0,0]"

               style="z-index: 5; white-space: nowrap; font-size: 60px; line-height: 60px; font-weight: 500; color: #ffffff; letter-spacing: 8px;font-family:Exo;">
            {{ $welcome->main }}
          </div>

          <!-- LAYER NR. 2 -->
          <div class="tp-caption   tp-resizeme"
               data-x="['center','center','center','center']" data-hoffset="['20' ,'-59','-69','811']"
               data-y="['top','top','middle','middle']" data-voffset="['730','678','-71','-71']"
               data-width="none"
               data-height="none"
               data-whitespace="nowrap"

               data-type="text"
               data-responsive_offset="on"

               data-frames='[{"delay":1560,"speed":1230,"frame":"0","from":"opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
               data-textAlign="['inherit','inherit','inherit','inherit']"
               data-paddingtop="[0,0,0,0]"
               data-paddingright="[0,0,0,0]"
               data-paddingbottom="[0,0,0,0]"
               data-paddingleft="[0,0,0,0]"

               style="z-index: 6; white-space: nowrap; font-size: 40px; line-height: 60px; font-weight: 300; color: rgba(255,255,255,0.49); letter-spacing: 0px;font-family: 'Exo', sans-serif;">
            <div class="slider-line">|</div>
          </div>

          <!-- LAYER NR. 3 -->
          <div class="tp-caption overlay-text tp-resizeme"
               data-x="['center','center','center','center']" data-hoffset="['322','236','0','0']"
               data-y="['bottom','bottom','middle','middle']" data-voffset="['90','40','78','0']"
               data-fontsize="['15','14','14','14']"
               data-width="['570','557','557','485']"
               data-height="none"
               data-whitespace="normal"

               data-type="text"
               data-responsive_offset="on"

               data-frames='[{"delay":1230,"speed":1500,"frame":"0","from":"y:50px;opacity:0;","to":"o:1;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
               data-textAlign="['left','left','center','center']"
               data-paddingtop="[0,0,0,0]"
               data-paddingright="[0,0,0,0]"
               data-paddingbottom="[0,0,0,0]"
               data-paddingleft="[0,0,0,0]"

               style="z-index: 7; white-space: nowrap;">
            <p class="slider-text" style=" font-size: 15px; line-height: 20px; font-weight: 300; color: #ffffff; letter-spacing: 2px;font-family: 'Exo', sans-serif;">
              {{ str_limit($welcome->description) }}</p>
          </div>
        </li>



        @if(count($projects))
        @for($key=0; $key<5; $key++)
          @php
            $row = $projects[$key % count($projects)];

            if(!optional($row->image)->image){
              continue;
            }
          @endphp
          <li data-index="rs-{{ $key+2 }}" data-transition="crossfade" data-slotamount="default" data-hideafterloop="0"
              data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="500"
              data-rotate="0"
              data-saveperformance="off" data-title="Slide" data-param1="0{{ $key+2 }}" data-param2="" data-param3=""
              data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9=""
              data-param10="" data-description="">
            <!-- MAIN IMAGE -->
            <img src="{{ asset($row->image->image) }}" alt="{{ $row->title }}" data-bgposition="center center"
                 data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
            <!-- LAYERS -->


            <div class="tp-caption overlay-text tp-resizeme"
                 data-x="['left','left','center','center']" data-hoffset="['35','16','0','0']"
                 data-y="['top','top','middle','middle']" data-voffset="['730','685','0','-70']"
                 data-fontsize="['60','40','40','40']"
                 data-lineheight="['60','50','50','40']"
                 data-width="none"
                 data-height="none"
                 data-whitespace="nowrap"
                 data-type="text"
                 data-responsive_offset="on"
                 data-frames='[{"delay":630,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":320,"frame":"999","to":"opacity:0;","ease":"Power3.easeInOut"}]'
                 data-textAlign="['inherit','inherit','center','center']"
                 data-paddingtop="[0,0,0,0]"
                 data-paddingright="[0,0,0,0]"
                 data-paddingbottom="[0,0,0,0]"
                 data-paddingleft="[0,0,0,0]"
                 style="z-index: 5; white-space: nowrap; font-size: 60px; line-height: 60px; font-weight: 500; color: #a9a6a6; letter-spacing: 8px;font-family:Exo;">
              {{ $row->title }}
            </div>

          </li>
        @endfor
        @endif
      </ul>
      <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
    </div>
    <div class="slider-social">
      @if($user->fb_link)
      <a href="{{ website_check_facebook_link($user->fb_link) }}" class="facebook-text-hvr"><i class="ti ti-facebook" aria-hidden="true"></i></a>
      @endif
      @if($user->tw_link)
      <a href="{{ website_check_twitter_link($user->tw_link) }}" class="twitter-text-hvr"><i class="ti ti-twitter-alt" aria-hidden="true"></i></a>
      @endif
      @if($user->ig_link)
      <a href="{{ website_check_instagram_link($user->ig_link) }}" class="instagram-text-hvr"><i class="ti ti-instagram" aria-hidden="true"></i></a>
      @endif
      @if($user->pin_link)
      <a href="{{ website_check_pinterest_link($user->pin_link) }}" class="pinterest-text-hvr"><i class="ti ti-pinterest" aria-hidden="true"></i></a>
      @endif
    </div>
    <a href="#services" class="scroll-down scroll d-none d-lg-block">
      <div class="triangle-down"></div>
    </a>
  </div>
</section>
<!--slider end-->

@if($services && count($services))
@php
  $servicesCount = count($services);
@endphp
<a name="service"></a>
<section id="services" class="feature circle-top pb-0">
  <div class="container">
    <div class="row">
      @for($key=0; $key<4; $key++)
        @php
          $service = $services[$key % count($services)];
        @endphp
      {{--@foreach($services as $service)--}}
      {{--<div class="col-md-{{ 12/$servicesCount }} mb-xs-5">--}}
      <div class="col-md-3 mb-xs-5">
        <div class="feature-box text-center">
          @if($service['icons'])
          <img class="service-icon" src="{{ $service['icons'] }}" />
          @else
          <i class="ti ti-plus feature-icon" aria-hidden="true"></i>
          @endif
          <p class="mt-3 mb-3">{{ $service['title'] }}</p>
          <h4 class="text-capitalize">{{ $service['description'] }}</h4>
          <span class="hr-line mt-4 mb-4"></span>
        </div>
      </div>
      {{--@endforeach--}}
      @endfor
    </div>
  </div>
</section>
@endif

@if(count($team))

  <!--team start-->
  <section id="team" class="team-three bg-light text-center text-lg-left">
    <div class="container sm-container-full">
      <div class="row">
        <div class="col-sm-12">
          <div class="title text-center pb-5">
            <h2 class="font-weight-600 m-0">TEAM</h2>
            <span class="hr-line mt-4 mb-4"></span>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <div class="owl-team-three owl-carousel owl-theme">
            @foreach($team as $key=>$row)
            <div class="team-box-three item">
              <div class="image">
                <img src="{{ asset($row['image']) }}" class="team-picture" alt="">
                {{--<div class="overlay center-block">--}}
                  {{--<ul class="social_icon">--}}
                    {{--<li><a class="facebook-bg-hvr" href="javascript:void(0)"><i class="ti ti-facebook"></i> </a> </li>--}}
                    {{--<li><a class="twitter-bg-hvr" href="javascript:void(0)"><i class="ti ti-twitter"></i> </a> </li>--}}
                    {{--<li><a class="linkedin-bg-hvr" href="javascript:void(0)"><i class="ti ti-linkedin"></i> </a> </li>--}}
                    {{--<li><a class="instagram-bg-hvr" href="javascript:void(0)"><i class="ti ti-instagram"></i> </a> </li>--}}
                    {{--<li><a class="pinterest-bg-hvr" href="javascript:void(0)"><i class="ti ti-pinterest-alt"></i> </a> </li>--}}
                  {{--</ul>--}}
                {{--</div>--}}
              </div>
              <div class="team-content">
                <h3 class="mb-1">{{ $row['title'] }}</h3>
                <p class="m-0 pre">{!! $row['description'] !!}</p>
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--team end-->
@endif

@if(count($projects))
<section id="work" class="our-work pb-0">
  <div class="row">
    <div class="col-sm-12">
      <div class="title text-center pb-5">
        <h2 class="font-weight-600 m-0">Creative Works</h2>
        <span class="hr-line mt-4 mb-4"></span>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 p-5 pt-0">
      <div class="content-carousel">
        <div class="owl-carousel owl-work">
          @foreach($projects as $key=>$project)
            @if($key>9)
              @break
            @endif
            <div> <img src="{{ asset(optional($project->image)->image) }}" alt="{{ $project->title }}" title="{{ $project->title }}" ></div>
          @endforeach
        </div>
      </div>

    </div>
  </div>
</section>
@endif

@if(count($articles))
<!--blog-->
<section id="blog" class="blog-list bg-light text-center text-md-left">
  <div class="row">
    <div class="col-sm-12">
      <div class="title text-center pb-5">
        <h2 class="font-weight-600 m-0">Blog</h2>
        <span class="hr-line mt-4 mb-4"></span>
      </div>
    </div>
  </div>
  <div class="container">
    @if(count($articles)>4)
    <div class="row">
      <div class="col-md-12 mb-5">
        <div class="blog-box text-right">
          <a class="btn btn-large btn-pink mb-xs-4" href="{{ websiteRoute('website.blog.list') }}"> View All</a>
        </div>
      </div>
    </div>
    @endif
    <div class="row">
      <!-- blog-item one -->
{{--      @foreach($articles as $key=>$article)--}}
      @for($key=0; $key<4; $key++)
        @php
          $article = $articles[$key % count($articles)];
        @endphp
      <div class="col-lg-3 col-md-6 mb-3 mb-xs-5">
        <div class="image" style="height: 255px;">
          @if($article->image)
          <img alt="image" class="article-img" src="{{ asset($article->image) }}">
          @else
          <img alt="image" class="article-img" src="{{ asset('themes/'.$theme.'/images/icon-no-image.svg') }}">
          @endif
        </div>
      </div>
      <div class="col-lg-3 col-md-6 mb-xs-5">
        <div class="blog-box">
          <h4 class="text-capitalize mb-4">{!! $article->title !!}</h4>
          <p class="mb-3 mb-xs-4">{!! str_limit(strip_tags($article->body), 30) !!}</p>
          <a class="btn btn-large btn-pink mb-xs-4" href="{{ websiteRoute('website.blog.show', [$article->id]) }}"> Read More</a>
        </div>
      </div>
      {{--@endforeach--}}
      @endfor
    </div>
  </div>
</section>
<!--blog end-->
@endif

@if($contact_us->show_tab)
<!-- Contact US -->
<section id="contact" class="contact">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="title text-center pb-5">
          <h2 class="font-weight-600 m-0">{{ $contact_us->title }}</h2>
          <span class="hr-line mt-4 mb-4"></span>
          <p class="mb-4">{{ $contact_us->message }}</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <form class="getin_form" action="{{ websiteRoute('website.contact_us') }}" method="post">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-sm-12" id="result"></div>

            <div class="col-md-12 col-sm-12">
              <div class="form-group">
                <input class="form-control" type="text" placeholder="Enter subject" required id="title" name="title">
              </div>
            </div>
            <div class="col-md-12 col-sm-12">
              <div class="form-group mb-4">
                <textarea class="form-control" placeholder="Enter message" id="message" name="message"></textarea>
              </div>
            </div>
            <div class="col-sm-12">
              <button type="submit" class="btn btn-large btn-pink w-100" id="submit_btn">Send</button>
            </div>
          </div>
        </form>
      </div>
      <div class="col-md-6 col-sm-12 pl-4">
        <p class="mb-lg-5 mb-4 mt-2">{{ $user->description }}</p>
        <div class="row">
          <div class="col-md-6 col-sm-6 our-address mb-4">
            <h6 class="mb-3 font-weight-600">Our Address</h6>
            <p class="mb-2">Country: {{ optional($user->country)->text }} </p>
            <p class="mb-2">City: {{ optional($user->city)->text }} </p>
          </div>
          <div class="col-md-6 col-sm-6 our-address mb-4">
            <h6 class="mb-3 font-weight-600">Our Contact</h6>
            <p class="mb-2">Phone No.: {{ $user->phone }}</p>
            <p class="mb-2">E-mail: {{ $user->email }}</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Contact US Ends-->
@endif

@endsection

@section('script')
<script>
  $('.getin_form').submit(function(e){
    e.preventDefault();
    var $this = $(this);

    $('#submit_btn').prop('disabled', true);

    $.post($this.attr('action'), $this.serialize())
      .done(function (d) {
        $('#result').empty().append($('<span>').addClass(d.status? 'alert-success': 'alert-danger').text(d.message))
      })
      .always(function(){
        $('#submit_btn').prop('disabled', false);
      });

  })
</script>
@endsection

@section('css')
  <style>
    .content-carousel .owl-carousel .owl-item img {
      height: 669px;
    }
  </style>
@endsection