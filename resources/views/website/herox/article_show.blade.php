@extends('website.herox.layout')
@section('content')
  <!--cover-->
  <section class="page-header bg-3">
    <div class="container">
      <div class="row text-center text-white">
        <div class="col-sm-12 pt-md-5">
          <div class="page-title pt-md-5">
            <h2><span class="overlay-head">ARTICLE</span></h2>
            @if($welcome->main)
              <h5 class="pt-md-5"><span class="overlay-head">{{ $welcome->main }}</span></h5>
              <p class="pt-md-2"><span class="overlay-head">{{ str_limit($welcome->description) }}</span></p>
            @endif
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--cover end-->

  <!--page content-->
  <section class="page-content">
    <div class="container">
      <div class="blog-detail-item row">
        <div class="col-md-8 mb-xs-5">
          @if($article->image)
            <div class="blog-detail-img col-sm-12 mb-5 hover-effect">
              <img src="{{ asset($article->image) }}" class="list-img" alt="{{ $titleHeader }}">
            </div>
        @endif
        <!--blog contetn-->
          <div class="blog-item-content">
            <div class="content-text">
              <span class="category main-color tex"><a
                    href="{{ websiteRoute('website.blog.list', ['category_id'=>$article->category_id]) }}">{{ $article->category->text }}</a></span>
              - <span class="date">{{ $article->created_at->format('M d, Y') }}</span>
              <h4 class="mt-2 mb-4">{{ $article->title }}
              </h4>

              <div style="min-height: 200px">
                {!! $article->body !!}
              </div>
            </div>

            <!--blog tags & share-->
            <div class="blog-detail-tag">
              @forelse($article->tags as $tag)
                <a class="btn btn-small btn-transparent-gray"
                   href="{{ websiteRoute('website.blog.list', ['filter'=> ['search_article'=>$tag->text]]) }}">{{ $tag->text }}</a>
              @empty
                <a class="btn btn-small btn-transparent-gray" href="javascript:void(0);">Tags</a>
              @endforelse
              <ul class="blog-share list-unstyled">
                <li><a target="_blank"
                       href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(request()->url()) }}"
                       class="facebook-bg-hvr"><i class="ti ti-facebook"></i></a></li>
                <li><a target="_blank" class="twitter-bg-hvr"
                       href="https://twitter.com/intent/tweet?url={{ urlencode(request()->url()) }}">
                    <i class="ti ti-twitter-alt"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <!--right side-->
        <div class="col-md-4">
          <!--search-->
          <form action="{{ websiteRoute('website.blog.list') }}">
            <div class="widget d-flex bg-light mb-4">
              <input class="search" name="filter[search_article]" placeholder="Search.." type="text"
                     value="{{ isset(($r = request('filter', ['search_article'=>'']))['search_article'])? $r['search_article']: '' }}">
              <button type="submit" class="search-btn"><i class="ti-search" aria-hidden="true"></i></button>
            </div>
          </form>

          <div class="widget bg-light">
            <h5 class="mb-4">Category</h5>
            <!--list-->
            <ul class="list-unstyled blog-category m-0">
              @foreach($categories as $category)
                <li><a
                      href="{{ websiteRoute('website.blog.list', ['filter'=>['category_parent'=> $category->id]]) }}">{{ $category->text }}</a>
                </li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--page content end-->

@endsection