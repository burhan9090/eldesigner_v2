
<!-- Footer -->
<footer class="footer-padding bg-dark font-weight-600">
  <div class="container">
    <div class="row text-center text-md-left">

      <div class="col-sm-12 mt-2 text-center">
        {{--<p class="mb-4 mt-3">© 2018 Herox LLC. All rights reserved. Proudly made in <a href="javascript:void(0)">Themes Industry</a></p>--}}
        <div class="footer-social">
          @if($user->fb_link)
            <a href="{{ website_check_facebook_link($user->fb_link) }}" class="facebook-text-hvr"><i class="ti ti-facebook" aria-hidden="true"></i></a>
          @endif
          @if($user->tw_link)
            <a href="{{ website_check_twitter_link($user->tw_link) }}" class="twitter-text-hvr"><i class="ti ti-twitter-alt" aria-hidden="true"></i></a>
          @endif
          @if($user->ig_link)
            <a href="{{ website_check_instagram_link($user->ig_link) }}" class="instagram-text-hvr"><i class="ti ti-instagram" aria-hidden="true"></i></a>
          @endif
          @if($user->pin_link)
            <a href="{{ website_check_pinterest_link($user->pin_link) }}" class="pinterest-text-hvr"><i class="ti ti-pinterest" aria-hidden="true"></i></a>
          @endif
        </div>
      </div>

    </div>
  </div>
</footer>
<!-- Footer End -->