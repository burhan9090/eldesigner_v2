@if ($paginator->hasPages())
    <ul class="blog-pagination p-0 list-unstyled text-center text-md-right">
        {{-- Previous Page Link --}}
        @if ($paginator->onFirstPage())
            <li><a href="javascript:void(0)"><i class="ti-angle-left" aria-hidden="true"></i></a></li>
        @else
            <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev"><i class="ti-angle-left" aria-hidden="true"></i></a></li>
        @endif

        {{-- Pagination Elements --}}
        @foreach ($elements as $element)
            {{-- "Three Dots" Separator --}}
            @if (is_string($element))
                <li class="disabled"><span>{{ $element }}</span></li>
            @endif

            {{-- Array Of Links --}}
            @if (is_array($element))
                @foreach ($element as $page => $url)
                    @if ($page == $paginator->currentPage())
                        <li class="active"><a href="javascript:void(0)">{{ $page }}</a></li>
                    @else
                        <li><a href="{{ $url }}">{{ $page }}</a></li>
                    @endif
                @endforeach
            @endif
        @endforeach

        {{-- Next Page Link --}}
        @if ($paginator->hasMorePages())
            <li><a href="{{ $paginator->nextPageUrl() }}" rel="next"><i class="ti-angle-right" aria-hidden="true"></i></a></li>
        @else
            <li class="disabled"><a href="javascript:void(0)"><i class="ti-angle-right" aria-hidden="true"></i></a></li>
        @endif
    </ul>
@endif
