@extends('website.ashton-light.layout')
@section('content')

<nav class="navbar navbar-expand-lg blog-nav">
  <div class="container sm-padding-10px-tb sm-padding-15px-lr">
    <!-- Logo -->
    <a class="logo" href="javascript:void(0);">
      <b style="text-transform: uppercase;">{{ $user->name }}</b>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="icon-bar"><i class="fas fa-bars"></i></span>
    </button>
    <!-- navbar links -->
    <div class="collapse navbar-collapse" id="navbarContent">
      <ul class="navbar-nav ml-auto">
        @include('website.ashton-light.nav')
      </ul>
    </div>
  </div>
</nav>
<!-- End Navbar  -->

<!-- Start Header -->
<section id="home" class="blog-header" data-scroll-index="0">
  <div class="container">
    <div class="row">
      <div class="text-center col-md-12">
        <h5>{{ $welcome->main }}</h5>
        <a href="{{ websiteRoute('website.index') }}">Home</a>
        <a href="javascript:void(0);" class="active">Blog List</a>
      </div>
    </div>
  </div>

</section>
<!-- End Header -->

<section class="blogs ">
  <div class="container">
    <div class="row">
      <!--  start blog left-->
      <div class="col-md-8 xs-margin-30px-bottom">
        <div class="posts">
          <!-- start post -->
          @forelse($articles as $article)
          <div class="post">
            @if($article->image)
            <div class="post-img">
              <a href="{{ websiteRoute('website.blog.show', [$article->id]) }}" class="width-100">
                <img src="{{ asset($article->image) }}" class="list-img" alt="{{ $article->title}}">
              </a>
            </div>
            @endif
            <div class="content">
              <div class="post-meta">
                <div class="post-title">
                  <h5>
                    <a href="{{ websiteRoute('website.blog.show', [$article->id]) }}">{{ $article->title}}</a>
                  </h5>
                </div>
                <ul class="meta">
                  <li>
                    <a href="javascript:void(0);">
                      <i class="fa fa-user" aria-hidden="true"></i> {{ optional($article->user)->name }}
                    </a>
                  </li>
                  <li>
                    <a href="javascript:void(0);">
                      <i class="fa fa-folder-open" aria-hidden="true"></i> {{ optional(optional($article->category)->parent)->text }}
                    </a>
                  </li>
                  <li>
                    <a href="javascript:void(0);">
                      <i class="fa fa-calendar" aria-hidden="true"></i> {{ optional($article->created_at)->format('M d, Y') }}
                    </a>
                  </li>
                </ul>
              </div>
              <div class="post-cont">
                <p>{{ str_limit(strip_tags($article->body)) }}</p>
              </div>
              <a href="{{ websiteRoute('website.blog.show', [$article->id]) }}" class="btn white">Read More</a>
            </div>
          </div>
        @empty
            <div class="post">
              <div class="content">
                <div class="post-meta">
                  <div class="post-title">
                    <h5>There are no Projects to view</h5>
                  </div>
                </div>
              </div>
            </div>
        @endforelse

        {{ $projects->links('website.ashton-light.pagination') }}

        </div>
      </div>
      <!--  end blog left-->
      <!--  start blog right-->
      <div class="col-md-4">
        <div class="side-bar">
          <div class="widget search">
            <form>
              <input type="search" name="filter[search_article]" placeholder="Type here ...">
              <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
            </form>
          </div>

          <div class="widget">
            <div class="widget-title">
              <h6>Categories</h6>
            </div>
            <ul>
              @foreach($categories as $category)
                <li>
                  <a href="{{ websiteRoute('website.blog.list', ['filter'=>['category_parent'=> $category->id]]) }}">{{ $category->text }}</a>
                </li>
              @endforeach
            </ul>
          </div>

          <div class="widget">
            <div class="widget-title">
              <h6>Follow Us</h6>
            </div>
            <ul class="social-listing">
              @include('website.ashton-light.social')
            </ul>
          </div>

        </div>
      </div>
      <!--  end blog right-->
    </div>
  </div>
</section>
@endsection

@section('css')
  <style>
    body .navbar-top-default .navbar-nav .nav-link {
      padding-right: 1.2rem;
      padding-left: 1.2rem;
      color: #FFFFFF;
      font-weight: 400;
    }
  </style>
@endsection