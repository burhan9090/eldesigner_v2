@extends('website.ashton-light.layout')
@section('content')

  <header class="header" data-scroll-index="0">

    <!-- Start Navbar -->
    <nav class="navbar navbar-expand-lg border-none bg-light-gray">
      <div class="container sm-padding-10px-tb sm-padding-15px-lr">

        <!-- start logo -->
        <a class="logo" href="javascript:void(0);" data-scroll-nav="0">
          {{--<img id="logo" src="{{ asset('themes/'.$theme.'/img/logo-dark.png') }}" data-light="{{ asset('themes/'.$theme.'/img/logo-light.png') }}" data-dark="{{ asset('themes/'.$theme.'/img/logo-dark.png') }}" alt="Ashton - One Page Portfolio" title="Ashton - One Page Portfolio">--}}
          <b style="text-transform: uppercase;">{{ $user->name }}</b>
        </a>
        <!-- end Logo -->

        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="icon-bar"><i class="fas fa-bars"></i></span>
        </button>

        <!-- navbar links -->
        <div class="collapse navbar-collapse" id="navbarContent">
          <ul class="navbar-nav ml-auto">
            @include('website.ashton-light.nav')
          </ul>
        </div>
        <!-- end navbar links -->

      </div>

      <a href="javascript:void(0)" class="sidemenu_btn sm-display-none" id="sidebar_toggle">
        <span></span> <span></span> <span></span>
      </a>

    </nav>
    <!-- End Navbar  -->

    <!-- Start Banner Container -->
    <div class="bg-light-gray padding-100px-top">
      <div class="container">
        <div class="row">
          <div class="col-md-7 col-sm-12 order-2 order-md-1 xs-padding-50px-tb">
            <!-- Start Left Content -->
            <div class="padding-twenty-top sm-padding-fifteen-top xs-no-padding">
              <h5 class="no-margin font-size18 xs-font-size16 text-medium-gray padding-twelve-top sm-padding-five-top xs-no-padding">Hello, I am</h5>
              <h1 class="text-uppercase font-size48 md-font-size42 sm-font-size36 xs-font-size26 font-weight-700 margin-20px-bottom">{{ $user->name }}</h1>
              <p class="width-50 md-width-75 xs-width-100 font-size16 xs-font-size14 border-top border-bottom padding-10px-tb">{{ implode(' / ', $user->services->pluck('service_name')->toArray()) }}</p>
              <div class="social-links margin-30px-bottom xs-margin-20px-bottom">
                @include('website.ashton-light.social')
              </div>
              <?php if($user->has_quiz): ?>
              <a class="btn white" href="{{ route('tool.quiz.take' , $user->id) }}" target="_blank">Request Design</a>
              <?php endif; ?>
              <a class="btn white" href="#portfolio">View My Portfolio <i class="fas fa-arrow-right font-size15 margin-5px-left"></i></a>
            </div>
            <!-- End Left Content -->
          </div>
          <div class="col-md-5 col-sm-12 order-1 order-md-2 text-right xs-text-center xs-border-bottom xs-padding-nineteen-half-lr border-color-medium-gray">
            <!-- Start Right Image -->
            @if($welcome->user_picture)
              <img src="{{ asset($welcome->user_picture) }}" class="user-logo" alt="" />
          @endif
          <!-- End Right Image -->
          </div>
        </div>
      </div>
    </div>
    <!-- End Banner Container -->

    <!-- Start Sidemenu -->
    <div class="side-menu">
      <div class="inner-wrapper">
        <span class="btn-close" id="btn_sidebar_colse"></span>
        <nav class="side-nav width-100">
          <ul class="navbar-nav ml-auto">
            @include('website.ashton-light.nav')
          </ul>
        </nav>

        <div class="side-footer width-100">
          <div class="social-links">
            @include('website.ashton-light.social')
          </div>
        </div>
      </div>
    </div>
    <!-- End Sidemenu -->

    <!-- Close Sidemenu -->
    <a id="close_sidebar" href="javascript:void(0);"></a>
    <!-- End Sidemenu -->

  </header>

  <!-- Start About Section-->
<section data-scroll-index="1">
  <div class="container">
    <div class="row">
      <div class="col-12 center-col margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom text-center">
        <h2 class="font-size34 md-font-size30 sm-font-size26 xs-font-size24 font-weight-700 section-title">About Me</h2>
      </div>
    </div>
    <div class="row">
      <!-- start about me section-->
      <div class="col-lg-6 col-md-12 sm-margin-30px-bottom">
        <p class="font-size15 xs-font-size14">{{ $user->description }}</p>
      </div>
      <!-- end about me section-->

      <!-- start skill section-->
      <div class="col-lg-6 col-md-12">
        <div class="padding-20px-left sm-no-padding">
          <div class="skills width-100 margin-40px-bottom sm-margin-30px-bottom xs-no-margin-bottom">
          @if(count($skills))
            <!-- skills progress -->
          @foreach($skills as $skill)
            <div class="prog-item">
              <p>{{ $skill['title'] }}</p>
              <div class="skills-progress">
                <span data-value="{{ $skill['weight'] }}%"></span>
              </div>
            </div>
          @endforeach
            @endif
          </div>
        </div>
      </div>
      <!-- end skill section-->
    </div>
  </div>
</section>
<!-- End About Section-->

<!-- Start Timeline Section-->
@if(count($information))
<section class="bg-light-gray" data-scroll-index="2">
  <div class="container">
    <div class="row">
      <div class="col-12 center-col margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom text-center">
        <h3 class="font-weight-700 font-size32 md-font-size27 sm-font-size24 xs-font-size20 section-title">My Experience</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="main-timeline">

        @foreach($information as $key=>$row)
          <div class="timeline">
            <div class="icon"></div>
            <div class="date-content">
              <div class="date-outer">
                <span class="date">
                  <span class="month">{{ $row->period }}</span>
                </span>
              </div>
            </div>
            <div class="timeline-content">
              <h5 class="title">{{ $row->title }}</h5>
              <span><sup>{{ $row->type==\App\UserHistory::TYPE_EDUCATION?"Education": "Employment" }}</sup></span>
              <p class="description">
                {{ $row->description }}
              </p>
            </div>
          </div>
          <!-- end experience section-->
        @endforeach
        </div>
      </div>
    </div>
  </div>
</section>
@endif
<!-- End Timeline Section-->

<!-- Start Portfolio Section -->
@if(count($projects))
<section class="portfolio" data-scroll-index="3" id="portfolio">
  <div class="container">
    <div class="row">
      <div class="col-12 center-col margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom text-center">
        <h3 class="font-weight-700 font-size32 md-font-size27 sm-font-size24 xs-font-size20 section-title">My Portfolio</h3>
      </div>
    </div>
    <div class="row">

    @php
      $categories=[];

      foreach($projects as $project){
        if($project->tags){
          $categories = array_merge($categories, $project->category->pluck('service_name')->toArray());
        }
      }

      $data=[];
      foreach(array_unique($categories) as $key=>$tag){
        if($key>2){
          break;
        }
        $data[]=$tag;
      }

      $categories=$data;

    @endphp
      <!-- Start links -->
      <div class="filtering col-sm-12 text-center">
        <span data-filter='*' class="active">All</span>
        @foreach($categories as $category)
        <span data-filter='.{{ str_slug($category) }}'>{{ $category }}</span>
        @endforeach
      </div>
      <!-- End links -->
      <div class="clearfix"></div>

      <!-- gallery -->
      <div class="gallery text-center width-100">
      @foreach($projects as $project)
        @if(!$project->image)
          @continue
        @endif
        <!-- start gallery item -->
        <div class="col-md-4 items {{ str_slug(optional($project->category)->service_name) }}">
          <div class="item-img">
            <img src="{{ asset(optional($project->image)->image) }}" alt="image" class="project-img">
            <div class="item-img-overlay valign">
              <div class="overlay-info width-100 vertical-center">
                <a href="{{ asset(optional($project->image)->image) }}" class="popimg">
                  <i class="fa fa-search-plus"></i>
                </a>
                <h6>{{ $project->title}}</h6>
              </div>
            </div>
          </div>
        </div>
      @endforeach

        <div class="clear-fix"></div>

      </div>
    </div>
    @if(count($projects)>3)
      <div class="mt-3 text-right">
        <a class="btn white" href="{{ websiteRoute('website.project.list') }}">
          <i class="fas fa-arrow-alt-circle-down font-size16"></i>
          <span class="alt-font margin-5px-left">View All Projects</span>
        </a>
      </div>
    @endif
  </div>
</section>
@endif
<!-- End Portfolio Section -->

<!--  Start Services Section -->
@if($services && count($services))
  @php
    $servicesCount = count($services);
  @endphp
<section class="services bg-light-gray" data-scroll-index="4">
  <div class="container">
    <div class="row">
      <div class="col-12 center-col margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom text-center">
        <h3 class="font-weight-700 font-size32 md-font-size27 sm-font-size24 xs-font-size20 section-title">My Services</h3>
      </div>
    </div>
    <div class="row">

    @for($key=0; $key<4; $key++)
      @php
        $service = $services[$key % count($services)];
      @endphp
      <!-- start services item -->
      <div class="col-md-6 border-right border-bottom border-color-medium-gray xs-no-border-right">
        <div class="text-center padding-40px-tb xs-padding-20px-bottom padding-20px-lr xs-no-padding-lr xs-no-padding-top">
          <div class="display-inline-block margin-20px-bottom xs-margin-15px-bottom">
            @if($service['icons'])
              <img class="service-icon" src="{{ $service['icons'] }}"/>
            @else
              <i class="icon icon-tools text-extra-dark-gray font-size36"></i>
            @endif
          </div>
          <div class="alt-font text-extra-dark-gray margin-10px-bottom font-size18 xs-font-size16">{{ $service['title'] }}</div>
          <p class="width-85 md-width-90 xs-width-100 center-col no-margin-bottom">{{ $service['description'] }}</p>
        </div>
      </div>
      <!-- end services item -->
    @endfor
    </div>
  </div>
</section>
@endif
<!-- End Services Section -->


<!-- Start Blog Section -->
@if(count($articles))
<section class="blog bg-light-gray" data-scroll-index="5">
  <div class="container">
    <div class="row">
      <div class="col-12 center-col margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom text-center">
        <h3 class="font-weight-700 font-size32 md-font-size27 sm-font-size24 xs-font-size20 section-title">Blog</h3>
      </div>
    </div>
    <div class="row">

      @foreach($articles as $key=>$row)
        @if($key>2)
          @break
        @endif
      <div class="col-lg-4 sm-margin-20px-bottom">
        <div class="item text-center">
          <div class="post-img">
            <img class="project-img" src="{{ asset($row->image?: 'img/icon-no-image.svg') }}" alt="{{ $row->image? $row->title: '' }}">
          </div>
          <div class="content">
            <div class="tag alt-font font-weight-300">
              <a href="{{ websiteRoute('website.blog.show', ['id' => $row->id]) }}"><span class="font-size12 display-inline-block">{{ $row->created_at->format('M d, Y') }}</span></a>
            </div>
            <h5 class="margin-15px-bottom">{{ $row->title }}</h5>
            <p>{{ str_limit(strip_tags($row->body), 35) }}</p>
            <a href="{{ websiteRoute('website.blog.show', ['id' => $row->id]) }}" class="read-more">Read more</a>
          </div>
        </div>
      </div>
      @endforeach
    </div>
    @if(count($articles)>3)
    <div class="mt-3 text-right">
    <a class="btn white" href="{{ websiteRoute('website.blog.list') }}">
      <i class="fas fa-arrow-alt-circle-down font-size16"></i>
      <span class="alt-font margin-5px-left">View All Article</span>
    </a>
    </div>
    @endif
  </div>
</section>
@endif
<!-- End Blog Section -->

    <!-- Start Contact Section -->
<section class="contact no-padding-bottom" data-scroll-index="6">
  @if(optional($user->websiteContactUs)->show_tab)
  <div class="container margin-100px-bottom md-margin-70px-bottom sm-margin-50px-bottom">
    <div class="row">
      <div class="col-12 center-col margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom text-center">
        <h3 class="font-weight-700 font-size32 md-font-size27 sm-font-size24 xs-font-size20 section-title">{{ $user->websiteContactUs->title }}</h3>
        <h6 class="mt-3">{{ $user->websiteContactUs->message }}</h6>
      </div>

    </div>
    <div class="row">

      <!-- start get in touch -->
      <div class="col-lg-12">
        <form  method="post" id="contact-form" action="{{ websiteRoute('website.contact_us') }}" class="mailform off2">
          {{ csrf_field() }}

          <div class="messages"></div>

          <div class="row">
            <div class="col-md-12">
              <input type="text" name="title" placeholder="Subject:" data-constraints="@NotEmpty">
            </div>
            <div class="col-md-12">
              <textarea name="message" rows="5" placeholder="Message:" data-constraints="@NotEmpty"></textarea>
            </div>
            <div class="mfControls col-md-12 text-center">
              <button type="submit" id="submit_btn" class="btn white">Send Message</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  @endif

  <div class="bg-light-gray padding-30px-tb border-top border-color-medium-gray">
    <div class="container">
      <div class="row info">
        <div class="col-lg-4 col-md-3 item text-center border-right xs-no-border-right xs-border-bottom border-color-medium-gray xs-margin-20px-bottom xs-padding-20px-bottom">
          <span class="icon font-size32"><i class="icon-phone"></i></span>
          <div class="cont">
            <h6>Phone</h6>
            <p>{{ $user->phone }}</p>
          </div>
        </div>
        <div class="col-lg-4 col-md-5 item text-center border-right xs-no-border-right xs-border-bottom border-color-medium-gray xs-margin-20px-bottom xs-padding-20px-bottom">
          <span class="icon font-size32"><i class="icon-map"></i></span>
          <div class="cont">
            <h6>Address</h6>
            <p>{{ $user->country->text }}</p>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 item text-center">
          <span class="icon font-size32"><i class="icon-envelope"></i></span>
          <div class="cont">
            <h6>Email</h6>
            <p>{{ $user->email }}</p>
          </div>
        </div>

      </div>
    </div>
  </div>

</section>
<!-- End Contact Section -->
@endsection

@section('script')
  <script>
    $('#contact-form').submit(function (e) {
      e.preventDefault();
      var $this = $(this);

      $('#submit_btn').prop('disabled', true);

      $.post($this.attr('action'), $this.serialize())
        .done(function (d) {
          $('.messages').empty()
            .append($('<span>').addClass(d.status ? 'alert-success' : 'alert-danger').text(d.message))
            .append('<br />')
        })
        .always(function () {
          $('#submit_btn').prop('disabled', false);
        });

    })
  </script>
@endsection