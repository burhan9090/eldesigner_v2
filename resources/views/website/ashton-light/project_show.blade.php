@extends('website.ashton-light.layout')
@section('content')

  <nav class="navbar navbar-expand-lg blog-nav">
    <div class="container sm-padding-10px-tb sm-padding-15px-lr">
      <!-- Logo -->
      <a class="logo" href="javascript:void(0);">
        <b style="text-transform: uppercase;">{{ $user->name }}</b>
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="icon-bar"><i class="fas fa-bars"></i></span>
      </button>
      <!-- navbar links -->
      <div class="collapse navbar-collapse" id="navbarContent">
        <ul class="navbar-nav ml-auto">
          @include('website.ashton-light.nav')
        </ul>
      </div>
    </div>
  </nav>
  <!-- End Navbar  -->

  <!-- Start Header -->
  <section id="home" class="blog-header" data-scroll-index="0">
    <div class="container">
      <div class="row">
        <div class="text-center col-md-12">
          <h5>{{ $welcome->title }}</h5>
          <a href="{{ websiteRoute('website.index') }}">Home</a>
          <a href="{{ websiteRoute('website.project.list') }}" class="active">Portfolio</a>
          <a href="javascript:void(0);" class="active">{{ $project->title }}</a>
        </div>
      </div>
    </div>

  </section>
  <!-- End Header -->


  @if(count($projects))
    <section class="portfolio" data-scroll-index="3" id="portfolio">
      <div class="container">
        <div class="row">
          <div class="col-12 center-col margin-70px-bottom sm-margin-40px-bottom xs-margin-30px-bottom text-center">
            <h3 class="font-weight-700 font-size32 md-font-size27 sm-font-size24 xs-font-size20 section-title">{{ $project->title }}</h3>
            <h6 class="mt-3">{{ optional($project->category)->service_name }} | {{ $project->created_at->format('M d, Y') }}</h6>
          </div>
        </div>
        <div class="row">

        <!-- Start links -->
          <div class="filtering col-sm-12 text-center">
            <span data-filter='*' class="active">All</span>

          </div>
          <!-- End links -->
          <div class="clearfix"></div>

          <!-- gallery -->
          <div class="gallery text-center width-100">
          @foreach($project->images as $image)
            <!-- start gallery item -->
              <div class="col-md-4 items {{ str_slug(optional($image->category)->service_name) }}">
                <div class="item-img">
                  <img src="{{ asset($image->image) }}" alt="{{ $project->title }}" class="project-img">
                  <div class="item-img-overlay valign">
                    <div class="overlay-info width-100 vertical-center">
                      <a href="{{ asset($image->image) }}" class="popimg">
                        <i class="fa fa-search-plus"></i>
                      </a>
                      <h6>{{ $image->description }}</h6>
                    </div>
                  </div>
                </div>
              </div>
            @endforeach
            <div class="clear-fix"></div>
          </div>
        </div>
      </div>
    </section>
  @endif

@endsection
