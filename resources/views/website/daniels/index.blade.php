@extends('website.daniels.layout')
@section('page')

  <!-- ====== Header ======  -->
  <section id="home" class="header" data-scroll-index="0"
           style="background-image: url({{ asset(optional($user->websiteWelcome)->background?: 'themes/daniels/img/bg.jpg') }});"
           data-stellar-background-ratio="0.8">

    <!-- particles -->
    <div id="particles-js"></div>

    <div class="v-middle">
      <div class="container">
        <div class="row">

          <!-- caption -->
          <div class="caption">
            <h5>Hello</h5>
            <h1 class="cd-headline clip">
              <span class="blc">I Am </span>
              <span class="cd-words-wrapper">
              <b class="is-visible">{{ $user->name. ' '.$user->last_name }}</b>
              <b>{{ $user->occupation }}</b>
            </span>
            </h1>

            <!-- social icons -->
            <div class="social-icon">
              @if($user->fb_link)
                <a href="{{ website_check_facebook_link($user->fb_link) }}">
                  <span><i class="fa fa-facebook" aria-hidden="true"></i></span>
                </a>
              @endif
              @if($user->tw_link)
                <a href="{{ website_check_twitter_link($user->tw_link) }}">
                  <span><i class="fa fa-twitter" aria-hidden="true"></i></span>
                </a>
              @endif
              @if($user->ig_link)
                <a href="{{ website_check_instagram_link($user->ig_link) }}">
                  <span><i class="fa fa-instagram" aria-hidden="true"></i></span>
                </a>
              @endif
              @if($user->pin_link)
                <a href="{{ website_check_pinterest_link($user->pin_link) }}">
                  <span><i class="fa fa-pinterest" aria-hidden="true"></i></span>
                </a>
              @endif
            </div>
            <br>
            <br>
            <br>
            <br>
              <?php if($user->has_quiz): ?>
            <a class="btn white btn-medium btn-primary" style="background-color: #29aecc;" href="{{ route('tool.quiz.take' , $user->id) }}" target="_blank">Request Design</a>
              <?php endif; ?>
          </div>
          <!-- end caption -->
        </div>
      </div><!-- /row -->
    </div><!-- /container -->
  </section>
  <!-- ====== End Header ======  -->

  <!-- ====== Hero ======  -->
  <section id="about" class="hero section-padding pb-70" data-scroll-index="1">
    <div class="container">
      <div class="row">

        <!-- hero image -->
        <div class="col-md-5">
          @if($welcome->user_picture)
            <div class="hero-img mb-30">
              <img src="{{ asset($welcome->user_picture) }}" alt="{{ $user->name. ' '.$user->last_name }}">
            </div>
          @endif
        </div>

        <!-- content -->
        <div class="col-md-7">
          <div class="content mb-30">
            <h3>About Me</h3>
            <span class="sub-title">{{ $user->occupation }}</span>
            <p>{{ $user->description }}</p>

            @if(count($skills))
            <!-- skills progress -->
            <div class="skills">
              @foreach($skills as $skill)
              <div class="item">
                <div class="skills-progress">
                  <h6>{{ $skill['title'] }}</h6>
                  <span data-value="{{ $skill['weight'] }}%"></span>
                </div>
              </div>
              @endforeach
            </div>
            <div class="clearfix"></div>
            @endif

            {{--<a href="particles.html#0" download>--}}
            {{--<span class="buton buton-bg">Download C.V</span>--}}
            {{--</a>--}}
            {{--<a href="particles.html#0" data-scroll-nav="6">--}}
            {{--<span class="buton">Contact Me</span>--}}
            {{--</a>--}}

          </div>
        </div>
      </div><!-- /row -->
    </div><!-- /container -->
  </section>
  <!-- ====== End Hero ======  -->

  <!-- ====== Services ======  -->

  @if($services && count($services))
    @php
      $servicesCount = count($services);
    @endphp
    <section id="services" class="services section-padding bg-gray text-center pb-70" data-scroll-index="2">
      <div class="container">
        <div class="row">
          <div class="section-head">
            <h3>Services</h3>
          </div>

          @for($key=0; $key<4; $key++)
            @php
              $service = $services[$key % count($services)];
            @endphp
            <div class="col-md-6">
              <div class="item">
                  @if($service['icons'])
                    <img class="service-icon" src="{{ $service['icons'] }}"/>
                  @else
                  <span class="icon">
                    <i class="fa fa-laptop" aria-hidden="true"></i>
                  </span>
                @endif
                <h6>{{ $service['title'] }}</h6>
                <p>{{ $service['description'] }}</p>
              </div>
            </div>
            @endfor
        </div>
      </div>
    </section>
  @endif


  @if(count($information))
    <section id="information" class="services section-padding bg-gray text-center pb-70" data-scroll-index="2">
      <div class="container">
        <div class="row">

          <!-- section heading -->
          <div class="section-head">
            <h3>Information</h3>
          </div>

          @foreach($information as $key=>$row)
            <div class="col-md-6">
              <div class="item">
                <span class="icon" title="{{ $row->type==\App\UserHistory::TYPE_EDUCATION?"Education": "Employment" }}">
                  <i class="fa fa-{{ $row->type==\App\UserHistory::TYPE_EDUCATION?"graduation-cap": "briefcase" }}"
                      aria-hidden="true"></i>
                </span>
                <h6>{{ $row->title }}</h6>
                <i>{{ $row->period }}</i>
                <p>{{ $row->description }}</p>
              </div>
            </div>
          @endforeach
        </div><!-- /row -->
      </div><!-- /container -->
    </section>
  @endif
  <!-- ====== End Services ======  -->

  <!--====== Portfolio ======-->
@if(count($projects))
  <section id="portfolio" class="portfolio section-padding pb-70" data-scroll-index="3">
    <div class="container">
      <div class="row">

        <!-- section heading -->
        <div class="section-head">
          <h3>Projects</h3>
        </div>
      @php
        $categories=[];

        foreach($projects as $project){
          if($project->tags){
            $categories = array_merge($categories, $project->category->pluck('service_name')->toArray());
          }
        }

        $data=[];
        foreach(array_unique($categories) as $key=>$category){
          if($key>2){
            break;
          }
          $data[]=$category;
        }

        $categories=$data;

      @endphp

      <!-- filter links -->
        <div class="filtering text-center mb-50">
          <span data-filter='*' class="active">All</span>
          @foreach($categories as $category)
            <span data-filter='.{{ str_slug($category) }}'>{{ $category }}</span>
          @endforeach
        </div>

        <!-- gallery -->
        <div class="gallery text-center">
          @foreach($projects as $project)
            @if(!$project->image)
              @continue
            @endif
            <div class="col-md-4 col-sm-6 items {{ implode(' ', slugArray($project->category->pluck('service_name'))) }}">
              <div class="item-img">
                <img src="{{ asset($project->image->image) }}" alt="image" class="project-img">
                <div class="item-img-overlay">
                  <div class="overlay-info v-middle text-center">
                    <h3 class="sm-titl">{{ $project->title}}</h3>
                    <h6 class="sm-titl">{{ $project->description }}</h6>
                    <div class="icons">
											<span class="icon">
												<a href="{{ websiteRoute('website.project.show', ['id' => $project->id]) }}">
													<i class="fa fa-chain-broken" aria-hidden="true"></i>
												</a>
											</span>

                      <span class="icon link">
                        <a href="{{ asset(optional($project->image)->image) }}">
                          <i class="fa fa-search-plus" aria-hidden="true"></i>
                        </a>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @endforeach

        </div>
      </div><!-- /row -->
    </div><!-- /container -->
  </section>
@endif
  <!--====== End Portfolio ======-->

  <!--====== Blog ======-->
  @if(count($articles))
    <section id="blog" class="blog section-padding bg-gray" data-scroll-index="5">
      <div class="container">
        <div class="row">

          <!-- section heading -->
          <div class="section-head">
            <h3>Articles</h3>
          </div>

          <!-- owl carsouel -->
          <div class="owl-carousel owl-theme">
            @for($key=0; $key<10; $key++)
              @php
                $row = $articles[$key % count($articles)];
              @endphp
              <div class="pitem">
                <div class="post-img">
                  <img src="{{ asset($row->image?: 'img/icon-no-image.svg') }}"
                       style="max-width: 360px; width: 100%; max-height: 250px;"
                       alt="{{ $row->image? $row->title: '' }}">
                </div>
                <div class="content">
                  <h4 class="">
                    <a href="#">{{ $row->title }}</a>
                  </h4>
                  <h6>
                    <a href="#">{{ str_limit(strip_tags($row->body), 35) }}</a>
                  </h6>
                  <br/>
                  <span class="more">
									<a href="{{ websiteRoute('website.blog.show', ['id' => $row->id]) }}">Read More</a>
								</span>
                </div>
              </div>
            @endfor
          </div>
        </div><!-- /row -->
      </div><!-- /container -->
    </section>
  @endif
  <!--====== End Blog ======-->

  <!--====== Contact ======-->
  <section id="contact" class="contact section-padding" data-scroll-index="6">
    <div class="container">
      <div class="row">

        <!-- section heading -->
        <div class="section-head">
          <h3>Contact Us</h3>
        </div>

        <div class="col-md-offset-1 col-md-10">

          <!-- contact info -->
          <div class="info text-center mb-50">

            <div class="col-md-4">
              <div class="item">
                <span class="icon"><i class="fa fa-location-arrow" aria-hidden="true"></i></span>
                <h6>Address</h6>
                <p>{{ $user->country->text }}</p>
              </div>
            </div>

            <div class="col-md-4">
              <div class="item">
                <span class="icon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                <h6>Email</h6>
                <p>{{ $user->email }}</p>
              </div>
            </div>

            <div class="col-md-4">
              <div class="item">
                <span class="icon"><i class="fa fa-phone" aria-hidden="true"></i></span>
                <h6>Phone</h6>
                <p>{{ $user->phone }}</p>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>

        @if(optional($user->websiteContactUs)->show_tab)
          <!-- contact form -->
            <form class="form" id="contact-form" method="post" action="{{ websiteRoute('website.contact_us') }}">
              {{ csrf_field() }}

              <div class="messages"></div>

              <div class="controls">

                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <input id="form_name" type="text" name="title" placeholder="{{ $user->websiteContactUs->title }}"
                             required="required">
                    </div>
                  </div>

                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <textarea id="form_message" name="message" placeholder="{{ $user->websiteContactUs->message }}"
                                rows="4" required="required"></textarea>
                    </div>

                    <input type="submit" value="Submit" class="buton buton-bg">
                  </div>
                </div>
              </div>
            </form>
          @endif

        </div>
      </div><!-- /row -->
    </div><!-- /container -->
  </section>
  <!--====== End Contact ======-->

@endsection

@section('script')
  <!-- app js -->
  <script src="{{ asset('themes/'.$theme.'/js/app.js') }}"></script>

  <script src="{{ asset('themes/'.$theme.'/js/custom.js') }}"></script>

  <script>
    $('#contact-form').submit(function (e) {
      e.preventDefault();
      var $this = $(this);

      $('#submit_btn').prop('disabled', true);

      $.post($this.attr('action'), $this.serialize())
        .done(function (d) {
          $('.messages').empty()
            .append($('<span>').addClass(d.status ? 'alert-success' : 'alert-danger').text(d.message))
            .append('<br />')
        })
        .always(function () {
          $('#submit_btn').prop('disabled', false);
        });

    })
  </script>
@endsection