<!doctype html>
<html class="no-js" lang="zxx">
<head>

  <!-- metas -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
@php

  if(!isset($keywordHeader)){
    $keywordHeader = $welcome->keyword;
  }

  if(!isset($titleHeader)){
    $titleHeader=$welcome->title;
  }

  if(!isset($descriptionHeader)){
    $descriptionHeader=$welcome->meta_description;
  }

  if(!isset($logoHeader)) {
    $logoHeader = $welcome->user_picture;
  }

  $logoHeader = asset($logoHeader);

  $typeHeader = 'profile';
  if($nav=='article'){
    $typeHeader = 'article';
  }
  elseif($nav=='project'){
    $typeHeader = 'project';
  }

@endphp

<!-- Page SEO -->
  <meta name="keywords" content="{{ $keywordHeader }}"/>

  <meta name="author" content="{{ $user->name }}"/>
  <meta name="copyright" content="{{ $user->name }}"/>

  <meta name="url" content="{{ websiteRoute('website.index') }}"/>
  <meta name="identifier-URL" content="{{ websiteRoute('website.index') }}"/>
  <meta name="twitter:url" content="{{ websiteRoute('website.index') }}">


  <title>{{ $titleHeader }}</title>
  <meta name="subject" content="{{ $titleHeader }}"/>
  <meta name="twitter:title" content="{{ $titleHeader }}">
  <meta property="og:title" content="{{ $titleHeader }}"/>


  <meta name="description" content="{{ $descriptionHeader }}"/>
  <meta name="twitter:description" content="{{ $descriptionHeader }}">
  <meta property="og:description" content="{{ $descriptionHeader }}"/>


  <meta name="twitter:image" content="{{ $logoHeader }}">
  <meta property="og:image" content="{{ $logoHeader }}"/>


  @if($welcome->gplus)
    <link rel="author" href="{{ $welcome->gplus }}"/>
    <link rel="publisher" href="{{ $welcome->gplus }}"/>
  @endif

  @if($welcome->fb_page_id)
    <meta name="fb:page_id" content="{{ $welcome->fb_page_id }}"/>
    <meta property=”fb:admins” content=”{{ $welcome->fb_page_id }}”/>
  @endif

  <meta name="twitter:card" content="{{ $typeHeader }}" />
  <meta property="og:type" content="{{ $typeHeader }}" />

  <meta property="og:url" content="{{ websiteRoute('website.index') }}"/>

  <meta name="og:site_name" content="{{ $user->name }}"/>
  <meta name="og:region" content="{{ optional(optional($user->city)->country)->code }}"/>
  <meta name="og:country-name" content="{{ optional(optional($user->city)->country)->text }}"/>

  <!-- favicon -->
  <link href="{{ asset('themes/'.$theme.'/img/favicon.ico') }}" rel="icon" type="image/png">

  <!-- bootstrap css -->
  <link rel="stylesheet" href="{{ asset('themes/'.$theme.'/css/bootstrap.min.css') }}">

  <!-- google fonts -->
  <link href="https://fonts.googleapis.com/css?family=Montserrat:200,300,400,500,600,700,800,900" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Poppins:200,300,400,500,600,700,800" rel="stylesheet">

  <!-- owl carousel CSS -->
  <link rel="stylesheet" href="{{ asset('themes/'.$theme.'/css/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ asset('themes/'.$theme.'/css/owl.theme.default.min.css') }}">

  <!-- magnific-popup CSS -->
  <link rel="stylesheet" href="{{ asset('themes/'.$theme.'/css/magnific-popup.css') }}">

  <!-- animate.min CSS -->
  <link rel="stylesheet" href="{{ asset('themes/'.$theme.'/css/animate.min.css') }}">

  <!-- Font Icon Core CSS -->
  <link rel="stylesheet" href="{{ asset('themes/'.$theme.'/css/font-awesome.min.css') }}">
  <link rel="stylesheet" href="{{ asset('themes/'.$theme.'/css/et-line.css') }}">


  <!-- Core Style Css -->
  <link rel="stylesheet" href="{{ asset('themes/'.$theme.'/css/style.css') }}">

  <!--[if lt IE 9]-->
  <script src="{{ asset('themes/'.$theme.'/js/html5shiv.min.js') }}"></script>
  <!--[endif]-->

  <style>
    .service-icon {
      object-fit: none;
      object-position: center;
      width: 64px;
      height: 64px;
      margin-bottom: 20px;
    }
  </style>

  @yield('style')

</head>

<body>

<!-- ====== Preloader ======  -->
<div class="loading">
  <div class="load-circle">
  </div>
</div>
<!-- ======End Preloader ======  -->

@include('website.daniels.nav')

@yield('page')

<!--====== Footer ======-->
<footer>
  <div class="container text-center">
    <div class="row">
      <p>Copy Right {{ date('Y') }} &copy; By El-Designer All Rights Reserved</p>
    </div>
  </div>
</footer>
<!--====== End Footer ======-->




<!-- jQuery -->
<script src="https://code.jquery.com/jquery-3.0.0.min.js"></script>
<script src="https://code.jquery.com/jquery-migrate-3.0.0.min.js"></script>

<!-- bootstrap -->
<script src="{{ asset('themes/'.$theme.'/js/bootstrap.min.js') }}"></script>

<!-- scrollIt -->
<script src="{{ asset('themes/'.$theme.'/js/scrollIt.min.js') }}"></script>

<!-- magnific-popup -->
<script src="{{ asset('themes/'.$theme.'/js/jquery.magnific-popup.min.js') }}"></script>

<!-- owl carousel -->
<script src="{{ asset('themes/'.$theme.'/js/owl.carousel.min.js') }}"></script>

<!-- stellar js -->
<script src="{{ asset('themes/'.$theme.'/js/jquery.stellar.min.js') }}"></script>

<!-- animated.headline -->
<script src="{{ asset('themes/'.$theme.'/js/animated.headline.js') }}"></script>

<!-- jquery.waypoints.min js -->
<script src="{{ asset('themes/'.$theme.'/js/jquery.waypoints.min.js') }}"></script>

<!-- jquery.counterup.min js -->
<script src="{{ asset('themes/'.$theme.'/js/jquery.counterup.min.js') }}"></script>

<!-- isotope.pkgd.min js -->
<script src="{{ asset('themes/'.$theme.'/js/isotope.pkgd.min.js') }}"></script>

<!-- particles.min js -->
<script src="{{ asset('themes/'.$theme.'/js/particles.min.js') }}"></script>


<!-- validator js -->
<script src="{{ asset('themes/'.$theme.'/js/validator.js') }}"></script>

@yield('script')
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133547867-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-133547867-1');
</script>


</body>
</html>
