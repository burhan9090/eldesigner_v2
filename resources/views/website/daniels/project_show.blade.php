@extends('website.daniels.layout')
@section('page')
  <section id="home" class="header" data-scroll-index="0"
           style="background-image: url({{ asset(optional($user->websiteWelcome)->background?: 'themes/daniels/img/bg.jpg') }});"
           data-stellar-background-ratio="0.8">

    <!-- particles -->
    <div id="particles-js"></div>

    <div class="v-middle">
      <div class="container">
        <div class="row">

          <!-- caption -->
          <div class="caption">
            <h3>Project / {{ $project->title }}</h3>

            <!-- social icons -->
            <div class="social-icon">
              @if($user->fb_link)
                <a href="{{ website_check_facebook_link($user->fb_link) }}"><span><i class="fa fa-facebook"
                                                                                     aria-hidden="true"></i></span></a>
              @endif
              @if($user->tw_link)
                <a href="{{ website_check_twitter_link($user->tw_link) }}"><span><i class="fa fa-twitter"
                                                                                    aria-hidden="true"></i></span></a>
              @endif
              @if($user->ig_link)
                <a href="{{ website_check_instagram_link($user->ig_link) }}"><span><i class="fa fa-instagram"
                                                                                      aria-hidden="true"></i></span></a>
              @endif
              @if($user->pin_link)
                <a href="{{ website_check_pinterest_link($user->pin_link) }}"><span><i class="fa fa-pinterest"
                                                                                       aria-hidden="true"></i></span></a>
              @endif
            </div>
          </div>
          <!-- end caption -->
        </div>
      </div><!-- /row -->
    </div><!-- /container -->
  </section>

  <section id="portfolio" class="portfolio section-padding pb-70" data-scroll-index="3">
    <div class="container">
      <div class="row">

        <!-- section heading -->
        <div class="section-head">
          <div class="row row--heading">
            <h1>{{ $project->title }}</h1>
            <p>{{ $project->description }}</p>
          </div>
        </div>
      @php
        $tags=[];

        foreach($project->images as $image){
          if($image->tags){
            $tags = array_merge($tags, $image->tags->pluck('text')->toArray());
          }
        }

        $tags = array_unique($tags);
      @endphp

      <!-- filter links -->
        <div class="filtering text-center mb-50">
          <span data-filter='*' class="active">All</span>
          @foreach($tags as $tag)
            <span data-filter='.{{ str_slug($tag) }}'>{{ $tag }}</span>
          @endforeach
        </div>

        <!-- gallery -->
        <div class="gallery text-center">
          @foreach($project->images as $image)
            <div class="col-md-4 col-sm-6 items {{ implode(' ', slugArray($image->tags->pluck('text'))) }}">
              <div class="item-img">
                <img src="{{ asset($image->image) }}" alt="{{ $project->title }}">
                <div class="item-img-overlay">
                  <div class="overlay-info v-middle text-center">
                    <h6 class="sm-titl">{{ $image->description }}</h6>
                    <div class="icons">
											<span class="icon">
												<a href="{{ websiteRoute('website.project.show', ['id' => $project->id]) }}">
													<i class="fa fa-chain-broken" aria-hidden="true"></i>
												</a>
											</span>

                      <span class="icon link">
                        <a href="{{ asset(optional($project->image)->image) }}">
                          <i class="fa fa-search-plus" aria-hidden="true"></i>
                        </a>
                      </span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          @endforeach

        </div>
      </div><!-- /row -->
    </div><!-- /container -->
  </section>




@endsection

@section('style')
  <style>
    .navbar-default .navbar-nav > li > a {
      color: #111 !important;
    }

    .article-img img {
      width: auto;
      max-width: 100%;
      max-height: 200px;
    }

    .navbar-default {
      border-bottom: solid 1px #cacaca;
    }

    .navbar-default .navbar-nav > li > a:after {
      background: #222 !important;
    }
  </style>

  <link href="{{ asset('themes/'.$theme.'/css/site.css') }}" rel="stylesheet"/>
  <link href="{{ asset('themes/'.$theme.'/css/photoswipe.css') }}" rel="stylesheet"/>
  <link href="{{ asset('themes/'.$theme.'/css/default-skin/default-skin.css') }}" rel="stylesheet"/>

  <script src="{{ asset('themes/'.$theme.'/js/photoswipe.min.js') }}"></script>
  <script src="{{ asset('themes/'.$theme.'/js/photoswipe-ui-default.min.js') }}"></script>

@endsection

@section('script')

  {{--<script src="{{ asset('themes/'.$theme.'/js/app.js') }}"></script>--}}
  <script src="{{ asset('themes/'.$theme.'/js/custom.js') }}"></script>

  <script>
    (function () {
      var wind = $(window);

      // navbar scrolling background
      wind.on("scroll", function () {

        var bodyScroll = wind.scrollTop(),
          navbar = $(".navbar-default"),
          h_hight = $(".navbar-default").outerHeight();

        if (bodyScroll > h_hight) {

          navbar.addClass("nav-scroll");

        } else {

          navbar.removeClass("nav-scroll");
        }
      });
    })();
    $(window).on("load", function () {

      // Preloader
      $(".loading").addClass("loading-end").fadeOut(1000);

    });

  </script>

@endsection
