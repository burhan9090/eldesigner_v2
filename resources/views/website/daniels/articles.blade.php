@extends('website.daniels.layout')
@section('page')
  <section id="home" class="header" data-scroll-index="0"
           style="background-image: url({{ asset(optional($user->websiteWelcome)->background?: 'themes/daniels/img/bg.jpg') }});"
           data-stellar-background-ratio="0.8">

    <!-- particles -->
    <div id="particles-js"></div>

    <div class="v-middle">
      <div class="container">
        <div class="row">

          <!-- caption -->
          <div class="caption">
            <h3>Articles</h3>

            <!-- social icons -->
            <div class="social-icon">
              @if($user->fb_link)
                <a href="{{ website_check_facebook_link($user->fb_link) }}">
                  <span><i class="fa fa-facebook" aria-hidden="true"></i></span>
                </a>
              @endif
              @if($user->tw_link)
                <a href="{{ website_check_twitter_link($user->tw_link) }}">
                  <span><i class="fa fa-twitter" aria-hidden="true"></i></span>
                </a>
              @endif
              @if($user->ig_link)
                <a href="{{ website_check_instagram_link($user->ig_link) }}">
                  <span><i class="fa fa-instagram" aria-hidden="true"></i></span>
                </a>
              @endif
              @if($user->pin_link)
                <a href="{{ website_check_pinterest_link($user->pin_link) }}">
                  <span><i class="fa fa-pinterest" aria-hidden="true"></i></span>
                </a>
              @endif
            </div>
          </div>
          <!-- end caption -->
        </div>
      </div><!-- /row -->
    </div><!-- /container -->
  </section>

  <section class="blogs ">
    <div class="container">
      <div class="row">
        <!--  start blog left-->
        <div class="col-md-8 xs-margin-30px-bottom">
          <div class="posts">
            <!-- start post -->
            @foreach($articles as $article)
              <div class="post">
                <div class="post-img">
                  <a href="{{ websiteRoute('website.blog.show', [$article->id]) }}" class="width-100">
                    <img src="{{ asset($article->image?: 'img/icon-no-image.svg') }}" class="list-img"
                         alt="{{ $article->image? $article->title: '' }}">
                  </a>
                </div>
                <div class="content">
                  <div class="post-meta">
                    <div class="post-title">
                      <h5>
                        <a href="{{ websiteRoute('website.blog.show', [$article->id]) }}">{{ $article->title }}</a>
                      </h5>
                    </div>
                    <ul class="meta">
                      <li>
                        <a href="javascript:void(0);">
                          <i class="fa fa-user" aria-hidden="true"></i> {{ optional($article->user)->name }}
                        </a>
                      </li>
                      <li>
                        <a href="javascript:void(0);">
                          <i class="fa fa-calendar"
                             aria-hidden="true"></i> {{ optional($article->created_at)->format('M d, Y') }}
                        </a>
                      </li>
                    </ul>
                  </div>
                  <div class="post-cont">
                    <p>{{ str_limit(strip_tags($article->body)) }}</p>
                  </div>
                  <a href="{{ websiteRoute('website.blog.show', [$article->id]) }}" class="btn white">Read More</a>
                </div>
              </div>
            @endforeach
          <!-- end post -->

            {{ $articles->links('website.daniels.pagination') }}

          </div>
        </div>
        <!--  end blog left-->
        <!--  start blog right-->
        <div class="col-md-4">
          <div class="side-bar">
            <div class="widget search">
              <form>
                <input name="filter[search_article]" placeholder="Search.." type="text"
                       value="{{ isset(($r = request('filter', ['search_article'=>'']))['search_article'])? $r['search_article']: '' }}">
                @if(isset(($r = request('filter'))['category_parent']))
                  <input name="filter[category_parent]" type="hidden" value="{{ $r['category_parent'] }}">
                @endif
                <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
              </form>
            </div>

            <div class="widget">
              <div class="widget-title">
                <h6>Categories</h6>
              </div>
              <ul>
                @foreach($categories as $category)
                  <li>
                    <a href="{{ websiteRoute('website.blog.list', ['filter'=>['category_parent'=> $category->id]]) }}">{{ $category->text }}</a>
                  </li>
                @endforeach
              </ul>
            </div>

          </div>
        </div>
        <!--  end blog right-->
      </div>
    </div>
  </section>

@endsection

@section('style')
  <style>
    .navbar-default .navbar-nav > li > a {
      color: #111 !important;
    }

    .article-img img {
      width: auto;
      max-width: 100%;
      max-height: 200px;
    }

    .navbar-default {
      border-bottom: solid 1px #cacaca;
    }

    .navbar-default .navbar-nav > li > a:after {
      background: #222 !important;
    }
  </style>
@endsection

@section('script')
  <script>
    (function () {
      var wind = $(window);

      // navbar scrolling background
      wind.on("scroll", function () {

        var bodyScroll = wind.scrollTop(),
          navbar = $(".navbar-default"),
          h_hight = $(".navbar-default").outerHeight();

        if (bodyScroll > h_hight) {

          navbar.addClass("nav-scroll");

        } else {

          navbar.removeClass("nav-scroll");
        }
      });
    })();
    $(window).on("load", function () {

      // Preloader
      $(".loading").addClass("loading-end").fadeOut(1000);

    });

  </script>
@endsection