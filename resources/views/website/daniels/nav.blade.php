<nav class="navbar navbar-default">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#nav-icon-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

            <!-- logo -->
            <a class="logo" href="{{ websiteRoute('website.index') }}">
                {{--@if($welcome->logo)--}}
{{--                <img src='{{ asset($welcome->logo) }}' style="height: 50px; width: 50px"/>--}}
                {{--@else--}}
                    {{ $user->name }}
                {{--@endif--}}
            </a>
        </div>

        <div class="collapse navbar-collapse" id="nav-icon-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ websiteRoute('website.index') }}#home"
                       data-scroll-nav="0" {!! $nav=='home'? 'class="active"': '' !!}>Home</a></li>
                <li><a href="{{ websiteRoute('website.index') }}#about" data-scroll-nav="1">About</a></li>
                <li><a href="{{ websiteRoute('website.index') }}#information" data-scroll-nav="2">information</a></li>
                <li><a href="{{ websiteRoute('website.project.list') }}" {!! $nav=='projects'? 'class="active"': '' !!}>Projects</a></li>
                {{--<li><a href="{{ websiteRoute('website.index', $user->user_name) }}#clients" data-scroll-nav="4">Clients</a></li>--}}
                <li>
                    <a href="{{ websiteRoute('website.blog.list') }}" {!! $nav=='article'? 'class="active"': '' !!}>Articles</a>
                </li>
                <li><a href="{{ websiteRoute('website.index') }}#contact"
                       data-scroll-nav="6">Contact</a></li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container -->
</nav>