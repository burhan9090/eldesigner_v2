@extends('website.herox2.layout')
@section('content')
<!--slider start-->
<section id="home" class="dark-slider p-0">
  <h2 class="d-none" aria-hidden="true"></h2>
  <div id="rev_slider_12_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="Herox-one"
       data-source="gallery" style="background:transparent;padding:0px;">
    <!-- START REVOLUTION SLIDER 5.4.8.1 fullscreen mode -->
    <div id="rev_slider_12_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.4.8.1">
      <ul>    <!-- SLIDE  -->
        <li data-index="rs-63" data-transition="crossfade" data-slotamount="default" data-hideafterloop="0"
            data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="500"
            data-rotate="0"
            data-saveperformance="off" data-title="Slide" data-param1="01" data-param2="" data-param3=""
            data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9=""
            data-param10="" data-description="">
          <!-- MAIN IMAGE -->
          <img src="{{ asset($welcome->background) }}" alt="" data-bgposition="center center"
               data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
          <!-- LAYERS -->

          <!-- LAYER NR. 1 -->
          <div class="tp-caption overlay-text tp-resizeme text-white"
               data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
               data-y="['middle','middle','middle','middle']" data-voffset="['-60','-60','-60','-60']"
               data-fontsize="['70','60','50','30']"
               data-lineheight="['90','90','50','35']"
               data-whitespace="nowrap" data-responsive_offset="on"
               data-width="['none','none','none','none']" data-type="text"
               data-textalign="['center','center','center','center']"
               data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"
               data-start="1200" data-splitin="none" data-splitout="none" style="font-weight:500">
            {{ $welcome->main }}
          </div>

          <div class="tp-caption overlay-text tp-resizeme text-white"
               data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
               data-y="['middle','middle','middle','middle']" data-voffset="['30','30','30','30']"
               data-whitespace="nowrap" data-responsive_offset="on"
               data-width="['none','none','none','none']" data-type="text"
               data-textalign="['center','center','center','center']" data-fontsize="['24','24','20','20']"
               data-transform_idle="o:1;"
               data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;s:1000;e:Power2.easeOut;"
               data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
               data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
               data-start="1500" data-splitin="none" data-splitout="none">
            <p style="letter-spacing: 2px">{{ str_limit($welcome->description) }}</p>
          </div>

          <div class="tp-caption tp-resizeme"
               data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
               data-y="['middle','middle','middle','middle']" data-voffset="['160','160','160','160']"
               data-width="200" data-height="none"
               data-whitespace="normal" data-type="button"
               data-actions='' data-responsive_offset="on"
               data-textAlign="['center','center','center','center']"
               data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]"
               data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]"
               data-frames='[{"delay":700,"speed":2000,"frame":"0","from":"sX:1;sY:1;opacity:0;fb:40px;","to":"o:1;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"200","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;fb:0;"}]'
               style=" box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;">
            <a href="#service" class="btn btn-pink btn-large">Learn More</a>
          </div>

        </li>

        @if($projects)
          @for($key=0; $key<5; $key++)
            @php
              $row = $projects[$key % count($projects)];

              if(!optional($row->image)->image){
                continue;
              }
            @endphp
            <li data-index="rs-{{ $key+2 }}" data-transition="crossfade" data-slotamount="default" data-hideafterloop="0"
                data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="500"
                data-rotate="0"
                data-saveperformance="off" data-title="Slide" data-param1="0{{ $key+2 }}" data-param2="" data-param3=""
                data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9=""
                data-param10="" data-description="">
              <!-- MAIN IMAGE -->
              <img src="{{ asset($row->image->image) }}" alt="{{ $row->title }}" data-bgposition="center center"
                   data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
              <!-- LAYERS -->

              <!-- LAYER NR. 1 -->
              {{--<div class="tp-caption overlay-text tp-resizeme text-white"--}}
                   {{--data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"--}}
                   {{--data-y="['middle','middle','middle','middle']" data-voffset="['-60','-60','-60','-60']"--}}
                   {{--data-fontsize="['70','60','50','30']"--}}
                   {{--data-lineheight="['90','90','50','35']"--}}
                   {{--data-whitespace="nowrap" data-responsive_offset="on"--}}
                   {{--data-width="['none','none','none','none']" data-type="text"--}}
                   {{--data-textalign="['center','center','center','center']"--}}
                   {{--data-transform_idle="o:1;" data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;s:inherit;e:inherit;"--}}
                   {{--data-start="1200" data-splitin="none" data-splitout="none" style="font-weight:500">--}}
                {{--{{ $welcome->main }}--}}
              {{--</div>--}}

              <div class="tp-caption overlay-text tp-resizeme text-white"
                   data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                   data-y="['middle','middle','middle','middle']" data-voffset="['30','30','30','30']"
                   data-whitespace="nowrap" data-responsive_offset="on"
                   data-width="['none','none','none','none']" data-type="text"
                   data-textalign="['center','center','center','center']" data-fontsize="['24','24','20','20']"
                   data-transform_idle="o:1;"
                   data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;s:1000;e:Power2.easeOut;"
                   data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                   data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                   data-start="1500" data-splitin="none" data-splitout="none">
                <p style="letter-spacing: 2px">{{ str_limit( $row->title ) }}</p>
              </div>

              <div class="tp-caption tp-resizeme"
                   data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                   data-y="['middle','middle','middle','middle']" data-voffset="['160','160','160','160']"
                   data-width="200" data-height="none"
                   data-whitespace="normal" data-type="button"
                   data-actions='' data-responsive_offset="on"
                   data-textAlign="['center','center','center','center']"
                   data-margintop="[0,0,0,0]" data-marginright="[0,0,0,0]"
                   data-marginbottom="[0,0,0,0]" data-marginleft="[0,0,0,0]"
                   data-frames='[{"delay":700,"speed":2000,"frame":"0","from":"sX:1;sY:1;opacity:0;fb:40px;","to":"o:1;fb:0;","ease":"Power4.easeInOut"},{"delay":"wait","speed":300,"frame":"999","to":"opacity:0;fb:0;","ease":"Power3.easeInOut"},{"frame":"hover","speed":"200","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;fb:0;"}]'
                   style=" box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;">
                <a href="#service" class="btn btn-pink btn-large">Learn More</a>
              </div>


            </li>
          @endfor
        @endif
      </ul>
      <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
    </div>
    <div class="slider-social">
      @if($user->fb_link)
      <a href="{{ website_check_facebook_link($user->fb_link) }}" class="facebook-text-hvr"><i class="ti ti-facebook" aria-hidden="true"></i></a>
      @endif
      @if($user->tw_link)
      <a href="{{ website_check_twitter_link($user->tw_link) }}" class="twitter-text-hvr"><i class="ti ti-twitter-alt" aria-hidden="true"></i></a>
      @endif
      @if($user->ig_link)
      <a href="{{ website_check_instagram_link($user->ig_link) }}" class="instagram-text-hvr"><i class="ti ti-instagram" aria-hidden="true"></i></a>
      @endif
      @if($user->pin_link)
      <a href="{{ website_check_pinterest_link($user->pin_link) }}" class="pinterest-text-hvr"><i class="ti ti-pinterest" aria-hidden="true"></i></a>
      @endif
    </div>

  </div>
</section>
<!--slider end-->

@if($services && count($services))
@php
  $servicesCount = count($services);
@endphp
<a name="service"></a>
<section id="services" class="feature circle-top pb-0">
  <div class="container">
    <div class="row">
      @for($key=0; $key<4; $key++)
        @php
          $service = $services[$key % count($services)];
        @endphp
      {{--@foreach($services as $service)--}}
      {{--<div class="col-md-{{ 12/$servicesCount }} mb-xs-5">--}}
      <div class="col-md-3 mb-xs-5">
        <div class="feature-box text-center">
          @if($service['icons'])
            <img class="service-icon" src="{{ $service['icons'] }}" />
          @else
            <i class="ti ti-target feature-icon" aria-hidden="true"></i>
          @endif
            <h4 class="text-capitalize">{{ $service['title'] }}</h4>
            <span class="hr-line mt-4 mb-4"></span>
            <p class="mt-3 mb-3">{{ $service['description'] }}</p>
        </div>
      </div>
      {{--@endforeach--}}
      @endfor
    </div>
  </div>
</section>
@endif

@if(count($team))
  <!--team start-->
  <section id="team" class="team circle-left">
    <div class="row">
      <div class="col-sm-12">
        <div class="title text-center pb-5">
          <h2 class="font-weight-600 m-0">TEAM</h2>
          <span class="hr-line mt-4 mb-4"></span>
        </div>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="team-box">
            <form>
              <ul>
                @foreach($team as $key=>$row)
                  @if($key>2)
                    @break
                  @endif

                  <li>
                    <input id="rad{{ $key+1 }}" type="radio" checked name="rad">
                    <!--verticle text-->
                    <label for="rad{{ $key+1 }}"><span class="team-name text-white">{{ $row['title'] }}</span></label>
                    <div class="accslide d-flex">
                      <div class="team-inner">
                        <div class="team-about">
                          <p>{{ $row['title'] }}</p>
                          <span class="hr-line ml-0 mt-4 mb-4"></span>
                          <p class="pre">{!! $row['description'] !!}</p>
                        </div>
                        @if($row['image'])
                          <div class="team-img">
                            <img src="{{ asset($row['image']) }}" class="team-picture" alt="team-img">
                          </div>
                        @endif
                      </div>
                    </div>
                  </li>
                @endforeach
              </ul>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
@endif

@if(count($projects))
<section id="portfolio_top" class="bg-light p-0">
  <div class="container">
    <div id="portfolio-measonry" class="cbp border-portfolio simple_overlay">

      @foreach($projects as $key=>$project)
        @if($key>3)
          @break
        @endif
      <div class="cbp-item itemshadow">
        <img src="{{ asset(optional($project->image)->image) }}" class="project-img" alt="{{ $project->title }}">
        <div class="overlay center-block text-white">
          <a class="plus" data-fancybox="gallery" href="{{ asset(optional($project->image)->image) }}"></a>
          {{--<h4 class="pt-3">Latest Work</h4>--}}
          <p>{{ $project->description }}</p>
        </div>
      </div>

      @if($key==0)
      <div class="cbp-item pt-5">
        <div class="text_wrap wow fadeIn" data-wow-delay="350ms">
          <div class="heading-title text-center pt-5">
            <h2 class="font-weight-600 m-0">CREATIVE WORK</h2>
            <span class="hr-line mt-4 mb-4"></span>
          </div>
        </div>
      </div>
      @endif
      @endforeach


      <div class="cbp-item">
        <div class="bottom-text">
          <div class="cells  wow fadeIn" data-wow-delay="350ms">
            {{--<p class="pb-3">We’ve Completed More Than </p>--}}
            {{--<h2 class="port_head">682</h2>--}}
            {{--<p>projects for our amazing clients,</p>--}}
          </div>
          <div class="cells wow fadeIn" data-wow-delay="350ms">
            <a href="{{ websiteRoute('website.project.list') }}" class="btn btn-large btn-gray">View All Work</a>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
@endif

<!-- our story -->
{{--<section class="our-story pb-0">--}}
  {{--<div class="container">--}}
    {{--<div class="row d-flex align-items-center">--}}
      {{--<div class="col-md-6">--}}
        {{--<div class="title pb-5">--}}
          {{--<h2 class="font-weight-600 m-0">{{ $welcome->main }}</h2>--}}
          {{--<span class="hr-line mt-4 mb-4 ml-lg-0"></span>--}}
          {{--<p class="mb-3 mb-lg-0">{{ $welcome->description }}</p>--}}
        {{--</div>--}}
      {{--</div>--}}
      {{--<div class="col-md-6">--}}
        {{--<div class="story-image">--}}
          {{--@if($welcome->user_picture)--}}
          {{--<img src="{{ asset($welcome->user_picture) }}" alt="logo">--}}
          {{--@endif--}}
        {{--</div>--}}
      {{--</div>--}}
    {{--</div>--}}
  {{--</div>--}}
{{--</section>--}}
<!-- our story end-->


{{--
<!-- our client -->
<section id="client" class="our-client">
  <h2 class="d-none" aria-hidden="true">hidden</h2>
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 p-0">
        <div class="owl-carousel partners-slider">
          <div class="item">
            <div class="logo-item"> <img alt="" src="{{ asset('themes/'.$theme.'/images/client-one.png') }}"></div>
          </div>
          <div class="item">
            <div class="logo-item"><img alt="" src="{{ asset('themes/'.$theme.'/images/client-two.png') }}"></div>
          </div>
          <div class="item">
            <div class="logo-item"> <img alt="" src="{{ asset('themes/'.$theme.'/images/client-one.png') }}"></div>
          </div>
          <div class="item">
            <div class="logo-item"><img alt="" src="{{ asset('themes/'.$theme.'/images/client-two.png') }}"></div>
          </div>
          <div class="item">
            <div class="logo-item"> <img alt="" src="{{ asset('themes/'.$theme.'/images/client-one.png') }}"></div>
          </div>
          <div class="item">
            <div class="logo-item"><img alt="" src="{{ asset('themes/'.$theme.'/images/client-two.png') }}"></div>
          </div>
          <div class="item">
            <div class="logo-item"> <img alt="" src="{{ asset('themes/'.$theme.'/images/client-one.png') }}"></div>
          </div>
          <div class="item">
            <div class="logo-item"><img alt="" src="{{ asset('themes/'.$theme.'/images/client-two.png') }}"></div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- our client end -->
--}}

@if(count($articles))
<!--blog-->
<section id="blog" class="blog-list bg-light text-center text-md-left">
  <h2 class="d-none" aria-hidden="true">slider</h2>
  <div class="container">
    @if(count($articles)>4)
    <div class="row">
      <div class="col-md-12 mb-5">
        <div class="blog-box text-right">
          <a class="btn btn-large btn-pink mb-xs-4" href="{{ websiteRoute('website.blog.list') }}"> View All</a>
        </div>
      </div>
    </div>
    @endif
    <div class="row">
      <!-- blog-item one -->
{{--      @foreach($articles as $key=>$article)--}}
      @for($key=0; $key<4; $key++)
        @php
          $article = $articles[$key % count($articles)];
        @endphp
      <div class="col-lg-3 col-md-6 mb-3 mb-xs-5">
        <div class="image" style="height: 255px;">
          @if($article->image)
          <img alt="image" class="article-img" src="{{ asset($article->image) }}">
          @else
          <img alt="image" class="article-img" src="{{ asset('themes/'.$theme.'/images/icon-no-image.svg') }}">
          @endif
        </div>
      </div>
      <div class="col-lg-3 col-md-6 mb-xs-5">
        <div class="blog-box">
          <h4 class="text-capitalize mb-4">{!! $article->title !!}</h4>
          <p class="mb-3 mb-xs-4">{!! str_limit(strip_tags($article->body), 30) !!}</p>
          <a class="btn btn-large btn-pink mb-xs-4" href="{{ websiteRoute('website.blog.show', [$article->id]) }}"> Read More</a>
        </div>
      </div>
      {{--@endforeach--}}
      @endfor
    </div>
  </div>
</section>
<!--blog end-->
@endif

@if($contact_us->show_tab)
<!-- Contact US -->
<section id="contact" class="contact">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <div class="title text-center pb-5">
          <h2 class="font-weight-600 m-0">{{ $contact_us->title }}</h2>
          <span class="hr-line mt-4 mb-4"></span>
          <p class="mb-4">{{ $contact_us->message }}</p>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-6">
        <form class="getin_form" action="{{ websiteRoute('website.contact_us') }}" method="post">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-sm-12" id="result"></div>

            <div class="col-md-12 col-sm-12">
              <div class="form-group">
                <input class="form-control" type="text" placeholder="Enter subject" required id="title" name="title">
              </div>
            </div>
            <div class="col-md-12 col-sm-12">
              <div class="form-group mb-4">
                <textarea class="form-control" placeholder="Enter message" id="message" name="message"></textarea>
              </div>
            </div>
            <div class="col-sm-12">
              <button type="submit" class="btn btn-large btn-pink w-100" id="submit_btn">Send</button>
            </div>
          </div>
        </form>
      </div>
      <div class="col-md-6 col-sm-12 pl-4">
        <p class="mb-lg-5 mb-4 mt-2">{{ $user->description }}</p>
        <div class="row">
          <div class="col-md-6 col-sm-6 our-address mb-4">
            <h6 class="mb-3 font-weight-600">Our Address</h6>
            <p class="mb-2">Country: {{ optional($user->country)->text }} </p>
            <p class="mb-2">City: {{ optional($user->city)->text }} </p>
          </div>
          <div class="col-md-6 col-sm-6 our-address mb-4">
            <h6 class="mb-3 font-weight-600">Our Contact</h6>
            <p class="mb-2">Phone No.: {{ $user->phone }}</p>
            <p class="mb-2">E-mail: {{ $user->email }}</p>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Contact US Ends-->
@endif

@endsection

@section('script')
<script>
  $('.getin_form').submit(function(e){
    e.preventDefault();
    var $this = $(this);

    $('#submit_btn').prop('disabled', true);

    $.post($this.attr('action'), $this.serialize())
      .done(function (d) {
        $('#result').empty().append($('<span>').addClass(d.status? 'alert-success': 'alert-danger').text(d.message))
      })
      .always(function(){
        $('#submit_btn').prop('disabled', false);
      });

  })
  var wow = new WOW(
        {
          boxClass:     'wow',      // animated element css class (default is wow)
          animateClass: 'animated', // animation css class (default is animated)
          offset:       0,          // distance to the element when triggering the animation (default is 0)
          mobile:       false       // trigger animations on mobile devices (true is default)
        }
      );
      wow.init();
</script>
@endsection