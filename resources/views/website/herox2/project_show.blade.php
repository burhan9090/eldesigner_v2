@extends('website.herox2.layout')
@section('content')
<!--cover-->
<section class="page-header bg-3">
  <div class="container">
    <div class="row text-center text-white">
      <div class="col-sm-12 pt-md-5">
        <div class="page-title">
          <h2 class="overlay-text" style="display: inline-block;">Project</h2><br />
          @if($welcome->main)
            <p class="overlay-text" style="display: inline-block;">{{ $welcome->main }}</p>
          @endif
        </div>
      </div>
    </div>
  </div>
</section>
<!--cover end-->

<!--work start-->
<section id="work" class="our-work pb-0">
  <div class="row">
    <div class="col-sm-12">
      <div class="title text-center pb-5">
        <h2 class="font-weight-600 m-0">{{ $project->title }}</h2>
        <h6 class="font-weight-600 mt-3">{{ $project->user->name }} - {{ $project->created_at->format('M d, Y') }}</h6>
        <span class="hr-line mt-4 mb-4"></span>
        <p class="mb-4">{!! $project->description !!}</p>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-sm-12 p-5 pt-0">
      <div class="content-carousel">
        <div class="owl-carousel owl-work">
          @foreach($project->images as $image)
          <div> <img src="{{ asset($image->image) }}" alt="{{ $project->title }}" title="{{ $image->description }}" ></div>
          @endforeach
        </div>
      </div>

    </div>
  </div>
</section>
<!--work end-->

@endsection
