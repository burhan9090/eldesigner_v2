@extends('website.herox2.layout')
@section('content')
<!--cover-->
<section class="page-header bg-3">
  <div class="container">
    <div class="row text-center text-white">
      <div class="col-sm-12 pt-md-5">
        <div class="page-title">
          <h2 class="overlay-text" style="display: inline-block;">Blog List</h2><br />
          @if($welcome->main)
            <p class="overlay-text" style="display: inline-block;">{{ $welcome->main }}</p>
          @endif
        </div>
      </div>
    </div>
  </div>
</section>
<!--cover end-->

<!--page content-->
<section>
  <div class="container">
    <div class="row">
      <!--left side-->
      <div class="col-lg-8 mb-xs-5">
        @forelse($articles as $article)
        <!--item-->
        <div class="blog-list-item">
          @if($article->image)
          <!--blog image-->
          <a href="{{ websiteRoute('website.blog.show', [$article->id]) }}">
            <div class="blog-item-img mb-5 hover-effect">
              <img src="{{ asset($article->image) }}" class="list-img" alt="image">
            </div>
          </a>
          @endif
          <!--blog contetn-->
          <div class="blog-item-content">
            <span class="category main-color tex">{{ optional(optional($article->category)->parent)->text }}</span> -
            <span class="date">{{ $article->created_at->format('M d, Y') }}</span>
            <h4 class="mt-2 mb-3"><a href="{{ websiteRoute('website.blog.show', [$article->id]) }}">{{ $article->title}}</a></h4>
            <p class="mb-4">{{ str_limit(strip_tags($article->body)) }}</p>
            <!--button-->
            <h6 class="text-capitalize"><a href="{{ websiteRoute('website.blog.show', [$article->id]) }}">Read More</a></h6>
          </div>
        </div>
        @empty
        <!--item-->
        <div class="blog-list-item">
          <div class="blog-item-content">
            <h4 class="mt-2 mb-3">There are no Articles to show</h4>
          </div>
        </div>
        @endforelse
        <!--item-->

        {{ $articles->links('website.herox2.pagination') }}
      </div>
      <!--right side-->
      <div class="col-lg-4">
        <!--search-->
        <form>
        <div class="widget d-flex bg-light mb-4">
          <input class="search" name="filter[search_article]" placeholder="Search.." type="text" value="{{ isset(($r = request('filter', ['search_article'=>'']))['search_article'])? $r['search_article']: '' }}">
          @if(isset(($r = request('filter'))['category_parent']))
          <input name="filter[category_parent]" type="hidden" value="{{ $r['category_parent'] }}">
          @endif
            <button type="submit" class="search-btn"><i class="ti-search" aria-hidden="true"></i></button>
        </div>
        </form>
        <!--recent post-->
        <!--category-->
        <div class="widget bg-light">
          <h5 class="mb-4">Category</h5>
          <!--list-->
          <ul class="list-unstyled blog-category m-0">
            @foreach($categories as $category)
            <li><a href="{{ websiteRoute('website.blog.list', ['filter'=>['category_parent'=> $category->id]]) }}">{{ $category->text }}</a></li>
            @endforeach
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<!--page content end-->
@endsection

@section('css')
  <style>
    body .navbar-top-default .navbar-nav .nav-link {
      padding-right: 1.2rem;
      padding-left: 1.2rem;
      color: #FFFFFF;
      font-weight: 400;
    }
  </style>
@endsection