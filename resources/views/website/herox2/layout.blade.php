<!doctype html>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <!-- Page Title -->
@php

  if(!isset($keywordHeader)){
    $keywordHeader = $welcome->keyword;
  }

  if(!isset($titleHeader)){
    $titleHeader=$welcome->title;
  }

  if(!isset($descriptionHeader)){
    $descriptionHeader=$welcome->meta_description;
  }

  if(!isset($logoHeader)){
    $logoHeader = $welcome->logo;
  }

  $logoHeader = asset($logoHeader);

  $typeHeader = 'profile';
  if($nav=='article'){
    $typeHeader = 'article';
  }
  elseif($nav=='project'){
    $typeHeader = 'project';
  }

@endphp

<!-- Page SEO -->
  <meta name="keywords" content="{{ $keywordHeader }}"/>

  <meta name="author" content="{{ $user->name }}"/>
  <meta name="copyright" content="{{ $user->name }}"/>

  <meta name="url" content="{{ websiteRoute('website.index') }}"/>
  <meta name="identifier-URL" content="{{ websiteRoute('website.index') }}"/>
  <meta name="twitter:url" content="{{ websiteRoute('website.index') }}">


  <title>{{ $titleHeader }}</title>
  <meta name="subject" content="{{ $titleHeader }}"/>
  <meta name="twitter:title" content="{{ $titleHeader }}">
  <meta property="og:title" content="{{ $titleHeader }}"/>


  <meta name="description" content="{{ $descriptionHeader }}"/>
  <meta name="twitter:description" content="{{ $descriptionHeader }}">
  <meta property="og:description" content="{{ $descriptionHeader }}"/>


  <meta name="twitter:image" content="{{ $logoHeader }}">
  <meta property="og:image" content="{{ $logoHeader }}"/>


  @if($welcome->gplus)
    <link rel="author" href="{{ $welcome->gplus }}"/>
    <link rel="publisher" href="{{ $welcome->gplus }}"/>
  @endif

  @if($welcome->fb_page_id)
    <meta name="fb:page_id" content="{{ $welcome->fb_page_id }}"/>
    <meta property=”fb:admins” content=”{{ $welcome->fb_page_id }}”/>
  @endif

  <meta name="twitter:card" content="{{ $typeHeader }}"/>
  <meta property="og:type" content="{{ $typeHeader }}"/>

  <meta property="og:url" content="{{ websiteRoute('website.index') }}"/>

  <meta name="og:site_name" content="{{ $user->name }}"/>
  <meta name="og:region" content="{{ optional(optional($user->city)->country)->code }}"/>
  <meta name="og:country-name" content="{{ optional(optional($user->city)->country)->text }}"/>


  <!-- Favicon -->
  <link rel="icon" href="http://www.themesindustry.com/html/herox/images/favicon.ico">
  <!-- Animate CSS-->
  <link rel="stylesheet" href="{{ asset('themes/'.$theme.'/css/animate.min.css') }}">
  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="{{ asset('themes/'.$theme.'/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('themes/'.$theme.'/css/themify-icons.css') }}">
  <!-- Owl Carousel -->
  <link rel="stylesheet" href="{{ asset('themes/'.$theme.'/css/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ asset('themes/'.$theme.'/css/owl.theme.default.min.css') }}">
  <!-- Fancy Box -->
  <link rel="stylesheet" href="{{ asset('themes/'.$theme.'/css/jquery.fancybox.min.css') }}">
  <!-- Cube Portfolio -->
  <link rel="stylesheet" href="{{ asset('themes/'.$theme.'/css/cubeportfolio.min.css') }}">
  <!-- Revolution Style Sheets -->
  <link rel="stylesheet" type="text/css" href="{{ asset('themes/'.$theme.'/revolution/css/settings.css') }}">
  <link rel="stylesheet" type="text/css" href="{{ asset('themes/'.$theme.'/revolution/css/navigation.css') }}">
  <!-- Style Sheet -->
  <link rel="stylesheet" href="{{ asset('themes/'.$theme.'/css/style.css') }}">
  <!-- Style Customizer's stylesheets -->
  <link rel="stylesheet" type="text/css"
        href="{{ asset('themes/'.$theme.'/color-switcher/js/style-customizer/css/style-customizer.css') }}">
  {{--<link rel="stylesheet/less" type="text/css" href="{{ asset('themes/'.$theme.'/color-switcher/less/skin.less') }}">--}}

  @if($welcome->background)
    <style>
      .bg-3 {
        background: url('{{ asset($welcome->background) }}') no-repeat;
        background-size: cover;
      }
    </style>
  @endif


  <style>
    .service-icon {
      object-fit: none;
      object-position: center;
      width: 64px;
      height: 64px;
    }
  </style>
  @php
    $userThemeSettings =  json_decode($user->theme_setting, 1)?: [];
    $color = "#ff6666";

    if(isset($userThemeSettings['custom'])){
      if(isset($userThemeSettings['custom']['background'])){
        $color ='#'.trim($userThemeSettings['custom']['background'], '#');
      }
    }
  @endphp
  <style>
    :root {
      @if(isset($userThemeSettings['bootstrap']))
      @foreach($userThemeSettings['bootstrap'] as $key=>$val )
        {!! "{$key}: {$val};" !!}
      @endforeach
      @endif
       --breakpoint-xs: 0;
      --breakpoint-sm: 576px;
      --breakpoint-md: 768px;
      --breakpoint-lg: 992px;
      --breakpoint-xl: 1200px;
      --font-family-sans-serif: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif, "Apple Color Emoji", "Segoe UI Emoji", "Segoe UI Symbol", "Noto Color Emoji";
      --font-family-monospace: SFMono-Regular, Menlo, Monaco, Consolas, "Liberation Mono", "Courier New", monospace;
    }

    .btn-pink {
      background: {{ $color }};
      border: 2px solid{{ $color }};
    }

    .btn-transparent-pink {
      color: {{ $color }};
      background: transparent;
      border: 2px solid{{ $color }};
    }

    .btn-transparent-pink:before {
      background: {{ $color }};
    }

    .main-color {
      color: {{ $color }};
    }

    ::-webkit-scrollbar,
    ::-webkit-scrollbar-thumb {
      background-color: {{ $color }};
    }

    .nav-line .navbar-nav .nav-link:before,
    .nav-line-left .navbar-nav .nav-link:before,
    .hr-line,
    .prog-item .skills-progress span,
    .team-box label,
    .team-box label:hover,
    .team-box #rad2 + .team-box label:hover,
    .team-two-right-nav,
    .team-two-left-nav,
    .overlay:before,
    .portfolio-three .item-img-overlay:before,
    .price-item.center,
    .button-play-two,
    .owl-testimonial .owl-item.center .testimonial-item .testimonial-quote,
    .owl-testimonial .owl-dots .owl-dot.active span,
    .owl-testimonial .owl-dots .owl-dot:hover span,
    .button-play,
    .widget .search-btn,
    .blog-tags li a:hover,
    .blog-tags li a:focus,
    .side-menu,
    .scroll-top-arrow,
    .scroll-top-arrow:focus,
    .heroex .tp-count:before {
      background: {{ $color }};
    }

    a:hover,
    a:active,
    .top-center-logo .navbar-nav .nav-link:hover,
    .header-appear .top-center-logo .navbar-nav .nav-link:hover,
    .top-left-logo .navbar-nav .nav-link:hover,
    .header-appear .top-left-logo .navbar-nav .nav-link:hover,
    .navbar-top-default .navbar-nav .nav-link:hover,
    .header-appear .navbar-top-default .navbar-nav .nav-link:hover,
    .port_head,
    .portfolio-three .filtering .active,
    .button-play-two:hover,
    .button-play-two:focus,
    .button-play-two:hover i,
    .button-play-two:focus i,
    .owl-testimonial .owl-item.center .testimonial-item .text-small,
    .button-play:hover,
    .button-play:focus,
    .button-play:hover i,
    .button-play:focus i,
    .our-address .pickus,
    .blog-pagination li a:hover,
    .blog-pagination li.active a,
    .blog-pagination li a:focus {
      color: {{ $color }};
    }

    .scroll-down .triangle-down {
      border-top: 10px solid{{ $color }};
    }

    .owl-testimonial .owl-dots .owl-dot span {
      border: 1px solid{{ $color }};
    }

    .blog-box h4 {
      border-left: 2px solid{{ $color }};
    }

    .cssload-inner.cssload-one {
      border-bottom: 5px solid{{ $color }};
    }

    .cssload-inner.cssload-two {
      border-right: 5px solid{{ $color }};
    }

    .cssload-inner.cssload-three {
      border-top: 5px solid{{ $color }};
    }
  </style>

  @yield('css')

</head>
<body data-spy="scroll" data-target=".navbar" data-offset="90">

<!--start loader-->
<div class="loader">
  <div class="cssload-loader">
    <div class="cssload-inner cssload-one"></div>
    <div class="cssload-inner cssload-two"></div>
    <div class="cssload-inner cssload-three"></div>
  </div>
</div>
<!--loader end-->

<!--header start-->
<header>
  <nav class="navbar navbar-top-default navbar-expand-lg nav-line {{ $nav=='home'? 'dark-navbar': '' }}">
    <div class="container">

      @if($welcome->logo)
        <a href="{{ websiteRoute('website.index') }}" title="Logo" class="logo scroll"><img
              src="{{ asset($welcome->logo) }}" class="logo-dark default" alt="logo">
        </a>
      @endif
      <div class="collapse navbar-collapse" id="Herox">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link  {{ $nav=='home'? 'scroll active': '' }}"
               href="{{ websiteRoute('website.index') }}#home">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ $nav=='home'? 'scroll': '' }}" href="{{ websiteRoute('website.index') }}#services">Services</a>
          </li>
          {{--<li class="nav-item">--}}
          {{--<a class="nav-link scroll" href="#team">Team</a>--}}
          {{--</li>--}}
          <li class="nav-item">
            <a class="nav-link {{ $nav=='project'? 'active': '' }}" href="{{ websiteRoute('website.project.list') }}">Projects</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ $nav=='article'? 'active': '' }}"
               href="{{ websiteRoute('website.blog.list') }}">Blog</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ $nav=='home'? 'scroll': '' }}" href="{{ websiteRoute('website.index') }}#contact">contact</a>
          </li>
        </ul>
      </div>
      <div class="header-social pl-3 d-none d-lg-inline-block">
        @if($user->fb_link)
          <a href="{{ website_check_facebook_link($user->fb_link) }}" class="facebook-text-hvr"><i
                class="ti ti-facebook" aria-hidden="true"></i></a>
        @endif
        @if($user->tw_link)
          <a href="{{ website_check_twitter_link($user->tw_link) }}" class="twitter-text-hvr"><i
                class="ti ti-twitter-alt" aria-hidden="true"></i></a>
        @endif
        @if($user->ig_link)
          <a href="{{ website_check_instagram_link($user->ig_link) }}" class="instagram-text-hvr"><i
                class="ti ti-instagram" aria-hidden="true"></i></a>
        @endif
        @if($user->pin_link)
          <a href="{{ website_check_pinterest_link($user->pin_link) }}" class="pinterest-text-hvr"><i
                class="ti ti-pinterest" aria-hidden="true"></i></a>
        @endif
      </div>

      <!--side menu open button-->
      <a href="javascript:void(0)" class="d-inline-block sidemenu_btn" id="sidemenu_toggle">
        <span></span> <span></span> <span></span>
      </a>
    </div>
  </nav>
  <!-- side menu -->
  <div class="side-menu">
    <div class="inner-wrapper">
      <span class="btn-close" id="btn_sideNavClose"><i></i><i></i></span>
      <nav class="side-nav w-100">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link {{ $nav=='home'? 'scroll active': '' }}" href="{{ websiteRoute('website.index') }}#home">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ $nav=='home'? 'scroll': '' }}" href="{{ websiteRoute('website.index') }}#services">Services</a>
          </li>
          {{--<li class="nav-item">--}}
          {{--<a class="nav-link scroll" href="#team">Team</a>--}}
          {{--</li>--}}
          <li class="nav-item">
            <a class="nav-link {{ $nav=='project'? 'active': '' }}" href="{{ websiteRoute('website.project.list') }}">Projects</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ $nav=='article'? 'active': '' }}"
               href="{{ websiteRoute('website.blog.list') }}">Blog</a>
          </li>
          <li class="nav-item">
            <a class="nav-link {{ $nav=='home'? 'scroll': '' }}" href="{{ websiteRoute('website.index') }}#contact">Contact</a>
          </li>
        </ul>
      </nav>

      <div class="side-footer text-white w-100">
        <ul class="social-icons-simple">
          @if($user->fb_link)
            <li><a class="facebook-bg-hvr" href="{{ website_check_facebook_link($user->fb_link) }}"><i
                    class="ti ti-facebook"></i> </a></li>
          @endif
          @if($user->tw_link)
            <li><a class="twitter-bg-hvr" href="{{ website_check_twitter_link($user->tw_link) }}"><i
                    class="ti ti-twitter"></i> </a></li>
          @endif
          @if($user->ig_link)
            <li><a class="instagram-bg-hvr" href="{{ website_check_instagram_link($user->ig_link) }}"><i
                    class="ti ti-instagram"></i> </a></li>
          @endif
          @if($user->pin_link)
            <li><a class="pinterest-bg-hvr" href="{{ website_check_pinterest_link($user->pin_link) }}"><i
                    class="ti ti-pinterest"></i> </a></li>
          @endif
        </ul>
        {{--<p class="whitecolor">&copy; 2018 Herox. Made With Love by Themesindustry</p>--}}
      </div>
    </div>
  </div>
  <a id="close_side_menu" href="javascript:void(0);"></a>
  <!-- End side menu -->
</header>
<!--header end-->

@yield('content')

@include('website.herox2.footer')

<!-- start scroll to top -->
<a class="scroll-top-arrow" href="javascript:void(0);"><i class="ti ti-angle-up"></i></a>
<!-- end scroll to top  -->


<!-- Optional JavaScript -->
<script src="{{ asset('themes/'.$theme.'/js/jquery-3.3.1.min.js') }}"></script>
<script src="{{ asset('themes/'.$theme.'/js/popper.min.js') }}"></script>
<script src="{{ asset('themes/'.$theme.'/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('themes/'.$theme.'/js/jquery.appear.js') }}"></script>
<!-- isotop gallery -->
<script src="{{ asset('themes/'.$theme.'/js/isotope.pkgd.min.js') }}"></script>
<!-- cube portfolio gallery -->
<script src="{{ asset('themes/'.$theme.'/js/jquery.cubeportfolio.min.js') }}"></script>
<!-- owl carousel slider -->
<script src="{{ asset('themes/'.$theme.'/js/owl.carousel.min.js') }}"></script>
<!-- parallax Background -->
<script src="{{ asset('themes/'.$theme.'/js/parallaxie.min.js') }}"></script>
<!-- Color Switcher -->
<script src="{{ asset('themes/'.$theme.'/color-switcher/js/less/less.min.js') }}" data-env="development"></script>
{{--<script src="{{ asset('themes/'.$theme.'/color-switcher/js/style-customizer/js/style-customizer.js') }}"></script>--}}
<!-- fancybox popup -->
<script src="{{ asset('themes/'.$theme.'/js/jquery.fancybox.min.js') }}"></script>
<!-- REVOLUTION JS FILES -->
<script src="{{ asset('themes/'.$theme.'/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
<script src="{{ asset('themes/'.$theme.'/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>
<!-- SLIDER REVOLUTION EXTENSIONS -->
<script src="{{ asset('themes/'.$theme.'/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
<script src="{{ asset('themes/'.$theme.'/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
<script src="{{ asset('themes/'.$theme.'/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script
    src="{{ asset('themes/'.$theme.'/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script src="{{ asset('themes/'.$theme.'/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
<script src="{{ asset('themes/'.$theme.'/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script src="{{ asset('themes/'.$theme.'/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
<script src="{{ asset('themes/'.$theme.'/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script src="{{ asset('themes/'.$theme.'/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
<!-- custom script -->
<script src="{{ asset('themes/'.$theme.'/js/script.js') }}"></script>

@php
  \App\WebsiteTracking::addHit($user->id);

  $trucker = $welcome->tracking_script;

  if(strpos($trucker, '<script')!==false){
  echo $trucker;
  }
  else {
  echo "<script>";
  echo $trucker;
  echo "</script>";
  }
@endphp

@yield('script')
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133547867-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-133547867-1');
</script>


</body>
</html>