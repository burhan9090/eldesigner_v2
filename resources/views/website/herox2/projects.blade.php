@extends('website.herox2.layout')
@section('content')
<!--cover-->
<section class="page-header bg-3">
  <div class="container">
    <div class="row text-center text-white">
      <div class="col-sm-12 pt-md-5">
        <div class="page-title">
          <h2>Project List</h2>
          @if($welcome->main)
          <p>{{ $welcome->main }}</p>
          @endif
        </div>
      </div>
    </div>
  </div>
</section>
<!--cover end-->

<!--page content-->
<section>
  <div class="container">
    <div class="row">
      <!--left side-->
      <div class="col-lg-8 mb-xs-5">
        @forelse($projects as $project)
        <!--item-->
        <div class="blog-list-item">
          @if(optional($project->image)->image)
          <!--blog image-->
          <a href="{{ websiteRoute('website.project.show', [$project->id]) }}">
            <div class="blog-item-img mb-5 hover-effect">
              <img src="{{ asset(optional($project->image)->image) }}" class="list-img" alt="image">
            </div>
          </a>
          @endif
          <!--blog contetn-->
          <div class="blog-item-content">
            <span class="date">{{ $project->created_at->format('M d, Y') }}</span>
            <h4 class="mt-2 mb-3"><a href="{{ websiteRoute('website.project.show', [$project->id]) }}">{{ $project->title}}</a></h4>
            <p class="mb-4">{{ str_limit(strip_tags($project->description)) }}</p>
            <!--button-->
            <h6 class="text-capitalize"><a href="{{ websiteRoute('website.project.show', [$project->id]) }}">Show</a></h6>
          </div>
        </div>
        @empty
        <!--item-->
        <div class="blog-list-item">
          <div class="blog-item-content">
            <h4 class="mt-2 mb-3">There are no Projects to view</h4>
          </div>
        </div>
        @endforelse
        <!--item-->

        {{ $projects->links('website.herox2.pagination') }}
      </div>
      <!--right side-->
      <div class="col-lg-4">
        <!--search-->
        <form>
        <div class="widget d-flex bg-light mb-4">
          <input class="search" name="filter[search_project]" placeholder="Search.." type="text" value="{{ isset(($r = request('filter', ['search_project'=>'']))['search_project'])? $r['search_project']: '' }}">
          <button type="submit" class="search-btn"><i class="ti-search" aria-hidden="true"></i></button>
        </div>
        </form>
      </div>
    </div>
  </div>
</section>
<!--page content end-->
@endsection

@section('css')
  <style>
    body .navbar-top-default .navbar-nav .nav-link {
      padding-right: 1.2rem;
      padding-left: 1.2rem;
      color: #FFFFFF;
      font-weight: 400;
    }
  </style>
@endsection