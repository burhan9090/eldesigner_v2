<!DOCTYPE html>
<html lang="en">

<head>

  <!-- metas -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
@php

  if(!isset($keywordHeader)){
    $keywordHeader = $welcome->keyword;
  }

  if(!isset($titleHeader)){
    $titleHeader=$welcome->title;
  }

  if(!isset($descriptionHeader)){
    $descriptionHeader=$welcome->meta_description;
  }

  if(!isset($logoHeader)){
    $logoHeader = $welcome->user_picture;
  }

  $logoHeader = asset($logoHeader);

  $typeHeader = 'profile';
  if($nav=='article'){
    $typeHeader = 'article';
  }
  elseif($nav=='project'){
    $typeHeader = 'project';
  }

@endphp
<!-- Page SEO -->
  <meta name="keywords" content="{{ $keywordHeader }}"/>

  <meta name="author" content="{{ $user->name }}"/>
  <meta name="copyright" content="{{ $user->name }}"/>

  <meta name="url" content="{{ websiteRoute('website.index') }}"/>
  <meta name="identifier-URL" content="{{ websiteRoute('website.index') }}"/>
  <meta name="twitter:url" content="{{ websiteRoute('website.index') }}">


  <title>{{ $titleHeader }}</title>
  <meta name="subject" content="{{ $titleHeader }}"/>
  <meta name="twitter:title" content="{{ $titleHeader }}">
  <meta property="og:title" content="{{ $titleHeader }}"/>


  <meta name="description" content="{{ $descriptionHeader }}"/>
  <meta name="twitter:description" content="{{ $descriptionHeader }}">
  <meta property="og:description" content="{{ $descriptionHeader }}"/>


  <meta name="twitter:image" content="{{ $logoHeader }}">
  <meta property="og:image" content="{{ $logoHeader }}"/>


  @if($welcome->gplus)
    <link rel="author" href="{{ $welcome->gplus }}"/>
    <link rel="publisher" href="{{ $welcome->gplus }}"/>
  @endif

  @if($welcome->fb_page_id)
    <meta name="fb:page_id" content="{{ $welcome->fb_page_id }}"/>
    <meta property=”fb:admins” content=”{{ $welcome->fb_page_id }}”/>
  @endif

  <meta name="twitter:card" content="{{ $typeHeader }}" />
  <meta property="og:type" content="{{ $typeHeader }}" />

  <meta property="og:url" content="{{ websiteRoute('website.index') }}"/>

  <meta name="og:site_name" content="{{ $user->name }}"/>
  <meta name="og:region" content="{{ optional(optional($user->city)->country)->code }}"/>
  <meta name="og:country-name" content="{{ optional(optional($user->city)->country)->text }}"/>

  <!-- favicon -->
  <link href="{{ asset('themes/'.$theme.'/img/favicon.ico') }}" rel="icon" type="image/png">

  <!-- Plugins -->
  <link rel="stylesheet" href="{{ asset('themes/'.$theme.'/css/plugins.css') }}" />

  <!-- Core Style Css -->
  <link rel="stylesheet" href="{{ asset('themes/'.$theme.'/css/style.css') }}" />

  <style>
    :root {
      @if(isset($userThemeSettings['bootstrap']))
      @foreach($userThemeSettings['bootstrap'] as $key=>$val )
       {!! "{$key}: {$val};" !!}
      @endforeach
      @endif
      --breakpoint-xs: 0;
      --breakpoint-sm: 576px;
      --breakpoint-md: 768px;
      --breakpoint-lg: 992px;
      --breakpoint-xl: 1200px;
      --font-family-sans-serif: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif,"Apple Color Emoji","Segoe UI Emoji","Segoe UI Symbol","Noto Color Emoji";
      --font-family-monospace: SFMono-Regular,Menlo,Monaco,Consolas,"Liberation Mono","Courier New",monospace;
    }
  </style>
</head>

<body>

<!-- Start page loading -->
<div id="preloader">
  <div class="row loader">
    <div class="loader-icon"></div>
  </div>
</div>
<!-- End page loading -->


@yield('content')

@include('website.ashton-dark.footer')

<!-- Java script -->
<script src="{{ asset('themes/'.$theme.'/js/core.min.js') }}"></script>

<!-- custom scripts -->
<script src="{{ asset('themes/'.$theme.'/js/scripts.js') }}"></script>

<!-- contact form scripts -->
{{--<script src="{{ asset('themes/'.$theme.'/js/mailform/jquery.form.min.js') }}"></script>--}}
{{--<script src="{{ asset('themes/'.$theme.'/js/mailform/jquery.rd-mailform.min.c.js') }}"></script>--}}

@yield('script')

<!-- all js include end -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133547867-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-133547867-1');
</script>

</body>

</html>