@extends('website.ashton-dark.layout')
@section('content')

  <nav class="navbar navbar-expand-lg blog-nav bg-black">
    <div class="container sm-padding-10px-tb sm-padding-15px-lr">
      <!-- Logo -->
      <a class="logo" href="javascript:void(0);">
        <b style="text-transform: uppercase;" class="text-light-gray">{{ $user->name }}</b>
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="icon-bar"><i class="fas fa-bars"></i></span>
      </button>
      <!-- navbar links -->
      <div class="collapse navbar-collapse" id="navbarContent">
        <ul class="navbar-nav ml-auto">
          @include('website.ashton-dark.nav')
        </ul>
      </div>
    </div>
  </nav>
  <!-- End Navbar  -->

  <!-- Start Header -->
  <section id="home" class="blog-header" data-scroll-index="0">
    <div class="container">
      <div class="row">
        <div class="text-center col-md-12">
          <h5>{{ $welcome->main }}</h5>
          <a href="{{ websiteRoute('website.index') }}">Home</a>
          <a href="javascript:void(0);" class="active">Portfolio</a>
        </div>
      </div>
    </div>

  </section>
  <!-- End Header -->

  <!--  Start Blog Section -->
  <section class="blogs bg-extra-medium-gray">
    <div class="container">
      <div class="row">
        <!--  start blog left-->
        <div class="col-md-8 xs-margin-30px-bottom">
          <div class="posts">
            <!-- start post -->
            @forelse($projects as $project)
            <div class="post">
              @if(optional($project->image)->image)
              <div class="post-img">
                <a href="{{ websiteRoute('website.project.show', [$project->id]) }}" class="width-100">
                  <img src="{{ asset(optional($project->image)->image) }}" class="list-img" alt="{{ $project->title }}">
                </a>
              </div>
              @endif
              <div class="content">
                <div class="post-meta">
                  <div class="post-title">
                    <h5>
                      <a href="{{ websiteRoute('website.project.show', [$project->id]) }}">{{ $project->title}}</a>
                    </h5>
                  </div>
                  <ul class="meta">
                    <li>
                      <a href="javascript:void(0);">
                        <i class="fa fa-folder-open" aria-hidden="true"></i> {{ optional($project->category)->service_name }}
                      </a>
                    </li>
                    <li>
                      <a href="javascript:void(0);">
                        <i class="fa fa-calendar" aria-hidden="true"></i> {{ $project->created_at->format('M d, Y') }}
                      </a>
                    </li>
                  </ul>
                </div>
                <div class="post-cont">
                  <p>{{ str_limit(strip_tags($project->description)) }}</p>
                </div>
                <a href="{{ websiteRoute('website.project.show', [$project->id]) }}" class="btn white">View More</a>
              </div>
            </div>
            @empty
              <div class="post">
                <div class="content">
                  <div class="post-meta">
                    <div class="post-title">
                      <h5>There are no Projects to view</h5>
                    </div>
                  </div>
                </div>
              </div>
            @endforelse
            <!-- end post -->
            {{ $projects->links('website.ashton-dark.pagination') }}
          </div>
        </div>
        <!--  end blog left-->
        <!--  start blog right-->
        <div class="col-md-4">
          <div class="side-bar">
            <div class="widget search">
              <form>
                <input type="search" name="filter[search_project]" placeholder="Type here ..." value="{{ isset(($r = request('filter', ['search_project'=>'']))['search_project'])? $r['search_project']: '' }}">
                <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
              </form>
            </div>

            <div class="widget">
              <div class="widget-title">
                <h6 class="text-light-gray">Follow Us</h6>
              </div>
              <ul class="social-listing">
                @include('website.ashton-dark.social')
              </ul>
            </div>

          </div>
        </div>
        <!--  end blog right-->
      </div>
    </div>
  </section>

<!--cover end-->
@endsection