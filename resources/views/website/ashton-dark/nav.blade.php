<li class="nav-item">
  <a class="nav-link active" href="{{ websiteRoute('website.index') }}#" data-scroll-nav="0">Home</a>
</li>
<li class="nav-item">
  <a class="nav-link" href="{{ websiteRoute('website.index') }}#" data-scroll-nav="1">About</a>
</li>
<li class="nav-item">
  <a class="nav-link" href="{{ websiteRoute('website.index') }}#" data-scroll-nav="2">Experience</a>
</li>
<li class="nav-item">
  <a class="nav-link" href="{{ websiteRoute('website.project.list') }}">Portfolio</a>
</li>
<li class="nav-item">
  <a class="nav-link" href="{{ websiteRoute('website.index') }}#" data-scroll-nav="4">Services</a>
</li>
<li class="nav-item">
  <a class="nav-link" href="{{ websiteRoute('website.blog.list') }}">Blog</a>
</li>
<li class="nav-item">
  <a class="nav-link" href="{{ websiteRoute('website.index') }}#" data-scroll-nav="6">Contact</a>
</li>