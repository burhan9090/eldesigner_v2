@if($user->fb_link)
  <a style="color: #fff;" href="{{ website_check_facebook_link($user->fb_link) }}"><i class="fab fa-facebook" aria-hidden="true"></i></a>
@endif
@if($user->tw_link)
  <a style="color: #fff;" href="{{ website_check_twitter_link($user->tw_link) }}"><i class="fab fa-twitter" aria-hidden="true"></i></a>
@endif
@if($user->ig_link)
  <a style="color: #fff;" href="{{ website_check_instagram_link($user->ig_link) }}"><i class="fab fa-instagram" aria-hidden="true"></i></a>
@endif
@if($user->pin_link)
  <a style="color: #fff;" href="{{ website_check_pinterest_link($user->pin_link) }}"><i class="fab fa-pinterest" aria-hidden="true"></i></a>
@endif