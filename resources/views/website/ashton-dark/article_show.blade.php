@extends('website.ashton-dark.layout')
@section('content')
  <nav class="navbar navbar-expand-lg blog-nav bg-black">
    <div class="container sm-padding-10px-tb sm-padding-15px-lr">
      <!-- Logo -->
      <a class="logo" href="javascript:void(0);">
        <b style="text-transform: uppercase;" class="text-light-gray">{{ $user->name }}</b>
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarContent" aria-controls="navbarContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="icon-bar"><i class="fas fa-bars"></i></span>
      </button>
      <!-- navbar links -->
      <div class="collapse navbar-collapse" id="navbarContent">
        <ul class="navbar-nav ml-auto">
          @include('website.ashton-dark.nav')
        </ul>
      </div>
    </div>
  </nav>
  <!-- End Navbar  -->

  <!-- Start Header -->
  <section id="home" class="blog-header" data-scroll-index="0">
    <div class="container">
      <div class="row">
        <div class="text-center col-md-12">
          <h5>{{ $welcome->main }}</h5>
          <a href="{{ websiteRoute('website.index') }}">Home</a>
          <a href="{{ websiteRoute('website.blog.list') }}">Blog List</a>
          <a href="javascript:void(0);" class="active">{{ $article->title }}</a>
        </div>
      </div>
    </div>

  </section>
  <!-- End Header -->


  <!--page content-->
  <section class="blogs bg-extra-medium-gray">
    <div class="container">
      <div class="blog-detail-item row">
        <div class="col-md-8 mb-xs-5">
          @if($article->image)
            <div class="blog-detail-img col-sm-12 mb-5 hover-effect">
              <img src="{{ asset($article->image) }}" class="list-img" alt="{{ $titleHeader }}">
            </div>
          @endif
          <!--blog contetn-->
          <div class="blog-item-content">
            <div class="content-text">
              <span class="category main-color text"><a
                    href="{{ websiteRoute('website.blog.list', ['category_id'=>$article->category_id]) }}">{{ $article->category->text }}</a></span>
              - <span class="date">{{ $article->created_at->format('M d, Y') }}</span>
              <h4 class="mt-2 mb-4" style="color: #fff;">{{ $article->title }}
              </h4>
              <hr style="border-color: #fff;" />

              <div style="min-height: 200px; color: #fff;">
                {!! $article->body !!}
              </div>
            </div>

            <!--blog tags & share-->
            <div class="blog-detail-tag">
              @forelse($article->tags as $tag)
                <a class="btn btn-small btn-transparent-gray"
                   href="{{ websiteRoute('website.blog.list', ['filter'=> ['search_article'=>$tag->text]]) }}">{{ $tag->text }}</a>
              @empty
                <a class="btn btn-small btn-transparent-gray" href="javascript:void(0);">Tags</a>
              @endforelse
              <ul class="blog-share list-unstyled">
                <li><a target="_blank"
                       href="https://www.facebook.com/sharer/sharer.php?u={{ urlencode(request()->url()) }}"
                       class="facebook-bg-hvr"><i class="ti ti-facebook"></i></a></li>
                <li><a target="_blank" class="twitter-bg-hvr"
                       href="https://twitter.com/intent/tweet?url={{ urlencode(request()->url()) }}">
                    <i class="ti ti-twitter-alt"></i></a></li>
              </ul>
            </div>
          </div>
        </div>
        <!--right side-->
        <div class="col-md-4">
          <div class="side-bar">
            <div class="widget search">
              <form>
                <input type="search" name="filter[search_article]" placeholder="Type here ...">
                <button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
              </form>
            </div>

            <div class="widget">
              <div class="widget-title">
                <h6 style="color: #fff;">Categories</h6>
              </div>
              <ul>
                @foreach($categories as $category)
                  <li>
                    <a style="color: #fff;" href="{{ websiteRoute('website.blog.list', ['filter'=>['category_parent'=> $category->id]]) }}">{{ $category->text }}</a>
                  </li>
                @endforeach
              </ul>
            </div>

            <div class="widget">
              <div class="widget-title">
                <h6 style="color: #fff;">Follow Us</h6>
              </div>
              <ul class="social-listing">
                @include('website.ashton-dark.social')
              </ul>
            </div>

          </div>
        </div>
      </div>
    </div>
  </section>
  <!--page content end-->

@endsection