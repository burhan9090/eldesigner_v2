/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
var pathname = window.location.pathname;
window.Vue = require('vue');
import Vue from 'vue'
import VueChatScroll from 'vue-chat-scroll'
import Notifications from 'vue-notification'
var infiniteScroll = require('vue-infinite-scroll');
const VueUploadComponent = require('vue-upload-component')
Vue.use(VueChatScroll)
Vue.use(Notifications)
Vue.use(infiniteScroll)
Vue.component('file-upload', VueUploadComponent)
Vue.prototype.$eventBus = new Vue();

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('chat-message', require('./components/ChatMessage.vue'));
Vue.component('chat-log', require('./components/ChatLog.vue'));
Vue.component('chat-composer', require('./components/ChatComposer.vue'));
Vue.component('chat-side-menu', require('./components/ChatSideMenu.vue'));
Vue.component('side-menu', require('./components/SideMenu.vue'));
Vue.component('chat-panel', require('./components/ChatPanel.vue'));
Vue.component('header-chat-panel', require('./components/HeaderChatPanel.vue'));
Vue.component('side-friends-menu', require('./components/SideFriendsMenu.vue'));
Vue.component('side-friends-menu-full', require('./components/SideFriendsMenuFull.vue'));
Vue.component('chat-modal-pop-up', require('./components/ChatModalPopUp.vue'));
Vue.component('pop-up', require('./components/PopUp.vue'));
Vue.component('follow-request', require('./components/FollowRequest.vue'));
Vue.component('notification', require('./components/Notification.vue'));
Vue.component('activity-feed', require('./components/ActivityFeed'));
Vue.component('friend-suggestions', require('./components/FriendSuggestions'));


if (document.getElementById("app")) {
    const app = new Vue({
            el: '#app',
            data: {
                messages: [],
                side_message: [],
                user_id: $('#user_id').val(),
                user: JSON.parse($('#user_obj').val()),
                friend_id: '',
                onlineFriends: [],
                query_conv_id: ''
            },
            methods: {
                addMessage(message) {
                    let self = this;
                    const formData = new FormData();
                    if (message.message.length > 0 || Object.keys(message.files).length > 0) {
                        if (message.files != undefined && Object.keys(message.files).length > 0) {

                            if (Object.keys(message.files).length > 0) {
                                Array
                                    .from(Array(message.files.length).keys())
                                    .map(x => {

                                        formData.append('file[]', message.files[x].file, message.files[x].file.name);
                                    })
                            }
                            formData.append('message', message.message)
                        } else {
                            formData.append('message', message.message)
                        }

                        axios.post('/set-message?user_id=' + this.user_id + '&friend_id=' + this.friend_id, formData).then(response => {
                            this.messages = response.data.data;
                            self.updateSideMenu()
                        });
                    }
                },

                typingMessageFriend(data) {
                    // console.log("===>>");
                    // window.Echo.join('Chat.'+this.friend_id).whisper('typing', {
                    //     data:data,
                    //     user:this.user
                    // })
                },
                openChatMessage(data) {
                    this.friend_id = '';
                    axios.get('/messages?user_id=' + data.user_id + '&friend_id=' + data.friend_id).then(response => {
                        this.messages = response.data.data;
                        this.friend_id = data.friend_id;
                    });
                },
                updateSideMenu() {
                    axios.get('/side-bar-messages?user_id=' + this.user_id).then(response_side_menu => {
                        if (response_side_menu.data.status != undefined && response_side_menu.data.data != undefined && Object.keys(response_side_menu.data.data).length > 0) {
                            //get first chat to load it in the chat panel

                            this.side_message = response_side_menu.data.data;
                            this.$eventBus.$emit('side-menu-updated', this.side_message)
                            if (this.side_message[0].is_seen == 0) {
                                this.updateSideMenuSeen();
                                ;
                            }
                        }
                    });
                    this.$eventBus.$emit('side-menu-updated', this.side_message);
                }, updateSideMenuSeen() {
                    let self = this;
                    var current_object = this.side_message.filter(obj => {
                        return obj.user_id == this.friend_id
                    })
                    if (Object.keys(current_object).length) {
                        if (current_object[0].is_seen == 0) {
                            axios.get('/update-as-seen?user_id=' + this.user_id + '&conversation_id=' + current_object[0].conversation_id).then(response => {
                                self.updateSideMenu();
                            });
                        }
                    }
                }
            },
            created() {
                const urlParams = new URLSearchParams(window.location.search);
                this.query_conv_id = urlParams.get('conv_id');
                if (this.query_conv_id != '') {
                    window.history.replaceState(null, null, 'show-chat')
                }

                var index = 0;
                let self = this;
                axios.get('/side-bar-messages?user_id=' + this.user_id).then(response_side_menu => {
                    if (response_side_menu.data.status != undefined && response_side_menu.data.data != undefined && Object.keys(response_side_menu.data.data).length > 0) {
                        this.side_message = response_side_menu.data.data;
                        // get the conversation on con_id from the query params
                        if (this.query_conv_id != undefined && this.query_conv_id != '') {
                            for (var key in this.side_message) {
                                if (this.side_message[key].conversation_id == this.query_conv_id) {
                                    index = key;
                                }
                            }
                        }
                        //get first chat to load it in the chat panel
                        var firstUser = this.side_message[index].user_id;
                        var secondUser = this.side_message[index].user_id_to;
                        this.query_conv_id = this.side_message[index].conversation_id
                        self.friend_id = secondUser;
                        if (secondUser == this.user_id) {
                            self.friend_id = firstUser;
                        }
                        self.updateSideMenuSeen();

                        axios.get('/messages?user_id=' + this.user_id + '&friend_id=' + self.friend_id).then(response => {
                            this.messages = response.data.data;
                        });
                    }
                });
                // Echo.join('plchat')
                //     .here((users) => {
                //         console.log('online',users);
                //         this.onlineFriends=users;
                //     })
                //     .joining((user) => {
                //         this.onlineFriends.push(user);
                //         console.log('joining',user.name);
                //     })
                //     .leaving((user) => {
                //         this.onlineFriends.splice(this.onlineFriends.indexOf(user),1);
                //         console.log('leaving',user.name);
                //     });
                window.Echo.join('Chat.' + this.user_id)
                    .listen('MessagePosted', (e) => {
                        if (e.oMessage[0].user_id == this.friend_id) {
                            this.messages.unshift({
                                message: e.oMessage[0].message,
                                created_at: e.oMessage[0].created_at,
                                have_attachments: e.oMessage[0].have_attachments,
                                attachments: e.oMessage[0].attachments,
                                user: {
                                    logo: e.oMessage[0].user.logo,
                                    name: e.oMessage[0].user.name,
                                }
                            })
                        }
                        this.updateSideMenu();
                    });
            }
            ,
        }
        )
    ;
}
if (document.getElementById("friend_requests")){
    const header_app = new Vue({
        el: '#friend_requests',
        data: {
            follow_request_data: [],
            user_id: $('#user_id').val(),

        },
        created(){
            this.getFollowRequested();
            this.$eventBus.$on('update_follow_requests', () => {
                this.getFollowRequested();
            });
        },
        methods:{
            getFollowRequested() {
                axios.get('/get-follow-request?user_id=' + this.user_id).then(follow_request_response => {
                    if (follow_request_response.data != undefined && Object.keys(follow_request_response.data).length > 0) {
                        this.follow_request_data = follow_request_response.data.data
                    }


                }).catch(error => {
                    console.log('Response Error From Get Follow Request', error)
                });
            },
        }
    })
}
const header_app = new Vue({
    el: '#header_app',
    data: {
        header_messages: [],
        user_id: $('#user_id').val(),
        user: JSON.parse($('#user_obj').val()),
        number_of_message_notification: 0,
        follow_request_data: [],
        number_of_follow_requests : 0,
        notification_data : [],
        number_of_notification : 0
    },
    methods: {
        markAllAsRead(){
            let self = this;
            if(self.number_of_notification > 0 && Object.keys(self.notification_data).length > 0){
                axios.get('/mark-all-read?user_id=' + this.user_id).then(mark_rsponse => {
                    if(mark_rsponse.data.status != undefined && mark_rsponse.data.status == true){
                        self.getLatestNotification();
                    }
                }).catch(error => {
                    console.log('Response Error From Get Follow Request', error)
                });
            }
        },
        openNotification(notification_data){
            let notification_id= notification_data.notification_obj.id;
            let notification_user_id = notification_data.notification_obj.user_id;
            let notification_type = notification_data.notification_obj.notification_type;
            let follow_profile_noti = notification_data.notification_obj.user.user_name;
            if(notification_id != '' && notification_user_id!= ''){
                axios.get('/open-notification?user_id=' + notification_user_id+'&notification_id='+notification_id).then(open_notification_response => {
                    if(open_notification_response.data.status != undefined && open_notification_response.data.status == true){
                        if(open_notification_response.data.data != undefined  ){
                            if(notification_type != 'follow'){
                                let resource_id = open_notification_response.data.data;
                                location.href = '/viewPost/'+resource_id;
                            }else{
                                location.href = '/profile/'+follow_profile_noti;
                            }

                        }
                    }
                }).catch(error => {
                    console.log('Response Error From Get Follow Request', error)
                });
            }


        },
        getLatestNotification(){
            axios.get('/get-notification?user_id=' + this.user_id).then(notification_response => {
                if (notification_response.data != undefined && Object.keys(notification_response.data).length > 0) {
                    this.notification_data = notification_response.data.data
                    this.checkIseen(this.notification_data)
                }

            }).catch(error => {
                console.log('Response Error From Get Follow Request', error)
            });
        },
        checkIseen(data){
            let self = this;
            self.number_of_notification = 0;
            Object.keys(data).forEach(function (key) {
                if (data[key].is_seen == 0) {
                    self.number_of_notification++;
                }
            });
            if(self.number_of_notification > 10){
                self.number_of_notification = '10+';
            }
        },
        getFollowRequested() {
            axios.get('/get-follow-request?user_id=' + this.user_id).then(follow_request_response => {
                if (follow_request_response.data != undefined && Object.keys(follow_request_response.data).length > 0) {
                    this.follow_request_data = follow_request_response.data.data
                    this.number_of_follow_requests = Object.keys(this.follow_request_data).length
                    if(this.number_of_follow_requests > 10){
                        this.number_of_follow_requests = '10+';
                    }
                }


            }).catch(error => {
                console.log('Response Error From Get Follow Request', error)
            });
        },
        myClickEvent($event) {
            document.getElementById('ChatAudio').play();
        },
        updateSideMenu() {
            let self = this;
            self.number_of_message_notification = 0;
            axios.get('/side-bar-messages?user_id=' + this.user_id).then(response_header_messages => {
                if (response_header_messages.data.status != undefined && response_header_messages.data.data != undefined && Object.keys(response_header_messages.data.data).length > 0) {
                    //get first chat to load it in the chat panel
                    this.header_messages = response_header_messages.data.data;
                    self.updateNotiCount(true);
                }
            });
        },
        updateNotiCount(sound) {
            let self = this;
            self.number_of_message_notification = 0;
            Object.keys(self.header_messages).forEach(function (key) {
                if (self.header_messages[key].is_seen == 0) {
                    if (self.user_id != self.header_messages[key].user_id) {
                        if (sound) {
                            $('#audio_btn').click()
                        }
                        self.number_of_message_notification++;
                    }
                }
            });
            if(self.number_of_message_notification > 10){
                self.number_of_message_notification = '10+';
            }
        }
    },
    created() {
        let self = this;
        this.updateSideMenu()
        window.Echo.join('Chat.' + this.user_id)
            .listen('MessagePosted', (e) => {
                this.updateSideMenu();
            });
        this.$eventBus.$on('side-menu-updated', (data) => {
            self.header_messages = data;
            self.updateNotiCount(false);
        });
        this.$eventBus.$on('update-side-from-side', () => {
            this.updateSideMenu()
        });
        this.$eventBus.$on('update_follow_requests', () => {
            self.getFollowRequested();
        });
        window.Echo.join('Friend.' + this.user_id)
            .listen('FollowRequestEvent', (e) => {
                self.getFollowRequested();
            });
        window.Echo.join('Notification.' + this.user_id)
            .listen('NotificationEvent', (e) => {
                self.getLatestNotification();
            });
    },
    mounted(){
        let self = this;
        setTimeout(function () {
            self.getFollowRequested();
            self.getLatestNotification();
        }, 2000);
    }
})


if (pathname.indexOf('/show-chat') !== 0) {
    const side_app = new Vue({
        el: '#side_app',
        data: {
            friends_data: [],
            friend_id: '',
            friends_messages: [],
            user_id: $('#user_id').val(),
            user: JSON.parse($('#user_obj').val()),
            friend_info: [],

        },
        mounted() {
            let self = this;
            setTimeout(function () {
                self.getAllFreinds();
            }, 2000);

        },
        created() {
            let self =this;
            this.$eventBus.$on('open_chat_discover_friend', (data) => {
                self.openFriendChat(data);
            });
            window.Echo.join('Chat.' + this.user_id)
                .listen('MessagePosted', (e) => {
                    if (e.oMessage[0].user_id == this.friend_id) {
                        this.friends_messages.unshift({
                            message: e.oMessage[0].message,
                            created_at: e.oMessage[0].created_at,
                            have_attachments: e.oMessage[0].have_attachments,
                            attachments: e.oMessage[0].attachments,
                            user: {
                                logo: e.oMessage[0].user.logo,
                                name: e.oMessage[0].user.name,
                            }
                        })
                    }
                    this.$eventBus.$emit('update-side-from-side')
                });
            let last_friend_id = localStorage.getItem('last_friend_id');
            if (last_friend_id != null && last_friend_id != undefined && last_friend_id != 'undefined') {
                this.openFriendChat({ id: last_friend_id });
            }

        }, methods: {
            getAllFreinds() {
                axios.get('/get-friends').then(friends_response => {
                    if (friends_response.data != undefined && Object.keys(friends_response.data).length > 0) {
                        this.friends_data = friends_response.data;
                    }

                }).catch(error => {
                    console.log('Response Error From Get Friends', error)
                });
            },
            openFriendChat(data) {
                localStorage.setItem('last_friend_id', data.id);
                axios.get('/messages?user_id=' + this.user_id + '&friend_id=' + data.id).then(response => {
                    this.friend_id = data.id;
                    this.friends_messages = response.data.data;
                    if (Object.keys(this.friends_messages).length == 0) {
                        if (response.data.friend_info != undefined) {
                            this.friend_info = response.data.friend_info
                        }

                    }
                });
            },
            sideMessageSent(message) {
                let self = this;
                const formData = new FormData();
                if (message.message.length > 0 || Object.keys(message.files).length > 0) {
                    if (message.files != undefined && Object.keys(message.files).length > 0) {

                        if (Object.keys(message.files).length > 0) {
                            Array
                                .from(Array(message.files.length).keys())
                                .map(x => {

                                    formData.append('file[]', message.files[x].file, message.files[x].file.name);
                                })
                        }
                        formData.append('message', message.message)
                    } else {
                        formData.append('message', message.message)
                    }

                    axios.post('/set-message?user_id=' + this.user_id + '&friend_id=' + this.friend_id, formData).then(response => {
                        this.friends_messages = response.data.data;
                        this.$eventBus.$emit('update-side-from-side');
                    });
                }

            },
            friendMenu(status) {
                this.$eventBus.$emit('side-menu-status', status)
            }

        },
    })
}
if (pathname.indexOf('/discover-friends') === 0) {
    const side_app = new Vue({
        el: '#discover_friend',
        mounted() {
            console.log('df mounted')
        },
        methods: {
            openChatDiscoverFriends(id){
                console.log(id)
                this.$eventBus.$emit('open_chat_discover_friend', {id:id})
            }
        },
        created() {

        }
    });
}


if (document.getElementById("profile")) {

    const profile = new Vue({
        el: '#profile',
        data:{
            notification_data : [],
            friend_suggestions : [],
            user_id: $('#user_id').val(),
        },
        mounted() {
            this.getLatestNotification();
            this.getFriendSuggesetions();
        },
        methods: {
            requestFriend(data){
                console.log('From The app.js')

                if(data.data.follow_setting != undefined){
                    if(data.data.follow_setting == 'public'){
                        axios.get('/public-friend-request-follow?user_id=' + this.user_id+'&request_id='+data.data.id).then(friend_response => {
                            console.log(friend_response)
                            if (friend_response.data.status != undefined && friend_response.data.status == true) {
                                this.$notify({
                                    group: 'foo',
                                    type: 'success',
                                    title: 'success',
                                    text: 'You are Now Following '+data.data.name ,
                                });

                            }else{
                                this.$notify({
                                    group: 'foo',
                                    type: 'error',
                                    title: 'Error',
                                    text: 'Something Went Wrong , Please Try Again Later',
                                });
                            }
                            this.getFriendSuggesetions();

                        }).catch(error => {
                            this.getFriendSuggesetions();
                            console.log('Response Error From Get Follow Request', error)
                        });
                    }else if(data.data.follow_setting == 'private'){
                        axios.get('/friend-request-follow?user_id=' + this.user_id+'&request_id='+data.data.id).then(friend_response => {
                            console.log(friend_response)
                            if (friend_response.data.status != undefined && friend_response.data.status == true) {
                                this.$notify({
                                    group: 'foo',
                                    type: 'success',
                                    title: 'success',
                                    text: 'Your Follow Request Has been Sent' ,
                                });
                            }else{
                                this.$notify({
                                    group: 'foo',
                                    type: 'error',
                                    title: 'Error',
                                    text: 'Something Went Wrong , Please Try Again Later',
                                });
                            }
                            this.getFriendSuggesetions();

                        }).catch(error => {
                            this.getFriendSuggesetions();
                        });
                    }
                }
            },
            getLatestNotification(){
                axios.get('/get-notification?user_id=' + this.user_id).then(notification_response => {
                    if (notification_response.data != undefined && Object.keys(notification_response.data).length > 0) {
                        this.notification_data = notification_response.data.data
                    }

                }).catch(error => {
                    console.log('Response Error From Get Follow Request', error)
                });
            },
            getFriendSuggesetions(){
                axios.get('/get-friend-suggestions?user_id=' + this.user_id).then(friends_response => {
                    if (friends_response.data != undefined && Object.keys(friends_response.data).length > 0) {
                        this.friend_suggestions = friends_response.data.data
                        console.log('Freind Sug')
                        console.log(this.friend_suggestions)
                    }

                }).catch(error => {
                    console.log('Response Error From Get Follow Request', error)
                });
            },
        },
        created() {

        }
    });
}