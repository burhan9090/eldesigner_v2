require('./bootstrap');

window.Vue = require('vue');
import Vue from 'vue'
import VueChatScroll from 'vue-chat-scroll'
const VueUploadComponent = require('vue-upload-component')
Vue.use(VueChatScroll)
Vue.component('file-upload', VueUploadComponent)
Vue.component('header-chat-panel', require('./components/HeaderChatPanel'));

const header_app = new Vue({
    el: '#header_app',
    data: {
        header_messages : [],
        user_id: $('#user_id').val(),
        user: JSON.parse($('#user_obj').val()),
        number_of_message_notification :0
    },
    methods:{
        updateSideMenu() {
            console.log('updateSideMenu');
            let self = this;
            axios.get('/side-bar-messages?user_id=' + this.user_id).then(response_header_messages => {
                if (response_header_messages.data.status != undefined && response_header_messages.data.data != undefined && Object.keys(response_header_messages.data.data).length > 0) {
                    //get first chat to load it in the chat panel
                    this.header_messages = response_header_messages.data.data;
                    Object.keys(response_header_messages.data.data).forEach(function(key) {
                        if( response_header_messages.data.data[key].is_seen == 0){
                            if(self.user_id != response_header_messages.data.data[key].user_id){
                                self.number_of_message_notification++;
                            }
                        }
                    });
                }
            });
        }
    },
    created() {
        console.log('header_app');
        window.Echo.join('Chat.' + this.user_id)
            .listen('MessagePosted', (e) => {
                this.updateSideMenu();
            });
    }
})