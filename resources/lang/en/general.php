<?php
/**
 * Created by PhpStorm.
 * User: shaikh
 * Date: 12/2/18
 * Time: 8:15 PM
 */

return [

    /*
    |--------------------------------------------------------------------------
    | General Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used in general in hole platform. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'welcome' => 'Welcome to El Designers Platform',
    'eldesigners' => 'Eldesigners',

];
