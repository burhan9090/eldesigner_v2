<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define( App\User::class, function ( Faker\Generator $faker ) {
	static $password;

	return [
		'name'           => $faker->name,
		'email'          => $faker->unique()->safeEmail,
		'password'       => $password ?: $password = bcrypt( 'password' ),
		'remember_token' => str_random( 10 ),
		'user_name'      => $faker->userName,
		'gender'         => $faker->randomElement( [ 'female', 'male' ] ),
	];
} );

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define( App\ContactUs::class, function ( Faker\Generator $faker) {

	return [
		'user_id'    => 1,
		'title'      => $faker->sentence,
		'message'    => $faker->paragraph,
		'is_read'    => rand(0, 1),
		'created_at' => $faker->date(),
	];
} );
