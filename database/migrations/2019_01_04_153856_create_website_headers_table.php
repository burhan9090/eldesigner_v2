<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebsiteHeadersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('website_headers', function (Blueprint $table) {
	        $table->unsignedTinyInteger('id');
	        $table->string('video');
	        $table->text('description');
	        $table->primary('id');
        });

        DB::insert("INSERT INTO website_headers (id, video, description) VALUES (1, 'https://youtube.com/watch?v=excVFQ2TWig', 'Designers can publish their own articles or blog posts. When you publish your articles it will appear on your website, your portfolio, feed and our public library.\n Publishing articles will make your profile more accessable by website visitors, improve your google ranking and popularity.');");
		DB::insert("INSERT INTO website_headers (id, video, description) VALUES (2, 'https://youtube.com/watch?v=excVFQ2TWig', 'Here you''ll find articles you have published as well as featured articles from the community and elDesigners on the platform.');");
		DB::insert("INSERT INTO website_headers (id, video, description) VALUES (3, 'https://youtube.com/watch?v=excVFQ2TWig', 'You''ll have a Contact Us tab on your website, here you can control the tab info and settings');");
		DB::insert("INSERT INTO website_headers (id, video, description) VALUES (4, 'https://youtube.com/watch?v=excVFQ2TWig', 'You can upload projects here or using your profile. Any project you upload will appear on your profile, website, newsfeed and the cmmunity library. Here you can manage as well to show or hide any projects from your website.')");
		DB::insert("INSERT INTO website_headers (id, video, description) VALUES (5, 'https://youtube.com/watch?v=excVFQ2TWig', 'Use this page to manage the services tab on your website. As a default option, the services you add on your profile will be the same here. The services which will appear on the website are still customizable. Check the section below to manage the settings and services.');");
		DB::insert("INSERT INTO website_headers (id, video, description) VALUES (6, 'https://youtube.com/watch?v=excVFQ2TWig', 'The welcoming screen is the landing section of your website, here you can edit the text, bio, logo and the background image.\nThe defautl bio, logo, background will be the same as your website, you can still customise them on the website here.\nCustomising any field here will not effect the information on your profile if they are already added.');");
		DB::insert("INSERT INTO website_headers (id, video, description) VALUES (7, 'https://youtube.com/watch?v=excVFQ2TWig', '');");
		DB::insert("INSERT INTO website_headers (id, video, description) VALUES (8, 'https://youtube.com/watch?v=excVFQ2TWig', '');");
		DB::insert("INSERT INTO website_headers (id, video, description) VALUES (9, 'https://youtube.com/watch?v=excVFQ2TWig', '');");
		DB::insert("INSERT INTO website_headers (id, video, description) VALUES (10, 'https://youtube.com/watch?v=excVFQ2TWig', '');");
		DB::insert("INSERT INTO website_headers (id, video, description) VALUES (11, 'https://youtube.com/watch?v=excVFQ2TWig', '');");
		DB::insert("INSERT INTO website_headers (id, video, description) VALUES (12, 'https://youtube.com/watch?v=excVFQ2TWig', '');");
		DB::insert("INSERT INTO website_headers (id, video, description) VALUES (13, 'https://youtube.com/watch?v=excVFQ2TWig', '');");

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('website_headers');
    }
}
