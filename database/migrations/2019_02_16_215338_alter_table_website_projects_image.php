<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableWebsiteProjectsImage extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('website_project_images', function (Blueprint $table) {
            $table->boolean('optimized_and_watermarked')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('website_project_images', function (Blueprint $table) {
            $table->dropColumn('optimized_and_watermarked');
        });
    }
}
