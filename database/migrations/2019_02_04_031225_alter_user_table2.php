<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterUserTable2 extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->boolean('email_notification')->default(true);
            $table->boolean('watermark_text_active')->default(false);
	        $table->string('watermark_text')->default('');
	        $table->boolean('watermark_file_active')->default(false);
	        $table->string('watermark_file')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
	        $table->dropColumn('email_notification');
	        $table->dropColumn('watermark_text_active');
	        $table->dropColumn('watermark_text');
	        $table->dropColumn('watermark_file_active');
	        $table->dropColumn('watermark_file');
        });
    }
}
