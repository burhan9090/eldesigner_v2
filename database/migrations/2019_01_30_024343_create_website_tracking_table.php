<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebsiteTrackingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('website_tracking', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id')->index();
	        $table->unsignedInteger('client_id')->default(0);
	        $table->string('uri');
            $table->string('client_ip');
            $table->string('host')->index();
            $table->string('user_agent');
            $table->string('referer');

            $table->string('country_code')->nullable();
            $table->string('browser_name')->nullable();
            $table->string('browser_version')->nullable();
            $table->string('engine')->nullable();
            $table->string('os')->nullable();
            $table->string('device')->nullable();
            $table->string('type')->nullable();



            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('website_tracking');
    }
}
