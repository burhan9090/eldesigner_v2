<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsForDomainUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->date('domain_created_at')->nullable();
            $table->date('domain_renewed_at')->nullable();
            $table->date('domain_renew_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
	        $table->dropColumn('domain_created_at');
	        $table->dropColumn('domain_renewed_at');
	        $table->dropColumn('domain_renew_at');
        });
    }
}
