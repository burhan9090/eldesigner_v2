<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStsPaymentsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create( 'sts_payments', function ( Blueprint $table ) {
			$table->increments( 'id' );
			$table->unsignedInteger( 'user_id')->index();
			$table->string( 'transaction_id', 50 )->nullable()->index();

			$table->string( 'amount', 50 )->nullable();
			$table->string( 'payment_description' )->nullable();



			$table->string( 'route_success', 30 )->nullable();
			$table->string( 'route_failed', 30 )->nullable();

			$table->string( 'secure_hash' )->nullable();
			$table->string( 'card_holder_name' )->nullable();
			$table->string( 'card_expiry_date' )->nullable();
			$table->string( 'card_number' )->nullable();
			$table->string( 'approval_code' )->nullable();
			$table->string( 'currency_ iso_code' )->nullable();
			$table->string( 'status_code' )->nullable();
			$table->string( 'status_description' )->nullable();
			$table->string( 'gateway_status_code' )->nullable();
			$table->string( 'gateway_status_description' )->nullable();
			$table->string( 'rrn' )->nullable();
			$table->string( 'gateway_name' )->nullable();
			$table->text( 'full_request' )->nullable();
			$table->timestamps();
		} );
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists( 'sts_payments' );
	}
}
