<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserPaymentPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_payment_plans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('plan_1');
            $table->string('plan_1_include');
            $table->string('plan_1_delivery_time');
            $table->string('plan_1_price');

            $table->string('plan_2')->nullable();
            $table->string('plan_2_include')->nullable();
            $table->string('plan_2_delivery_time')->nullable();
            $table->string('plan_2_price')->nullable();

            $table->string('plan_3')->nullable();
            $table->string('plan_3_include')->nullable();
            $table->string('plan_3_delivery_time')->nullable();
            $table->string('plan_3_price')->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_payment_plans');
    }
}
