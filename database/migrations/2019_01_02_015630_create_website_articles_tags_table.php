<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebsiteArticlesTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::create('website_articles_tags', function (Blueprint $table) {
		    $table->bigIncrements('id');
		    $table->unsignedInteger('article_id')->index();
		    $table->unsignedInteger('tag_id')->index();
	    });
    }

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop( 'website_articles_tags' );
    }
}
