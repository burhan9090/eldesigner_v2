<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterWebsiteWelcomeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('website_welcome', function (Blueprint $table) {
            $table->string('title')->nullable();
            $table->string('keyword')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('fb_page_id')->nullable();
            $table->string('gplus')->nullable();
            $table->text('tracking_script')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('website_welcome', function (Blueprint $table) {
	        $table->dropColumn('title');
	        $table->dropColumn('keyword');
	        $table->dropColumn('meta_description');
	        $table->dropColumn('fb_page_id');
	        $table->dropColumn('gplus');
	        $table->dropColumn('tracking_script');
        });
    }
}
