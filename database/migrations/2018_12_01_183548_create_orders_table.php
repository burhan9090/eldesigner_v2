<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('client_id');
            $table->unsignedInteger('user_id');
            $table->foreign('client_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('project_name' , 255);
            $table->string('project_desc' , 500 );
            $table->text('project_summary' )->nullable();
            $table->string('project_space',10)->nullable();
            $table->integer('num_of_rooms')->nullable();
            $table->integer('duration'); // num of days expected to deliver project
            $table->enum('project_type', ['residential' , 'commercial']);
            $table->enum('area_type', ['scratch' , 'redesign']);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
