<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePaypal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('paybal', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('bundle');
            $table->string('paybal_order_id');
            $table->string('paybal_status');
            $table->string('paybal_payer_id');
            $table->string('paybal_payer_email');
            $table->string('paybal_payer_name');
            $table->string('paybal_payer_surname');
            $table->string('paybal_payer_phone_number');
            $table->string('paybal_payments_id');
            $table->float('paybal_payments_amount');
            $table->string('paybal_payments_currency_code');
            $table->string('paybal_payments_create_time');
            $table->string('paybal_payments_update_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('table_paypal');
    }
}
