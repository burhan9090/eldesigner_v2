<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_projects', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->string('project_name', 100);
            $table->string('project_desc', 255);
            $table->unsignedTinyInteger('style_1')->default(0);
            $table->unsignedTinyInteger('style_2')->default(0);
            $table->unsignedTinyInteger('style_3')->default(0);
            $table->unsignedTinyInteger('style_4')->default(0);
            $table->unsignedInteger('project_views')->default(0);
            $table->unsignedInteger('project_likes')->default(0);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_projects');
    }
}
