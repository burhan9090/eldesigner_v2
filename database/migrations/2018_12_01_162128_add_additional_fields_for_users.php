<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalFieldsForUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            //
            $table->string('user_name')->unique();
            $table->enum('profile_type', ['company' , 'freelancer']);
            $table->enum('user_type' , ['designer' , 'architect' , 'client']);
            $table->string('logo', 255)->nullable();
            $table->string('cover', 255)->nullable();
            $table->string('phone', 15);
            $table->string('about', 255)->nullable();
            $table->string('country', 100)->nullable();
            $table->string('city', 100)->nullable();
            $table->string('fb_link', 255)->nullable();
            $table->string('ig_link', 255)->nullable();
            $table->string('tw_link', 255)->nullable();
            $table->string('pin_link', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
