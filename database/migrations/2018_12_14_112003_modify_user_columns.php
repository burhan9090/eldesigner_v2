<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUserColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('profile_type');
            $table->dropColumn('user_type');
            $table->dropColumn('phone');
        });

        Schema::table('users', function (Blueprint $table) {

            $table->enum('profile_type', ['company' , 'freelancer'])->nullable()->default(null);
            $table->enum('user_type' , ['designer' , 'architect' , 'client'])->nullable()->default(null);
            $table->string('phone', 15)->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
