<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDesignQuizzes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quizzes', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->enum('quiz_level' , ['3' , '4'])->nullable();
            $table->enum('quiz_type' , ['NRBDS' , 'RBDS'])->nullable();
            $table->tinyInteger('required_info_name')->nullable();
            $table->tinyInteger('required_info_email')->nullable();
            $table->tinyInteger('required_info_phone')->nullable();
            $table->tinyInteger('required_info_address')->nullable();
            $table->tinyInteger('required_info_attachment')->nullable();
            $table->tinyInteger('required_info_budget')->nullable();
            $table->tinyInteger('required_info_overview')->nullable();
            $table->tinyInteger('required_info_location')->nullable();
            $table->tinyInteger('active')->default(0);
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quizzes');
    }
}
