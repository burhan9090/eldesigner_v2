<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
            $table->dropColumn('project_name');
            $table->dropColumn('project_desc');
            $table->dropColumn('project_summary' );
            $table->dropColumn('project_space');
            $table->dropColumn('num_of_rooms');
            $table->dropColumn('duration'); // num of days expected to deliver project
            $table->dropColumn('project_type');
            $table->dropColumn('area_type');
        });
        Schema::table('orders', function (Blueprint $table) {
            $table->string('client_name' , 255)->nullable();
            $table->string('client_email' , 255 )->nullable();
            $table->string('client_phone' , 255 )->nullable();
            $table->string('client_address' , 255 )->nullable();
            $table->string('project_budget' , 255 )->nullable();
            $table->string('project_location' , 255 )->nullable();
            $table->string('requested_service' , 255 )->nullable();
            $table->string('requested_sub_service' , 255 )->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
//        Schema::dropIfExists('orders');
    }
}
