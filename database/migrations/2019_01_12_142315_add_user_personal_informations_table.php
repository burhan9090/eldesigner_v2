<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserPersonalInformationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
	        $table->string('last_name')->default('');
	        $table->string('website')->default('');
	        $table->date('bod')->nullable();
	        $table->unsignedInteger('country_id')->nullable();
	        $table->unsignedInteger('city_id')->nullable();
	        $table->text('description')->nullable();
	        $table->string('occupation')->default('');
	        $table->dropColumn('country');
	        $table->dropColumn('city');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {

	        $table->dropIfExists('last_name');
	        $table->dropIfExists('website');
	        $table->dropIfExists('bod');
	        $table->dropIfExists('country_id');
	        $table->dropIfExists('city_id');
	        $table->dropIfExists('description');
	        $table->dropIfExists('occupation');
	        $table->string('country')->default('');
	        $table->string('city')->default('');

        });
    }
}
