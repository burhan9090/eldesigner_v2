<?php

use Illuminate\Database\Seeder;

class GivePermisions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $oAllCurrentUsers = \App\User::select('id')->get()->all();
        // adding 3 month for each user
        $date_object = \Carbon\Carbon::now()->addMonths(3)->setTime(23, 50, 59);
        $date_timestamp = $date_object->timestamp;
        $user_counter = 0;

        foreach ($oAllCurrentUsers as $oUser){
            \App\UserPermission::create([
                'user_id' => $oUser->id,
                'plans_id' => 5, // custom plan only to use it here
                'expiration_timestamp' => $date_timestamp,
                'is_deleted' => 0,
            ]);
            $user_counter++;
        }
        print "Number Of Users Synced : ".$user_counter.PHP_EOL;

    }
}
