<?php

use Illuminate\Database\Seeder;

class CreateCategoriesSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
        $categoryTab = \App\WebsiteArticlesCategory::query();
        $categoryTab->truncate();
        \App\WebsiteArticlesCategory::insert( [
			[ "id" => 1, "text" => "cat 1", "category_id" => 0 ],
			[ "id" => 2, "text" => "cat 2", "category_id" => 0 ],
			[ "id" => 3, "text" => "cat 3", "category_id" => 0 ],
			[ "id" => 4, "text" => "cat 11", "category_id" => 1 ],
			[ "id" => 5, "text" => "cat 12", "category_id" => 1 ],
			[ "id" => 6, "text" => "cat 21", "category_id" => 2 ],
			[ "id" => 7, "text" => "cat 31", "category_id" => 3 ],
			[ "id" => 8, "text" => "cat 32", "category_id" => 3 ],
			[ "id" => 9, "text" => "cat 33", "category_id" => 3 ],
		] );
	}
}
