<?php

use Illuminate\Database\Seeder;

class UserAgentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

	    $faker = \Faker\Factory::create();

	    foreach (range(1, 100) as $num) {

		    \App\WebsiteTracking::addHit( 1, $faker->userAgent, $faker->ipv4 );
	    }

    }
}
