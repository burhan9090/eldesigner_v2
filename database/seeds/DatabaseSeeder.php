<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		// $this->call(UsersTableSeeder::class);

		$tab = \App\Country::query();
		$tab->truncate();
		$tab = \App\City::query();
		$tab->truncate();

		unset( $tab );

		$countries = [];
//		city	city_ascii	lat	lng	country	iso2	iso3	admin_name	capital	population	id

//		$csv = fopen( base_path( 'database/seeds/City-Locations-en.csv' ), 'r' );
		$csv = fopen( base_path( 'database/seeds/worldcities.csv' ), 'r' );

		$headers = fgetcsv( $csv );

		while ( $row = fgetcsv( $csv ) ) {
			$countryName = trim( $row[ array_search( 'country', $headers ) ] );
			if ( $countryName == '' ) {
				continue;
			}
			$cityName = trim( $row[ array_search( 'city_ascii', $headers ) ] );
			if ( $cityName == '' ) {
				continue;
			}
			$isoCode  = trim( $row[ array_search( 'iso2', $headers ) ] );
			$timeZone = trim( $row[ array_search( 'time_zone', $headers ) ] );

			if ( ( $countryId = array_search( $countryName, $countries ) ) === false ) {

				$country = \App\Country::create( [
					'text' => $countryName,
					'code' => $isoCode
				] );

				$countries[ $country->id ] = $countryName;

				$countryId = $country->id;
			}

			\App\City::firstOrNew( [
				'country_id' => $countryId,
				'text'       => $cityName
			], [
				'zone' => $timeZone
			] )->save();
		}

		fclose($csv);
	}

}
