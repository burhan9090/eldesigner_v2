<?php

use Illuminate\Database\Seeder;

class ServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $serviceTab = \App\Service::query();
        $serviceTab->truncate();
        \App\Service::insert( [
            [ "id" => 1, "service_name" => "Architecture Design" ],
            [ "id" => 2, "service_name" => "Interior Design" ],
            [ "id" => 3, "service_name" => "Graphic Design" ],
            [ "id" => 4, "service_name" => "Fashion Design" ],
            [ "id" => 5, "service_name" => "Photography" ],
            [ "id" => 6, "service_name" => "Illustration" ],
            [ "id" => 7, "service_name" => "Motion" ],
        ] );
    }
}
