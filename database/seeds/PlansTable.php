<?php

use Illuminate\Database\Seeder;

class PlansTable extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    //command to run [ php artisan db:seed --class=PlansTable ]
    public function run()
    {




        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Plans ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans (plans_id, plans_name, plans_price, created_at, updated_at, description) VALUES (1, 'START', 7, null, null, 'Start Plan');");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans (plans_id, plans_name, plans_price, created_at, updated_at, description) VALUES (2, 'GROW', 12, null, null, 'Grow Plan');");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans (plans_id, plans_name, plans_price, created_at, updated_at, description) VALUES (3, 'PREMIUM', 25, null, null, 'Premium Plan');");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans (plans_id, plans_name, plans_price, created_at, updated_at, description) VALUES (4, 'ENTREPRISE', 0, null, null, 'Entreprise Plan');");




        //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Plans Options ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options (plans_options_id, plans_options_name, plans_options_view_name, created_at, updated_at) VALUES (1, 'image_protection', 'Image Protection', null, null)");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options (plans_options_id, plans_options_name, plans_options_view_name, created_at, updated_at) VALUES (2, 'unique_domain', 'Unique Domain', null, null)");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options (plans_options_id, plans_options_name, plans_options_view_name, created_at, updated_at) VALUES (3, 'professional_email', 'Professional Email', null, null)");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options (plans_options_id, plans_options_name, plans_options_view_name, created_at, updated_at) VALUES (4, 'design_request', 'Design Request', null, null)");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options (plans_options_id, plans_options_name, plans_options_view_name, created_at, updated_at) VALUES (5, 'image_tagging', 'Image Tagging', null, null)");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options (plans_options_id, plans_options_name, plans_options_view_name, created_at, updated_at) VALUES (6, 'ecommerce', 'E-Commerce', null, null)");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options (plans_options_id, plans_options_name, plans_options_view_name, created_at, updated_at) VALUES (7, 'limited_projects', 'Limited Projects', null, null)");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options (plans_options_id, plans_options_name, plans_options_view_name, created_at, updated_at) VALUES (8, 'limitedt_articles', 'Limited Articles', null, null)");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options (plans_options_id, plans_options_name, plans_options_view_name, created_at, updated_at) VALUES (9, 'website_generator', 'Website Generator', null, null)");



        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ Plans Options Selected++++++++++++++++++++++++++++++++++++++++++++++++++++++++


        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (1, 1, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (1, 4, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (1, 7, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (1, 8, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (2, 1, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (2, 2, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (2, 3, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (2, 4, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (2, 7, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (2, 8, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (3, 1, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (3, 2, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (3, 3, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (3, 4, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (3, 7, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (3, 8, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (4, 1, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (4, 2, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (4, 3, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (4, 4, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (4, 7, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (4, 8, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (2, 9, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (3, 9, null, null);");
        \Illuminate\Support\Facades\DB::insert("INSERT INTO eldesignersV2DB.plans_options_selected ( plans_id, plans_options_id, created_at, updated_at) VALUES (4, 9, null, null);");
    }
}
